﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Sabana
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LayoutSabana = New DevExpress.XtraLayout.LayoutControl
        Me.LayoutRaiz = New DevExpress.XtraLayout.LayoutControlGroup
        Me.EspacioDerecha = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EspacioArriba = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EspacioIzquierda = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EspacioAbajo = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EtiquetaMensaje = New DevExpress.XtraEditors.LabelControl
        Me.ItemParaMensaje = New DevExpress.XtraLayout.LayoutControlItem
        Me.ItemParaProgreso = New DevExpress.XtraLayout.LayoutControlItem
        Me.GrupoProgreso = New DevExpress.XtraLayout.LayoutControlGroup
        Me.ProgresoContinuoSabana = New DevExpress.XtraEditors.MarqueeProgressBarControl
        CType(Me.LayoutSabana, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutSabana.SuspendLayout()
        CType(Me.LayoutRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioDerecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioArriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioIzquierda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioAbajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemParaMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemParaProgreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrupoProgreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgresoContinuoSabana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutSabana
        '
        Me.LayoutSabana.AllowCustomizationMenu = False
        Me.LayoutSabana.Controls.Add(Me.EtiquetaMensaje)
        Me.LayoutSabana.Controls.Add(Me.ProgresoContinuoSabana)
        Me.LayoutSabana.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutSabana.Location = New System.Drawing.Point(0, 0)
        Me.LayoutSabana.Name = "LayoutSabana"
        Me.LayoutSabana.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutSabana.Root = Me.LayoutRaiz
        Me.LayoutSabana.Size = New System.Drawing.Size(300, 300)
        Me.LayoutSabana.TabIndex = 0
        Me.LayoutSabana.Text = "LayoutSabana"
        '
        'LayoutRaiz
        '
        Me.LayoutRaiz.CustomizationFormText = "LayoutRaiz"
        Me.LayoutRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutRaiz.GroupBordersVisible = False
        Me.LayoutRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EspacioDerecha, Me.EspacioArriba, Me.EspacioAbajo, Me.EspacioIzquierda, Me.GrupoProgreso})
        Me.LayoutRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutRaiz.Name = "LayoutRaiz"
        Me.LayoutRaiz.Size = New System.Drawing.Size(300, 300)
        Me.LayoutRaiz.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutRaiz.Text = "LayoutRaiz"
        Me.LayoutRaiz.TextVisible = False
        '
        'EspacioDerecha
        '
        Me.EspacioDerecha.CustomizationFormText = "EspacioDerecha"
        Me.EspacioDerecha.Location = New System.Drawing.Point(179, 101)
        Me.EspacioDerecha.Name = "EspacioDerecha"
        Me.EspacioDerecha.Size = New System.Drawing.Size(101, 78)
        Me.EspacioDerecha.Text = "EspacioDerecha"
        Me.EspacioDerecha.TextSize = New System.Drawing.Size(0, 0)
        '
        'EspacioArriba
        '
        Me.EspacioArriba.CustomizationFormText = "EspacioArriba"
        Me.EspacioArriba.Location = New System.Drawing.Point(0, 0)
        Me.EspacioArriba.Name = "EspacioArriba"
        Me.EspacioArriba.Size = New System.Drawing.Size(280, 101)
        Me.EspacioArriba.Text = "EspacioArriba"
        Me.EspacioArriba.TextSize = New System.Drawing.Size(0, 0)
        '
        'EspacioIzquierda
        '
        Me.EspacioIzquierda.CustomizationFormText = "EspacioIzquierda"
        Me.EspacioIzquierda.Location = New System.Drawing.Point(0, 101)
        Me.EspacioIzquierda.Name = "EspacioIzquierda"
        Me.EspacioIzquierda.Size = New System.Drawing.Size(101, 78)
        Me.EspacioIzquierda.Text = "EspacioIzquierda"
        Me.EspacioIzquierda.TextSize = New System.Drawing.Size(0, 0)
        '
        'EspacioAbajo
        '
        Me.EspacioAbajo.CustomizationFormText = "EspacioAbajo"
        Me.EspacioAbajo.Location = New System.Drawing.Point(0, 179)
        Me.EspacioAbajo.Name = "EspacioAbajo"
        Me.EspacioAbajo.Size = New System.Drawing.Size(280, 101)
        Me.EspacioAbajo.Text = "EspacioAbajo"
        Me.EspacioAbajo.TextSize = New System.Drawing.Size(0, 0)
        '
        'EtiquetaMensaje
        '
        Me.EtiquetaMensaje.AllowHtmlString = True
        Me.EtiquetaMensaje.Location = New System.Drawing.Point(125, 145)
        Me.EtiquetaMensaje.Name = "EtiquetaMensaje"
        Me.EtiquetaMensaje.Size = New System.Drawing.Size(48, 14)
        Me.EtiquetaMensaje.StyleController = Me.LayoutSabana
        Me.EtiquetaMensaje.TabIndex = 4
        Me.EtiquetaMensaje.Text = "[Mensaje]"
        '
        'ItemParaMensaje
        '
        Me.ItemParaMensaje.Control = Me.EtiquetaMensaje
        Me.ItemParaMensaje.CustomizationFormText = "ItemParaMensaje"
        Me.ItemParaMensaje.Location = New System.Drawing.Point(0, 0)
        Me.ItemParaMensaje.Name = "ItemParaMensaje"
        Me.ItemParaMensaje.Size = New System.Drawing.Size(54, 18)
        Me.ItemParaMensaje.Text = "ItemParaMensaje"
        Me.ItemParaMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemParaMensaje.TextToControlDistance = 0
        Me.ItemParaMensaje.TextVisible = False
        '
        'ItemParaProgreso
        '
        Me.ItemParaProgreso.Control = Me.ProgresoContinuoSabana
        Me.ItemParaProgreso.CustomizationFormText = "ItemParaProgreso"
        Me.ItemParaProgreso.Location = New System.Drawing.Point(0, 18)
        Me.ItemParaProgreso.Name = "ItemParaProgreso"
        Me.ItemParaProgreso.Size = New System.Drawing.Size(54, 16)
        Me.ItemParaProgreso.Text = "ItemParaProgreso"
        Me.ItemParaProgreso.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemParaProgreso.TextToControlDistance = 0
        Me.ItemParaProgreso.TextVisible = False
        '
        'GrupoProgreso
        '
        Me.GrupoProgreso.CustomizationFormText = "[Titulo]"
        Me.GrupoProgreso.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.ItemParaProgreso, Me.ItemParaMensaje})
        Me.GrupoProgreso.Location = New System.Drawing.Point(101, 101)
        Me.GrupoProgreso.Name = "GrupoProgreso"
        Me.GrupoProgreso.Size = New System.Drawing.Size(78, 78)
        Me.GrupoProgreso.Text = "[Titulo]"
        '
        'ProgresoContinuoSabana
        '
        Me.ProgresoContinuoSabana.EditValue = 0
        Me.ProgresoContinuoSabana.Location = New System.Drawing.Point(125, 163)
        Me.ProgresoContinuoSabana.Name = "ProgresoContinuoSabana"
        Me.ProgresoContinuoSabana.Properties.AllowFocused = False
        Me.ProgresoContinuoSabana.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.ProgresoContinuoSabana.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.PingPong
        Me.ProgresoContinuoSabana.Size = New System.Drawing.Size(50, 12)
        Me.ProgresoContinuoSabana.StyleController = Me.LayoutSabana
        Me.ProgresoContinuoSabana.TabIndex = 5
        '
        'Sabana
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(300, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.LayoutSabana)
        Me.Cursor = System.Windows.Forms.Cursors.No
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Sabana"
        Me.Opacity = 0.75
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        CType(Me.LayoutSabana, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutSabana.ResumeLayout(False)
        CType(Me.LayoutRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioDerecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioArriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioIzquierda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioAbajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemParaMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemParaProgreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrupoProgreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgresoContinuoSabana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutSabana As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EspacioDerecha As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EspacioArriba As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EspacioAbajo As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EspacioIzquierda As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EtiquetaMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ItemParaMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemParaProgreso As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GrupoProgreso As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ProgresoContinuoSabana As DevExpress.XtraEditors.MarqueeProgressBarControl
End Class
