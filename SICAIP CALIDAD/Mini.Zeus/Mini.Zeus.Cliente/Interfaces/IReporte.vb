﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas reporte de Zeus.
''' </summary>
''' <remarks>Todas las ventanas reporte basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IReporte

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Sub Foco()

    ''' <summary>
    ''' Muestra la pestaña dentro de la ventana padre.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Sub MostrarVentana(ByVal owner As IWin32Window)

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    Property VentanaPadre() As IFormaPrincipal

    ''' <summary>
    ''' Actualiza el contenido de la pestaña.
    ''' </summary>
    ''' <remarks>Esta función actualiza la información que se muestra en la pestaña. La función tiene un evento Antes y un evento Despues.</remarks>
    Sub ActualizarVentana()

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Event AntesActualizarVentana As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Event DespuesActualizarVentana As EventHandler

    ''' <summary>
    ''' Genera el reporte con su información para desplegar en la ventana.
    ''' </summary>
    Sub GenerarReporte()

End Interface
