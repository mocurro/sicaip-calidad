﻿Imports System.ComponentModel

Public Class TarjetaComponente_Unidad_Negocio
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoComponente_Unidad_Negocio))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Componente_Unidad_NegocioCampos  , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Componente_Unidad_NegocioCampos.cve_Componente  , ItemComponente )
            .Add(Datos.Componente_Unidad_NegocioCampos.cve_Unidad_negocio  , itemUnidadNegocio )
            .Add(Datos.Componente_Unidad_NegocioCampos.fecha_inicio  , itemFechaInicio)
            .Add(Datos.Componente_Unidad_NegocioCampos.Fecha_fin  , ItemFechaFin)
            .Add(Datos.Componente_Unidad_NegocioCampos.Estatus   , ItemEstatus)
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Componente_Unidad_NegocioCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Componente_Unidad_Negocio  )()
        '
        Select Case sender.Name
            Case ComboComponente .Name
                If  ComboComponente.EditValue IsNot Nothing Then
                    Dim MiGUID As Integer = CInt (ComboComponente.EditValue.ToString )
                 Paquete.cve_componente    = MiGUID
                End If
                 
                Return True 
                
            Case ComboUnidadNegocio .Name
                If ComboUnidadNegocio.EditValue IsNot Nothing Then
                    Dim MiGUID As Long = CInt (ComboUnidadNegocio.EditValue.ToString )
                 Paquete.cve_unidad_negocio   = MiGUID
                End If
                 
                Return True
            Case ComboEstatus.Name

                Dim MiGUID As Long = CInt(ComboEstatus.SelectedIndex  )
                Paquete.estatus = MiGUID
                Return True

            Case DateFechaInicio.Name
                 'Dim MiGUID As Integer = CInt (ComboComponente.EditValue.ToString )
                 Paquete.fecha_inicio  = DateFechaInicio.EditValue 
                Return True 
                
            Case DateFechaFin  .Name
                
                 'Dim MiGUID As Long = CInt (ComboUnidadNegocio.EditValue.ToString )
                 Paquete.fecha_fin    = DateFechaFin .EditValue 
                Return True 
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Componente_Unidad_Negocio )()
        'Cargar los combos
        With EntidadDe(Of Datos.Componente_Unidad_Negocio  )()
            FuenteComponentes.DataSource = CatalogoDe(Of CatalogoComponente_Unidad_Negocio  ).componentes  .FindAll(Function(Item) Item.Estatus  = 1)
        End With
         'Cargar los combos
        With EntidadDe(Of Datos.Componente_Unidad_Negocio )()
            FuenteUnidadNegocio .DataSource = CatalogoDe(Of CatalogoComponente_Unidad_Negocio).Unidades_Negocio .FindAll(Function(Item) Item.Estatus =0 )
        End With
        OmitirCambiosPersonalizados = True
        If paquete.Estatus = 0 Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If
        
        ComboComponente  .EditValue = paquete.cve_componente  
        comboUnidadNegocio.EditValue = paquete.cve_unidad_negocio  
        DateFechaInicio.EditValue =paquete.fecha_inicio 
        DateFechaFin .EditValue =paquete.fecha_fin  
        OmitirCambiosPersonalizados = False 

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Componente_Unidad_Negocio )()
        
    End Sub
End Class