﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Usuario", "Usuarios", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Cliente.Nombre", 1, "Cliente",Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Usuarios: {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "modelo.np_gkn", 2, "Modelo")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 3, "Estatus")>
Public Class SEGURIDAD_USUARIO
    Inherits Entidad(Of SEGURIDAD_USUARIO, SEGURIDAD_USUARIOCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As SEGURIDAD_USUARIO)
        With EntidadNueva
            .CVE_Usuario =CVE_Usuario 
            .Id_Usuario =Id_Usuario 
            .Pass =Pass
            .Nombre =Nombre 
            .Email =Email 
            .CVE_TIPO_USUARIO =CVE_TIPO_USUARIO 
            .Estatus =Estatus 
            .codigo_empleado =codigo_empleado 
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
            .CVE_Usuario =CVE_Usuario 
            .Id_Usuario =Id_Usuario 
            .Pass =Pass
            .Nombre =Nombre 
            .Email =Email 
            .CVE_TIPO_USUARIO =CVE_TIPO_USUARIO 
            .Estatus =Estatus 
            .codigo_empleado =codigo_empleado
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            CVE_Usuario =.CVE_Usuario 
            Id_Usuario =.Id_Usuario 
            Pass =.Pass
            Nombre =.Nombre 
            Email =.Email 
            CVE_TIPO_USUARIO =.CVE_TIPO_USUARIO 
            Estatus =.Estatus 
            codigo_empleado =.codigo_empleado
            Estatus =.Estatus 
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As SEGURIDAD_USUARIO 
        Return New SEGURIDAD_USUARIO 
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As SEGURIDAD_USUARIO ) As Boolean
        Return Equals(Entidad.CVE_Usuario  , CVE_Usuario )
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return estatus 
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As SEGURIDAD_USUARIOCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case SEGURIDAD_USUARIOCampos .cve_usuario     
                If Equals(CVE_Usuario, 0) Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If
          
        End Select
        Return Resultado
    End Function
End Class
