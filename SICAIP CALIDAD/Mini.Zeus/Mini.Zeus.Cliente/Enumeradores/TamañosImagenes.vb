﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los diferentes tamaños estandar para imágenes.
''' </summary>
''' <remarks>La lista contiene los valores en pixeles de imágenes de tamaño estandar. Todos los valores representan medidas cuadradas.</remarks>
Public Enum TamañosImagenes As Integer

    ''' <summary>
    ''' Tamaño desconocido. El contexto determinará el tamaño final.
    ''' </summary>
    xOtro = 0

    ''' <summary>
    ''' 16 x 16 pixels.
    ''' </summary>
    x16 = 16

    ''' <summary>
    ''' 32 x 32 pixels.
    ''' </summary>
    x32 = 32

    ''' <summary>
    ''' 48 x 48 pixels.
    ''' </summary>
    x48 = 48

    ''' <summary>
    ''' 256 x 256 pixels.
    ''' </summary>
    x256 = 256

End Enum