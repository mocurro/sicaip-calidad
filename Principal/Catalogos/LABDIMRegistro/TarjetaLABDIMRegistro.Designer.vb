﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaLABDIMRegistro
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboComponente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteModelos = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemComponente = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtSolicitante = New DevExpress.XtraEditors.TextEdit()
        Me.itemSolicitante = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit()
        Me.ItemDescripción = New DevExpress.XtraLayout.LayoutControlItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txtComentarios = New DevExpress.XtraEditors.TextEdit()
        Me.itemComentarios = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboTipoinspeccion = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteTipoInspeccion = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemtipoinspeccion = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SpinNoPiezas = New DevExpress.XtraEditors.SpinEdit()
        Me.ItemNoPiezas = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SpinNoCotas = New DevExpress.XtraEditors.SpinEdit()
        Me.itemNoCotas = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ManejadorValidacion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutTarjeta,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutTarjeta.SuspendLayout
        CType(Me.LayoutTarjetaRaiz,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboComponente.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.FuenteModelos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemComponente,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtSolicitante.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemSolicitante,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtDescripcion.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ItemDescripción,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtComentarios.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemComentarios,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboTipoinspeccion.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.FuenteTipoInspeccion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemtipoinspeccion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SpinNoPiezas.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ItemNoPiezas,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SpinNoCotas.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemNoCotas,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.SpinNoCotas)
        Me.LayoutTarjeta.Controls.Add(Me.SpinNoPiezas)
        Me.LayoutTarjeta.Controls.Add(Me.ComboTipoinspeccion)
        Me.LayoutTarjeta.Controls.Add(Me.txtComentarios)
        Me.LayoutTarjeta.Controls.Add(Me.txtDescripcion)
        Me.LayoutTarjeta.Controls.Add(Me.txtSolicitante)
        Me.LayoutTarjeta.Controls.Add(Me.ComboComponente)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = true
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 222)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.itemComponente, Me.itemSolicitante, Me.ItemDescripción, Me.itemComentarios, Me.itemtipoinspeccion, Me.ItemNoPiezas, Me.itemNoCotas})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 222)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 168)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 34)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboComponente
        '
        Me.ComboComponente.Location = New System.Drawing.Point(103, 36)
        Me.ComboComponente.Name = "ComboComponente"
        Me.ComboComponente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboComponente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("np_gkn", "modelo")})
        Me.ComboComponente.Properties.DataSource = Me.FuenteModelos
        Me.ComboComponente.Properties.DisplayMember = "np_gkn"
        Me.ComboComponente.Properties.NullText = "Seleccione número de parte"
        Me.ComboComponente.Properties.ValueMember = "cve_modelo"
        Me.ComboComponente.Size = New System.Drawing.Size(405, 20)
        Me.ComboComponente.StyleController = Me.LayoutTarjeta
        Me.ComboComponente.TabIndex = 8
        '
        'itemComponente
        '
        Me.itemComponente.Control = Me.ComboComponente
        Me.itemComponente.Location = New System.Drawing.Point(0, 24)
        Me.itemComponente.Name = "itemComponente"
        Me.itemComponente.Size = New System.Drawing.Size(500, 24)
        Me.itemComponente.Text = "Número de parte"
        Me.itemComponente.TextSize = New System.Drawing.Size(87, 13)
        '
        'txtSolicitante
        '
        Me.txtSolicitante.Enabled = False
        Me.txtSolicitante.Location = New System.Drawing.Point(103, 12)
        Me.txtSolicitante.Name = "txtSolicitante"
        Me.txtSolicitante.Size = New System.Drawing.Size(405, 20)
        Me.txtSolicitante.StyleController = Me.LayoutTarjeta
        Me.txtSolicitante.TabIndex = 9
        '
        'itemSolicitante
        '
        Me.itemSolicitante.Control = Me.txtSolicitante
        Me.itemSolicitante.Location = New System.Drawing.Point(0, 0)
        Me.itemSolicitante.Name = "itemSolicitante"
        Me.itemSolicitante.Size = New System.Drawing.Size(500, 24)
        Me.itemSolicitante.Text = "Solicitante"
        Me.itemSolicitante.TextSize = New System.Drawing.Size(87, 13)
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Enabled = False
        Me.txtDescripcion.Location = New System.Drawing.Point(103, 60)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(405, 20)
        Me.txtDescripcion.StyleController = Me.LayoutTarjeta
        Me.txtDescripcion.TabIndex = 11
        '
        'ItemDescripción
        '
        Me.ItemDescripción.Control = Me.txtDescripcion
        Me.ItemDescripción.Location = New System.Drawing.Point(0, 48)
        Me.ItemDescripción.Name = "ItemDescripción"
        Me.ItemDescripción.Size = New System.Drawing.Size(500, 24)
        Me.ItemDescripción.Text = "Descripción"
        Me.ItemDescripción.TextSize = New System.Drawing.Size(87, 13)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txtComentarios
        '
        Me.txtComentarios.Location = New System.Drawing.Point(103, 156)
        Me.txtComentarios.Name = "txtComentarios"
        Me.txtComentarios.Size = New System.Drawing.Size(405, 20)
        Me.txtComentarios.StyleController = Me.LayoutTarjeta
        Me.txtComentarios.TabIndex = 13
        '
        'itemComentarios
        '
        Me.itemComentarios.Control = Me.txtComentarios
        Me.itemComentarios.Location = New System.Drawing.Point(0, 144)
        Me.itemComentarios.Name = "itemComentarios"
        Me.itemComentarios.Size = New System.Drawing.Size(500, 24)
        Me.itemComentarios.Text = "Comentarios"
        Me.itemComentarios.TextSize = New System.Drawing.Size(87, 13)
        '
        'ComboTipoinspeccion
        '
        Me.ComboTipoinspeccion.Location = New System.Drawing.Point(103, 132)
        Me.ComboTipoinspeccion.Name = "ComboTipoinspeccion"
        Me.ComboTipoinspeccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboTipoinspeccion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("nombre", "Nombre")})
        Me.ComboTipoinspeccion.Properties.DataSource = Me.FuenteTipoInspeccion
        Me.ComboTipoinspeccion.Properties.DisplayMember = "nombre"
        Me.ComboTipoinspeccion.Properties.ValueMember = "cve_tipo_inspeccion"
        Me.ComboTipoinspeccion.Size = New System.Drawing.Size(405, 20)
        Me.ComboTipoinspeccion.StyleController = Me.LayoutTarjeta
        Me.ComboTipoinspeccion.TabIndex = 15
        '
        'itemtipoinspeccion
        '
        Me.itemtipoinspeccion.Control = Me.ComboTipoinspeccion
        Me.itemtipoinspeccion.Location = New System.Drawing.Point(0, 120)
        Me.itemtipoinspeccion.Name = "itemtipoinspeccion"
        Me.itemtipoinspeccion.Size = New System.Drawing.Size(500, 24)
        Me.itemtipoinspeccion.Text = "Tipo de inspección"
        Me.itemtipoinspeccion.TextSize = New System.Drawing.Size(87, 13)
        '
        'SpinNoPiezas
        '
        Me.SpinNoPiezas.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinNoPiezas.Location = New System.Drawing.Point(103, 84)
        Me.SpinNoPiezas.Name = "SpinNoPiezas"
        Me.SpinNoPiezas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinNoPiezas.Properties.MaxValue = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.SpinNoPiezas.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinNoPiezas.Properties.NullText = "1"
        Me.SpinNoPiezas.Size = New System.Drawing.Size(405, 20)
        Me.SpinNoPiezas.StyleController = Me.LayoutTarjeta
        Me.SpinNoPiezas.TabIndex = 16
        '
        'ItemNoPiezas
        '
        Me.ItemNoPiezas.Control = Me.SpinNoPiezas
        Me.ItemNoPiezas.Location = New System.Drawing.Point(0, 72)
        Me.ItemNoPiezas.Name = "ItemNoPiezas"
        Me.ItemNoPiezas.Size = New System.Drawing.Size(500, 24)
        Me.ItemNoPiezas.Text = "Número de piezas"
        Me.ItemNoPiezas.TextSize = New System.Drawing.Size(87, 13)
        '
        'SpinNoCotas
        '
        Me.SpinNoCotas.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinNoCotas.Location = New System.Drawing.Point(103, 108)
        Me.SpinNoCotas.Name = "SpinNoCotas"
        Me.SpinNoCotas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinNoCotas.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.SpinNoCotas.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinNoCotas.Properties.NullText = "1"
        Me.SpinNoCotas.Size = New System.Drawing.Size(405, 20)
        Me.SpinNoCotas.StyleController = Me.LayoutTarjeta
        Me.SpinNoCotas.TabIndex = 17
        '
        'itemNoCotas
        '
        Me.itemNoCotas.Control = Me.SpinNoCotas
        Me.itemNoCotas.Location = New System.Drawing.Point(0, 96)
        Me.itemNoCotas.Name = "itemNoCotas"
        Me.itemNoCotas.Size = New System.Drawing.Size(500, 24)
        Me.itemNoCotas.Text = "Número de cotas"
        Me.itemNoCotas.TextSize = New System.Drawing.Size(87, 13)
        '
        'TarjetaLABDIMRegistro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 348)
        Me.Name = "TarjetaLABDIMRegistro"
        Me.Text = "TarjetaLabDim"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolicitante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemSolicitante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemDescripción, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComentarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComentarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboTipoinspeccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteTipoInspeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemtipoinspeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpinNoPiezas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemNoPiezas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpinNoCotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemNoCotas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboComponente As LookUpEdit
    Friend WithEvents itemComponente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteModelos As BindingSource
    Friend WithEvents txtComentarios As TextEdit
    Friend WithEvents txtDescripcion As TextEdit
    Friend WithEvents txtSolicitante As TextEdit
    Friend WithEvents itemSolicitante As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemDescripción As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemComentarios As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents ComboTipoinspeccion As LookUpEdit
    Friend WithEvents itemtipoinspeccion As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteTipoInspeccion As BindingSource
    Friend WithEvents SpinNoPiezas As SpinEdit
    Friend WithEvents ItemNoPiezas As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SpinNoCotas As SpinEdit
    Friend WithEvents itemNoCotas As DevExpress.XtraLayout.LayoutControlItem
End Class
