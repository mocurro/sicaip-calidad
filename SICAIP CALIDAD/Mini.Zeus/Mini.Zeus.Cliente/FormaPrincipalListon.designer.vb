﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormaPrincipalListon
    Inherits FormaListon

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TemaDefault = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.MenuAplicacionSalir = New DevExpress.XtraBars.BarButtonItem
        Me.ManejadorPestañas = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.ToolTipDefault = New DevExpress.Utils.DefaultToolTipController(Me.components)
        Me.BarraEtiquetaSesionUsuario = New DevExpress.XtraBars.BarStaticItem
        Me.BarraEtiquetaAmbiente = New DevExpress.XtraBars.BarStaticItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.MenuAplicacionOpciones = New DevExpress.XtraBars.BarButtonItem
        Me.MenuAplicacionSesionUsuario = New DevExpress.XtraBars.BarSubItem
        Me.MenuAplicacionSesionUsuarioCambiarSesion = New DevExpress.XtraBars.BarButtonItem
        Me.MenuAplicacionSesionUsuarioCambiarContraseña = New DevExpress.XtraBars.BarButtonItem
        Me.BarraEtiquetaCopyright = New DevExpress.XtraBars.BarStaticItem
        CType(Me.Liston, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MenuAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorPestañas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Liston
        '
        Me.Liston.BackColor = System.Drawing.SystemColors.AppWorkspace
        '
        '
        '
        Me.Liston.ExpandCollapseItem.Id = 0
        Me.Liston.ExpandCollapseItem.Name = ""
        Me.Liston.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.MenuAplicacionOpciones, Me.MenuAplicacionSesionUsuario, Me.MenuAplicacionSalir, Me.BarraEtiquetaSesionUsuario, Me.BarraEtiquetaAmbiente, Me.BarButtonItem1, Me.MenuAplicacionSesionUsuarioCambiarSesion, Me.MenuAplicacionSesionUsuarioCambiarContraseña, Me.BarraEtiquetaCopyright})
        Me.Liston.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Liston.MaxItemId = 9
        Me.Liston.Toolbar.ShowCustomizeItem = False
        '
        'BarraEstado
        '
        Me.BarraEstado.ItemLinks.Add(Me.BarraEtiquetaSesionUsuario)
        Me.BarraEstado.ItemLinks.Add(Me.BarraEtiquetaAmbiente, True)
        Me.BarraEstado.ItemLinks.Add(Me.BarraEtiquetaCopyright)
        '
        'PanelCliente
        '
        Me.ToolTipDefault.SetAllowHtmlText(Me.PanelCliente, DevExpress.Utils.DefaultBoolean.[Default])
        '
        'MenuAplicacion
        '
        Me.MenuAplicacion.ItemLinks.Add(Me.MenuAplicacionOpciones)
        Me.MenuAplicacion.ItemLinks.Add(Me.MenuAplicacionSalir, True)
        '
        'MenuAplicacionSalir
        '
        Me.MenuAplicacionSalir.Caption = "&Salir"
        Me.MenuAplicacionSalir.Description = "Cierra la aplicación."
        Me.MenuAplicacionSalir.Glyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen16_Salir
        Me.MenuAplicacionSalir.Id = 0
        Me.MenuAplicacionSalir.LargeGlyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen32_Salir
        Me.MenuAplicacionSalir.Name = "MenuAplicacionSalir"
        '
        'ManejadorPestañas
        '
        Me.ManejadorPestañas.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders
        Me.ManejadorPestañas.MdiParent = Me
        Me.ManejadorPestañas.ToolTipController = Me.ToolTipDefault.DefaultController
        '
        'ToolTipDefault
        '
        '
        '
        '
        Me.ToolTipDefault.DefaultController.AllowHtmlText = True
        Me.ToolTipDefault.DefaultController.AutoPopDelay = 10000
        Me.ToolTipDefault.DefaultController.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip
        '
        'BarraEtiquetaSesionUsuario
        '
        Me.BarraEtiquetaSesionUsuario.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarraEtiquetaSesionUsuario.Caption = "[Usuario]"
        Me.BarraEtiquetaSesionUsuario.Glyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen16_SesionUsuario
        Me.BarraEtiquetaSesionUsuario.Id = 2
        Me.BarraEtiquetaSesionUsuario.Name = "BarraEtiquetaSesionUsuario"
        Me.BarraEtiquetaSesionUsuario.TextAlignment = System.Drawing.StringAlignment.Near
        Me.BarraEtiquetaSesionUsuario.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarraEtiquetaAmbiente
        '
        Me.BarraEtiquetaAmbiente.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarraEtiquetaAmbiente.Caption = "[Ambiente]"
        Me.BarraEtiquetaAmbiente.Glyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen16_Ambientes
        Me.BarraEtiquetaAmbiente.Id = 3
        Me.BarraEtiquetaAmbiente.Name = "BarraEtiquetaAmbiente"
        Me.BarraEtiquetaAmbiente.TextAlignment = System.Drawing.StringAlignment.Near
        Me.BarraEtiquetaAmbiente.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 4
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'MenuAplicacionOpciones
        '
        Me.MenuAplicacionOpciones.Caption = "&Opciones"
        Me.MenuAplicacionOpciones.Description = "Modifica opciones de la aplicación y da acceso a recursos disponibles."
        Me.MenuAplicacionOpciones.Id = 1
        Me.MenuAplicacionOpciones.LargeGlyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen32_GloboInformacion
        Me.MenuAplicacionOpciones.Name = "MenuAplicacionOpciones"
        '
        'MenuAplicacionSesionUsuario
        '
        Me.MenuAplicacionSesionUsuario.Caption = "Sesión de &usuario"
        Me.MenuAplicacionSesionUsuario.Description = "Permite hacer cambios a la sesión activa."
        Me.MenuAplicacionSesionUsuario.Id = 5
        Me.MenuAplicacionSesionUsuario.LargeGlyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen32_SesionUsuario
        Me.MenuAplicacionSesionUsuario.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.MenuAplicacionSesionUsuarioCambiarSesion), New DevExpress.XtraBars.LinkPersistInfo(Me.MenuAplicacionSesionUsuarioCambiarContraseña)})
        Me.MenuAplicacionSesionUsuario.Name = "MenuAplicacionSesionUsuario"
        '
        'MenuAplicacionSesionUsuarioCambiarSesion
        '
        Me.MenuAplicacionSesionUsuarioCambiarSesion.Caption = "&Cambiar de usuario"
        Me.MenuAplicacionSesionUsuarioCambiarSesion.Description = "Cierra la sesión de usuario actual y permite iniciar una nueva sesión."
        Me.MenuAplicacionSesionUsuarioCambiarSesion.Id = 6
        Me.MenuAplicacionSesionUsuarioCambiarSesion.LargeGlyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen32_SesionUsuarioCambiar
        Me.MenuAplicacionSesionUsuarioCambiarSesion.Name = "MenuAplicacionSesionUsuarioCambiarSesion"
        '
        'MenuAplicacionSesionUsuarioCambiarContraseña
        '
        Me.MenuAplicacionSesionUsuarioCambiarContraseña.Caption = "&Modificar mi contraseña"
        Me.MenuAplicacionSesionUsuarioCambiarContraseña.Description = "Permite cambiar la contraseña del usuario actual."
        Me.MenuAplicacionSesionUsuarioCambiarContraseña.Id = 7
        Me.MenuAplicacionSesionUsuarioCambiarContraseña.LargeGlyph = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen32_UsuarioCambiarContraseña
        Me.MenuAplicacionSesionUsuarioCambiarContraseña.Name = "MenuAplicacionSesionUsuarioCambiarContraseña"
        '
        'BarraEtiquetaCopyright
        '
        Me.BarraEtiquetaCopyright.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarraEtiquetaCopyright.Caption = "[Trademark | Copyright]"
        Me.BarraEtiquetaCopyright.Id = 8
        Me.BarraEtiquetaCopyright.Name = "BarraEtiquetaCopyright"
        Me.BarraEtiquetaCopyright.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'FormaPrincipalListon
        '
        Me.ToolTipDefault.SetAllowHtmlText(Me, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Appearance.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(632, 479)
        Me.IsMdiContainer = True
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MinimumSize = New System.Drawing.Size(320, 240)
        Me.Name = "FormaPrincipalListon"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Liston, 0)
        Me.Controls.SetChildIndex(Me.BarraEstado, 0)
        Me.Controls.SetChildIndex(Me.PanelCliente, 0)
        CType(Me.Liston, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MenuAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorPestañas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents MenuAplicacionSalir As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents ManejadorPestañas As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Protected Friend WithEvents ToolTipDefault As DevExpress.Utils.DefaultToolTipController
    Friend WithEvents BarraEtiquetaSesionUsuario As DevExpress.XtraBars.BarStaticItem
    Protected Friend WithEvents TemaDefault As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BarraEtiquetaAmbiente As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MenuAplicacionSesionUsuarioCambiarSesion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents MenuAplicacionSesionUsuarioCambiarContraseña As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarraEtiquetaCopyright As DevExpress.XtraBars.BarStaticItem
    Protected Friend WithEvents MenuAplicacionOpciones As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents MenuAplicacionSesionUsuario As DevExpress.XtraBars.BarSubItem
End Class
