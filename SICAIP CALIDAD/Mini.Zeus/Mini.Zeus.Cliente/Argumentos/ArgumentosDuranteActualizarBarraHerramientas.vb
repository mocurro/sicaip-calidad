﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2011 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Argumentos para acciones durante la actualización de una barra de herramientas.
''' </summary>
Public Class ArgumentosDuranteActualizarBarraHerramientas
    Inherits System.EventArgs

    ''' <summary>
    ''' Identifica si hay un único renglón seleccionado.
    ''' </summary>
    Private MiUnRenglon As Boolean

    ''' <summary>
    ''' Identifica si hay uno o más renglones seleccionados.
    ''' </summary>
    Private MiUnoOMasRenglones As Boolean

    ''' <summary>
    ''' Identifica si el catálogo no está ocupado procesando.
    ''' </summary>
    Private MiNoEstaOcupado As Boolean

    ''' <summary>
    ''' Crea un nuevo argumento.
    ''' </summary>
    ''' <param name="UnRenglon">Identifica si hay un único renglón seleccionado.</param>
    ''' <param name="UnoOMasRenglones">Identifica si hay uno o más renglones seleccionados.</param>
    ''' <param name="NoEstaOcupado">Identifica si el catálogo no está ocupado procesando.</param>
    Public Sub New(ByVal UnRenglon As Boolean, ByVal UnoOMasRenglones As Boolean, ByVal NoEstaOcupado As Boolean)

        MyBase.New()
        MiUnRenglon = UnRenglon
        MiUnoOMasRenglones = UnoOMasRenglones
        MiNoEstaOcupado = NoEstaOcupado

    End Sub

    ''' <summary>
    ''' Identifica si hay un único renglón seleccionado.
    ''' </summary>
    Public ReadOnly Property UnRenglon() As Boolean
        Get
            Return MiUnRenglon
        End Get
    End Property

    ''' <summary>
    ''' Identifica si hay uno o más renglones seleccionados.
    ''' </summary>
    Public ReadOnly Property UnoOMasRenglones() As Boolean
        Get
            Return MiUnoOMasRenglones
        End Get
    End Property

    ''' <summary>
    ''' Identifica si el catálogo no está ocupado procesando.
    ''' </summary>
    Public ReadOnly Property NoEstaOcupado() As Boolean
        Get
            Return MiNoEstaOcupado
        End Get
    End Property

End Class