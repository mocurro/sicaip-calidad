﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2011 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Public Class TareaProgramada

    ''' <summary>
    ''' Delegado para ejecutar la función asíncrona.
    ''' </summary>
    Private MiProcesamientoAsincrono As Sincronizacion.DelegadoProcesamiento

    ''' <summary>
    ''' Delegado para ejecutar la función asíncrona con parametros.
    ''' </summary>
    Private MiProcesamientoAsincronoParametrizado As Sincronizacion.DelegadoProcesamientoParametrizado

    ''' <summary>
    ''' Contiene el tiempo en que se repetirá la ejecución de la tarea.
    ''' </summary>
    Private MiIntervaloEjecucionTarea As TimeSpan

    ''' <summary>
    ''' Contiene si es una tarea recurrente.
    ''' </summary>
    Private MiEsRecurrente As Boolean

    ''' <summary>
    ''' Contiene si la tarea está cancelada.
    ''' </summary>
    Private MiEsCancelada As Boolean

    ''' <summary>
    ''' Contiene si la tarea está pausada.
    ''' </summary>
    Private MiEstaPausada As Boolean

    ''' <summary>
    ''' Contiene si la tarea se ha reanudado
    ''' </summary>
    Private MiEsReanudada As Boolean

    ''' <summary>
    ''' Contiene el nombre de la tarea
    ''' </summary>
    Private MiNombreTarea As String

    ''' <summary>
    ''' Contiene la fecha y hora en la que se debe ejecutar la tarea.
    ''' </summary>
    Private MiFechaHoraEjecucion As Date

    ''' <summary>
    ''' Contiene la fecha y hora en que se deberá ejecutar la tarea.
    ''' </summary>
    Friend ReadOnly Property FechaHoraEjecucion() As Date
        Get
            Return MiFechaHoraEjecucion
        End Get
    End Property

    ''' <summary>
    ''' Regresa el intervalo de ejecución de la tarea.
    ''' </summary>
    Friend ReadOnly Property IntervaloEjecucionTarea() As TimeSpan
        Get
            Return MiIntervaloEjecucionTarea
        End Get
    End Property

    ''' <summary>
    ''' Regresa si la función fue cancelada.
    ''' </summary>
    Friend ReadOnly Property EsCancelada() As Boolean
        Get
            Return MiEsCancelada
        End Get
    End Property

    ''' <summary>
    ''' Regresa si la tarea es recurrente o no.
    ''' </summary>
    Friend ReadOnly Property EsRecurrente() As Boolean
        Get
            Return MiEsRecurrente
        End Get
    End Property

    ''' <summary>
    ''' Regresa el nombre de la tarea.
    ''' </summary>
    Friend ReadOnly Property Nombre() As String
        Get
            Return MiNombreTarea
        End Get
    End Property

    ''' <summary>
    ''' Inicializa un objeto TareaProgramada.
    ''' </summary>
    ''' <param name="Nombre">Nombre de la tarea</param>
    ''' <param name="Tarea">Tarea que se ejecutará cada determinado tiempo (función)</param>
    ''' <param name="IntervaloEjecucion">Intervalo de tiempo para ejecutar la tarea</param>
    Public Sub New(ByVal Nombre As String, ByVal Tarea As Sincronizacion.DelegadoProcesamiento, ByVal IntervaloEjecucion As TimeSpan)

        MiNombreTarea = Nombre
        MiFechaHoraEjecucion = Date.UtcNow
        MiIntervaloEjecucionTarea = IntervaloEjecucion
        MiEsRecurrente = IntervaloEjecucion.Ticks > 0
        MiProcesamientoAsincrono = Tarea

    End Sub

    ''' <summary>
    ''' Inicializa un objeto TareaProgramada.
    ''' </summary>
    ''' <param name="Nombre">Nombre de la tarea</param>
    ''' <param name="Tarea">Tarea que se ejecutará cada determinado tiempo (función)</param>
    ''' <param name="IntervaloEjecucion">Intervalo de tiempo para ejecutar la tarea</param>
    Public Sub New(ByVal Nombre As String, ByVal Tarea As Sincronizacion.DelegadoProcesamientoParametrizado, ByVal IntervaloEjecucion As TimeSpan)

        MiNombreTarea = Nombre
        MiFechaHoraEjecucion = Date.UtcNow
        MiIntervaloEjecucionTarea = IntervaloEjecucion
        MiEsRecurrente = IntervaloEjecucion.Ticks > 0
        MiProcesamientoAsincronoParametrizado = Tarea

    End Sub

    ''' <summary>
    ''' Manda a ejecutar la tarea.
    ''' </summary>
    Friend Sub ProcesamientoAsincrono()

        If Not MiEstaPausada Then
            MiProcesamientoAsincrono.BeginInvoke(Nothing, Nothing)
        End If

    End Sub

    Friend Sub ProcesamientoAsincronoParametrizado(ByVal MiObjecto As Object)
        If Not MiEstaPausada Then
            'MiProcesamientoAsincronoParametrizado.BeginInvoke(New MiProcesamientoAsincronoParametrizado(AddressOf ProcesamientoAsincronoParametrizado), MiObjecto)
            MiProcesamientoAsincronoParametrizado.BeginInvoke(MiObjecto, New AsyncCallback(AddressOf ProcesamientoAsincronoParametrizado), MiObjecto)
        End If
    End Sub

    ''' <summary>
    ''' Cancela la tarea programada.
    ''' </summary>
    ''' <remarks>Una vez cancelada la tarea, ya no puede hacerse referencia a esta tarea. 
    ''' Se tendrá que hacer instancia nuevamente de ésta para volverla a programar</remarks>
    Public Sub CancelarTarea()

        MiEsCancelada = True

    End Sub

    ''' <summary>
    ''' Pausa una tarea programada.
    ''' </summary>
    ''' <remarks>Esta tarea puede ser reanudada en cualquier momento</remarks>
    Public Sub PausarTarea()

        MiEstaPausada = True
        MiEsReanudada = False

    End Sub

    ''' <summary>
    ''' Reanuda la tarea que ha sido pausada anteriormente.
    ''' </summary>
    Public Sub ReanudarTarea()

        MiEsReanudada = True
        MiEstaPausada = False

    End Sub

    ''' <summary>
    ''' Recalcula el tiempo en que se volverá a ejecutar una tarea
    ''' </summary>
    Friend Sub RecalcularTarea()

        MiFechaHoraEjecucion = MiFechaHoraEjecucion.AddTicks(MiIntervaloEjecucionTarea.Ticks)

    End Sub

End Class
