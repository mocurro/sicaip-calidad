﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas de tipo diálogo de Zeus.
''' </summary>
''' <remarks>Todas las ventanas de tipo diálogo basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IDialogo

    ''' <summary>
    ''' Abre la ventana de diálogo.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Function MostrarVentana(ByVal owner As IWin32Window) As DialogResult

    '''' <summary>
    '''' Abre la ventana de diálogo para un usuario en particular.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza la ventana.</param>
    '''' <param name="owner">Ventana padre.</param>
    'Function MostrarVentana(ByVal UsuarioActual As Usuarios, ByVal owner As IWin32Window) As DialogResult

End Interface
