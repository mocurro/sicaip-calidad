﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para objetos de Zeus.
''' </summary>
''' <remarks>Todos los objetos basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IObjeto

    ''' <summary>
    ''' Regresa el identificador del objeto.
    ''' </summary>
    ReadOnly Property Id() As Guid

    ''' <summary>
    ''' Regresa el identificador del objeto.
    ''' </summary>
    ReadOnly Property IdTexto() As String

End Interface
