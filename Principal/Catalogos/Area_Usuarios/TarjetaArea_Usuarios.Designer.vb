﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaArea_Usuarios
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboEstatus = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ItemEstatus = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboArea = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteArea = New System.Windows.Forms.BindingSource(Me.components)
        Me.FuenteUsuarios = New System.Windows.Forms.BindingSource(Me.components)
        Me.ItemUnidadNegocio = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboUsuario = New DevExpress.XtraEditors.LookUpEdit()
        Me.itemUsuario = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutTarjeta.SuspendLayout()
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemUnidadNegocio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboUsuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.ComboUsuario)
        Me.LayoutTarjeta.Controls.Add(Me.ComboArea)
        Me.LayoutTarjeta.Controls.Add(Me.ComboEstatus)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 102)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.ItemEstatus, Me.ItemUnidadNegocio, Me.itemUsuario})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 102)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 72)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 10)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboEstatus
        '
        Me.ComboEstatus.Location = New System.Drawing.Point(60, 60)
        Me.ComboEstatus.Name = "ComboEstatus"
        Me.ComboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboEstatus.Properties.Items.AddRange(New Object() {"Activo", "Inactivo"})
        Me.ComboEstatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboEstatus.Size = New System.Drawing.Size(448, 20)
        Me.ComboEstatus.StyleController = Me.LayoutTarjeta
        Me.ComboEstatus.TabIndex = 6
        '
        'ItemEstatus
        '
        Me.ItemEstatus.Control = Me.ComboEstatus
        Me.ItemEstatus.Location = New System.Drawing.Point(0, 48)
        Me.ItemEstatus.Name = "ItemEstatus"
        Me.ItemEstatus.Size = New System.Drawing.Size(500, 24)
        Me.ItemEstatus.Text = "Estatus"
        Me.ItemEstatus.TextSize = New System.Drawing.Size(44, 13)
        '
        'ComboArea
        '
        Me.ComboArea.Location = New System.Drawing.Point(60, 12)
        Me.ComboArea.Name = "ComboArea"
        Me.ComboArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboArea.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Sub área", 46, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.ComboArea.Properties.DataSource = Me.FuenteArea
        Me.ComboArea.Properties.DisplayMember = "descripcion"
        Me.ComboArea.Properties.ValueMember = "cve_detalle"
        Me.ComboArea.Size = New System.Drawing.Size(448, 20)
        Me.ComboArea.StyleController = Me.LayoutTarjeta
        Me.ComboArea.TabIndex = 7
        '
        'ItemUnidadNegocio
        '
        Me.ItemUnidadNegocio.Control = Me.ComboArea
        Me.ItemUnidadNegocio.Location = New System.Drawing.Point(0, 0)
        Me.ItemUnidadNegocio.Name = "ItemUnidadNegocio"
        Me.ItemUnidadNegocio.Size = New System.Drawing.Size(500, 24)
        Me.ItemUnidadNegocio.Text = "Sub Área"
        Me.ItemUnidadNegocio.TextSize = New System.Drawing.Size(44, 13)
        '
        'ComboUsuario
        '
        Me.ComboUsuario.Location = New System.Drawing.Point(60, 36)
        Me.ComboUsuario.Name = "ComboUsuario"
        Me.ComboUsuario.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboUsuario.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Nombre", "Nombre usuario")})
        Me.ComboUsuario.Properties.DataSource = Me.FuenteUsuarios
        Me.ComboUsuario.Properties.DisplayMember = "Nombre"
        Me.ComboUsuario.Properties.ValueMember = "CVE_Usuario"
        Me.ComboUsuario.Size = New System.Drawing.Size(448, 20)
        Me.ComboUsuario.StyleController = Me.LayoutTarjeta
        Me.ComboUsuario.TabIndex = 8
        '
        'itemUsuario
        '
        Me.itemUsuario.Control = Me.ComboUsuario
        Me.itemUsuario.Location = New System.Drawing.Point(0, 24)
        Me.itemUsuario.Name = "itemUsuario"
        Me.itemUsuario.Size = New System.Drawing.Size(500, 24)
        Me.itemUsuario.Text = "Usuario"
        Me.itemUsuario.TextSize = New System.Drawing.Size(44, 13)
        '
        'TarjetaArea_Usuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 228)
        Me.Name = "TarjetaArea_Usuarios"
        Me.Text = "TarjetaArea_Usuario"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemUnidadNegocio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboUsuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboEstatus As ComboBoxEdit
    Friend WithEvents ItemEstatus As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ComboUsuario As LookUpEdit
    Friend WithEvents ComboArea As LookUpEdit
    Friend WithEvents ItemUnidadNegocio As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemUsuario As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteArea As BindingSource
    Friend WithEvents FuenteUsuarios As BindingSource
End Class
