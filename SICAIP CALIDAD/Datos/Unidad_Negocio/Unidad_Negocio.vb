﻿Public Class Unidad_Negocio

    ''' <summary>
    ''' Llave primaria de la tabla componente_unidad_negocio
    ''' </summary>
    Private MiCve_Componente_Unidad_Negocio As Integer 
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case Unidad_NegocioEstatus.Activo 
                    Return "Activo"
                Case Unidad_NegocioEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property
    ''' <summary>
    ''' Llave primaria de la tabla componente_unidad_negocio
    ''' </summary>
    Public Property Cve_Componente_Unidad_Negocio As Integer
        Get
            Return MiCve_Componente_Unidad_Negocio
        End Get
        Set(ByVal value As Integer)
            MiCve_Componente_Unidad_Negocio = value
        End Set
    End Property



End Class
