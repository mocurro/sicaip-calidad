﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2011 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Public Class ProgramadorTareas

    ''' <summary>
    ''' Contiene el hilo del programador 
    ''' </summary>
    Private MiHiloProgramador As System.Threading.Thread



    ''' <summary>
    ''' Contiene la lista del tiempo de espera para ejecutar el monitoreo de las tareas.
    ''' </summary>
    Private MiTiempoEsperaMonitoreo As Integer

    ''' <summary>
    ''' Contiene las tareas programadas con su respectivo Nombre de cada uno.
    ''' </summary>
    Private MiDiccionarioTareasProgramadas As Dictionary(Of String, TareaProgramada)

    ''' <summary>
    ''' Regresa una tarea programada específica.
    ''' </summary>
    ''' <param name="Nombre">Nombre de la tarea programada</param>
    Public Function Tarea(ByVal Nombre As String) As TareaProgramada

        If MiDiccionarioTareasProgramadas.ContainsKey(Nombre) Then
            Return MiDiccionarioTareasProgramadas(Nombre)
        End If

        'Lanza una excepción cuando no existe en el diccionario la tarea solicitada.
        Throw New ExcepcionTareaNoExiste(Me, Reflection.MethodBase.GetCurrentMethod)

    End Function

    ''' <summary>
    ''' Inicializa el programador de tareas
    ''' </summary>
    ''' <param name="TiempoEspera">Tiempo de espera para revisión de las tareas programadas</param>
    Public Sub New(ByVal TiempoEspera As Integer)

        'El tiempo mínimo entre cada ciclo es de 500 milisegundos.
        MiTiempoEsperaMonitoreo = TiempoEspera
        MiDiccionarioTareasProgramadas = New Dictionary(Of String, TareaProgramada)

    End Sub

    ''' <summary>
    ''' Inicia el hilo del programador de tareas.
    ''' </summary>
    Private Sub IniciarHiloProgramador()

        'Inicializamos el hilo del programador de tareas
        If MiHiloProgramador Is Nothing Then
            MiHiloProgramador = New System.Threading.Thread(AddressOf MonitoreaTareasProgramadas)
        End If

        'Verificamos el estatus actual del hilo para iniciarlo.
        If MiHiloProgramador.ThreadState = Threading.ThreadState.Unstarted Then
            MiHiloProgramador.Start()
        End If

    End Sub


    ''' <summary>
    ''' Monitorea la lista de las tareas programadas para checar cuales tareas se deben ejecutar.
    ''' </summary>
    Private Sub MonitoreaTareasProgramadas()

        While MiDiccionarioTareasProgramadas.Count > 0
            For Each TareaProgramada In MiDiccionarioTareasProgramadas.Values.ToArray
                Dim TareaLambda = TareaProgramada
                If TareaLambda.FechaHoraEjecucion <= Date.UtcNow Then
                    ' Manda a ejecutar el método de la tarea programada
                    TareaLambda.ProcesamientoAsincrono()
                    TareaLambda.RecalcularTarea()
                End If
            Next

            'Limpiamos la lista de tareas quitando aquellas que ya no estén programadas.
            For Each TareaProgramada In MiDiccionarioTareasProgramadas.ToArray
                Dim TareaLambda = TareaProgramada
                If TareaLambda.Value.EsCancelada Or Not TareaLambda.Value.EsRecurrente Then
                    MiDiccionarioTareasProgramadas.Remove(TareaLambda.Value.Nombre)
                End If
            Next

            'Manda a dormir el hilo un tiempo determinado
            System.Threading.Thread.Sleep(MiTiempoEsperaMonitoreo)
        End While

        MiHiloProgramador = Nothing

    End Sub

    ''' <summary>
    ''' Registra la tarea en el programador.
    ''' </summary>
    ''' <param name="Tarea">Tarea a ejecutar</param>
    Public Sub RegistrarTarea(ByVal Tarea As TareaProgramada)

        'Agrega a la lista y al diccionario una nueva tarea.
        MiDiccionarioTareasProgramadas.Add(Tarea.Nombre, Tarea)

        'Arranca el hilo de monitoreo de tareas
        IniciarHiloProgramador()

    End Sub

End Class
