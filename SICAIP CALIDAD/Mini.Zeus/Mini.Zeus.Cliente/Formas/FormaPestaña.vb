﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Ventana base para cualquier pestaña que estará contenida en una venta principal de Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana pestaña que implemente UX de Zeus. Todas las ventanas pestaña heredan finalmente de esta ventana.</remarks>
Public Class FormaPestaña
    Implements IPestaña

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Public Event AntesActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Implements IPestaña.AntesActualizarVentana

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Public Event DespuesActualizarVentana(ByVal sender As Object, ByVal e As System.EventArgs) Implements IPestaña.DespuesActualizarVentana

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Public Overridable Sub Foco() Implements IPestaña.Foco

        Focus()

    End Sub

    ''' <summary>
    ''' Muestra la pestaña dentro de la ventana padre.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Sub MostrarVentana(ByVal owner As System.Windows.Forms.IWin32Window) Implements IPestaña.MostrarVentana

        VentanaPadre = DirectCast(owner, IFormaPrincipal)
        show()

    End Sub

    '''' <summary>
    '''' Muestra la pestaña dentro de la ventana padre para un usuario en particular.
    '''' </summary>
    '''' <param name="owner">Ventana padre.</param>
    '''' <param name="UsuarioActual">Usuario dueño de la pestaña.</param>
    'Public Shadows Sub MostrarVentana(ByVal UsuarioActual As Usuarios, ByVal owner As System.Windows.Forms.IWin32Window) Implements IPestaña.MostrarVentana

    '    Me.UsuarioActual = UsuarioActual
    '    MostrarVentana(owner)

    'End Sub

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    <System.ComponentModel.Browsable(False)> _
    Public Overridable Property VentanaPadre() As IFormaPrincipal Implements IPestaña.VentanaPadre
        Get
            Return DirectCast(MdiParent, IFormaPrincipal)
        End Get
        Set(ByVal value As IFormaPrincipal)
            MdiParent = DirectCast(value, Form)
        End Set
    End Property

    ''' <summary>
    ''' Actualiza el contenido de la pestaña.
    ''' </summary>
    ''' <remarks>Esta función actualiza la información que se muestra en la pestaña. La función tiene un evento Antes y un evento Despues.</remarks>
    Public Overridable Sub ActualizarVentana() Implements IPestaña.ActualizarVentana

        Throw New NotImplementedException()

    End Sub

    ''' <summary>
    ''' Regresa la imagen a utilizar en la pestaña.
    ''' </summary>
    Public Overridable ReadOnly Property Imagen() As System.Drawing.Image Implements IPestaña.Imagen
        Get
            Throw New NotImplementedException()
        End Get
    End Property

End Class