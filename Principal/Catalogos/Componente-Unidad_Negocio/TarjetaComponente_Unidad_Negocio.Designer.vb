﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaComponente_Unidad_Negocio
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboEstatus = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ItemEstatus = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboComponente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteComponentes = New System.Windows.Forms.BindingSource(Me.components)
        Me.ItemComponente = New DevExpress.XtraLayout.LayoutControlItem()
        Me.comboUnidadNegocio = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteUnidadNegocio = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemUnidadNegocio = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DateFechaInicio = New DevExpress.XtraEditors.DateEdit()
        Me.itemFechaInicio = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DateFechaFin = New DevExpress.XtraEditors.DateEdit()
        Me.ItemFechaFin = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutTarjeta.SuspendLayout()
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteComponentes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemComponente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.comboUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteUnidadNegocio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemUnidadNegocio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFechaInicio.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFechaInicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFechaFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFechaFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.DateFechaFin)
        Me.LayoutTarjeta.Controls.Add(Me.DateFechaInicio)
        Me.LayoutTarjeta.Controls.Add(Me.comboUnidadNegocio)
        Me.LayoutTarjeta.Controls.Add(Me.ComboComponente)
        Me.LayoutTarjeta.Controls.Add(Me.ComboEstatus)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 152)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.ItemComponente, Me.itemUnidadNegocio, Me.itemFechaInicio, Me.ItemFechaFin, Me.ItemEstatus})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 152)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 120)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 12)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboEstatus
        '
        Me.ComboEstatus.Location = New System.Drawing.Point(104, 108)
        Me.ComboEstatus.Name = "ComboEstatus"
        Me.ComboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboEstatus.Properties.Items.AddRange(New Object() {"Activo", "Inactivo"})
        Me.ComboEstatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboEstatus.Size = New System.Drawing.Size(404, 20)
        Me.ComboEstatus.StyleController = Me.LayoutTarjeta
        Me.ComboEstatus.TabIndex = 6
        '
        'ItemEstatus
        '
        Me.ItemEstatus.Control = Me.ComboEstatus
        Me.ItemEstatus.Location = New System.Drawing.Point(0, 96)
        Me.ItemEstatus.Name = "ItemEstatus"
        Me.ItemEstatus.Size = New System.Drawing.Size(500, 24)
        Me.ItemEstatus.Text = "Estatus"
        Me.ItemEstatus.TextSize = New System.Drawing.Size(88, 13)
        '
        'ComboComponente
        '
        Me.ComboComponente.Location = New System.Drawing.Point(104, 12)
        Me.ComboComponente.Name = "ComboComponente"
        Me.ComboComponente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboComponente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("componente", "Componente")})
        Me.ComboComponente.Properties.DataSource = Me.FuenteComponentes
        Me.ComboComponente.Properties.DisplayMember = "componente"
        Me.ComboComponente.Properties.ValueMember = "cve_componente"
        Me.ComboComponente.Size = New System.Drawing.Size(404, 20)
        Me.ComboComponente.StyleController = Me.LayoutTarjeta
        Me.ComboComponente.TabIndex = 7
        '
        'ItemComponente
        '
        Me.ItemComponente.Control = Me.ComboComponente
        Me.ItemComponente.Location = New System.Drawing.Point(0, 0)
        Me.ItemComponente.Name = "ItemComponente"
        Me.ItemComponente.Size = New System.Drawing.Size(500, 24)
        Me.ItemComponente.Text = "Componente"
        Me.ItemComponente.TextSize = New System.Drawing.Size(88, 13)
        '
        'comboUnidadNegocio
        '
        Me.comboUnidadNegocio.Location = New System.Drawing.Point(104, 36)
        Me.comboUnidadNegocio.Name = "comboUnidadNegocio"
        Me.comboUnidadNegocio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.comboUnidadNegocio.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("nombre", "Unidad de negocio")})
        Me.comboUnidadNegocio.Properties.DataSource = Me.FuenteUnidadNegocio
        Me.comboUnidadNegocio.Properties.DisplayMember = "nombre"
        Me.comboUnidadNegocio.Properties.ValueMember = "cve_unidad_negocio"
        Me.comboUnidadNegocio.Size = New System.Drawing.Size(404, 20)
        Me.comboUnidadNegocio.StyleController = Me.LayoutTarjeta
        Me.comboUnidadNegocio.TabIndex = 8
        '
        'itemUnidadNegocio
        '
        Me.itemUnidadNegocio.Control = Me.comboUnidadNegocio
        Me.itemUnidadNegocio.Location = New System.Drawing.Point(0, 24)
        Me.itemUnidadNegocio.Name = "itemUnidadNegocio"
        Me.itemUnidadNegocio.Size = New System.Drawing.Size(500, 24)
        Me.itemUnidadNegocio.Text = "Unidad de negocio"
        Me.itemUnidadNegocio.TextSize = New System.Drawing.Size(88, 13)
        '
        'DateFechaInicio
        '
        Me.DateFechaInicio.EditValue = Nothing
        Me.DateFechaInicio.Location = New System.Drawing.Point(104, 60)
        Me.DateFechaInicio.Name = "DateFechaInicio"
        Me.DateFechaInicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFechaInicio.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFechaInicio.Size = New System.Drawing.Size(404, 20)
        Me.DateFechaInicio.StyleController = Me.LayoutTarjeta
        Me.DateFechaInicio.TabIndex = 9
        '
        'itemFechaInicio
        '
        Me.itemFechaInicio.Control = Me.DateFechaInicio
        Me.itemFechaInicio.Location = New System.Drawing.Point(0, 48)
        Me.itemFechaInicio.Name = "itemFechaInicio"
        Me.itemFechaInicio.Size = New System.Drawing.Size(500, 24)
        Me.itemFechaInicio.Text = "Fecha inicio"
        Me.itemFechaInicio.TextSize = New System.Drawing.Size(88, 13)
        '
        'DateFechaFin
        '
        Me.DateFechaFin.EditValue = Nothing
        Me.DateFechaFin.Location = New System.Drawing.Point(104, 84)
        Me.DateFechaFin.Name = "DateFechaFin"
        Me.DateFechaFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFechaFin.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFechaFin.Size = New System.Drawing.Size(404, 20)
        Me.DateFechaFin.StyleController = Me.LayoutTarjeta
        Me.DateFechaFin.TabIndex = 10
        '
        'ItemFechaFin
        '
        Me.ItemFechaFin.Control = Me.DateFechaFin
        Me.ItemFechaFin.Location = New System.Drawing.Point(0, 72)
        Me.ItemFechaFin.Name = "ItemFechaFin"
        Me.ItemFechaFin.Size = New System.Drawing.Size(500, 24)
        Me.ItemFechaFin.Text = "Fecha fin"
        Me.ItemFechaFin.TextSize = New System.Drawing.Size(88, 13)
        '
        'TarjetaComponente_Unidad_Negocio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 278)
        Me.Name = "TarjetaComponente_Unidad_Negocio"
        Me.Text = "TarjetaComponente_Unidad_Negocio"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteComponentes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemComponente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.comboUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteUnidadNegocio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemUnidadNegocio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFechaInicio.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFechaInicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFechaFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFechaFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboEstatus As ComboBoxEdit
    Friend WithEvents ItemEstatus As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents comboUnidadNegocio As LookUpEdit
    Friend WithEvents ComboComponente As LookUpEdit
    Friend WithEvents ItemComponente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemUnidadNegocio As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteUnidadNegocio As BindingSource
    Friend WithEvents FuenteComponentes As BindingSource
    Friend WithEvents DateFechaFin As DateEdit
    Friend WithEvents DateFechaInicio As DateEdit
    Friend WithEvents itemFechaInicio As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemFechaFin As DevExpress.XtraLayout.LayoutControlItem
End Class
