﻿Public Class Componente_Unidad_Negocio
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case ClienteEstatus.Activo 
                    Return "Activo"
                Case ClienteEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property

    Public ReadOnly Property component As componente 
    Get
            Dim MiComponente=Datos.Fabricas .componente .ObtenerUno (cve_componente  )
            Return MiComponente 
    End Get
    End Property

     Public ReadOnly Property Unidad_Nego As Unidad_Negocio 
    Get
            Dim Unidad_Neg=Datos.Fabricas .Unidad_Negocio   .ObtenerUno (cve_unidad_negocio   )
            Return Unidad_Neg 
    End Get
    End Property

End Class
