﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface genérica para entidades de Zeus.
''' </summary>
''' <typeparam name="TTipoEntidad">Define el tipo específico de entidad.</typeparam>
''' <typeparam name="TEnumeradorCampos">Define el enumerador que contiene los campos de la entidad específica.</typeparam>
''' <remarks>Todos los objetos de datos basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos)

    ''' <summary>
    ''' Valida la información de un campo específico de la entidad y regresa todos los mensajes posibles cuando el campo no es válido.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a validar.</param>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato del campo. Si la lista resultante está vacia, se puede interpretar que el campo de la entidad es válido.</remarks>
    Function Validar(ByVal Campo As TEnumeradorCampos) As List(Of String)

    ''' <summary>
    ''' Indica si la información de un campo de la entidad es válida.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a verificar su validez.</param>
    ''' <remarks>El resultado de esta función se basa en los resulados de la función Validar(Integer) para determinar la validez del campo.</remarks>
    Function EsValido(ByVal Campo As TEnumeradorCampos) As Boolean

    ''' <summary>
    ''' Determina si otra entidad del mismo tipo es equivalente a esta entidad.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será evaluada para determinar equivalencia.</param>
    ''' <remarks>Esta función determina si dos entidades del mismo tipo son equivalentes, es decir, que la información de los campos significativos contengan la misma información.</remarks>
    Function Equivalente(ByVal Entidad As TTipoEntidad) As Boolean

    ''' <summary>
    ''' Crea un clon de la entidad.
    ''' </summary>
    ''' <remarks>Esta función crea un nuevo objeto identico.</remarks>
    Function Clonar() As TTipoEntidad

End Interface
