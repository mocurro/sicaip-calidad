﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Reclamo", "Reclamos", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Client.Nombre", 1, "Cliente", Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Reclamos : {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Model.np_gkn", 2, "Modelo")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Folio", 3, "Folio")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Component.componente", 4, "Componente")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Defect.nombre", 5, "Defecto")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "fecha_registro", 6, "Fecha de registro")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "User.Nombre", 7, "Usuario reporta")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "cantidad_encontrada", 8, "Cantidad encontrada")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "cantidad_rechazada", 9, "Cantidad rechazada")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 10, "Estatus")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "detalle", 11, "Detalle")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "comentarios", 12, "Comentarios")>
Public Class Reclamo
    Inherits Entidad(Of Reclamo, ReclamoCampos)

    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As Reclamo)
        With EntidadNueva
            .cve_reclamo =cve_reclamo 
            .cve_cliente_modelo =cve_cliente_modelo 
            .cve_defecto =cve_defecto 
            .fecha_registro =fecha_registro 
            .fecha_atiende =fecha_atiende 
            .cve_usuario_reporta =cve_usuario_reporta 
            .estampada =estampada 
            .cantidad_encontrada =cantidad_encontrada 
            .cantidad_rechazada =cantidad_rechazada 
            .cve_usuario_unidad_negocio_atiende =cve_usuario_unidad_negocio_atiende 
            ._cve_linea =cve_linea 
            .detalle =detalle 
            .comentarios =comentarios 
            .tipo_reclamo =tipo_reclamo 
            .estatus =estatus 
            .aceptada =aceptada 
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
          .cve_reclamo =cve_reclamo 
            .cve_cliente_modelo =cve_cliente_modelo 
            .cve_defecto =cve_defecto 
            .fecha_registro =fecha_registro 
            .fecha_atiende =fecha_atiende 
            .cve_usuario_reporta =cve_usuario_reporta 
            .estampada =estampada 
            .cantidad_encontrada =cantidad_encontrada 
            .cantidad_rechazada =cantidad_rechazada 
            .cve_usuario_unidad_negocio_atiende =cve_usuario_unidad_negocio_atiende 
            ._cve_linea =cve_linea 
            .detalle =detalle 
            .comentarios =comentarios 
            .tipo_reclamo =tipo_reclamo 
            .estatus =estatus 
            .aceptada =aceptada 
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
           cve_reclamo =.cve_reclamo 
            cve_cliente_modelo =.cve_cliente_modelo 
            cve_defecto =.cve_defecto 
            fecha_registro =.fecha_registro 
            fecha_atiende =.fecha_atiende 
            cve_usuario_reporta =.cve_usuario_reporta 
            estampada =.estampada 
            cantidad_encontrada =.cantidad_encontrada 
            cantidad_rechazada =.cantidad_rechazada 
            cve_usuario_unidad_negocio_atiende =.cve_usuario_unidad_negocio_atiende 
            cve_linea =.cve_linea 
            detalle =.detalle 
            comentarios =.comentarios 
            tipo_reclamo =.tipo_reclamo 
            estatus =.estatus 
            aceptada =.aceptada 
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As Reclamo
        Return New Reclamo
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As Reclamo) As Boolean
        Return Equals(Entidad.cve_reclamo , cve_reclamo ) AndAlso
             Equals(Entidad._cve_cliente_modelo  , _cve_cliente_modelo)
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return cve_reclamo   
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As ReclamoCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case ReclamoCampos .cve_reclamo     
                If Equals(cve_reclamo, 0) Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If
           
        End Select
        Return Resultado
    End Function
End Class
