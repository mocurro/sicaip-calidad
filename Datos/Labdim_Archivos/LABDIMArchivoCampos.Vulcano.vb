﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla LABDIMArchivo
''' </summary>
Public Enum LABDIMArchivoCampos As Integer

    cve_labdim_archivos
    cve_solicitud
    Tipo
    Archivo
    Nombre

End Enum

