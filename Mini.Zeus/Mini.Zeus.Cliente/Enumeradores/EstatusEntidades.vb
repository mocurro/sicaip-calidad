﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los posible estados que puede tener una entidad.
''' </summary>
''' <remarks>La lista contiene los diferentes estatus en los que se puede encontrar una entidad. Los valores son excluyentes.</remarks>
Public Enum EstatusEntidades As Integer

    ''' <summary>
    ''' No se conoce el estatus de la entidad.
    ''' </summary>
    Desconocido = 0

    ''' <summary>
    ''' La entidad es nueva; los datos no existen aun en la base de datos.
    ''' </summary>
    Nuevo = 1

    ''' <summary>
    ''' La entidad ya existe en la base de datos.
    ''' </summary>
    Existente = 2

    ''' <summary>
    ''' La entidad existe en la base de datos pero está marcada como borrada.
    ''' </summary>
    Borrado = 3

    ''' <summary>
    ''' La entidad ya no existe en la base de datos; fue destruida.
    ''' </summary>
    Destruido = 4

    ''' <summary>
    ''' La entidad es nueva o existente pero ha sido marcada para ser borrada.
    ''' </summary>
    PendienteBorrar = 5

    '''' <summary>
    '''' La entidad está borrada pero ha sido marcada para ser recuperada.
    '''' </summary>
    'PendienteRecuperar = 6

    ''' <summary>
    ''' La entidad es nueva o existente pero ha sido marcada para ser destruida.
    ''' </summary>
    PendienteDestruir = 7

End Enum