﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Usuario - Sub Área", "Usuarios - Sub Áreas", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Are.descripcion", 1, "Área", Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Usuarios - Areas : {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "user.Nombre", 2, "Nombre usuario")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 3, "Estatus")>
Public Class Area_Usuario
    Inherits Entidad(Of Area_Usuario, Area_UsuarioCampos)

    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As Area_Usuario)
        With EntidadNueva
            .Cve_area_usuario =Cve_area_usuario 
            .cve_usuario =cve_usuario
            .Cve_Sub_Area = Cve_Sub_Area
            .Estatus =Estatus 
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
           .Cve_area_usuario =Cve_area_usuario 
            .cve_usuario =cve_usuario
            .Cve_Sub_Area = Cve_Sub_Area
            .Estatus =Estatus 
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            Cve_area_usuario =.Cve_area_usuario 
            cve_usuario =.cve_usuario
            Cve_Sub_Area = .Cve_Sub_Area
            Estatus =.Estatus 
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As Area_Usuario
        Return New Area_Usuario
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As Area_Usuario) As Boolean
        'Return false
        Return Equals(Entidad.cve_usuario, cve_usuario) AndAlso
             Equals(Entidad.Cve_Sub_Area, Cve_Sub_Area) AndAlso
             Equals(Entidad.Estatus, Estatus)
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return estatus 
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As Area_UsuarioCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case Area_UsuarioCampos .cve_usuario       
                If cve_usuario =0 Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If 
           Case Area_UsuarioCampos .Cve_Area
                If Cve_Sub_Area = 0 Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If
        End Select
        Return Resultado
    End Function
End Class
