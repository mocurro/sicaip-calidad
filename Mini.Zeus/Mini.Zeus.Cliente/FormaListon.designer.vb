﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormaListon
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Liston = New DevExpress.XtraBars.Ribbon.RibbonControl
        Me.MenuAplicacion = New DevExpress.XtraBars.Ribbon.ApplicationMenu(Me.components)
        Me.BarraEstado = New DevExpress.XtraBars.Ribbon.RibbonStatusBar
        Me.PanelCliente = New DevExpress.XtraEditors.PanelControl
        CType(Me.Liston, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MenuAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Liston
        '
        Me.Liston.ApplicationButtonDropDownControl = Me.MenuAplicacion
        Me.Liston.ApplicationButtonText = Nothing
        '
        '
        '
        Me.Liston.ExpandCollapseItem.Id = 0
        Me.Liston.ExpandCollapseItem.Name = ""
        Me.Liston.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.Liston.ExpandCollapseItem})
        Me.Liston.Location = New System.Drawing.Point(0, 0)
        Me.Liston.MaxItemId = 1
        Me.Liston.Name = "Liston"
        Me.Liston.ShowToolbarCustomizeItem = False
        Me.Liston.Size = New System.Drawing.Size(632, 49)
        Me.Liston.StatusBar = Me.BarraEstado
        Me.Liston.Toolbar.ShowCustomizeItem = False
        '
        'MenuAplicacion
        '
        Me.MenuAplicacion.Name = "MenuAplicacion"
        Me.MenuAplicacion.Ribbon = Me.Liston
        '
        'BarraEstado
        '
        Me.BarraEstado.Location = New System.Drawing.Point(0, 448)
        Me.BarraEstado.Name = "BarraEstado"
        Me.BarraEstado.Ribbon = Me.Liston
        Me.BarraEstado.Size = New System.Drawing.Size(632, 31)
        '
        'PanelCliente
        '
        Me.PanelCliente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelCliente.Location = New System.Drawing.Point(0, 49)
        Me.PanelCliente.Name = "PanelCliente"
        Me.PanelCliente.Size = New System.Drawing.Size(632, 399)
        Me.PanelCliente.TabIndex = 2
        '
        'FormaListon
        '
        Me.AllowFormGlass = DevExpress.Utils.DefaultBoolean.[False]
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 479)
        Me.Controls.Add(Me.PanelCliente)
        Me.Controls.Add(Me.BarraEstado)
        Me.Controls.Add(Me.Liston)
        Me.Name = "FormaListon"
        Me.Ribbon = Me.Liston
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.StatusBar = Me.BarraEstado
        Me.Text = "FormaListon"
        CType(Me.Liston, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MenuAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents Liston As DevExpress.XtraBars.Ribbon.RibbonControl
    Protected Friend WithEvents BarraEstado As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Protected Friend WithEvents PanelCliente As DevExpress.XtraEditors.PanelControl
    Protected Friend WithEvents MenuAplicacion As DevExpress.XtraBars.Ribbon.ApplicationMenu



End Class
