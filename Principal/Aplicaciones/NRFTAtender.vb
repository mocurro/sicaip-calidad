﻿Imports System.IO
Imports System.Text
Imports Datos

Public Class NRFTAtender
    Private MiIdReclamo As Integer

    Private MiReclamo As Datos.Reclamo

    Private MiReclamoArchivo As Datos.Reclamo_Archivo 

    Private MiCadenaConexión As String

    Private MiListaUsuarios As New List(Of Datos.SEGURIDAD_USUARIO)

    Private MiListaLineas As New List(Of Datos.linea)

    Private MiLinea As Datos.linea

    Private MiListaEquipo As New List(Of Datos.equipo)

    Private MiListaEquiposSelccionados As New List(Of Datos.equipo)

    Private MiEquipoSeleccionado As Datos.equipo

    Private EstaAbriendoArchivo As Boolean =False

    Private RutaArchivo As String

    Private MiArea As Datos.detalle

    Private SeleccionoArea As Boolean = True

    Private seleccionoLinea As Boolean = False

    Public Property IdReclamo As Integer
        Get
            Return MiIdReclamo
        End Get
        Set(value As Integer)
            MiIdReclamo = value
        End Set
    End Property

    Private Sub NRFTAtender_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            MiCadenaConexión = Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer
            If Datos.SesiónLocal.Usuario IsNot Nothing Then
                EtiquetaUsuario.Text = Datos.SesiónLocal.Usuario.Nombre
                'EtiquetaRol .Text =Datos.SesiónLocal .Usuario.TipoUsuario .Nombre_Tipo_Usuario 
            End If
            Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                Dim MiListaIncidenciasReportada2s = From reclamo In MiModelo.Reclamos
                                                    Where reclamo.cve_reclamo = IdReclamo
                MiReclamo = MiListaIncidenciasReportada2s.SingleOrDefault
            End Using
            EtiquetaFolio.Text = CType(MiReclamo.cve_reclamo.ToString.PadLeft(5, CChar("0")), String)
            Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                Dim Query = From team In MiModelo.equipos
                            Order By team.cve_equipo Ascending
                            Select team
                MiListaEquipo = Query.ToList()
            End Using

            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("Excepto", MiReclamo.Cve_Sub_Area)

            Dim MiListaAreas = Fabricas.SubArea.ObtenerExistentesParaSerializar(Filtro)
            DatosArea.DataSource = MiListaAreas

            Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                MiReclamoArchivo = (From ra In MiModelo.Reclamo_Archivos
                                    Where ra.cve_reclamo = MiReclamo.cve_reclamo
                                    Select ra).FirstOrDefault
            End Using



            DatosEquipo.DataSource = MiListaEquipo
            MiListaLineas = Datos.Fabricas.Lineas.ObtenerExistentes()
            DatosLineas.DataSource = MiListaLineas

            Dim noParte As List(Of Datos.Modelo)
            Filtro.Clear()
            Filtro.Add("cve_cliente_modelo", MiReclamo.cve_cliente_modelo)
            noParte = Datos.Fabricas.Modelo.ObtenerExistentes(Filtro)
            txtColada.Text = MiReclamo.estampada
            txtCantidadEncontrada.Text = MiReclamo.cantidad_encontrada
            txtCantidadRechazada.Text = MiReclamo.cantidad_rechazada
            txtDetalles.Text = MiReclamo.detalle
            Dim user = Datos.Fabricas.Seguridad_Usuarios.ObtenerUno(MiReclamo.cve_usuario_reporta)


            If user IsNot Nothing Then
                txtReporto.Text = user.Nombre
            End If

            If noParte IsNot Nothing AndAlso noParte.Count > 0 Then
                txtNoParte.Text = noParte(0).np_gkn
                Dim compo = Datos.Fabricas.Componente.ObtenerUno(noParte(0).cve_componente)
                txtComponente.Text = compo.componente
                txtModelo.Text = noParte(0).descripcion
                Filtro.Clear()
                Filtro.Add("cve_cliente_modelo", MiReclamo.cve_cliente_modelo)
                Dim Cliente = Datos.Fabricas.Cliente.ObtenerExistentes(Filtro)
                txtCliente.Text = Cliente(0).Nombre

                Dim MiUnidadNegocio As Datos.Unidad_Negocio
                If compo IsNot Nothing Then
                    txtComponente.Text = compo.componente
                    Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                        MiUnidadNegocio = (From unidad In MiModelo.Unidad_Negocios
                                           Join componenteunidad In MiModelo.Componente_Unidad_Negocios
                                           On unidad.cve_unidad_negocio Equals componenteunidad.cve_unidad_negocio
                                           Where componenteunidad.cve_componente = compo.cve_componente
                                           Select unidad).FirstOrDefault
                    End Using
                    If MiUnidadNegocio IsNot Nothing Then
                        txtUnidadNegocio.Text = MiUnidadNegocio.nombre
                    Else
                        txtUnidadNegocio.Text = "No hay unidad de negocio asignada"
                    End If
                End If

                Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                    Dim MiListaUsuarios2 = From usuario In MiModelo.SEGURIDAD_USUARIOs
                                           Join usuariounidad In MiModelo.Usuario_Unidad_Negocios On usuario.CVE_Usuario Equals usuariounidad.cve_usuario
                                           Join componente In MiModelo.Componente_Unidad_Negocios On componente.cve_componente_unidad_negocio Equals usuariounidad.cve_componente_unidad_negocio
                                           Where componente.cve_componente = compo.cve_componente
                                           Select usuario
                    MiListaUsuarios = MiListaUsuarios2.ToList
                End Using
                If MiListaUsuarios.Count > 0 Then
                    Dim listadoAtiende As String = String.Empty
                    For Each user In MiListaUsuarios
                        listadoAtiende += user.Nombre + ","
                    Next
                    txtAtiende.Text = listadoAtiende
                Else
                    txtAtiende.Text = "No se han asignado personas que atienden"
                End If


            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al cargar la información", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try



    End Sub

    Private Sub btnFeedBack_Click(sender As Object, e As EventArgs) Handles btnFeedBack.Click

        Using Contexto = New Datos.ModeloDatosDataContext(MiCadenaConexión)
            Dim Objeto = Contexto.Reclamos.Single(Function(p As Datos.Reclamo) p.cve_reclamo = MiReclamo.cve_reclamo)
            Objeto.cve_linea = MiLinea.cve_linea
            Objeto.comentarios = txtDescripción.Text.Trim
            Objeto.detalle = txtDetalles.Text.Trim
            Objeto.cantidad_encontrada = CInt(txtCantidadEncontrada.Text)
            Objeto.cantidad_rechazada = CInt(txtCantidadRechazada.Text)
            Objeto.aceptada = CByte(ComboAceptado.SelectedIndex)
            If MiArea IsNot Nothing Then
                Objeto.Cve_Sub_Area = MiArea.cve_detalle
            End If
            If Objeto.aceptada=1 Then
                Objeto.estatus = 3
            Else
                Objeto.estatus = 1
            End If
            
            Contexto.SubmitChanges
        End Using

        For Each element In MiListaEquiposSelccionados
            Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                Dim miReclamoEquipo As New Datos.Reclamos_Equipo
                miReclamoEquipo.cve_reclamo = MiReclamo.cve_reclamo
                miReclamoEquipo.cve_equipo = element.cve_equipo
                MiModelo.Reclamos_Equipos.InsertOnSubmit(miReclamoEquipo)
                MiModelo.SubmitChanges
            End Using
        Next

        MessageBox.Show("Reclamo atendido correctamente", "Reclamo atendido", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub

    Private Sub ComboLinea_EditValueChanged(sender As Object, e As EventArgs) Handles ComboLinea.EditValueChanged
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiLinea = CType(combo.GetSelectedDataRow, Datos.Linea)
        seleccionoLinea = True
        ValidarBoton()

    End Sub

    Private Sub ValidarBoton()
        If ComboAceptado.SelectedIndex = 1 Then
            SeleccionoArea = CBool(IIf(ComboArea.ItemIndex > 0, True, False))
        End If
        If SeleccionoArea And seleccionoLinea Then
            btnFeedBack.Enabled = True
        Else
            btnFeedBack.Enabled = False
        End If
    End Sub

    Private Sub btnEquipoTlatoani_Click(sender As Object, e As EventArgs) Handles btnEquipoTlatoani.Click
        If MiEquipoSeleccionado IsNot Nothing Then

            If Not MiListaEquiposSelccionados.Contains(MiEquipoSeleccionado) Then
                MiListaEquiposSelccionados.Add(MiEquipoSeleccionado)

                gridEquipos.DataSource = Nothing
                gridEquipos.DataSource = MiListaEquiposSelccionados
                With gridEquipos
                    .Columns(0).Visible = False
                    .Columns(1).Visible = False
                    .Columns(2).Visible = False
                    .Columns(4).HeaderText = "Tlatoani"
                    .Columns(3).Visible = False
                    .Columns(5).Visible = False
                    .Columns(6).Visible = False
                    .Columns(7).Visible = False

                    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                    .MultiSelect = True
                    .Dock = DockStyle.Fill
                End With
                gridEquipos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Else
                MessageBox.Show("El equipo Tlatoani seleccionado ya ha sido agregado anteriormente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("No se ha seleccionado un equipo tlatoani")
        End If
        'MiEquipoSeleccionado = Nothing
    End Sub

    Private Sub ComboEquipo_EditValueChanged(sender As Object, e As EventArgs) Handles ComboEquipo.EditValueChanged
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiEquipoSeleccionado = CType(combo.GetSelectedDataRow, Datos.equipo)
    End Sub

    Private Sub LinkArchivoAdjunto_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkArchivoAdjunto.LinkClicked
        If Not EstaAbriendoArchivo Then
            EstaAbriendoArchivo=True
            If MiReclamoArchivo IsNot Nothing Then
                 Dim direcotorioEtiquetas As String = "C:\\GKN\\"
                RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas,MiReclamoArchivo.Nombre  )
                If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                    System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                End If
                If File.Exists (RutaArchivo)Then
                    File.Delete (RutaArchivo)
                End If
                File.WriteAllBytes(RutaArchivo, MiReclamoArchivo.archivo.ToArray  )
                Dim proceso As New System.Diagnostics.Process
                 With proceso
                    .StartInfo.FileName = RutaArchivo
                    .Start()
                End With

            Else
                MessageBox .Show ("No hay archivos cargados al reclamo.","No hay archivos cargado.",MessageBoxButtons.OK ,MessageBoxIcon.Information )
            End If
        End If
        EstaAbriendoArchivo=false
    End Sub

    Private Sub ComboArea_EditValueChanged(sender As Object, e As EventArgs) Handles ComboArea.EditValueChanged
        ComboArea.Enabled = False
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiArea = CType(combo.GetSelectedDataRow, Datos.detalle)
        SeleccionoArea = True
        If MiArea IsNot Nothing Then
            ComboArea.Enabled = True
            SeleccionoArea = True
        Else
            SeleccionoArea = False
        End If
        ValidarBoton()
    End Sub

    Private Sub ComboAceptado_EditValueChanged(sender As Object, e As EventArgs) Handles ComboAceptado.EditValueChanged
        If ComboAceptado.SelectedIndex = 1 Then
            ItemÁrea.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            SeleccionoArea = False
            'Dim combo = CType(ComboArea, DevExpress.XtraEditors.LookUpEdit)
            MiArea = CType(ComboArea.GetSelectedDataRow, Datos.detalle)
        Else
            ItemÁrea.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
            MiArea = Nothing

        End If
        ValidarBoton()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If MiEquipoSelccionadoParaBorrar IsNot Nothing Then
            MiListaEquiposSelccionados.Remove(MiEquipoSelccionadoParaBorrar)

            gridEquipos.DataSource = Nothing
            gridEquipos.DataSource = MiListaEquiposSelccionados
            With gridEquipos
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(4).HeaderText = "Tlatoani"
                .Columns(3).Visible = False
                .Columns(5).Visible = False
                .Columns(6).Visible = False
                .Columns(7).Visible = False

                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .MultiSelect = True
                .Dock = DockStyle.Fill
            End With
            gridEquipos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        End If
        If MiListaEquiposSelccionados.Count = 0 Then
            btnEliminar.Enabled = False
        End If
    End Sub

    Private MiEquipoSelccionadoParaBorrar As equipo
    Private Sub gridEquipos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles gridEquipos.CellClick
        If e.RowIndex >= 0 Then
            btnEliminar.Enabled = True
            Dim valor = gridEquipos.Rows(e.RowIndex).DataBoundItem
            If valor IsNot Nothing Then
                MiEquipoSelccionadoParaBorrar = CType(valor, equipo)
            Else
                MiEquipoSelccionadoParaBorrar = Nothing
            End If
        End If
    End Sub

    Private Sub gridEquipos_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles gridEquipos.DataError

    End Sub
End Class