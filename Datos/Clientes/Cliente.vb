﻿Public Class Cliente
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case ClienteEstatus.Activo 
                    Return "Activo"
                Case ClienteEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property
End Class
