﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NRFTAtender
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NRFTAtender))
        Me.Raiz = New DevExpress.XtraLayout.LayoutControl()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboArea = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosArea = New System.Windows.Forms.BindingSource(Me.components)
        Me.EtiquetaFolio = New System.Windows.Forms.Label()
        Me.LinkArchivoAdjunto = New System.Windows.Forms.LinkLabel()
        Me.ComboEquipo = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosEquipo = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboLinea = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosLineas = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnFeedBack = New DevExpress.XtraEditors.SimpleButton()
        Me.gridEquipos = New System.Windows.Forms.DataGridView()
        Me.btnEquipoTlatoani = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.ComboAceptado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtReporto = New DevExpress.XtraEditors.TextEdit()
        Me.txtCantidadRechazada = New DevExpress.XtraEditors.TextEdit()
        Me.txtCantidadEncontrada = New DevExpress.XtraEditors.TextEdit()
        Me.txtNoParte = New DevExpress.XtraEditors.TextEdit()
        Me.txtDetalles = New DevExpress.XtraEditors.MemoEdit()
        Me.txtDescripción = New DevExpress.XtraEditors.TextEdit()
        Me.txtColada = New DevExpress.XtraEditors.TextEdit()
        Me.txtUnidadNegocio = New DevExpress.XtraEditors.TextEdit()
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit()
        Me.txtModelo = New DevExpress.XtraEditors.TextEdit()
        Me.txtComponente = New DevExpress.XtraEditors.TextEdit()
        Me.EtiquetaUsuario = New DevExpress.XtraEditors.LabelControl()
        Me.txtAtiende = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ItemLinea = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemEquipo = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemArchivoAdjunto = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemÁrea = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Raiz.SuspendLayout()
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboEquipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosEquipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboLinea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosLineas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridEquipos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboAceptado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReporto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidadRechazada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidadEncontrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoParte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDetalles.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripción.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComponente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAtiende.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemLinea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemEquipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemArchivoAdjunto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemÁrea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Raiz
        '
        Me.Raiz.Controls.Add(Me.btnEliminar)
        Me.Raiz.Controls.Add(Me.ComboArea)
        Me.Raiz.Controls.Add(Me.EtiquetaFolio)
        Me.Raiz.Controls.Add(Me.LinkArchivoAdjunto)
        Me.Raiz.Controls.Add(Me.ComboEquipo)
        Me.Raiz.Controls.Add(Me.ComboLinea)
        Me.Raiz.Controls.Add(Me.btnFeedBack)
        Me.Raiz.Controls.Add(Me.gridEquipos)
        Me.Raiz.Controls.Add(Me.btnEquipoTlatoani)
        Me.Raiz.Controls.Add(Me.MemoEdit2)
        Me.Raiz.Controls.Add(Me.ComboAceptado)
        Me.Raiz.Controls.Add(Me.txtReporto)
        Me.Raiz.Controls.Add(Me.txtCantidadRechazada)
        Me.Raiz.Controls.Add(Me.txtCantidadEncontrada)
        Me.Raiz.Controls.Add(Me.txtNoParte)
        Me.Raiz.Controls.Add(Me.txtDetalles)
        Me.Raiz.Controls.Add(Me.txtDescripción)
        Me.Raiz.Controls.Add(Me.txtColada)
        Me.Raiz.Controls.Add(Me.txtUnidadNegocio)
        Me.Raiz.Controls.Add(Me.txtCliente)
        Me.Raiz.Controls.Add(Me.txtModelo)
        Me.Raiz.Controls.Add(Me.txtComponente)
        Me.Raiz.Controls.Add(Me.EtiquetaUsuario)
        Me.Raiz.Controls.Add(Me.txtAtiende)
        Me.Raiz.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Raiz.Location = New System.Drawing.Point(0, 0)
        Me.Raiz.Name = "Raiz"
        Me.Raiz.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1104, 212, 450, 400)
        Me.Raiz.Root = Me.LayoutControlGroup1
        Me.Raiz.Size = New System.Drawing.Size(754, 633)
        Me.Raiz.TabIndex = 0
        Me.Raiz.Text = "LayoutControl1"
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Location = New System.Drawing.Point(665, 462)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(77, 22)
        Me.btnEliminar.StyleController = Me.Raiz
        Me.btnEliminar.TabIndex = 39
        Me.btnEliminar.Text = "Eliminar"
        '
        'ComboArea
        '
        Me.ComboArea.Location = New System.Drawing.Point(12, 478)
        Me.ComboArea.Name = "ComboArea"
        Me.ComboArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboArea.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Sub área")})
        Me.ComboArea.Properties.DataSource = Me.DatosArea
        Me.ComboArea.Properties.DisplayMember = "descripcion"
        Me.ComboArea.Properties.NullText = "Seleccione área"
        Me.ComboArea.Properties.ValueMember = "cve_detalle"
        Me.ComboArea.Size = New System.Drawing.Size(373, 20)
        Me.ComboArea.StyleController = Me.Raiz
        Me.ComboArea.TabIndex = 38
        '
        'EtiquetaFolio
        '
        Me.EtiquetaFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaFolio.Location = New System.Drawing.Point(399, 12)
        Me.EtiquetaFolio.Name = "EtiquetaFolio"
        Me.EtiquetaFolio.Size = New System.Drawing.Size(343, 20)
        Me.EtiquetaFolio.TabIndex = 37
        Me.EtiquetaFolio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LinkArchivoAdjunto
        '
        Me.LinkArchivoAdjunto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkArchivoAdjunto.Location = New System.Drawing.Point(12, 398)
        Me.LinkArchivoAdjunto.Name = "LinkArchivoAdjunto"
        Me.LinkArchivoAdjunto.Size = New System.Drawing.Size(730, 20)
        Me.LinkArchivoAdjunto.TabIndex = 36
        Me.LinkArchivoAdjunto.TabStop = True
        Me.LinkArchivoAdjunto.Text = "Archivo Adjunto"
        Me.LinkArchivoAdjunto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ComboEquipo
        '
        Me.ComboEquipo.Location = New System.Drawing.Point(389, 438)
        Me.ComboEquipo.Name = "ComboEquipo"
        Me.ComboEquipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboEquipo.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("equipo", "Equipo")})
        Me.ComboEquipo.Properties.DataSource = Me.DatosEquipo
        Me.ComboEquipo.Properties.DisplayMember = "equipo"
        Me.ComboEquipo.Properties.NullText = "Seleccione equipo"
        Me.ComboEquipo.Properties.ValueMember = "cve_equipo"
        Me.ComboEquipo.Size = New System.Drawing.Size(353, 20)
        Me.ComboEquipo.StyleController = Me.Raiz
        Me.ComboEquipo.TabIndex = 35
        '
        'ComboLinea
        '
        Me.ComboLinea.Location = New System.Drawing.Point(12, 438)
        Me.ComboLinea.Name = "ComboLinea"
        Me.ComboLinea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboLinea.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("linea", "Línea")})
        Me.ComboLinea.Properties.DataSource = Me.DatosLineas
        Me.ComboLinea.Properties.DisplayMember = "linea"
        Me.ComboLinea.Properties.NullText = "Seleccione línea"
        Me.ComboLinea.Properties.ValueMember = "cve_linea"
        Me.ComboLinea.Size = New System.Drawing.Size(231, 20)
        Me.ComboLinea.StyleController = Me.Raiz
        Me.ComboLinea.TabIndex = 34
        '
        'btnFeedBack
        '
        Me.btnFeedBack.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFeedBack.Appearance.Options.UseFont = True
        Me.btnFeedBack.Enabled = False
        Me.btnFeedBack.Location = New System.Drawing.Point(200, 581)
        Me.btnFeedBack.Name = "btnFeedBack"
        Me.btnFeedBack.Size = New System.Drawing.Size(185, 40)
        Me.btnFeedBack.StyleController = Me.Raiz
        Me.btnFeedBack.TabIndex = 33
        Me.btnFeedBack.Text = "FeedBack"
        '
        'gridEquipos
        '
        Me.gridEquipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridEquipos.Location = New System.Drawing.Point(389, 488)
        Me.gridEquipos.Name = "gridEquipos"
        Me.gridEquipos.ReadOnly = True
        Me.gridEquipos.Size = New System.Drawing.Size(353, 133)
        Me.gridEquipos.TabIndex = 32
        '
        'btnEquipoTlatoani
        '
        Me.btnEquipoTlatoani.Location = New System.Drawing.Point(552, 462)
        Me.btnEquipoTlatoani.Name = "btnEquipoTlatoani"
        Me.btnEquipoTlatoani.Size = New System.Drawing.Size(91, 22)
        Me.btnEquipoTlatoani.StyleController = Me.Raiz
        Me.btnEquipoTlatoani.TabIndex = 31
        Me.btnEquipoTlatoani.Text = "Agregar"
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(12, 518)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(373, 59)
        Me.MemoEdit2.StyleController = Me.Raiz
        Me.MemoEdit2.TabIndex = 30
        '
        'ComboAceptado
        '
        Me.ComboAceptado.EditValue = "SI"
        Me.ComboAceptado.Location = New System.Drawing.Point(247, 438)
        Me.ComboAceptado.Name = "ComboAceptado"
        Me.ComboAceptado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboAceptado.Properties.Items.AddRange(New Object() {"SI", "NO"})
        Me.ComboAceptado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboAceptado.Size = New System.Drawing.Size(138, 20)
        Me.ComboAceptado.StyleController = Me.Raiz
        Me.ComboAceptado.TabIndex = 28
        '
        'txtReporto
        '
        Me.txtReporto.Enabled = False
        Me.txtReporto.Location = New System.Drawing.Point(153, 374)
        Me.txtReporto.Name = "txtReporto"
        Me.txtReporto.Size = New System.Drawing.Size(589, 20)
        Me.txtReporto.StyleController = Me.Raiz
        Me.txtReporto.TabIndex = 26
        '
        'txtCantidadRechazada
        '
        Me.txtCantidadRechazada.Location = New System.Drawing.Point(153, 350)
        Me.txtCantidadRechazada.Name = "txtCantidadRechazada"
        Me.txtCantidadRechazada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCantidadRechazada.Properties.Mask.EditMask = "n0"
        Me.txtCantidadRechazada.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCantidadRechazada.Properties.NullText = "0"
        Me.txtCantidadRechazada.Size = New System.Drawing.Size(589, 20)
        Me.txtCantidadRechazada.StyleController = Me.Raiz
        Me.txtCantidadRechazada.TabIndex = 24
        '
        'txtCantidadEncontrada
        '
        Me.txtCantidadEncontrada.Location = New System.Drawing.Point(153, 326)
        Me.txtCantidadEncontrada.Name = "txtCantidadEncontrada"
        Me.txtCantidadEncontrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCantidadEncontrada.Properties.Mask.EditMask = "d"
        Me.txtCantidadEncontrada.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCantidadEncontrada.Properties.NullText = "0"
        Me.txtCantidadEncontrada.Size = New System.Drawing.Size(589, 20)
        Me.txtCantidadEncontrada.StyleController = Me.Raiz
        Me.txtCantidadEncontrada.TabIndex = 23
        '
        'txtNoParte
        '
        Me.txtNoParte.Enabled = False
        Me.txtNoParte.Location = New System.Drawing.Point(153, 47)
        Me.txtNoParte.Name = "txtNoParte"
        Me.txtNoParte.Size = New System.Drawing.Size(589, 20)
        Me.txtNoParte.StyleController = Me.Raiz
        Me.txtNoParte.TabIndex = 22
        '
        'txtDetalles
        '
        Me.txtDetalles.Location = New System.Drawing.Point(153, 283)
        Me.txtDetalles.Name = "txtDetalles"
        Me.txtDetalles.Size = New System.Drawing.Size(589, 39)
        Me.txtDetalles.StyleController = Me.Raiz
        Me.txtDetalles.TabIndex = 15
        '
        'txtDescripción
        '
        Me.txtDescripción.Location = New System.Drawing.Point(153, 259)
        Me.txtDescripción.Name = "txtDescripción"
        Me.txtDescripción.Size = New System.Drawing.Size(589, 20)
        Me.txtDescripción.StyleController = Me.Raiz
        Me.txtDescripción.TabIndex = 13
        '
        'txtColada
        '
        Me.txtColada.Enabled = False
        Me.txtColada.Location = New System.Drawing.Point(153, 235)
        Me.txtColada.Name = "txtColada"
        Me.txtColada.Size = New System.Drawing.Size(589, 20)
        Me.txtColada.StyleController = Me.Raiz
        Me.txtColada.TabIndex = 11
        '
        'txtUnidadNegocio
        '
        Me.txtUnidadNegocio.Enabled = False
        Me.txtUnidadNegocio.Location = New System.Drawing.Point(153, 161)
        Me.txtUnidadNegocio.Name = "txtUnidadNegocio"
        Me.txtUnidadNegocio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnidadNegocio.Properties.Appearance.Options.UseFont = True
        Me.txtUnidadNegocio.Size = New System.Drawing.Size(589, 26)
        Me.txtUnidadNegocio.StyleController = Me.Raiz
        Me.txtUnidadNegocio.TabIndex = 9
        '
        'txtCliente
        '
        Me.txtCliente.Enabled = False
        Me.txtCliente.Location = New System.Drawing.Point(153, 131)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.Properties.Appearance.Options.UseFont = True
        Me.txtCliente.Size = New System.Drawing.Size(589, 26)
        Me.txtCliente.StyleController = Me.Raiz
        Me.txtCliente.TabIndex = 8
        '
        'txtModelo
        '
        Me.txtModelo.Enabled = False
        Me.txtModelo.Location = New System.Drawing.Point(153, 101)
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModelo.Properties.Appearance.Options.UseFont = True
        Me.txtModelo.Size = New System.Drawing.Size(589, 26)
        Me.txtModelo.StyleController = Me.Raiz
        Me.txtModelo.TabIndex = 7
        '
        'txtComponente
        '
        Me.txtComponente.Enabled = False
        Me.txtComponente.Location = New System.Drawing.Point(153, 71)
        Me.txtComponente.Name = "txtComponente"
        Me.txtComponente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComponente.Properties.Appearance.Options.UseFont = True
        Me.txtComponente.Size = New System.Drawing.Size(589, 26)
        Me.txtComponente.StyleController = Me.Raiz
        Me.txtComponente.TabIndex = 6
        '
        'EtiquetaUsuario
        '
        Me.EtiquetaUsuario.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaUsuario.Appearance.Options.UseFont = True
        Me.EtiquetaUsuario.Location = New System.Drawing.Point(12, 12)
        Me.EtiquetaUsuario.Name = "EtiquetaUsuario"
        Me.EtiquetaUsuario.Size = New System.Drawing.Size(352, 19)
        Me.EtiquetaUsuario.StyleController = Me.Raiz
        Me.EtiquetaUsuario.TabIndex = 4
        '
        'txtAtiende
        '
        Me.txtAtiende.Enabled = False
        Me.txtAtiende.Location = New System.Drawing.Point(153, 191)
        Me.txtAtiende.Name = "txtAtiende"
        Me.txtAtiende.Size = New System.Drawing.Size(589, 40)
        Me.txtAtiende.StyleController = Me.Raiz
        Me.txtAtiende.TabIndex = 10
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.LayoutControlItem1, Me.EmptySpaceItem3, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.LayoutControlItem12, Me.LayoutControlItem18, Me.LayoutControlItem2, Me.LayoutControlItem9, Me.LayoutControlItem16, Me.LayoutControlItem13, Me.LayoutControlItem17, Me.LayoutControlItem19, Me.EmptySpaceItem1, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.EmptySpaceItem4, Me.ItemLinea, Me.ItemEquipo, Me.ItemArchivoAdjunto, Me.LayoutControlItem11, Me.ItemÁrea, Me.LayoutControlItem14, Me.EmptySpaceItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(754, 633)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(356, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(31, 24)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.EtiquetaUsuario
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(356, 24)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 24)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(734, 11)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtComponente
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 59)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem3.Text = "Componente"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtModelo
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 89)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem4.Text = "Modelo"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtCliente
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 119)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem5.Text = "Cliente"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtUnidadNegocio
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 149)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem6.Text = "Unidad de negocio"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtAtiende
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 179)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(734, 44)
        Me.LayoutControlItem7.Text = "Atiende"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txtColada
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 223)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem8.Text = "Estampado /  Cargo / Colada"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtDescripción
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 247)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem10.Text = "Descripción"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.txtDetalles
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 271)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(734, 43)
        Me.LayoutControlItem12.Text = "Detalles"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.txtNoParte
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 35)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem18.Text = "Número de parte"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtCantidadEncontrada
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 314)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem2.Text = "Cantidad encontrada"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.txtCantidadRechazada
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 338)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem9.Text = "Cantidad rechazada"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.txtReporto
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 362)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem16.Text = "Reportó"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.ComboAceptado
        Me.LayoutControlItem13.Location = New System.Drawing.Point(235, 410)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(142, 40)
        Me.LayoutControlItem13.Text = "Aceptado"
        Me.LayoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.MemoEdit2
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 490)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(377, 79)
        Me.LayoutControlItem17.Text = "Comentarios"
        Me.LayoutControlItem17.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.btnEquipoTlatoani
        Me.LayoutControlItem19.Location = New System.Drawing.Point(540, 450)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(95, 26)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(377, 450)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(163, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.gridEquipos
        Me.LayoutControlItem20.Location = New System.Drawing.Point(377, 476)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(357, 137)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.btnFeedBack
        Me.LayoutControlItem21.Location = New System.Drawing.Point(188, 569)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(189, 44)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 569)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(188, 44)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'ItemLinea
        '
        Me.ItemLinea.Control = Me.ComboLinea
        Me.ItemLinea.Location = New System.Drawing.Point(0, 410)
        Me.ItemLinea.Name = "ItemLinea"
        Me.ItemLinea.Size = New System.Drawing.Size(235, 40)
        Me.ItemLinea.Text = "Línea"
        Me.ItemLinea.TextLocation = DevExpress.Utils.Locations.Top
        Me.ItemLinea.TextSize = New System.Drawing.Size(138, 13)
        '
        'ItemEquipo
        '
        Me.ItemEquipo.Control = Me.ComboEquipo
        Me.ItemEquipo.Location = New System.Drawing.Point(377, 410)
        Me.ItemEquipo.Name = "ItemEquipo"
        Me.ItemEquipo.Size = New System.Drawing.Size(357, 40)
        Me.ItemEquipo.Text = "Equipo tlatoani"
        Me.ItemEquipo.TextLocation = DevExpress.Utils.Locations.Top
        Me.ItemEquipo.TextSize = New System.Drawing.Size(138, 13)
        '
        'ItemArchivoAdjunto
        '
        Me.ItemArchivoAdjunto.Control = Me.LinkArchivoAdjunto
        Me.ItemArchivoAdjunto.Location = New System.Drawing.Point(0, 386)
        Me.ItemArchivoAdjunto.Name = "ItemArchivoAdjunto"
        Me.ItemArchivoAdjunto.Size = New System.Drawing.Size(734, 24)
        Me.ItemArchivoAdjunto.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemArchivoAdjunto.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.EtiquetaFolio
        Me.LayoutControlItem11.Location = New System.Drawing.Point(387, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(347, 24)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'ItemÁrea
        '
        Me.ItemÁrea.Control = Me.ComboArea
        Me.ItemÁrea.Location = New System.Drawing.Point(0, 450)
        Me.ItemÁrea.Name = "ItemÁrea"
        Me.ItemÁrea.Size = New System.Drawing.Size(377, 40)
        Me.ItemÁrea.Text = "Sub área a reasignar"
        Me.ItemÁrea.TextLocation = DevExpress.Utils.Locations.Top
        Me.ItemÁrea.TextSize = New System.Drawing.Size(138, 13)
        Me.ItemÁrea.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.btnEliminar
        Me.LayoutControlItem14.Location = New System.Drawing.Point(653, 450)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(81, 26)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(635, 450)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(18, 26)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'NRFTAtender
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 633)
        Me.Controls.Add(Me.Raiz)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NRFTAtender"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NRFTAtender"
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Raiz.ResumeLayout(False)
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboEquipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosEquipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboLinea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosLineas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridEquipos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboAceptado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReporto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidadRechazada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidadEncontrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoParte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDetalles.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripción.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAtiende.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemLinea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEquipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemArchivoAdjunto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemÁrea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Raiz As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtDetalles As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtDescripción As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtColada As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUnidadNegocio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtModelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtComponente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents EtiquetaUsuario As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtAtiende As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtNoParte As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnEquipoTlatoani As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ComboAceptado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtReporto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCantidadRechazada As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCantidadEncontrada As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents btnFeedBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gridEquipos As System.Windows.Forms.DataGridView
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboLinea As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ItemLinea As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DatosLineas As BindingSource
    Friend WithEvents ComboEquipo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents DatosEquipo As BindingSource
    Friend WithEvents ItemEquipo As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LinkArchivoAdjunto As LinkLabel
    Friend WithEvents ItemArchivoAdjunto As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaFolio As Label
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ComboArea As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ItemÁrea As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DatosArea As BindingSource
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
End Class
