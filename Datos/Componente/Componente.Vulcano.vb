﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Componente", "Componentes", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Cliente.Nombre", 1, "Cliente",Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Componenetes : {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "modelo.np_gkn", 2, "Modelo")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 3, "Estatus")>
Public Class componente
    Inherits Entidad(Of componente, componenteCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As componente)
        With EntidadNueva
            .cve_componente =cve_componente 
            .cve_cadena_valor =cve_cadena_valor 
            .componente =componente 
            .precio =precio 
            .Cve_Usuario_Creacion =Cve_Usuario_Creacion 
            .Fecha_Creacion =Fecha_Creacion 
            .Cve_Usuario_Modificacion =Cve_Usuario_Modificacion 
            .Estatus =Estatus 
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
            .cve_componente =cve_componente 
            .cve_cadena_valor =cve_cadena_valor 
            .componente =componente 
            .precio =precio 
            .Cve_Usuario_Creacion =Cve_Usuario_Creacion 
            .Fecha_Creacion =Fecha_Creacion 
            .Cve_Usuario_Modificacion =Cve_Usuario_Modificacion 
            .Estatus =Estatus 
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
             cve_componente =.cve_componente 
            cve_cadena_valor =.cve_cadena_valor 
            componente =.componente 
            precio =.precio 
           Cve_Usuario_Creacion =.Cve_Usuario_Creacion 
            Fecha_Creacion =.Fecha_Creacion 
            Cve_Usuario_Modificacion =.Cve_Usuario_Modificacion 
            Estatus =.Estatus 
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As componente 
        Return New componente 
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As componente ) As Boolean
        Return Equals(Entidad.cve_cadena_valor , cve_cadena_valor) AndAlso
             Equals(Entidad.componente  , componente)
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return estatus 
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As ComponenteCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case ComponenteCampos .cve_cadena_valor    
                If Equals(cve_cadena_valor, 0) Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If
            Case ComponenteCampos .cve_componente      
                If Equals(cve_componente, 0) Then
                    Resultado.Add(My.Resources.Str_CveModeloRequerido)
                End If
        End Select
        Return Resultado
    End Function
End Class
