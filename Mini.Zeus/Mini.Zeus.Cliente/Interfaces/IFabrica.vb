﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para fabrica de entidades de Zeus.
''' </summary>
''' <remarks>Todas las fábricas de objetos de datos basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IFabrica

    ''' <summary>
    ''' Notifica que el proceso de destruir está iniciando.
    ''' </summary>
    Event IniciandoDestruir As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de destruir.
    ''' </summary>
    Event Destruyendo As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de borrar está iniciando.
    ''' </summary>
    Event IniciandoBorrar As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de borrar.
    ''' </summary>
    Event Borrando As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de recuperar está iniciando.
    ''' </summary>
    Event IniciandoRecuperar As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de recuperar.
    ''' </summary>
    Event Recuperando As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de guardar está iniciando.
    ''' </summary>
    Event IniciandoGuardar As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de guardar.
    ''' </summary>
    Event Guardando As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de obtener uno está iniciando.
    ''' </summary>
    Event IniciandoObtenerUno As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de obtener uno.
    ''' </summary>
    Event ObteniendoUno As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de obtener existentes está iniciando.
    ''' </summary>
    Event IniciandoObtenerExistentes As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de obtener existentes.
    ''' </summary>
    Event ObteniendoExistentes As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de crear nuevo está iniciando.
    ''' </summary>
    Event IniciandoCrearNuevo As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de crear nuevo.
    ''' </summary>
    Event CreandoNuevo As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de deserializar está iniciando.
    ''' </summary>
    Event IniciandoDeserializar As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de deserializar.
    ''' </summary>
    Event Deserializando As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de deserializar lista está iniciando.
    ''' </summary>
    Event IniciandoDeserializarLista As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de deserializar lista.
    ''' </summary>
    Event DeserializandoLista As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Notifica que el proceso de serializar lista está iniciando.
    ''' </summary>
    Event IniciandoSerializarLista As EventHandler(Of ArgumentosInicioProceso)

    ''' <summary>
    ''' Notifica progreso del proceso de serializar lista.
    ''' </summary>
    Event SerializandoLista As EventHandler(Of ArgumentosProgresoProceso)

    ''' <summary>
    ''' Determina la ubicación de la fuente de datos para la fábrica.
    ''' </summary>
    ''' <remarks>Este valor determina si todas las operaciones se ejecutan directamente en la base de datos local, o dependen de un servicio Web remoto.</remarks>
    Property UbicacionFuenteDatos() As UbicacionesFuentesDatos

    ''' <summary>
    ''' Regresa la cadena de conexión a la base de datos.
    ''' </summary>
    ''' <remarks>Este valor contiene la cadena de conexión a la base de datos cuando la ubicación de la fuente de datos es SQLServer; en otro caso siempre regresa una cadena vacía.</remarks>
    Property CadenaConexionSQLServer() As String

    ''' <summary>
    ''' Regresa la ruta al servicio web.
    ''' </summary>
    ''' <remarks>Este valor contiene la ruta Url al servicio web que da acceso a la información cuando la ubicación de la fuente de datos es ServicioWeb; en otro caso siempre regresa una cadena vacía.</remarks>
    ReadOnly Property UrlServicioWeb() As Uri

    '''' <summary>
    '''' Regresa el usuario que está utilizando la fábrica.
    '''' </summary>
    '''' <remarks>Este valor contiene el usuario que utiliza la fábrica, y es el mismo que se utiliza al momento de crear entidades.</remarks>
    'ReadOnly Property UsuarioActual() As Usuarios

    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    Sub Destruir(ByVal Entidad As IEntidad)

    '''' <summary>
    '''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    '''' </summary>
    '''' <param name="Entidad">La entidad que será destruida.</param>
    '''' <param name="Usuario">El usuario que destruirá la entidad.</param>
    'Sub Destruir(ByVal Entidad As IEntidad, ByVal Usuario As Usuarios)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    Sub Borrar(ByVal Entidad As IEntidad)

    '''' <summary>
    '''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como borrada.
    '''' </summary>
    '''' <param name="Entidad">La entidad que será borrada.</param>
    '''' <param name="Usuario">El usuario que borrará la entidad.</param>
    'Sub Borrar(ByVal Entidad As IEntidad, ByVal Usuario As Usuarios)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    Sub Recuperar(ByVal Entidad As IEntidad)

    '''' <summary>
    '''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    '''' </summary>
    '''' <param name="Entidad">La entidad que será recuperada.</param>
    '''' <param name="Usuario">El usuario que recuperará la entidad.</param>
    'Sub Recuperar(ByVal Entidad As IEntidad, ByVal Usuario As Usuarios)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Sub Guardar(ByVal Entidad As IEntidad)

    '''' <summary>
    '''' Ejecuta el guardado de la entidad en la base de datos.
    '''' </summary>
    '''' <param name="Entidad">La entidad que será guardada.</param>
    '''' <param name="Usuario">El usuario que guardará la entidad.</param>
    '''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    'Sub Guardar(ByVal Entidad As IEntidad, ByVal Usuario As Usuarios)

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Function ObtenerUno(ByVal Id As Guid) As Object

    '''' <summary>
    '''' Ejecuta la selección de una entidad en particular.
    '''' </summary>
    '''' <param name="Id">El identificador de la entidad a obtener.</param>
    '''' <param name="Usuario">El usuario que desea obtener la entidad.</param>
    'Function ObtenerUno(ByVal Id As Guid, ByVal Usuario As Usuarios) As Object

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Function ObtenerUnoParaSerializar(ByVal Id As Guid) As Object

    Function ObtenerUnoParaSerializar(ByVal Id As String) As Object

    '''' <summary>
    '''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    '''' </summary>
    '''' <param name="Id">El identificador de la entidad a obtener.</param>
    '''' <param name="Usuario">El usuario que desea obtener la entidad.</param>
    'Function ObtenerUnoParaSerializar(ByVal Id As Guid, ByVal Usuario As Usuarios) As Object

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes.
    ''' </summary>
    Function ObtenerExistentes() As List(Of Object)

    '''' <summary>
    '''' Ejecuta la selección de todas las entidades existentes.
    '''' </summary>
    '''' <param name="Usuario">El usuario que desea obtener las entidades.</param>
    'Function ObtenerExistentes(ByVal Usuario As Usuarios) As List(Of Object)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Function ObtenerExistentes(ByVal Filtro As Dictionary(Of String, Object)) As List(Of Object)

    '''' <summary>
    '''' Ejecuta la selección de todas las entidades existentes con base en un filtro.
    '''' </summary>
    '''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    '''' <param name="Usuario">El usuario que desea obtener las entidades.</param>
    'Function ObtenerExistentes(ByVal Filtro As Dictionary(Of String, Object), ByVal Usuario As Usuarios) As List(Of Object)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes sin datos relacionados.
    ''' </summary>
    Function ObtenerExistentesParaSerializar() As List(Of Object)

    '''' <summary>
    '''' Ejecuta la selección de todas las entidades existentes sin datos relacionados.
    '''' </summary>
    '''' <param name="Usuario">El usuario que desea obtener las entidades.</param>
    'Function ObtenerExistentesParaSerializar(ByVal Usuario As Usuarios) As List(Of Object)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro sin datos relacionados.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Function ObtenerExistentesParaSerializar(ByVal Filtro As Dictionary(Of String, Object)) As List(Of Object)

    '''' <summary>
    '''' Ejecuta la selección de todas las entidades existentes con base en un filtro sin datos relacionados.
    '''' </summary>
    '''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    '''' <param name="Usuario">El usuario que desea obtener las entidades.</param>
    'Function ObtenerExistentesParaSerializar(ByVal Filtro As Dictionary(Of String, Object), ByVal Usuario As Usuarios) As List(Of Object)

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    Function CrearNuevo() As Object

    '''' <summary>
    '''' Crea una nueva entidad.
    '''' </summary>
    '''' <param name="Usuario">El usuario que desea crear la entidad.</param>
    'Function CrearNuevo(ByVal Usuario As Usuarios) As Object

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    Function Deserializar(ByVal EntidadSerializada As String) As Object

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se inicializará como nueva o como existente.</param>
    Function Deserializar(ByVal EntidadSerializada As String, ByVal InicializarComoNuevo As Boolean) As Object

End Interface