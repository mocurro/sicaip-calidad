﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Solicitud", "Solicitudes", Generos.Femenino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Folio", 1, "Folio", "Folio", Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Solicitudes: {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "user.Nombre", 2, "Nombre")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Fecha_Solicitud", 3, "Fecha solicitud")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Fecha_Entrada", 3, "Fecha entrada")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "NoParte.np_gkn", 3, "Número de parte")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "descripcion", 4, "Descripción")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "no_piezas", 5, "Número de piezas")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "comentarios", 6, "Comentarios")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 7, "Estatus")>
Public Class LABDIMSolicitud
    Inherits Entidad(Of LABDIMSolicitud, LABDIMSolicitudCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As LABDIMSolicitud)
        With EntidadNueva
            .cve_solicitud = cve_solicitud
            .cve_usuario = cve_usuario
            .no_piezas = no_piezas
            .no_cotas = no_cotas
            .cve_modelo = cve_modelo
            .descripcion = descripcion
            .comentarios = comentarios
            .Estatus = Estatus
            .Fecha_Solicitud = Fecha_Solicitud
            .Fecha_Entrada = Fecha_Entrada
            .tipo_inspeccion = tipo_inspeccion
            .fecha_prevista = fecha_prevista
            .detalle = detalle
            .Fecha_Salida = Fecha_Salida
            If Archivo IsNot Nothing Then
                .NombreArchivo = Archivo.Nombre
            End If

        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
            .cve_solicitud = cve_solicitud
            .cve_usuario = cve_usuario
            .no_piezas = no_piezas
            .no_cotas = no_cotas
            .cve_modelo = cve_modelo
            .descripcion = descripcion
            .comentarios = comentarios
            .Estatus = Estatus
            .Fecha_Solicitud = Fecha_Solicitud
            .Fecha_Entrada = Fecha_Entrada
            .tipo_inspeccion = tipo_inspeccion
            .fecha_prevista = fecha_prevista
            .detalle = detalle
            .Fecha_Salida = Fecha_Salida
            .NombreArchivo = NombreArchivo
            If Archivo IsNot Nothing Then
                .NombreArchivo = Archivo.Nombre
            End If
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            cve_solicitud = .cve_solicitud
            cve_usuario = .cve_usuario
            no_piezas = .no_piezas
            no_cotas = .no_cotas
            cve_modelo = .cve_modelo
            descripcion = .descripcion
            comentarios = .comentarios
            Estatus = .Estatus
            Fecha_Solicitud = .Fecha_Solicitud
            Fecha_Entrada = .Fecha_Entrada
            tipo_inspeccion = .tipo_inspeccion
            fecha_prevista = .fecha_prevista
            detalle = .detalle
            Fecha_Salida = .Fecha_Salida
            NombreArchivo = .NombreArchivo
            If Archivo IsNot Nothing Then
                NombreArchivo = Archivo.Nombre
            End If
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As LABDIMSolicitud
        Return New LABDIMSolicitud
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As LABDIMSolicitud) As Boolean
        If Archivo IsNot Nothing Then
            Return Equals(Entidad.no_piezas, no_piezas) AndAlso
             Equals(Entidad.cve_modelo, cve_modelo) AndAlso
             Equals(Entidad.comentarios, comentarios) AndAlso
             Equals(Entidad.no_cotas, no_cotas) AndAlso
             Equals(Entidad.tipo_inspeccion, tipo_inspeccion) AndAlso
             Equals(Entidad.NombreArchivo, Archivo.Nombre)
        Else
            Return Equals(Entidad.no_piezas, no_piezas) AndAlso
             Equals(Entidad.cve_modelo, cve_modelo) AndAlso
             Equals(Entidad.comentarios, comentarios) AndAlso
             Equals(Entidad.no_cotas, no_cotas) AndAlso
             Equals(Entidad.tipo_inspeccion, tipo_inspeccion) AndAlso
             Equals(Entidad.NombreArchivo, NombreArchivo)
        End If



    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
            Return My.Resources.businessman
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return descripcion
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As LABDIMSolicitudCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case LABDIMSolicitudCampos.cve_modelo   '.Nombre  
                If EsNew Then
                    If cve_modelo = 0 Then
                        Resultado.Add(My.Resources.Str_ModeloRequerido)
                    End If
                Else
                    If cve_modelo = 0 Then
                        Resultado.Add(My.Resources.Str_ModeloRequerido)
                    End If
                End If

            Case LABDIMSolicitudCampos.no_piezas
                If no_piezas <= 0 Then
                    Resultado.Add(My.Resources.Str_NoPiezas)
                End If

            Case LABDIMSolicitudCampos.cve_modelonew
                If EsNew Then
                    If cve_modelonew = 0 Then
                        Resultado.Add(My.Resources.Str_ModeloRequerido)
                    End If
                End If

                'Case LineasProductosCampos.Id_Codigo
                '    If Equals(Id_Codigo, Guid.Empty) Then
                '        Resultado.Add(My.Resources.Str_LineaProductosValidarProductoRequerido)
                '    End If
                'Case LineasProductosCampos.Lote
                '    If String.IsNullOrEmpty(Lote) Then
                '        Resultado.Add(My.Resources.Str_LineaProductosValidarLoteRequerido)
                '    End If
        End Select
        Return Resultado
    End Function
End Class
