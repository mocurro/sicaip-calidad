﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los metadatos de la tabla Unidad_Negocio_Usuario
''' </summary>
<Flags()>
Public Enum Area_UsuarioMetadatos As Integer

    ''' <summary>
    ''' Ninguno
    ''' </summary>
    Ninguno = 0

End Enum