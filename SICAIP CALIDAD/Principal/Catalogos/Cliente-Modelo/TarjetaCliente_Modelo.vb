﻿ Imports System.ComponentModel

Public Class TarjetaCliente_Modelo

    Private client As Datos.Defecto

    Private component As Datos.modelo 
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoCliente_Modelo ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Cliente_ModeloCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Cliente_ModeloCampos.cve_cliente , ItemCliente )
            .Add(Datos.Cliente_ModeloCampos.cve_modelo , itemComponente )
            .Add(Datos.Cliente_ModeloCampos.Estatus  , ItemEstatus  )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Cliente_ModeloCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Cliente_Modelo )()
        '
        Select Case sender.Name
            Case ComboCliente .Name
                If ComboCliente.EditValue IsNot Nothing Then
                  Dim MiGUID As Integer = CInt (ComboCliente.EditValue.ToString )
                   Paquete.cve_cliente   = MiGUID
                End If
               
                
                Return True 
                
            Case ComboComponente .Name
                If ComboComponente.EditValue IsNot Nothing Then
                     Dim MiGUID As Long = CInt (ComboComponente.EditValue.ToString )
                 component = Datos.Fabricas .Modelo  .ObtenerUno (MiGUID)
                 Paquete.cve_modelo  = MiGUID
                End If
                
                Return True 

            Case ComboEstatus .Name 
                Paquete.estatus = CByte(ComboEstatus.SelectedIndex)
                
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Cliente_Modelo)()
        'Cargar los combos
        With EntidadDe(Of Datos.Cliente_Modelo  )()
            FuenteClientes.DataSource = CatalogoDe(Of CatalogoCliente_Modelo).Clientes .FindAll(Function(Item) Item.Estatus  = 0)
        End With
         'Cargar los combos
        With EntidadDe(Of Datos.Cliente_Modelo )()
            FuenteModelos .DataSource = CatalogoDe(Of CatalogoCliente_Modelo).Modelos  '.FindAll(Function(Item) Item )
        End With

        If paquete.Estatus = 0 Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If
        OmitirCambiosPersonalizados = True
        ComboCliente .EditValue = paquete.cve_cliente 
        ComboComponente.EditValue = paquete.cve_modelo 
        OmitirCambiosPersonalizados = False 

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Cliente_Modelo )()
        
    End Sub
End Class