﻿Imports Datos
Imports Mini.Zeus.Cliente

Public Class CatalogoCliente_Modelo

    Private MiListaModelos As List(Of Datos.modelo)

    Private MiListaClientes As List(Of Datos.Cliente )

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))


        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Cliente_Modelo), Datos.Fabricas.Cliente_Modelof, GetType(TarjetaCliente_Modelo)))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Cliente_Modelo)

    End Function

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Modelos() As List(Of Datos.modelo)
        Get
            If MiListaModelos Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaModelos = Fabricas.Modelo .ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaModelos
        End Get
    End Property

    
    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Clientes() As List(Of Datos.Cliente  )
        Get
            If MiListaClientes  Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaClientes = Fabricas.Cliente  .ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaClientes
        End Get
    End Property
End Class