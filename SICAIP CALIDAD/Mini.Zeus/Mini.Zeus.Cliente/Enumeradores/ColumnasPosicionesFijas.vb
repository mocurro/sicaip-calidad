﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista las diferentes posiciones donde se puede fijar una columna para el atributo de Columna.
''' </summary>
''' <remarks>Este enumerador es compatible con el enumerador DevExpress.XtraGrid.Columns.FixedStyle</remarks>
Public Enum ColumnasPosicionesFijas As Integer

    ''' <summary>
    ''' La columna es libre de desplazarse horizontalmente.
    ''' </summary>
    Libre = 0

    ''' <summary>
    ''' La columna queda fija al extremo izquierdo de la vista.
    ''' </summary>
    FijoIzquierda = 1

    ''' <summary>
    ''' La columna queda fija al extremo derecho de la vista.
    ''' </summary>
    FijoDerecha = 2

End Enum