﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports Microsoft.WindowsAPICodePack
Imports Microsoft.WindowsAPICodePack.Dialogs

''' <summary>
''' Ventana base para cualquier catálogo que estará contenido en una venta principal de Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier catálogo que implemente UX de Zeus. Todas las ventanas catálogo heredan finalmente de esta ventana.</remarks>
Public Class FormaCatalogo
    Implements ICatalogo

    ''' <summary>
    ''' Clase para traspasar datos entre hilos.
    ''' </summary>
    Private Class ArgumentosHilos

        ''' <summary>
        ''' Crea un nuevo argumento.
        ''' </summary>
        ''' <param name="Entidad">Entidad a traspasar entre hilos.</param>
        ''' <param name="Tarjeta">Tarjeta a traspasar entre hilos.</param>
        Public Sub New(ByVal Entidad As IEntidad, ByVal Tarjeta As ITarjeta)
            Me.Entidad = Entidad
            Me.Tarjeta = Tarjeta
        End Sub

        ''' <summary>
        ''' Entidad.
        ''' </summary>
        Public Entidad As IEntidad

        ''' <summary>
        ''' Tarjeta.
        ''' </summary>
        Public Tarjeta As ITarjeta

    End Class

    ''' <summary>
    ''' Listado de los diferentes medios de exportación habilitados.
    ''' </summary>
    Private Enum MediosExportacion As Integer
        Ninguno = 0
        XLSX
        XLS
        PDF
        CSV
        HTML
    End Enum

    ''' <summary>
    ''' Contiene la lista de entidades que el catálogo utiliza, incluyendo la tarjeta y fábrica asociada.
    ''' </summary>
    Private MisEntidades As List(Of EntidadParaCatalogo)

    ''' <summary>
    ''' Contiene la información del filtro para realizar la selección de datos.
    ''' </summary>
    Private MiFiltro As Dictionary(Of String, Object)

    ''' <summary>
    ''' Contiene si la obtención de datos debe incluir datos relacionados.
    ''' </summary>
    Private MiIncluirDatosRelacionados As Boolean

    ''' <summary>
    ''' Contiene la fábrica relacionada con la entidad raíz.
    ''' </summary>
    Private MiFabrica As IFabrica

    ''' <summary>
    ''' Contiene el tipo de la entidad raíz del catálogo.
    ''' </summary>
    Private MiTipoEntidadRaiz As Type

    ''' <summary>
    ''' Contiene el atributo de texto de la entidad raíz del catálogo.
    ''' </summary>
    Private MiTipoEntidadRaizTexto As TextoAttribute

    ''' <summary>
    ''' Contiene el permiso general de agregar.
    ''' </summary>
    Private MiPuedeAgrear As Boolean

    ''' <summary>
    ''' Contiene el permiso general de exportar.
    ''' </summary>
    Private MiPuedeExportar As Boolean

    ''' <summary>
    ''' Contiene el permiso de ese momento para modificar.
    ''' </summary>
    Private MiPuedeModificar As Boolean

    ''' <summary>
    ''' Contiene el permiso de ese momento para ver detalles.
    ''' </summary>
    Private MiPuedeVerDetalle As Boolean

    ''' <summary>
    ''' Contiene el permiso de ese momento para borrar.
    ''' </summary>
    Private MiPuedeBorrar As Boolean

    ''' <summary>
    ''' Contiene el permiso de ese momento para destruir.
    ''' </summary>
    Private MiPuedeDestruir As Boolean

    ''' <summary>
    ''' Contiene el permiso de ese momento para recuperar.
    ''' </summary>
    Private MiPuedeRecuperar As Boolean

    ''' <summary>
    ''' Contiene cuales entidades serán visibles en el catálogo.
    ''' </summary>
    Private MisEntidadesVisibles As EntidadesVisibles

    ''' <summary>
    ''' Determina si el catálogo está ocupado en base al número de operaciones concurrentes. 0 significa que no está ocupado.
    ''' </summary>
    Private MiEstaOcupado As Integer

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de agregar.
    ''' </summary>
    Private MiHabilitarAgregar As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de editar.
    ''' </summary>
    Private MiHabilitarEditar As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de borrar.
    ''' </summary>
    Private MiHabilitarBorrar As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de actualizar.
    ''' </summary>
    Private MiHabilitarActualizar As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de exportar.
    ''' </summary>
    Private MiHabilitarExportar As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de exportar.
    ''' </summary>
    Private MiDetenerCargaInicial As Boolean

    ''' <summary>
    ''' Bandera para saber si se mostrará el nombre por defaul de los descuentos
    ''' </summary>
    ''' <remarks></remarks>
    Private MiMostrarNombreDefaulDescuentos As Boolean

    ''' <summary>
    ''' Nombre por default MOstrado en los descuentos
    ''' </summary>
    ''' <remarks></remarks>
    Private MiNombreDefaulDescuentos As String

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de ver detalles.
    ''' </summary>
    Private MiHabilitarVerDetalles As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de destruir.
    ''' </summary>
    Private MiHabilitarDestruir As Boolean

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de recuperar.
    ''' </summary>
    Private MiHabilitarRecuperar As Boolean

    ''' <summary>
    ''' Contiene la fecha y hora de inicio de un proceso para medir el tiempo utilizado.
    ''' </summary>
    Private MiTiempoInicioProgreso As Long

    ''' <summary>
    ''' Contiene los datos obtenidos desde el Store procedure
    ''' </summary>
    Public Shadows DataTableBoletos As DataTable

    ''' <summary>
    ''' Bandera para pintar el fondo del registro
    ''' </summary>
    ''' <remarks></remarks>
    Private MiRowConBackColor As Boolean

    ''' <summary>
    ''' Delegado para la actualización del grid dentro del hilo
    ''' </summary>
    Public Delegate Sub DelegadoActualizaSourceCatalogo()

    ''' <summary>
    ''' Título asigando de forma fija
    ''' </summary>
    Public TituloFijo As String 

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la consulta; por ejemplo, modificar el filtro.</remarks>
    Public Shadows Event AntesActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Implements ICatalogo.AntesActualizarVentana

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Public Shadows Event DespuesActualizarVentana(ByVal sender As Object, ByVal e As System.EventArgs) Implements ICatalogo.DespuesActualizarVentana

    ''' <summary>
    ''' Evento que ocurre antes de mandar llamar por primera vez ActualizarVentana.
    ''' </summary>
    ''' <remarks>Este evento ocurre durante la carga de la ventana base, antes de ejecutar el método ActualizarVentana.</remarks>
    Protected Event AntesPrimerActualizarVentana As EventHandler(Of System.ComponentModel.CancelEventArgs)

    Protected Event DuranteActualizarVentana As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of List(Of IEntidad)))

    Protected Event DespuesVentanaPadreActualizoVentana As EventHandler

    Protected Event AntesMostrarTarjeta As EventHandler(Of ArgumentosAccionCatalogo(Of ITarjeta))

    Protected Event AntesAgregarNuevo As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesAgregarNuevo As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event AntesBorrarSeleccionados As EventHandler(Of System.ComponentModel.CancelEventArgs)
    Protected Event DespuesBorrarSeleccionados As EventHandler
    Protected Event AntesBorrarEntidad As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesBorrarEntidad As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event AntesDestruirSeleccionados As EventHandler(Of System.ComponentModel.CancelEventArgs)
    Protected Event DespuesDestruirSeleccionados As EventHandler
    Protected Event AntesDestruirEntidad As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesDestruirEntidad As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event AntesExportarLista As EventHandler(Of System.ComponentModel.CancelEventArgs)
    Protected Event DespuesExportarLista As EventHandler

    Protected Event AntesModificarSeleccionado As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesModificarSeleccionado As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event AntesRecuperarSeleccionados As EventHandler(Of System.ComponentModel.CancelEventArgs)
    Protected Event DespuesRecuperarSeleccionados As EventHandler
    Protected Event AntesRecuperarEntidad As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesRecuperarEntidad As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event AntesVerDetalleSeleccionado As EventHandler(Of ArgumentosAntesAccionCatalogo(Of IEntidad))
    Protected Event DespuesVerDetalleSeleccionado As EventHandler(Of ArgumentosDespuesAccionCatalogo(Of IEntidad))

    Protected Event DuranteActualizarBarraHerramientas As EventHandler(Of ArgumentosDuranteActualizarBarraHerramientas)

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Public Overloads Sub Foco() Implements ICatalogo.Foco

        MyBase.Foco()

    End Sub

    ''' <summary>
    ''' Muestra el catálogo para un tipo de entidad particular dentro de la ventana padre.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Sub MostrarVentana(ByVal owner As System.Windows.Forms.IWin32Window) Implements ICatalogo.MostrarVentana

        MyBase.MostrarVentana(owner)

    End Sub

    '''' <summary>
    '''' Muestra el catálogo para un tipo de entidad y usuario particular dentro de la ventana padre.
    '''' </summary>
    '''' <param name="UsuarioActual">Usuario dueño del catálogo.</param>
    '''' <param name="owner">Ventana padre.</param>
    'Public Shadows Sub MostrarVentana(ByVal UsuarioActual As Usuarios, ByVal owner As System.Windows.Forms.IWin32Window) Implements ICatalogo.MostrarVentana

    '    MyBase.MostrarVentana(UsuarioActual, owner)

    'End Sub

    ''' <summary>
    ''' Da acceso a la lista de entidades para el catálogo.
    ''' </summary>
    Protected ReadOnly Property EntidadesParaCatalogo() As List(Of EntidadParaCatalogo)
        Get
            Return MisEntidades
        End Get
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de agregar.
    ''' </summary>
    Public Property HabilitarAgregar() As Boolean
        Get
            Return MiHabilitarAgregar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarAgregar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de editar.
    ''' </summary>
    Public Property HabilitarEditar() As Boolean
        Get
            Return MiHabilitarEditar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarEditar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de borrar.
    ''' </summary>
    Public Property HabilitarBorrar() As Boolean
        Get
            Return MiHabilitarBorrar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarBorrar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de actualizar.
    ''' </summary>
    Public Property HabilitarActualizar() As Boolean
        Get
            Return MiHabilitarActualizar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarActualizar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de exportar.
    ''' </summary>
    Public Property HabilitarExportar() As Boolean
        Get
            Return MiHabilitarExportar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarExportar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita carga inicial
    ''' </summary>
    Public Property DetenerCargaInicial() As Boolean
        Get
            Return MiDetenerCargaInicial
        End Get
        Set(ByVal value As Boolean)
            MiDetenerCargaInicial = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de ver detalles.
    ''' </summary>
    Public Property HabilitarVerDetalles() As Boolean
        Get
            Return MiHabilitarVerDetalles
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarVerDetalles = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de recuperar.
    ''' </summary>
    Public Property HabilitarRecuperar() As Boolean
        Get
            Return MiHabilitarRecuperar
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarRecuperar = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si el catálogo habilita la acción de destruir.
    ''' </summary>
    Public Property HabilitarDestruir() As Boolean
        Get
            Return MiHabilitarDestruir
        End Get
        Set(ByVal value As Boolean)
            MiHabilitarDestruir = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre por default MOstrado en los descuentos
    ''' </summary>
    ''' <remarks></remarks>
    Public Property NombreDefaulDescuentos() As String
        Get
            Return MiNombreDefaulDescuentos
        End Get
        Set(ByVal value As String)
            MiNombreDefaulDescuentos = value
        End Set
    End Property
    ''' <summary>
    ''' Bandera para pintar el fondo del registro
    ''' </summary>
    ''' <remarks></remarks>
    Public Property RowConBackColor() As Boolean
        Get
            Return MiRowConBackColor
        End Get
        Set(ByVal value As Boolean)
            MiRowConBackColor = value
        End Set
    End Property
    ''' <summary>
    ''' Bandera para saber si se mostrará el nombre por defaul de los descuentos
    ''' </summary>
    ''' <remarks></remarks>
    Public Property MostrarNombreDefaulDescuentos() As Boolean
        Get
            Return MiMostrarNombreDefaulDescuentos
        End Get
        Set(ByVal value As Boolean)
            MiMostrarNombreDefaulDescuentos = value
        End Set
    End Property
    ''' <summary>
    ''' Determina cuales entidades serán visibles en el catálogo.
    ''' </summary>
    <System.ComponentModel.Description("Determina cuales entidades serán visibles en el catálogo.")>
    <System.ComponentModel.Category("Zeus")>
    <System.ComponentModel.DefaultValue(CInt(EntidadesVisibles.Existentes))>
    Public Property EntidadesVisibles() As EntidadesVisibles Implements ICatalogo.EntidadesVisibles
        Get
            Return MisEntidadesVisibles
        End Get
        Set(ByVal value As EntidadesVisibles)
            MisEntidadesVisibles = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si al obtener la información se debe incluir los datos relacionados.
    ''' </summary>
    Public Property IncluirDatosRelacionados() As Boolean Implements ICatalogo.IncluirDatosRelacionados
        Get
            Return MiIncluirDatosRelacionados
        End Get
        Set(ByVal value As Boolean)
            MiIncluirDatosRelacionados = value
        End Set
    End Property

    ''' <summary>
    ''' Determina si la tira del filtro es visible.
    ''' </summary>
    Public Property FiltroVisible() As Boolean Implements ICatalogo.FiltroVisible
        Get
            Return PanelFiltro.Visible
        End Get
        Set(ByVal value As Boolean)
            PanelFiltro.Visible = value
        End Set
    End Property

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public Overloads Property VentanaPadre() As IFormaPrincipal Implements ICatalogo.VentanaPadre
        Get
            Return MyBase.VentanaPadre
        End Get
        Set(ByVal value As IFormaPrincipal)
            MyBase.VentanaPadre = value
        End Set
    End Property

    ''' <summary>
    ''' Regresa el tipo de entidad raíz que se muestra en el catálogo.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public ReadOnly Property TipoEntidadRaiz() As System.Type Implements ICatalogo.TipoEntidadRaiz
        Get
            Return MiTipoEntidadRaiz
        End Get
    End Property

    ''' <summary>
    ''' Regresa el atributo de texto de la entidad raíz.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public ReadOnly Property TipoEntidadRaizTexto() As TextoAttribute Implements ICatalogo.TipoEntidadRaizTexto
        Get
            Return MiTipoEntidadRaizTexto
        End Get
    End Property

    ''' <summary>
    ''' Regresa la imagen de la entidad.
    ''' </summary>
    Public Overrides ReadOnly Property Imagen() As System.Drawing.Image
        Get
            'Dim EntidadRaiz = CType(Activator.CreateInstance(MiTipoEntidadRaiz), IEntidad)
            'Return EntidadRaiz.ImagenEntidad
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' Prepara el catálogo.
    ''' </summary>
    ''' <exception cref="ArgumentNullException">Arroja la excepción cuando en la lista de entidades no existe la definición para la entidad raíz.</exception>
    Private Sub FormaCatalogo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Definir colores en base al tema
            PanelMensaje.BackColor = VentanaPadre.ColorInfo
            EtiquetaMensaje.ForeColor = VentanaPadre.ColorInfoTexto

            'Definir elementos visuales
            BotonAgregar.Glyph = My.Resources.Imagen16_Agregar
            BotonAgregar.LargeGlyph = My.Resources.Imagen32_Agregar
            BotonModificar.Glyph = My.Resources.Imagen16_Modificar
            BotonModificar.LargeGlyph = My.Resources.Imagen32_Modificar
            BotonBorrar.Glyph = My.Resources.Imagen16_Borrar
            BotonBorrar.LargeGlyph = My.Resources.Imagen32_Borrar
            BotonDestruir.Glyph = My.Resources.Imagen16_Destruir
            BotonDestruir.LargeGlyph = My.Resources.Imagen32_Destruir
            BotonActualizar.Glyph = My.Resources.Imagen16_Actualizar
            BotonActualizar.LargeGlyph = My.Resources.Imagen32_Actualizar
            BotonExportar.Glyph = My.Resources.Imagen16_Exportar
            BotonExportar.LargeGlyph = My.Resources.Imagen32_Exportar
            BotonVerDetalle.Glyph = My.Resources.Imagen16_VerDetalles
            BotonVerDetalle.LargeGlyph = My.Resources.Imagen32_VerDetalles

            'Solicitar información general
            MiTipoEntidadRaiz = DefinirTipoEntidadRaiz()
            MisEntidades = New List(Of EntidadParaCatalogo)
            DefinirEntidadesParaCatalogo(MisEntidades)
            Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(MiTipoEntidadRaiz))
            If EntidadParaCatalogo IsNot Nothing Then
                MiFabrica = EntidadParaCatalogo.Fabrica
            Else
                Throw New ArgumentNullException("TipoEntidadRaiz", "No existe la entidad raíz en la lista de entidades.")
            End If

            'Modifica aspectos visuales y de seguridad generales
            Dim EntidadRaiz = CType(Activator.CreateInstance(MiTipoEntidadRaiz), IEntidad)
            MiTipoEntidadRaizTexto = EntidadRaiz.Texto(Cultura)
            Icon = EntidadRaiz.IconoEntidad
            If not String.IsNullOrEmpty (TituloFijo)Then
                Text = TituloFijo
                Else
                Text = MiTipoEntidadRaizTexto.Plural
            End If
            
            BarraEtiquetaCopyright.Caption = String.Format("{0} | {1}", My.Application.Info.Trademark.Replace("&", "&&"), My.Application.Info.Copyright.Replace("&", "&&"))
            MiPuedeAgrear = True ' UsuarioActual.Puede(TiposPermisos.Agregar, EntidadRaiz)
            MiPuedeExportar = True 'UsuarioActual.Puede(TiposPermisos.Exportar, EntidadRaiz)
            MiPuedeBorrar = False
            MiPuedeDestruir = False
            MiPuedeModificar = False
            MiPuedeRecuperar = False
            MiPuedeVerDetalle = False
            MiEstaOcupado = 0
            PanelMensaje.Visible = False

            'Modificar acción agregar
            With BotonAgregar
                Dim Hint = String.Empty
                Select Case MiTipoEntidadRaizTexto.Genero
                    Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintAgregarMasculino
                    Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintAgregarFemenino
                    Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintAgregarNeutro
                End Select
                .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Singular.ToLower())

                .Visibilidad(MiHabilitarAgregar) 'And MiHabilitarAgregar And (MisEntidadesVisibles = EntidadesVisibles.Existentes Or MisEntidadesVisibles = EntidadesVisibles.Todas))
            End With

            'Modificar acción modificar
            With BotonModificar
                Dim Hint = String.Empty
                Select Case MiTipoEntidadRaizTexto.Genero
                    Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintModificarMasculino
                    Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintModificarFemenino
                    Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintModificarNeutro
                End Select
                .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Singular.ToLower())
                .Visibilidad(MiHabilitarEditar)
            End With

            'Modificar acción borrar
            With BotonBorrar
                Dim Hint = String.Empty
                Select Case MiTipoEntidadRaizTexto.Genero
                    Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintBorrarMasculino
                    Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintBorrarFemenino
                    Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintBorrarNeutro
                End Select
                .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Plural.ToLower())
                .Visibilidad(MiHabilitarBorrar)
            End With

            ''Modificar acción ver detalles
            'With BotonVerDetalle
            '    Dim Hint = String.Empty
            '    Select Case MiTipoEntidadRaizTexto.Genero
            '        Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintVerDetalleMasculino
            '        Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintVerDetalleFemenino
            '        Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintVerDetalleNeutro
            '    End Select
            '    .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Singular.ToLower())
            'End With

            'Modificar acción actualizar
            With BotonActualizar
                Dim Hint = String.Empty
                Select Case MiTipoEntidadRaizTexto.Genero
                    Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintActualizarMasculino
                    Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintActualizarFemenino
                    Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintActualizarNeutro
                End Select
                .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Plural.ToLower())
                .Visibilidad(MiHabilitarActualizar)
            End With

            'Modificar acción exportar
            With BotonExportar
                Dim Hint = String.Empty
                Select Case MiTipoEntidadRaizTexto.Genero
                    Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintExportarMasculino
                    Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintExportarFemenino
                    Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintExportarNeutro
                End Select
                .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Plural.ToLower())
                .Visibilidad(MiPuedeExportar And MiHabilitarExportar And (MisEntidadesVisibles <> EntidadesVisibles.Ninguna))
            End With

            ''Modificar acción destruir
            'With BotonDestruir
            '    Dim Hint = String.Empty
            '    Select Case MiTipoEntidadRaizTexto.Genero
            '        Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintDestruirMasculino
            '        Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintDestruirFemenino
            '        Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintDestruirNeutro
            '    End Select
            '    .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Plural.ToLower())
            'End With

            ''Modificar acción recuperar
            'With BotonRecuperar
            '    Dim Hint = String.Empty
            '    Select Case MiTipoEntidadRaizTexto.Genero
            '        Case Generos.Masculino : Hint = My.Resources.Str_CatalogoHintRecuperarMasculino
            '        Case Generos.Femenino : Hint = My.Resources.Str_CatalogoHintRecuperarFemenino
            '        Case Generos.Neutro : Hint = My.Resources.Str_CatalogoHintRecuperarNeutro
            '    End Select
            '    .Hint = String.Format(Hint, MiTipoEntidadRaizTexto.Plural.ToLower())
            'End With

            'Crea la columna básica de la imagen
            With VistaCatalogo.Columns.Insert(0)
                .Visible = True
                .FieldName = "ImagenEntidad"
                .Caption = String.Empty
                .MaxWidth = 20
                .MinWidth = 20
                .Width = 20
                .OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False
                .OptionsColumn.AllowMove = False
                .OptionsColumn.AllowSize = True
                .OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
                .OptionsColumn.ReadOnly = True
                .OptionsColumn.ShowCaption = False
                .OptionsFilter.AllowAutoFilter = False
                .OptionsFilter.AllowFilter = False
                .ColumnEdit = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
                GridCatalogo.RepositoryItems.Add(.ColumnEdit)
            End With
            'VistaCatalogo.OptionsBehavior.Editable = False
            ''VistaCatalogo.OptionsView.ShowGroupPanel = False
            'Crear estructura de columnas
            For Each Columna In EntidadRaiz.Columnas(Cultura)
                With VistaCatalogo.Columns.Insert(Columna.Posicion)

                    .Visible = True
                    .FieldName = Columna.Campo
                    .Caption = Columna.Titulo
                    Select Case Columna.TipoFormato
                        Case ColumnasTiposFormatos.FechaHora
                            .DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                        Case ColumnasTiposFormatos.Ninguno
                            .DisplayFormat.FormatType = DevExpress.Utils.FormatType.None
                        Case ColumnasTiposFormatos.Numerico
                            .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        Case ColumnasTiposFormatos.Personalizado
                            .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
                            .DisplayFormat.FormatString = Columna.CadenaFormato
                    End Select
                    Select Case Columna.PosicionFija
                        Case ColumnasPosicionesFijas.FijoDerecha
                            .Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right
                        Case ColumnasPosicionesFijas.FijoIzquierda
                            .Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left
                        Case ColumnasPosicionesFijas.Libre
                            .Fixed = DevExpress.XtraGrid.Columns.FixedStyle.None
                    End Select
                    If Columna.Agrupar Then

                        .Group()
                        .Visible = False
                    End If
                    Select Case Columna.TipoFormato
                        Case ColumnasTiposFormatos.FechaHora
                            .GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                        Case ColumnasTiposFormatos.Ninguno
                            .GroupFormat.FormatType = DevExpress.Utils.FormatType.None
                        Case ColumnasTiposFormatos.Numerico
                            .GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        Case ColumnasTiposFormatos.Personalizado
                            .GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
                            .GroupFormat.FormatString = Columna.CadenaFormato
                    End Select
                    If Columna.TipoResumen <> ColumnasTiposResumenes.Ninguno Then
                        Dim Tipo As DevExpress.Data.SummaryItemType
                        Select Case Columna.TipoResumen
                            Case ColumnasTiposResumenes.Cuenta
                                Tipo = DevExpress.Data.SummaryItemType.Count
                            Case ColumnasTiposResumenes.Maximo
                                Tipo = DevExpress.Data.SummaryItemType.Max
                            Case ColumnasTiposResumenes.Minimo
                                Tipo = DevExpress.Data.SummaryItemType.Min
                            Case ColumnasTiposResumenes.Ninguno
                                Tipo = DevExpress.Data.SummaryItemType.None
                            Case ColumnasTiposResumenes.Personalizado
                                Tipo = DevExpress.Data.SummaryItemType.Custom
                            Case ColumnasTiposResumenes.Promedio
                                Tipo = DevExpress.Data.SummaryItemType.Average
                            Case ColumnasTiposResumenes.Suma
                                Tipo = DevExpress.Data.SummaryItemType.Sum
                        End Select
                        .SummaryItem.SetSummary(Tipo, Columna.CadenaResumen)
                    End If
                    .ToolTip = Columna.Hint
                    .OptionsColumn.ReadOnly = True
                    .OptionsFilter.AllowAutoFilter = Columna.PermiteFiltar
                    .OptionsFilter.AllowFilter = Columna.PermiteFiltar
                    If Columna.PermiteAgrupar Then
                        .OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.Default
                    Else
                        .OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False
                    End If
                    If Columna.PermiteOrdenar Then
                        .OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.Default
                    Else
                        .OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
                    End If

                End With
            Next

            'Carga la lista con datos
            Dim Argumentos = New System.ComponentModel.CancelEventArgs(False)
            RaiseEvent AntesPrimerActualizarVentana(Me, Argumentos)
            If Not Argumentos.Cancel And Not DetenerCargaInicial Then
                ActualizarVentana()
            End If

        End If

    End Sub

    ''' <summary>
    ''' Dibuja la línea inferior divisora.
    ''' </summary>
    Private Sub PanelMensaje_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelMensaje.Paint

        Using Pluma = New Drawing.Pen(SystemColors.GrayText)
            e.Graphics.DrawLine(Pluma, 0, PanelMensaje.Height - 1, PanelMensaje.Width, PanelMensaje.Height - 1)
        End Using

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de agregar una nueva entidad.
    ''' </summary>
    Private Sub BotonAgregar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonAgregar.ItemClick

        AgregarNuevo()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de modificar la entidad seleccionada.
    ''' </summary>
    Private Sub BotonModificar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonModificar.ItemClick

        ModificarSeleccionado()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de borrar las entidades seleccionadas.
    ''' </summary>
    Private Sub BotonBorrar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonBorrar.ItemClick

        BorrarSeleccionados()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de actualizar la lista de entidades.
    ''' </summary>
    Private Sub BotonActualizar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonActualizar.ItemClick

        ActualizarVentana()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de exportar la lista de entidades.
    ''' </summary>
    Private Sub BotonExportar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonExportarXLSX.ItemClick, BotonExportarCSV.ItemClick, BotonExportarHTML.ItemClick, BotonExportarPDF.ItemClick, BotonExportarXLS.ItemClick

        Dim MedioExportacion = MediosExportacion.Ninguno
        Select Case e.Item.Name
            Case BotonExportarCSV.Name : MedioExportacion = MediosExportacion.CSV
            Case BotonExportarHTML.Name : MedioExportacion = MediosExportacion.HTML
            Case BotonExportarPDF.Name : MedioExportacion = MediosExportacion.PDF
            Case BotonExportarXLS.Name : MedioExportacion = MediosExportacion.XLS
            Case BotonExportarXLSX.Name : MedioExportacion = MediosExportacion.XLSX
        End Select
        ExportarLista(MedioExportacion)

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de ver los detalles de la entidad seleccionada.
    ''' </summary>
    Private Sub BotonVerDetalle_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonVerDetalle.ItemClick

        VerDetalleSeleccionado()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de destruir las entidades seleccionadas.
    ''' </summary>
    Private Sub BotonDestruir_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonDestruir.ItemClick

        DestruirSeleccionados()

    End Sub

    ''' <summary>
    ''' Ejecuta la acción de recuperar las entidades seleccionadas.
    ''' </summary>
    Private Sub BotonRecuperar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BotonRecuperar.ItemClick

        RecuperarSeleccionados()

    End Sub

    ''' <summary>
    ''' Realiza acciones después de cambiar la selección del grid.
    ''' </summary>
    Private Sub VistaCatalogo_RowClick(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles VistaCatalogo.RowClick

        'Determina el número de clics realizados
        Select Case e.Clicks
            Case 1
                '1 clic es cambio de selección
                ActualizarBarraHerramientas()

            Case 2
                '2 clics representa editar
                ModificarSeleccionado()

        End Select

    End Sub

    ''' <summary>
    ''' Realiza acciones después de cambiar la selección del grid.
    ''' </summary>
    Private Sub VistaCatalogo_SelectionChanged(ByVal sender As Object, ByVal e As DevExpress.Data.SelectionChangedEventArgs) Handles VistaCatalogo.SelectionChanged

        ActualizarBarraHerramientas()

    End Sub

    ''' <summary>
    ''' Define la lista de entidades que el catálogo utiliza, incluyendo la tarjeta y fábrica asociada.
    ''' </summary>
    ''' <param name="Entidades">La lista donde se guardan las entidades que manipula el catálogo.</param>
    ''' <remarks>Esta función rellena una lista donde se indica, para cada tipo de entidad, la fábrica y el tipo de tarjeta que le corresponde.</remarks>
    Protected Overridable Sub DefinirEntidadesParaCatalogo(ByVal Entidades As List(Of EntidadParaCatalogo))

        Throw New NotImplementedException()

    End Sub

    ''' <summary>
    ''' Define el tipo que representa la entidad raíz del catálogo.
    ''' </summary>
    ''' <remarks>Esta función regresa el tipo de entidad raíz del catálogo. Este tipo se utiliza para definir aspectos visuales, columnas y ciertos permisos.</remarks>
    Protected Overridable Function DefinirTipoEntidadRaiz() As Type

        Throw New NotImplementedException()

    End Function

    ''' <summary>
    ''' Define el filtro a utilizar cada vez que se actualiza el contenido del catálogo.
    ''' </summary>
    ''' <param name="Filtro">El diccionario donde se guarda la información del filtro.</param>
    ''' <remarks>Esta función rellena un diccionario con pares de variables-valor. El filtro resultante es utilizado por la fábrica para realizar la consulta a la base de datos.</remarks>
    Protected Overridable Sub DefinirFiltro(ByVal Filtro As Dictionary(Of String, Object))

        'Por default no se realiza ninguna operación, para que no exista filtro

    End Sub

    ''' <summary>
    ''' Regresa la lista de entidades de los renglones seleccionados que tienen datos.
    ''' </summary>
    Protected Function EntidadesSeleccionadasDe(Of TEntidad)() As List(Of TEntidad)

        Return EntidadesSeleccionadas().ConvertAll(Of TEntidad)(Function(Item) DirectCast(Item, TEntidad))

    End Function

    ''' <summary>
    ''' Regresa la lista de entidades de los renglones seleccionados que tienen datos.
    ''' </summary>
    Protected Function EntidadesSeleccionadas() As List(Of IEntidad)

        Dim Resultado = New List(Of IEntidad)
        Dim Renglones = VistaCatalogo.GetSelectedRows()
        For Each Renglon In Renglones
            If Renglon >= 0 Then
                If EntidadesParaCatalogo(0).TipoEntidad.Name <> "CortesBoletos" Then
                    Resultado.Add(DirectCast(VistaCatalogo.GetRow(Renglon), IEntidad))
                Else
                    'El Guid del boleto actualmente se encuentra en la columna 18
                    Dim Resultado2 = MiFabrica.ObtenerUnoParaSerializar(CType(CType(VistaCatalogo.GetRow(Renglon), DataRowView).Row(18), Guid))
                    Resultado.Add(DirectCast(Resultado2, IEntidad))
                End If
            Else
                Try
                    If EntidadesParaCatalogo(0).TipoEntidad.Name = "PedidosEmbarcados" Then
                        Resultado.Add(DirectCast(VistaCatalogo.GetRow(Renglon), IEntidad))
                    End If
                Catch ex As Exception

                End Try

            End If
        Next
        Return Resultado

    End Function

    ''' <summary>
    ''' Actualiza la disponibilidad de las acciones de la barra de herramientas en base a la selección actual.
    ''' </summary>
    Private Sub ActualizarBarraHerramientas()

        ''Obtener la cuenta de renglones seleccionados y determinar que no sean agrupados
        Dim Renglones = VistaCatalogo.GetSelectedRows()
        Dim TieneRenglonAgrupado = Array.Exists(Of Integer)(Renglones, Function(Item) Item < 0)
        Dim UnRenglon = Renglones.Length = 1 And (Not TieneRenglonAgrupado)
        Dim UnoOMasRenglones = Renglones.Length > 0 And (Not TieneRenglonAgrupado)
        Dim ModoEntidadesExistentes = (MisEntidadesVisibles = EntidadesVisibles.Existentes Or MisEntidadesVisibles = EntidadesVisibles.Todas)

        If EntidadesParaCatalogo(0).TipoEntidad.Name <> "CortesBoletos" Then
            'Obtener la disponibilidad según permisos
            If UnRenglon Then

                'Obtener permisos para entidad seleccionada
                Dim Entidad = CType(VistaCatalogo.GetRow(Renglones(0)), IEntidad)
                'MiPuedeModificar = UsuarioActual.Puede(TiposPermisos.Editar, Entidad)
                'MiPuedeVerDetalle = UsuarioActual.Puede(TiposPermisos.VerDetalles, Entidad)
                'MiPuedeBorrar = UsuarioActual.Puede(TiposPermisos.Borrar, Entidad)
                'MiPuedeDestruir = UsuarioActual.Puede(TiposPermisos.Destruir, Entidad)
                'MiPuedeRecuperar = UsuarioActual.Puede(TiposPermisos.Recuperar, Entidad)

            ElseIf UnoOMasRenglones Then

                'Obtener permisos acumulativos para entidades seleccionadas
                MiPuedeBorrar = True
                MiPuedeDestruir = True
                MiPuedeModificar = True
                MiPuedeRecuperar = True
                MiPuedeVerDetalle = True
                For Each Renglon In Renglones
                    Dim Entidad = CType(VistaCatalogo.GetRow(Renglon), IEntidad)
                    'MiPuedeModificar = MiPuedeModificar And UsuarioActual.Puede(TiposPermisos.Editar, Entidad)
                    'MiPuedeVerDetalle = MiPuedeVerDetalle And UsuarioActual.Puede(TiposPermisos.VerDetalles, Entidad)
                    'MiPuedeBorrar = MiPuedeBorrar And UsuarioActual.Puede(TiposPermisos.Borrar, Entidad)
                    'MiPuedeDestruir = MiPuedeDestruir And UsuarioActual.Puede(TiposPermisos.Destruir, Entidad)
                    'MiPuedeRecuperar = MiPuedeRecuperar And UsuarioActual.Puede(TiposPermisos.Recuperar, Entidad)
                Next

            End If
        End If

        'Modifica la visiblidad según permisos
        BotonModificar.Visibilidad(MiHabilitarEditar)
        BotonVerDetalle.Visibilidad(MiHabilitarVerDetalles)
        BotonBorrar.Visibilidad(MiHabilitarBorrar)
        BotonDestruir.Visibilidad(MiHabilitarDestruir)
        BotonRecuperar.Visibilidad(MiHabilitarRecuperar)

        'Modifica la disponibilidad según selección
        Dim NoEstaOcupado = (MiEstaOcupado = 0)
        BotonModificar.Enabled = UnRenglon And NoEstaOcupado
        BotonVerDetalle.Enabled = UnRenglon And NoEstaOcupado
        BotonBorrar.Enabled = UnoOMasRenglones And NoEstaOcupado
        BotonDestruir.Enabled = UnoOMasRenglones And NoEstaOcupado
        BotonRecuperar.Enabled = UnoOMasRenglones And NoEstaOcupado
        BotonAgregar.Enabled = NoEstaOcupado
        BotonActualizar.Enabled = NoEstaOcupado
        BotonExportar.Enabled = NoEstaOcupado

        'Levanta el evento para operaciones personalizadas
        RaiseEvent DuranteActualizarBarraHerramientas(Me, New ArgumentosDuranteActualizarBarraHerramientas(UnRenglon, UnoOMasRenglones, NoEstaOcupado))

    End Sub

    ''' <summary>
    ''' Agrega una nueva entidad.
    ''' </summary>
    Private Sub AgregarNuevo()

        'Ejecuta si se puede
        If MiPuedeAgrear And MiHabilitarAgregar Then

            'Crear entidad y tarjeta
            Dim NuevaEntidad = DirectCast(MiFabrica.CrearNuevo(), IEntidad)
            InicializarNuevaEntidad(NuevaEntidad)
            Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(MiTipoEntidadRaiz))
            Dim Tarjeta = CType(Activator.CreateInstance(EntidadParaCatalogo.TipoTarjeta), ITarjeta)
            RaiseEvent AntesMostrarTarjeta(Me, New ArgumentosAccionCatalogo(Of ITarjeta)(Tarjeta))

            'Mostrar la ventana y actualizar el contenido de ser necesario
            Tarjeta.VentanaPrincipal = VentanaPadre
            'Tarjeta.MostrarInformacion(NuevaEntidad, Me)
            If Tarjeta.MostrarVentana(NuevaEntidad, Me) = System.Windows.Forms.DialogResult.OK Then
                Dim Argumentos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(NuevaEntidad)
                RaiseEvent AntesAgregarNuevo(Me, Argumentos)
                If Not Argumentos.Cancel Then

                    'Iniciar hilo de agregar
                    UseWaitCursor = True
                    MostrarMensaje(String.Format("<color=HotTrack><size=11><b>Agregando {0}</b></size></color><br><size=8>Espere un momento por favor ...</size>", MiTipoEntidadRaizTexto.Singular.ToLower()), My.Resources.Imagen48_TrabajandoAgregar)
                    HiloAgregar.RunWorkerAsync(New ArgumentosHilos(NuevaEntidad, Tarjeta))

                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Realiza el guardado de la nueva entidad.
    ''' </summary>
    Private Sub HiloAgregar_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloAgregar.DoWork

        MiFabrica.Guardar(DirectCast(e.Argument, ArgumentosHilos).Entidad)
        e.Result = e.Argument

    End Sub

    ''' <summary>
    ''' Completa el proceso de agregado.
    ''' </summary>
    Private Sub HiloAgregar_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloAgregar.RunWorkerCompleted

        'Detecta si hubo error, para propagarlo
        If e.Error IsNot Nothing Then Throw e.Error

        'Completa el proceso
        Dim Argumentos = DirectCast(e.Result, ArgumentosHilos)
        RaiseEvent DespuesAgregarNuevo(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Argumentos.Entidad))
        OcultarMensaje()
        UseWaitCursor = False
        If VentanaPadre IsNot Nothing Then
            VentanaPadre.ActualizaPestañasAfectadas(Argumentos.Tarjeta.VentanasAfectadas)
            RaiseEvent DespuesVentanaPadreActualizoVentana(Me, EventArgs.Empty)
        End If

    End Sub

    ''' <summary>
    ''' Permite realizar tareas adicionales al momento de crear una nueva entidad.
    ''' </summary>
    ''' <param name="Entidad">La entidad creada en la fábrica.</param>
    ''' <remarks>Esta función toma la entidad creada de la fábrica y permite realizar operaciones específicas antes de enviarla a la tarjeta.</remarks>
    Protected Overridable Sub InicializarNuevaEntidad(ByVal Entidad As IEntidad)

        'No hace nada, los catálogos heredados son los responsables de implementar código

    End Sub

    ''' <summary>
    ''' Modifica la entidad seleccionada.
    ''' </summary>
    Private Sub ModificarSeleccionado()
        Try
            'Ejecuta si se puede
            If MiHabilitarEditar Then

                'Determina que exista únicamente 1 entidad seleccionada
                Dim Renglon = VistaCatalogo.GetSelectedRows()
                If Renglon.Length = 1 Then

                    'Obtiene entidad y crea tarjeta
                    Dim Entidad = CType(VistaCatalogo.GetRow(Renglon(0)), IEntidad)
                    Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(Entidad.GetType()))
                    Dim Tarjeta = CType(Activator.CreateInstance(EntidadParaCatalogo.TipoTarjeta), ITarjeta)
                    RaiseEvent AntesMostrarTarjeta(Me, New ArgumentosAccionCatalogo(Of ITarjeta)(Tarjeta))

                    'Mostrar la ventana y actualizar el contenido de ser necesario (o restablecer el original)
                    Tarjeta.VentanaPrincipal = VentanaPadre
                    If Tarjeta.MostrarVentana(Entidad, Me) = System.Windows.Forms.DialogResult.OK Then
                        Dim Argumentos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(Entidad)
                        RaiseEvent AntesModificarSeleccionado(Me, Argumentos)
                        If Not Argumentos.Cancel Then

                            'Iniciar hilo de modificar
                            UseWaitCursor = True
                            MostrarMensaje(String.Format("<color=HotTrack><size=11><b>Modificando {0}</b></size></color><br><size=8>Espere un momento por favor ...</size>", MiTipoEntidadRaizTexto.Singular.ToLower()), My.Resources.Imagen48_TrabajandoEditar)
                            HiloEditar.RunWorkerAsync(New ArgumentosHilos(Entidad, Tarjeta))

                        End If
                    Else
                        Entidad.RestablecerOriginal()
                    End If

                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Realiza el guardado de la entidad existente.
    ''' </summary>
    Private Sub HiloEditar_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloEditar.DoWork

        MiFabrica.Guardar(DirectCast(e.Argument, ArgumentosHilos).Entidad)
        e.Result = e.Argument

    End Sub

    ''' <summary>
    ''' Completa el proceso de editado.
    ''' </summary>
    Private Sub HiloEditar_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloEditar.RunWorkerCompleted

        'Detecta si hubo error, para propagarlo
        If e.Error IsNot Nothing Then Throw e.Error

        'Completa el proceso
        Dim Argumentos = DirectCast(e.Result, ArgumentosHilos)
        RaiseEvent DespuesModificarSeleccionado(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Argumentos.Entidad))
        OcultarMensaje()
        UseWaitCursor = False
        If VentanaPadre IsNot Nothing Then
            VentanaPadre.ActualizaPestañasAfectadas(Argumentos.Tarjeta.VentanasAfectadas)
            RaiseEvent DespuesVentanaPadreActualizoVentana(Me, EventArgs.Empty)
        End If

    End Sub

    ''' <summary>
    ''' Borra las entidades seleccionadas.
    ''' </summary>
    Private Sub BorrarSeleccionados()

        'Ejecuta si se puede
        If MiHabilitarBorrar Then

            'Obtener confirmación del usuario
            If MessageBox.Show("¿Está seguro que desea borrar la información seleccionada?", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = System.Windows.Forms.DialogResult.Yes Then

                'Notifica la operación completa
                Dim Argumentos = New System.ComponentModel.CancelEventArgs()
                RaiseEvent AntesBorrarSeleccionados(Me, Argumentos)
                If Not Argumentos.Cancel Then

                    'Recorre las entidades para borrarlas
                    Dim Entidad As IEntidad = Nothing
                    Dim Renglones = VistaCatalogo.GetSelectedRows()
                    For Each Renglon In Renglones
                        Entidad = DirectCast(VistaCatalogo.GetRow(Renglon), IEntidad)

                        'Realiza operación notificando
                        Dim ArgumentosInternos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(Entidad)
                        RaiseEvent AntesBorrarEntidad(Me, ArgumentosInternos)
                        If Not ArgumentosInternos.Cancel Then
                            MiFabrica.Borrar(Entidad)
                            RaiseEvent DespuesBorrarEntidad(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Entidad))
                        End If

                    Next

                    'Crea una tarjeta para actualizar el contenido
                    If Entidad IsNot Nothing Then
                        Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(Entidad.GetType()))
                        Dim Tarjeta = CType(Activator.CreateInstance(EntidadParaCatalogo.TipoTarjeta), ITarjeta)
                        VentanaPadre.ActualizaPestañasAfectadas(Tarjeta.VentanasAfectadas)
                        RaiseEvent DespuesVentanaPadreActualizoVentana(Me, EventArgs.Empty)
                    End If

                    'Notifica final de operación
                    RaiseEvent DespuesBorrarSeleccionados(Me, EventArgs.Empty)

                End If

            End If

        End If

    End Sub

    Private Sub HiloBorrar_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloBorrar.DoWork

    End Sub

    Private Sub HiloBorrar_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloBorrar.RunWorkerCompleted

    End Sub

    ''' <summary>
    ''' Muestra el detalle de la entidad seleccionada.
    ''' </summary>
    Private Sub VerDetalleSeleccionado()

        'Ejecuta si se puede
        If MiPuedeVerDetalle And MiHabilitarVerDetalles Then

            'Determina que exista únicamente 1 entidad seleccionada
            Dim Renglon = VistaCatalogo.GetSelectedRows()
            If Renglon.Length = 1 Then

                'Notifica antes de realizar la acción sobre la entidad seleccionada
                Dim Entidad = CType(VistaCatalogo.GetRow(Renglon(0)), IEntidad)
                Dim Argumentos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(Entidad)
                RaiseEvent AntesVerDetalleSeleccionado(Me, Argumentos)

                'Muestra los detalles de la entidad seleccionada de ser posible
                If Not Argumentos.Cancel Then
                    'Using Dialogo = New FormaVerDetallesEntidad()
                    '    Dialogo.MostrarVentana(Entidad, Me)
                    'End Using
                End If

                'Notifica después de realizar la acción
                RaiseEvent DespuesVerDetalleSeleccionado(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Entidad))

            End If

        End If

    End Sub

    Private Sub HiloVerDetalles_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloVerDetalles.DoWork

    End Sub

    Private Sub HiloVerDetalles_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloVerDetalles.RunWorkerCompleted

    End Sub

    ''' <summary>
    ''' Actualiza la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <remarks>Esta función utiliza la fábrica de la entidad raíz y el filtro pre-definido para realizar la consulta. La función tiene un evento Antes y un evento Despues.</remarks>
    Public Overrides Sub ActualizarVentana() Implements ICatalogo.ActualizarVentana

        'Realiza la actualizacion cuando existen entidades visibles de algún tipo
        If MiHabilitarActualizar And MisEntidadesVisibles <> EntidadesVisibles.Ninguna Then

            'Inicia evento y continua si no se canceló
            Dim Argumentos = New System.ComponentModel.CancelEventArgs(False)
            RaiseEvent AntesActualizarVentana(Me, Argumentos)
            If Not Argumentos.Cancel Then

                'Inicia proceso
                UseWaitCursor = True
                MostrarMensaje(String.Format("<color=HotTrack><size=11><b>Cargando {0}</b></size></color><br><size=8>Espere un momento por favor ...</size>", Text.ToLower()), My.Resources.Imagen48_Actualizar)
                VistaCatalogo.BeginDataUpdate()
                MiFiltro = New Dictionary(Of String, Object)
                DefinirFiltro(MiFiltro)

                'Ejecuta el hilo
                HiloActualizarVentana.RunWorkerAsync()

            Else

                'Asegurar la actualización de la barra de herramientas
                ActualizarBarraHerramientas()

            End If

        End If

    End Sub

    ''' <summary>
    ''' Realiza la consulta a la base de datos a través del hilo.
    ''' </summary>
    Private Sub HiloActualizarVentana_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloActualizarVentana.DoWork

        'Carga lista de entidades
        Dim Datos = New List(Of Object)

        'Si se trata del catálogo de boletos realizamos la consulta directa
        If EntidadesParaCatalogo(0).TipoEntidad.Name <> "CortesBoletos" Then
            Try
                If MiIncluirDatosRelacionados Then
                    Datos = MiFabrica.ObtenerExistentes(MiFiltro)
                Else
                    Datos = MiFabrica.ObtenerExistentesParaSerializar(MiFiltro)
                End If
            
            Catch ex As SqlClient.SqlException
                If ex.ErrorCode = -2146232060 And ex.Class = 20 Then
                    MessageBox.Show(String.Format("Error al obtener la información, se encontró un error relacionado con la conexión de red. Favor de revizar su conexión de red. {0}", ex.Message), "Problema para obtener información", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    MessageBox.Show(String.Format("Error al obtener la información. {0}", ex.Message), "Problema para obtener información", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Catch ex As Exception
                MessageBox.Show(String.Format("Error al obtener la información. {0}", ex.Message), "Problema para obtener información", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Select Case MisEntidadesVisibles
                Case EntidadesVisibles.Existentes
                    Datos = Datos.FindAll(Function(Item) DirectCast(Item, IEntidad).CuentaEliminadoEntidad = 0)
                Case EntidadesVisibles.Borradas
                    Datos = Datos.FindAll(Function(Item) DirectCast(Item, IEntidad).CuentaEliminadoEntidad > 0)
            End Select

            'Else

            '    'Creamos la conexión y la ejecución del procedimiento almacenado
            '    Dim Inicio = CDate(MiFiltro("FechaHoraInicio")).ToUniversalTime
            '    Dim Fin = CDate(MiFiltro("FechaHoraFin"))
            '    Dim da As SqlClient.SqlDataAdapter
            '    Dim ds As New DataSet
            '    Dim DataTableBoletosClon As DataTable
            '    Dim CadenaConexionSQLServer As String = Zeus.Cliente.UX.Sincronizacion.CadenaConexionSQLServer
            '    Dim con As New SqlClient.SqlConnection(CadenaConexionSQLServer)
            '    Dim cmd As New SqlClient.SqlCommand("SP_ConsultaBoletos_V1", con)
            '    cmd.CommandType = CommandType.StoredProcedure

            '    Try
            '        con.Open()
            '        cmd.CommandTimeout = Integer.MaxValue
            '        cmd.Connection = con
            '        cmd.Parameters.Add("@FHInicio", SqlDbType.DateTime).Value = Inicio
            '        cmd.Parameters.Add("@FHFin", SqlDbType.DateTime).Value = Fin
            '        cmd.Parameters.Add("@Cliente", SqlDbType.NVarChar).Value = MiFiltro("GuidZeusCliente").ToString

            '        da = New SqlClient.SqlDataAdapter(cmd)

            '        da.Fill(ds)

            '        DataTableBoletosClon = ds.Tables(0).Clone
            '        DataTableBoletosClon.Columns(17).DataType = GetType(String)

            '        Dim Resultado As String = ""

            '        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '            Dim RowACopiar As DataRow = ds.Tables(0).Rows(i)
            '            DataTableBoletosClon.ImportRow(RowACopiar)

            '            'Verifica si contiene el metadato "Verifica información"
            '            If (CType(RowACopiar(17), Integer) And 16) = 16 Then
            '                If (CType(RowACopiar(17), Integer) And 8) = 8 Then
            '                    Resultado = "Completo"
            '                Else
            '                    Resultado = "Incompleto"
            '                End If
            '            End If

            '            If CType(RowACopiar(16), String) = "Cancelado" Then
            '                DataTableBoletosClon.Rows(i).SetField(6, CType(RowACopiar(6), Integer) * -1)
            '            End If

            '            DataTableBoletosClon.Rows(i).SetField(17, Resultado)
            '            DataTableBoletosClon.Rows(i).SetField(3, CType(RowACopiar(3), Date).ToLocalTime)
            '            If MostrarNombreDefaulDescuentos Then
            '                If DBNull.Value.Equals(RowACopiar(7)) Then
            '                    DataTableBoletosClon.Rows(i).SetField(7, NombreDefaulDescuentos)
            '                End If
            '            End If
            '        Next

            '        DataTableBoletos = DataTableBoletosClon

            '    Catch ex As Exception
            '    Finally
            '        'Libera todos los objetos utilizados
            '        da.Dispose()
            '        cmd.Dispose()
            '        ds.Dispose()
            '        If DataTableBoletosClon IsNot Nothing Then
            '            DataTableBoletosClon.Dispose()
            '        End If
            '        con.Close()
            '    End Try

        End If

        'Notifica que se tienen los datos
        Dim DatosEntidades = Datos.ConvertAll(Of IEntidad)(Function(Item) CType(Item, IEntidad))
        RaiseEvent DuranteActualizarVentana(Me, New ArgumentosDespuesAccionCatalogo(Of List(Of IEntidad))(DatosEntidades))

        'Regresa la lista de datos resultante
        If EntidadesParaCatalogo(0).TipoEntidad.Name <> "CortesBoletos" Then
            Try
                'FuenteDatos.DataSource = Datos
                e.Result = Datos
            Catch ex As Exception

            End Try

        End If

    End Sub

    ''' <summary>
    ''' Completa el proceso de actualización de la ventana.
    ''' </summary>
    Private Sub HiloActualizarVentana_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloActualizarVentana.RunWorkerCompleted

        'Detecta si hubo error, para propagarlo
        If e.Error IsNot Nothing Then Throw e.Error

        'Asocia los datos con el grid
        If EntidadesParaCatalogo(0).TipoEntidad.Name = "CortesBoletos" Then

            FuenteDatos.DataSource = DataTableBoletos
            DataTableBoletos.Dispose()
            ActualizaSourceCatalogo()
        Else
            GridCatalogo.DataSource = e.Result
            'FuenteDatos.DataSource = e.Result

        End If

        'Termina proceso
        VistaCatalogo.EndDataUpdate()
        VistaCatalogo.ExpandAllGroups()
        ActualizarBarraHerramientas()
        OcultarMensaje()
        UseWaitCursor = False

        'Termina evento
        RaiseEvent DespuesActualizarVentana(Me, EventArgs.Empty)

    End Sub

    ''' <summary>
    ''' Exporta la lista de entidades.
    ''' </summary>
    ''' <param name="MedioExportacion">Determina el medio al que se exportará la información.</param>
    Private Sub ExportarLista(ByVal MedioExportacion As MediosExportacion)

        'Ejecuta si se puede
        If MiPuedeExportar And MiHabilitarExportar Then

            'Notifica el inicio del proceso
            Dim Argumentos = New System.ComponentModel.CancelEventArgs(False)
            RaiseEvent AntesExportarLista(Me, Argumentos)
            If Not Argumentos.Cancel Then

                'Obtener la ruta del archivo a guardar
                Dim EntidadRaiz = CType(Activator.CreateInstance(MiTipoEntidadRaiz), IEntidad)
                Dim Texto = EntidadRaiz.Texto(Cultura)
                With DialogoExportar
                    Select Case MedioExportacion
                        Case MediosExportacion.XLSX
                            .DefaultExt = "xlsx"
                            .Filter = "Archivos de Microsoft Excel|*.xls;*.xlsx|Todos los archivos|*.*"

                        'Case MediosExportacion.XLS
                        '    .DefaultExt = "xls"
                        '    .Filter = "Archivos de Microsoft Excel|*.xls;*.xlsx|Todos los archivos|*.*"

                        Case MediosExportacion.CSV
                            .DefaultExt = "csv"
                            .Filter = "Archivos separados por comas|*.csv|Todos los archivos|*.*"

                        Case MediosExportacion.PDF
                            .DefaultExt = "pdf"
                            .Filter = "Archivos PDF|*.pdf|Todos los archivos|*.*"

                        Case MediosExportacion.HTML
                            .DefaultExt = "html"
                            .Filter = "Archivos de página Web|*.html;*.htm|Todos los archivos|*.*"

                    End Select
                    .FileName = Texto.Plural
                End With
                If DialogoExportar.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then

                    If Not EntidadesParaCatalogo(0).TipoEntidad.Name = "CortesBoletos" Then
                        'Ocultar la columna de imagen
                        VistaCatalogo.Columns("ImagenEntidad").Visible = False
                    End If

                    'Manejar errores al intentar exportar
                    Try

                        'Exportar el contenido del catálogo al medio seleccionado
                        Select Case MedioExportacion
                            Case MediosExportacion.XLSX
                                'Exportar a Excel 2007
                                Dim Opciones = New DevExpress.XtraPrinting.XlsxExportOptions(DevExpress.XtraPrinting.TextExportMode.Value, True, False)
                                Opciones.ExportMode = DevExpress.XtraPrinting.XlsxExportMode.SingleFile
                                GridCatalogo.ExportToXlsx(DialogoExportar.FileName, Opciones)

                            Case MediosExportacion.XLS
                                ''Exportar a Excel 2003
                                'Dim Opciones = New DevExpress.XtraPrinting.XlsExportOptions(DevExpress.XtraPrinting.TextExportMode.Value, True, False, True, True)
                                'Opciones.ExportMode = DevExpress.XtraPrinting.XlsExportMode.SingleFile
                                'GridCatalogo.ExportToXls(DialogoExportar.FileName, Opciones)

                            Case MediosExportacion.CSV
                                'Exportar a texto separado por comas
                                Dim Opciones = New DevExpress.XtraPrinting.TextExportOptions(",", System.Text.Encoding.UTF8, DevExpress.XtraPrinting.TextExportMode.Value)
                                GridCatalogo.ExportToText(DialogoExportar.FileName, Opciones)

                            Case MediosExportacion.PDF
                                'Exportar a PDF
                                Dim Opciones = New DevExpress.XtraPrinting.PdfExportOptions()
                                Opciones.Compressed = True
                                Opciones.DocumentOptions.Application = String.Format("{0} {1}.{2}", My.Application.Info.Title, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)
                                GridCatalogo.ExportToPdf(DialogoExportar.FileName)

                            Case MediosExportacion.HTML
                                'Exportar a HTML
                                Dim Opciones = New DevExpress.XtraPrinting.HtmlExportOptions("UTF-8", Text)
                                Opciones.EmbedImagesInHTML = True
                                Opciones.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFile
                                GridCatalogo.ExportToHtml(DialogoExportar.FileName, Opciones)

                            Case Else
                                'Indicar que el medio no es reconocido
                                Throw New Exception("No se reconoce el formato al que se debe exportar la información. Por favor seleccione un formato válido.")

                        End Select

                        'Notificar el éxito en la exportación
                        If TaskDialog.IsPlatformSupported Then
                            MessageBox.Show(String.Format("La exportación de {0} fue exitosa.", Texto.Plural.ToLower), "Exportación exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            'Using Dialogo = New TaskDialog()
                            '    Dialogo.Icon = TaskDialogStandardIcon.Information
                            '    Dialogo.Caption = "Exportación exitosa"
                            '    Dialogo.InstructionText = String.Format("La exportación de {0} fué exitosa.", Texto.Plural.ToLower)
                            '    Dialogo.FooterText = String.Format("Datos exportados a <a href=""{0}"">{0}</a>", DialogoExportar.FileName)
                            '    Dialogo.Cancelable = True
                            '    Dialogo.HyperlinksEnabled = True
                            '    Dialogo.StandardButtons = TaskDialogStandardButtons.Close
                            '    AddHandler Dialogo.HyperlinkClick, AddressOf DialogoExportar_HyperlinkClick
                            '    Dialogo.Show()
                            'End Using
                        Else
                            MessageBox.Show(String.Format("La exportación de {0} fue exitosa.", Texto.Plural.ToLower), "Exportación exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    Catch ex As System.OutOfMemoryException

                        'Notificar el fallo en la exportación
                        If TaskDialog.IsPlatformSupported Then
                            MessageBox.Show(String.Format("No fue posible exportar {1} por la siguiente razón:{0}{0}{2}{0}{0}Modifique la lista de información aplicando filtros para que el número de renglones a exportar sea menor.", Environment.NewLine, Texto.Plural.ToLower, ex.Message), "Error en la exportación", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'Using Dialogo = New TaskDialog()
                            '    Dialogo.Icon = TaskDialogStandardIcon.Error
                            '    Dialogo.Caption = "Error en la exportación"
                            '    Dialogo.InstructionText = String.Format("No fué posible exportar {1} por la siguiente razón:{0}{0}{2}{0}{0}Modifique la lista de información aplicando filtros para que el número de renglones a exportar sea menor.", Environment.NewLine, Texto.Plural.ToLower, ex.Message)
                            '    Dialogo.Cancelable = True
                            '    Dialogo.StandardButtons = TaskDialogStandardButtons.Close
                            '    Dialogo.Show()
                            'End Using
                        Else
                            MessageBox.Show(String.Format("No fue posible exportar {1} por la siguiente razón:{0}{0}{2}{0}{0}Modifique la lista de información aplicando filtros para que el número de renglones a exportar sea menor.", Environment.NewLine, Texto.Plural.ToLower, ex.Message), "Error en la exportación", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    Catch ex As Exception

                        'Notificar el fallo en la exportación
                        If TaskDialog.IsPlatformSupported Then
                            MessageBox.Show(String.Format("No fue posible exportar {1} por la siguiente razón:{0}{0}{2}", Environment.NewLine, Texto.Plural.ToLower, ex.Message), "Error en la exportación", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'Using Dialogo = New TaskDialog()
                            '    Dialogo.Icon = TaskDialogStandardIcon.Error
                            '    Dialogo.Caption = "Error en la exportación"
                            '    Dialogo.InstructionText = String.Format("No fué posible exportar {1} por la siguiente razón:{0}{0}{2}", Environment.NewLine, Texto.Plural.ToLower, ex.Message)
                            '    Dialogo.Cancelable = True
                            '    Dialogo.StandardButtons = TaskDialogStandardButtons.Close
                            '    Dialogo.Show()
                            'End Using
                        Else
                            MessageBox.Show(String.Format("No fue posible exportar {1} por la siguiente razón:{0}{0}{2}", Environment.NewLine, Texto.Plural.ToLower, ex.Message), "Error en la exportación", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    End Try

                    If Not EntidadesParaCatalogo(0).TipoEntidad.Name = "CortesBoletos" Then
                        'Mostrar la columna de imagen
                        VistaCatalogo.Columns("ImagenEntidad").Visible = True
                    End If

                    'Notifica fin de exportación
                    RaiseEvent DespuesExportarLista(Me, EventArgs.Empty)

                End If

            End If

        End If

    End Sub

    Private Sub HiloExportar_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloExportar.DoWork

    End Sub

    Private Sub HiloExportar_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloExportar.RunWorkerCompleted

    End Sub

    ''' <summary>
    ''' Capta el evento de la liga para abrir el archivo resultante.
    ''' </summary>
    Private Sub DialogoExportar_HyperlinkClick(ByVal sender As Object, ByVal e As TaskDialogHyperlinkClickedEventArgs)

        Process.Start(e.LinkText)

    End Sub

    ''' <summary>
    ''' Destruye las entidades seleccionadas.
    ''' </summary>
    Private Sub DestruirSeleccionados()

        'Ejecuta si se puede
        If MiPuedeDestruir And MiHabilitarDestruir Then

            'Obtener confirmación del usuario
            If MessageBox.Show("¿Está seguro que desea destruir permanentemente la información seleccionada?", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = System.Windows.Forms.DialogResult.Yes Then

                'Notifica operación completa
                Dim Argumentos = New System.ComponentModel.CancelEventArgs()
                RaiseEvent AntesDestruirSeleccionados(Me, Argumentos)
                If Not Argumentos.Cancel Then

                    'Recorre las entidades para destruirlas
                    Dim Entidad As IEntidad = Nothing
                    Dim Renglones = VistaCatalogo.GetSelectedRows()
                    For Each Renglon In Renglones

                        'Realiza operación con notificación
                        Entidad = DirectCast(VistaCatalogo.GetRow(Renglon), IEntidad)
                        Dim ArgumentosInternos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(Entidad)
                        RaiseEvent AntesDestruirEntidad(Me, ArgumentosInternos)
                        If Not ArgumentosInternos.Cancel Then
                            MiFabrica.Destruir(Entidad)
                            RaiseEvent DespuesDestruirEntidad(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Entidad))
                        End If

                    Next

                    'Crea una tarjeta para actualizar el contenido
                    If Entidad IsNot Nothing Then
                        Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(Entidad.GetType()))
                        Dim Tarjeta = CType(Activator.CreateInstance(EntidadParaCatalogo.TipoTarjeta), ITarjeta)
                        VentanaPadre.ActualizaPestañasAfectadas(Tarjeta.VentanasAfectadas)
                        RaiseEvent DespuesVentanaPadreActualizoVentana(Me, EventArgs.Empty)
                    End If

                    'Notifica final de operación
                    RaiseEvent DespuesDestruirSeleccionados(Me, EventArgs.Empty)

                End If

            End If

        End If

    End Sub

    Private Sub HiloDestruir_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles HiloDestruir.DoWork

    End Sub

    Private Sub HiloDestruir_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles HiloDestruir.RunWorkerCompleted

    End Sub

    ''' <summary>
    ''' Recupera las entidades seleccionadas.
    ''' </summary>
    Private Sub RecuperarSeleccionados()

        'Ejecuta si se puede
        If MiPuedeRecuperar And MiHabilitarRecuperar Then

            'Obtener confirmación del usuario
            If MessageBox.Show("¿Está seguro que desea recuperar la información seleccionada?", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = System.Windows.Forms.DialogResult.Yes Then

                'Notifica operación completa
                Dim Argumentos = New System.ComponentModel.CancelEventArgs()
                RaiseEvent AntesRecuperarSeleccionados(Me, Argumentos)
                If Not Argumentos.Cancel Then

                    'Recorre las entidades para recuperarlas
                    Dim Entidad As IEntidad = Nothing
                    Dim Renglones = VistaCatalogo.GetSelectedRows()
                    For Each Renglon In Renglones

                        'Realiza operación con notificaciones
                        Entidad = DirectCast(VistaCatalogo.GetRow(Renglon), IEntidad)
                        Dim ArgumentosInternos = New ArgumentosAntesAccionCatalogo(Of IEntidad)(Entidad)
                        RaiseEvent AntesRecuperarEntidad(Me, ArgumentosInternos)
                        If Not ArgumentosInternos.Cancel Then
                            MiFabrica.Recuperar(Entidad)
                            RaiseEvent DespuesRecuperarEntidad(Me, New ArgumentosDespuesAccionCatalogo(Of IEntidad)(Entidad))
                        End If

                    Next

                    'Crea una tarjeta para actualizar el contenido
                    If Entidad IsNot Nothing Then
                        Dim EntidadParaCatalogo = MisEntidades.Find(Function(Item) Item.TipoEntidad.Equals(Entidad.GetType()))
                        Dim Tarjeta = CType(Activator.CreateInstance(EntidadParaCatalogo.TipoTarjeta), ITarjeta)
                        VentanaPadre.ActualizaPestañasAfectadas(Tarjeta.VentanasAfectadas)
                        RaiseEvent DespuesVentanaPadreActualizoVentana(Me, EventArgs.Empty)
                    End If

                    'Notifica fin operación completa
                    RaiseEvent DespuesRecuperarSeleccionados(Me, EventArgs.Empty)

                End If

            End If

        End If

    End Sub

    ''' <summary>
    ''' Muestra un mensaje en la tira de mensajes del catálogo.
    ''' </summary>
    ''' <param name="Mensaje">El mensaje específico a mostrar.</param>
    ''' <param name="Imagen">La imagen a utilizar.</param>
    Protected Sub MostrarMensaje(ByVal Mensaje As String, ByVal Imagen As Image)

        'Definir mensaje e imagen
        EtiquetaMensaje.Text = Mensaje
        ImagenMensaje.Image = Imagen
        MiEstaOcupado += 1

        'Preparar información de control de tiempo
        MiTiempoInicioProgreso = Date.Now.Ticks
        Dim DiferenciaTiempo = New Date(MiTiempoInicioProgreso) - New Date(MiTiempoInicioProgreso)
        EtiquetaTiempoMensaje.Text = String.Format("{0:D2}:{1:D2}:{2:D2}", DiferenciaTiempo.Hours, DiferenciaTiempo.Minutes, DiferenciaTiempo.Seconds)
        RelojProgreso.Enabled = True

        'Si es el primer proceso que está ocupado, modificar la interface visual
        If MiEstaOcupado = 1 Then
            VentanaPadre.IniciaTrabajoPestaña()
            DefinirProgresoMensaje(0)
            PanelMensaje.Visible = True
            PanelFiltro.Enabled = False
            GridCatalogo.Enabled = False
            ActualizarBarraHerramientas()
        End If

    End Sub

    ''' <summary>
    ''' Oculta el mensaje en la tira de mensajes del catálogo.
    ''' </summary>
    ''' <remarks>Si el catálogo estaba ocupado, quedará desocupado.</remarks>
    Protected Sub OcultarMensaje()

        'Determinar si es el último proceso, para modificar la interface visual
        MiEstaOcupado -= 1
        If MiEstaOcupado = 0 Then
            RelojProgreso.Enabled = False
            PanelMensaje.Visible = False
            PanelFiltro.Enabled = True
            GridCatalogo.Enabled = True
            ActualizarBarraHerramientas()
            VentanaPadre.TerminaTrabajoPestaña()
        End If

    End Sub

    ''' <summary>
    ''' Cronometra el paso del tiempo para un progreso.
    ''' </summary>
    Private Sub RelojProgreso_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelojProgreso.Tick

        Dim DiferenciaTiempo = Date.Now - New Date(MiTiempoInicioProgreso)
        EtiquetaTiempoMensaje.Text = String.Format("{0:D2}:{1:D2}:{2:D2}", DiferenciaTiempo.Hours, DiferenciaTiempo.Minutes, DiferenciaTiempo.Seconds)

    End Sub

    ''' <summary>
    ''' Delegado para definir el valor máximo del progreso conocido.
    ''' </summary>
    ''' <param name="ValorMaximo">El valor máximo de progreso. Si el valor es 0, la barra de progreso no es visible. Al definir este dato, el valor de la barra de progreso se reinicia en 0.</param>
    Protected Delegate Sub DefinirProgresoMensajeDelegado(ByVal ValorMaximo As Integer)

    ''' <summary>
    ''' Define el valor máximo del progreso conocido.
    ''' </summary>
    ''' <param name="ValorMaximo">El valor máximo de progreso. Si el valor es 0, la barra de progreso no es visible. Al definir este dato, el valor de la barra de progreso se reinicia en 0.</param>
    Protected Sub DefinirProgresoMensaje(ByVal ValorMaximo As Integer)

        'Determina si se requiere invocar
        If ProgresoDefinidoMensaje.InvokeRequired Then
            ProgresoDefinidoMensaje.Invoke(New DefinirProgresoMensajeDelegado(AddressOf DefinirProgresoMensaje), New Object() {ValorMaximo})
            Exit Sub
        End If

        'Realiza los cambios en caso de ser necesario
        If MiEstaOcupado > 0 Then
            With ProgresoDefinidoMensaje
                .EditValue = 0
                .Properties.Maximum = ValorMaximo
                ItemForProgresoDefinidoMensaje.Visibilidad(ValorMaximo > 0)
                ItemForProgresoMensaje.Visibilidad(ValorMaximo = 0)
            End With
        End If

    End Sub

    ''' <summary>
    ''' Aumenta el avance de la barra de progreso conocida.
    ''' </summary>
    Protected Sub IncrementarProgresoMensaje()

        IncrementarProgresoMensaje(1)

    End Sub

    ''' <summary>
    ''' Delegado para aumentar el avance de la barra de progreso conocida.
    ''' </summary>
    ''' <param name="Cantidad">La cantidad de "pasos" a incrementar.</param>
    Protected Delegate Sub IncrementarProgresoMensajeDelegado(ByVal Cantidad As Integer)

    ''' <summary>
    ''' Aumenta el avance de la barra de progreso conocida.
    ''' </summary>
    ''' <param name="Cantidad">La cantidad de "pasos" a incrementar.</param>
    Protected Sub IncrementarProgresoMensaje(ByVal Cantidad As Integer)

        'Determina si se requiere invocar
        If ProgresoDefinidoMensaje.InvokeRequired Then
            ProgresoDefinidoMensaje.Invoke(New IncrementarProgresoMensajeDelegado(AddressOf IncrementarProgresoMensaje), New Object() {Cantidad})
            Exit Sub
        End If

        'Realiza el incremento de ser necesario
        If MiEstaOcupado > 0 Then
            With ProgresoDefinidoMensaje
                .Increment(Cantidad)
            End With
        End If

    End Sub

    'Pobla el grid con los datos obtenidos y da formato a algunos campos
    Public Sub ActualizaSourceCatalogo()

        If Me.EtiquetaMensaje.InvokeRequired Then
            EtiquetaMensaje.Invoke(New DelegadoActualizaSourceCatalogo(AddressOf ActualizaSourceCatalogo))
        Else
            'Asocia los datos con el grid
            VistaCatalogo.PopulateColumns(DataTableBoletos)

            VistaCatalogo.Columns(3).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            VistaCatalogo.Columns(3).DisplayFormat.FormatString = "d/MMM/yyyy h:mm:ss tt"

            VistaCatalogo.Columns(4).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            VistaCatalogo.Columns(4).DisplayFormat.FormatString = "c2"

            VistaCatalogo.Columns(5).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            VistaCatalogo.Columns(5).DisplayFormat.FormatString = "c2"

            VistaCatalogo.Columns(6).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            VistaCatalogo.Columns(6).DisplayFormat.FormatString = "c2"

            VistaCatalogo.Columns(8).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            VistaCatalogo.Columns(8).DisplayFormat.FormatString = "p0"

            VistaCatalogo.Columns(2).SummaryItem.SetSummary(DevExpress.Data.SummaryItemType.Count, "Boletos: {0:n0}")
            VistaCatalogo.Columns(6).SummaryItem.SetSummary(DevExpress.Data.SummaryItemType.Sum, "Total: {0:c2}")
            VistaCatalogo.Columns(8).SummaryItem.SetSummary(DevExpress.Data.SummaryItemType.Average, "Promedio: {0:p0}")

            VistaCatalogo.Columns(18).Visible = False
            VistaCatalogo.Columns(0).Group()
            VistaCatalogo.Columns(1).Group()
        End If

    End Sub

    Private Sub VistaCatalogo_RowStyle(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles VistaCatalogo.RowStyle
        If RowConBackColor Then
            Dim View As DevExpress.XtraGrid.Views.Grid.GridView = sender
            'If (e.RowHandle >= 0) Then
            Dim valor = View.GetRowCellDisplayText(e.RowHandle, View.Columns(11))
            If valor.Equals("Fuera del pedido") Then
                e.Appearance.BackColor = Color.Salmon
                'e.Appearance.BackColor2 = Color.SeaShell
            End If
            'End If
        End If
    End Sub
End Class