﻿Imports System.ComponentModel

Public Class TarjetaUnidad_Negocio
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoUnidad_Negocio  ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Unidad_NegocioCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Unidad_NegocioCampos.Nombre, itemNombre)
            .Add(Datos.Unidad_NegocioCampos.Descripcion, itemDescripcion)
            .Add(Datos.Unidad_NegocioCampos.Estatus , ItemEstatus )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Unidad_NegocioCampos )(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Unidad_Negocio )()
        Select Case sender.Name
            Case TextoNombre.Name
                Paquete.Nombre = TextoNombre.Text.Trim
            Case TextoNombre.Name
                Paquete.Descripcion = TextoDescripcion.Text.Trim
            CASE ComboEstatus .name 
                Paquete .Estatus = CByte(ComboEstatus .SelectedIndex)

        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Unidad_Negocio )()
        If paquete.Estatus = 0 Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Unidad_Negocio )()
        'Paquete.Estatus  = CByte(ComboEstatus.SelectedIndex)
    End Sub
End Class