﻿Public Class NRFTsys

    Private MiListaIncidenciasRecibidas As New List(Of Datos.Reclamo )

    Private MiListaIncidenciasReportadas As New List(Of Datos.Reclamo)

    Private MiCadenaConexión As String

    Private IdReclamo As Integer 

    Private Sub NRFTsys_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        MiCadenaConexión =Datos.Fabricas .Usuario_Unidad_Negocio .CadenaConexionSQLServer
        If Datos.SesiónLocal.Usuario IsNot Nothing Then
            EtiquetaUsuario.Text = Datos.SesiónLocal.Usuario.Nombre
            EtiquetaRol.Text = Datos.SesiónLocal.Usuario.TipoUsuario.Nombre_Tipo_Usuario
        End If

        mostrarReportadas()

    End Sub

    Private Sub btnReportar_Click(sender As System.Object, e As System.EventArgs) Handles btnReportar.Click
        Using reportar = New NRFTNueva
            
            reportar.ShowDialog()
        End Using
        mostrarReportadas()
    End Sub

    Private Sub BotonAtender_Click(sender As System.Object, e As System.EventArgs) Handles BotonAtender.Click
        MostarAtender
    End Sub

    Private sub MostarAtender
         Using reportar = New NRFTAtender
            reportar.idReclamo=IdReclamo
            reportar.ShowDialog()
        End Using
        mostrarReportadas()
    End sub


    Private Sub mostrarReportadas()

        If Datos.SesiónLocal .Usuario IsNot Nothing Then
            If Datos.SesiónLocal.Usuario.TipoUsuario.Nombre_Tipo_Usuario.Trim = "SICAIP CALIDAD ADMINISTRADOR" Then
                MiListaIncidenciasReportadas.Clear()

                Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                    Dim MiListaIncidenciasReportada2s = From reclamo In MiModelo.Reclamos
                                                        Where reclamo.estatus = 0
                    MiListaIncidenciasReportadas = MiListaIncidenciasReportada2s.ToList
                End Using

                GridRecibidas.DataSource = ObtenerListadoIncidencias()
                GridReportadas.DataSource = ObtenerListadoIncidencias()

            Else
                MiListaIncidenciasReportadas.Clear
                Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                    Dim Query = From reclamo In MiModelo.Reclamos
                                Where reclamo.cve_usuario_reporta = Datos.SesiónLocal.Usuario.CVE_Usuario
                    MiListaIncidenciasReportadas = Query.ToList
                End Using

                Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                    Dim Query = From reclamo In MiModelo.Reclamos
                                Join UUN In MiModelo .Cliente_Modelos  On UUN.cve_cliente_modelo  Equals reclamo.cve_cliente_modelo
                                Join modelo in mimodelo.modelos On UUN.cve_modelo Equals modelo.cve_modelo 
                                Join component In MiModelo .componentes On modelo .cve_componente Equals component .cve_componente 
                                Join cv In MiModelo.cadena_valors On component .cve_cadena_valor Equals cv.cve_cadena_valor 
                                Join ucv In MiModelo .USUARIO_CADENA_VALORs On cv.cve_cadena_valor Equals ucv.CVE_Cadena_Valor 
                                where ucv.CVE_Usuario  =Datos.SesiónLocal .Usuario.CVE_Usuario 
                                Select reclamo 
                    MiListaIncidenciasRecibidas  = Query.ToList
                End Using

                GridRecibidas.DataSource = ObtenerListadoIncidenciasRecibidas()
                GridReportadas.DataSource = ObtenerListadoIncidencias()
            End If
        End If
       
    End Sub

    Private Function ObtenerListadoIncidencias()  As List(Of Datos.Incidencia )
        Dim MiListaIncidencia As  New List(Of Datos.Incidencia )
        For Each  incidenciaReportada in MiListaIncidenciasReportadas
            Dim MiIncidencia= New Datos.Incidencia 
            MiIncidencia .Folio = incidenciaReportada .cve_reclamo .ToString .PadLeft (5,CChar ("0"))
            MiIncidencia.FechaReporte =incidenciaReportada .fecha_registro
            Dim MiDefecto = Datos.Fabricas .Defecto .ObtenerUno (incidenciaReportada .cve_defecto)
            If MiDefecto IsNot Nothing Then
                MiIncidencia.Seccion = MiDefecto.nombre
                MiIncidencia.Atributo = MiDefecto.descripcion
            End If
            If incidenciaReportada.Component IsNot Nothing Then
                MiIncidencia.Componente = incidenciaReportada.Component.componente
            End If
            MiIncidencia.Detalles =incidenciaReportada.detalle
            MiIncidencia.Comentarios = incidenciaReportada.comentarios

            '************************Obtener unidad de negocio
            Dim Filtro = New Dictionary(Of String, Object)
            Dim noParte As List(Of Datos.Modelo)
            Filtro.Clear()
            Filtro.Add("cve_cliente_modelo", incidenciaReportada.cve_cliente_modelo)
            noParte = Datos.Fabricas.Modelo.ObtenerExistentes(Filtro)
            If noParte IsNot Nothing AndAlso noParte.Count > 0 Then
                Dim compo = Datos.Fabricas.Componente.ObtenerUno(noParte(0).cve_componente)
                Filtro.Clear()
                Filtro.Add("cve_cliente_modelo", incidenciaReportada.cve_cliente_modelo)
                Dim Cliente = Datos.Fabricas.Cliente.ObtenerExistentes(Filtro)

                Dim MiUnidadNegocio As Datos.Unidad_Negocio
                If compo IsNot Nothing Then
                    Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                        MiUnidadNegocio = (From unidad In MiModelo.Unidad_Negocios
                                           Join componenteunidad In MiModelo.Componente_Unidad_Negocios
                                           On unidad.cve_unidad_negocio Equals componenteunidad.cve_unidad_negocio
                                           Where componenteunidad.cve_componente = compo.cve_componente
                                           Select unidad).FirstOrDefault
                    End Using
                    If MiUnidadNegocio IsNot Nothing Then
                        MiIncidencia.UnidadNegocio = MiUnidadNegocio.nombre
                    Else
                        MiIncidencia.UnidadNegocio = "No hay unidad de negocio asignada"
                    End If
                End If

            End If


            MiListaIncidencia.Add (MiIncidencia )
        Next
        Return MiListaIncidencia 
    End Function


    Private Function ObtenerListadoIncidenciasRecibidas() As List(Of Datos.Incidencia)
        Dim MiListaIncidencia As New List(Of Datos.Incidencia)
        For Each incidenciaReportada In MiListaIncidenciasRecibidas
            Dim MiIncidencia = New Datos.Incidencia
            MiIncidencia.Folio = incidenciaReportada.cve_reclamo.ToString.PadLeft(5, CChar("0"))
            MiIncidencia.FechaReporte = incidenciaReportada.fecha_registro
            Dim MiDefecto = Datos.Fabricas.Defecto.ObtenerUno(incidenciaReportada.cve_defecto)
            If MiDefecto IsNot Nothing Then
                MiIncidencia.Seccion = MiDefecto.nombre
                MiIncidencia.Atributo = MiDefecto.descripcion
            End If
            If incidenciaReportada.Component IsNot Nothing Then
                MiIncidencia.Componente = incidenciaReportada.Component.componente
            End If

            MiIncidencia.Detalles = incidenciaReportada.detalle
            MiIncidencia.Comentarios = incidenciaReportada.comentarios
            MiListaIncidencia.Add(MiIncidencia)
        Next
        Return MiListaIncidencia
    End Function


    ''' <summary>
    ''' Realiza acciones después de cambiar la selección del grid.
    ''' </summary>
    Private Sub VistaRecibidas_RowClick(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles VistaRecibidas.RowClick

        'Determina que exista únicamente 1 entidad seleccionada
        Dim Renglon = VistaRecibidas.GetSelectedRows
        If Renglon.Length = 1 Then
            'Obtiene entidad y crea tarjeta
            Dim Entidad = CType(VistaRecibidas.GetRow(Renglon(0)), Datos.Incidencia)
            IdReclamo = CInt(Entidad.Folio)
        Else
            IdReclamo = 0
            BotonAtender.Enabled = False
        End If
        'Determina el número de clics realizados
        Select Case e.Clicks

            Case 1

                BotonAtender.Enabled = True
            Case 2
                '2 clics representa editar

                MostarAtender()

        End Select

    End Sub


End Class