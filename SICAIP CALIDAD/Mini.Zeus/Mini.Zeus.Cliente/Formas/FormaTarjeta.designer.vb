﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormaTarjeta
    Inherits FormaDialogo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BotonAceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutAcciones = New DevExpress.XtraLayout.LayoutControl()
        Me.EtiquetaCopyright = New DevExpress.XtraEditors.LabelControl()
        Me.LigaAyuda = New DevExpress.XtraEditors.HyperLinkEdit()
        Me.BotonCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutAccionesRaiz = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.ItemForBotonCancelar = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EspacioAcciones = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ItemForBotonAceptar = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemForLigaAyuda = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EspacioAccionesRelleno = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.FuenteDatos = New System.Windows.Forms.BindingSource(Me.components)
        Me.ManejadorValidacion = New DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(Me.components)
        Me.PanelEncabezado = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutEncabezado = New DevExpress.XtraLayout.LayoutControl()
        Me.EtiquetaEncabezado = New DevExpress.XtraEditors.LabelControl()
        Me.ImagenEncabezado = New System.Windows.Forms.PictureBox()
        Me.LayoutEncabezadoRaiz = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.ItemForImagenEncabezado = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemForEtiquetaEncabezado = New DevExpress.XtraLayout.LayoutControlItem()
        Me.PanelAcciones = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutTarjetaRaiz = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutTarjeta = New DevExpress.XtraDataLayout.DataLayoutControl()
        CType(Me.LayoutAcciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutAcciones.SuspendLayout()
        CType(Me.LigaAyuda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutAccionesRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForBotonCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioAcciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForBotonAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForLigaAyuda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioAccionesRelleno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelEncabezado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelEncabezado.SuspendLayout()
        CType(Me.LayoutEncabezado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutEncabezado.SuspendLayout()
        CType(Me.ImagenEncabezado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutEncabezadoRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForImagenEncabezado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForEtiquetaEncabezado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelAcciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelAcciones.SuspendLayout()
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BotonAceptar
        '
        Me.BotonAceptar.Location = New System.Drawing.Point(285, 31)
        Me.BotonAceptar.Name = "BotonAceptar"
        Me.BotonAceptar.Size = New System.Drawing.Size(68, 29)
        Me.BotonAceptar.StyleController = Me.LayoutAcciones
        Me.BotonAceptar.TabIndex = 5
        Me.BotonAceptar.Text = "Aceptar"
        '
        'LayoutAcciones
        '
        Me.LayoutAcciones.AllowCustomization = False
        Me.LayoutAcciones.Controls.Add(Me.EtiquetaCopyright)
        Me.LayoutAcciones.Controls.Add(Me.LigaAyuda)
        Me.LayoutAcciones.Controls.Add(Me.BotonCancelar)
        Me.LayoutAcciones.Controls.Add(Me.BotonAceptar)
        Me.LayoutAcciones.Dock = System.Windows.Forms.DockStyle.Top
        Me.LayoutAcciones.Location = New System.Drawing.Point(0, 0)
        Me.LayoutAcciones.Name = "LayoutAcciones"
        Me.LayoutAcciones.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(796, 76, 450, 400)
        Me.LayoutAcciones.Root = Me.LayoutAccionesRaiz
        Me.LayoutAcciones.Size = New System.Drawing.Size(453, 70)
        Me.LayoutAcciones.TabIndex = 0
        Me.LayoutAcciones.Text = "LayoutControl1"
        '
        'EtiquetaCopyright
        '
        Me.EtiquetaCopyright.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaCopyright.Appearance.Options.UseFont = True
        Me.EtiquetaCopyright.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaCopyright.Location = New System.Drawing.Point(10, 10)
        Me.EtiquetaCopyright.Name = "EtiquetaCopyright"
        Me.EtiquetaCopyright.Size = New System.Drawing.Size(433, 11)
        Me.EtiquetaCopyright.StyleController = Me.LayoutAcciones
        Me.EtiquetaCopyright.TabIndex = 7
        Me.EtiquetaCopyright.Text = "[Trademark | Copyright]"
        '
        'LigaAyuda
        '
        Me.LigaAyuda.EditValue = "¿Por qué no puedo guardar?"
        Me.LigaAyuda.Location = New System.Drawing.Point(10, 31)
        Me.LigaAyuda.Name = "LigaAyuda"
        Me.LigaAyuda.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LigaAyuda.Properties.Appearance.Options.UseBackColor = True
        Me.LigaAyuda.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LigaAyuda.Properties.Image = Global.Mini.Zeus.Cliente.UX.My.Resources.Resources.Imagen16_GloboAyuda
        Me.LigaAyuda.Size = New System.Drawing.Size(162, 22)
        Me.LigaAyuda.StyleController = Me.LayoutAcciones
        Me.LigaAyuda.TabIndex = 6
        '
        'BotonCancelar
        '
        Me.BotonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BotonCancelar.Location = New System.Drawing.Point(373, 31)
        Me.BotonCancelar.Name = "BotonCancelar"
        Me.BotonCancelar.Size = New System.Drawing.Size(70, 29)
        Me.BotonCancelar.StyleController = Me.LayoutAcciones
        Me.BotonCancelar.TabIndex = 4
        Me.BotonCancelar.Text = "Cancelar"
        '
        'LayoutAccionesRaiz
        '
        Me.LayoutAccionesRaiz.CustomizationFormText = "LayoutAccionesRaiz"
        Me.LayoutAccionesRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutAccionesRaiz.GroupBordersVisible = False
        Me.LayoutAccionesRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.ItemForBotonCancelar, Me.EspacioAcciones, Me.ItemForBotonAceptar, Me.ItemForLigaAyuda, Me.EspacioAccionesRelleno, Me.LayoutControlItem1})
        Me.LayoutAccionesRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutAccionesRaiz.Name = "Root"
        Me.LayoutAccionesRaiz.Padding = New DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5)
        Me.LayoutAccionesRaiz.Size = New System.Drawing.Size(453, 70)
        Me.LayoutAccionesRaiz.TextVisible = False
        '
        'ItemForBotonCancelar
        '
        Me.ItemForBotonCancelar.Control = Me.BotonCancelar
        Me.ItemForBotonCancelar.CustomizationFormText = "ItemForBotonCancelar"
        Me.ItemForBotonCancelar.Location = New System.Drawing.Point(363, 21)
        Me.ItemForBotonCancelar.MaxSize = New System.Drawing.Size(80, 0)
        Me.ItemForBotonCancelar.MinSize = New System.Drawing.Size(80, 34)
        Me.ItemForBotonCancelar.Name = "ItemForBotonCancelar"
        Me.ItemForBotonCancelar.Size = New System.Drawing.Size(80, 39)
        Me.ItemForBotonCancelar.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForBotonCancelar.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForBotonCancelar.TextVisible = False
        '
        'EspacioAcciones
        '
        Me.EspacioAcciones.AllowHotTrack = False
        Me.EspacioAcciones.CustomizationFormText = "EspacioAccionesEntreBotones"
        Me.EspacioAcciones.Location = New System.Drawing.Point(353, 21)
        Me.EspacioAcciones.MaxSize = New System.Drawing.Size(10, 0)
        Me.EspacioAcciones.MinSize = New System.Drawing.Size(10, 10)
        Me.EspacioAcciones.Name = "EspacioAcciones"
        Me.EspacioAcciones.Size = New System.Drawing.Size(10, 39)
        Me.EspacioAcciones.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EspacioAcciones.TextSize = New System.Drawing.Size(0, 0)
        '
        'ItemForBotonAceptar
        '
        Me.ItemForBotonAceptar.Control = Me.BotonAceptar
        Me.ItemForBotonAceptar.CustomizationFormText = "ItemForBotonAceptar"
        Me.ItemForBotonAceptar.Location = New System.Drawing.Point(275, 21)
        Me.ItemForBotonAceptar.MaxSize = New System.Drawing.Size(78, 0)
        Me.ItemForBotonAceptar.MinSize = New System.Drawing.Size(78, 34)
        Me.ItemForBotonAceptar.Name = "ItemForBotonAceptar"
        Me.ItemForBotonAceptar.Size = New System.Drawing.Size(78, 39)
        Me.ItemForBotonAceptar.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForBotonAceptar.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForBotonAceptar.TextVisible = False
        '
        'ItemForLigaAyuda
        '
        Me.ItemForLigaAyuda.Control = Me.LigaAyuda
        Me.ItemForLigaAyuda.CustomizationFormText = "LayoutControlItem1"
        Me.ItemForLigaAyuda.Location = New System.Drawing.Point(0, 21)
        Me.ItemForLigaAyuda.MaxSize = New System.Drawing.Size(172, 26)
        Me.ItemForLigaAyuda.MinSize = New System.Drawing.Size(172, 26)
        Me.ItemForLigaAyuda.Name = "ItemForLigaAyuda"
        Me.ItemForLigaAyuda.Size = New System.Drawing.Size(172, 39)
        Me.ItemForLigaAyuda.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForLigaAyuda.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForLigaAyuda.TextVisible = False
        '
        'EspacioAccionesRelleno
        '
        Me.EspacioAccionesRelleno.AllowHotTrack = False
        Me.EspacioAccionesRelleno.CustomizationFormText = "EspacioAccionesRelleno"
        Me.EspacioAccionesRelleno.Location = New System.Drawing.Point(172, 21)
        Me.EspacioAccionesRelleno.Name = "EspacioAccionesRelleno"
        Me.EspacioAccionesRelleno.Size = New System.Drawing.Size(103, 39)
        Me.EspacioAccionesRelleno.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.EtiquetaCopyright
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(443, 21)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'FuenteDatos
        '
        Me.FuenteDatos.AllowNew = False
        '
        'ManejadorValidacion
        '
        Me.ManejadorValidacion.ContainerControl = Me
        '
        'PanelEncabezado
        '
        Me.PanelEncabezado.Appearance.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.PanelEncabezado.Appearance.BackColor2 = System.Drawing.SystemColors.GradientActiveCaption
        Me.PanelEncabezado.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanelEncabezado.Appearance.Options.UseBackColor = True
        Me.PanelEncabezado.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelEncabezado.Controls.Add(Me.LayoutEncabezado)
        Me.PanelEncabezado.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelEncabezado.Location = New System.Drawing.Point(0, 0)
        Me.PanelEncabezado.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelEncabezado.LookAndFeel.UseWindowsXPTheme = True
        Me.PanelEncabezado.Name = "PanelEncabezado"
        Me.PanelEncabezado.Size = New System.Drawing.Size(453, 56)
        Me.PanelEncabezado.TabIndex = 1
        '
        'LayoutEncabezado
        '
        Me.LayoutEncabezado.AllowCustomization = False
        Me.LayoutEncabezado.Controls.Add(Me.EtiquetaEncabezado)
        Me.LayoutEncabezado.Controls.Add(Me.ImagenEncabezado)
        Me.LayoutEncabezado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutEncabezado.Location = New System.Drawing.Point(0, 0)
        Me.LayoutEncabezado.Name = "LayoutEncabezado"
        Me.LayoutEncabezado.Root = Me.LayoutEncabezadoRaiz
        Me.LayoutEncabezado.Size = New System.Drawing.Size(453, 56)
        Me.LayoutEncabezado.TabIndex = 0
        Me.LayoutEncabezado.Text = "LayoutEncabezado"
        '
        'EtiquetaEncabezado
        '
        Me.EtiquetaEncabezado.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaEncabezado.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.EtiquetaEncabezado.Appearance.Options.UseFont = True
        Me.EtiquetaEncabezado.Appearance.Options.UseForeColor = True
        Me.EtiquetaEncabezado.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaEncabezado.Location = New System.Drawing.Point(46, 5)
        Me.EtiquetaEncabezado.Name = "EtiquetaEncabezado"
        Me.EtiquetaEncabezado.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.EtiquetaEncabezado.Size = New System.Drawing.Size(402, 26)
        Me.EtiquetaEncabezado.StyleController = Me.LayoutEncabezado
        Me.EtiquetaEncabezado.TabIndex = 5
        Me.EtiquetaEncabezado.Text = "[Entidad]"
        '
        'ImagenEncabezado
        '
        Me.ImagenEncabezado.Location = New System.Drawing.Point(5, 5)
        Me.ImagenEncabezado.Name = "ImagenEncabezado"
        Me.ImagenEncabezado.Size = New System.Drawing.Size(26, 26)
        Me.ImagenEncabezado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ImagenEncabezado.TabIndex = 4
        Me.ImagenEncabezado.TabStop = False
        '
        'LayoutEncabezadoRaiz
        '
        Me.LayoutEncabezadoRaiz.CustomizationFormText = "LayoutEncabezadoRaiz"
        Me.LayoutEncabezadoRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutEncabezadoRaiz.GroupBordersVisible = False
        Me.LayoutEncabezadoRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.ItemForImagenEncabezado, Me.ItemForEtiquetaEncabezado})
        Me.LayoutEncabezadoRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutEncabezadoRaiz.Name = "LayoutEncabezadoRaiz"
        Me.LayoutEncabezadoRaiz.Size = New System.Drawing.Size(453, 56)
        Me.LayoutEncabezadoRaiz.TextVisible = False
        '
        'ItemForImagenEncabezado
        '
        Me.ItemForImagenEncabezado.Control = Me.ImagenEncabezado
        Me.ItemForImagenEncabezado.CustomizationFormText = "ItemForImagenEncabezado"
        Me.ItemForImagenEncabezado.Location = New System.Drawing.Point(0, 0)
        Me.ItemForImagenEncabezado.MaxSize = New System.Drawing.Size(41, 36)
        Me.ItemForImagenEncabezado.MinSize = New System.Drawing.Size(41, 36)
        Me.ItemForImagenEncabezado.Name = "ItemForImagenEncabezado"
        Me.ItemForImagenEncabezado.Size = New System.Drawing.Size(41, 56)
        Me.ItemForImagenEncabezado.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForImagenEncabezado.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 5, 0, 0)
        Me.ItemForImagenEncabezado.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForImagenEncabezado.TextVisible = False
        '
        'ItemForEtiquetaEncabezado
        '
        Me.ItemForEtiquetaEncabezado.Control = Me.EtiquetaEncabezado
        Me.ItemForEtiquetaEncabezado.CustomizationFormText = "ItemForEtiquetaEncabezado"
        Me.ItemForEtiquetaEncabezado.Location = New System.Drawing.Point(41, 0)
        Me.ItemForEtiquetaEncabezado.Name = "ItemForEtiquetaEncabezado"
        Me.ItemForEtiquetaEncabezado.Size = New System.Drawing.Size(412, 56)
        Me.ItemForEtiquetaEncabezado.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForEtiquetaEncabezado.TextVisible = False
        '
        'PanelAcciones
        '
        Me.PanelAcciones.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelAcciones.Appearance.Options.UseBackColor = True
        Me.PanelAcciones.AutoSize = True
        Me.PanelAcciones.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelAcciones.Controls.Add(Me.LayoutAcciones)
        Me.PanelAcciones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelAcciones.Location = New System.Drawing.Point(0, 210)
        Me.PanelAcciones.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelAcciones.LookAndFeel.UseWindowsXPTheme = True
        Me.PanelAcciones.Name = "PanelAcciones"
        Me.PanelAcciones.Size = New System.Drawing.Size(453, 70)
        Me.PanelAcciones.TabIndex = 2
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.CustomizationFormText = "LayoutTarjetaRaiz"
        Me.LayoutTarjetaRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutTarjetaRaiz.GroupBordersVisible = False
        Me.LayoutTarjetaRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutTarjetaRaiz.Name = "Root"
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(453, 154)
        Me.LayoutTarjetaRaiz.TextVisible = False
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.AllowCustomization = False
        Me.LayoutTarjeta.DataSource = Me.FuenteDatos
        Me.LayoutTarjeta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutTarjeta.Location = New System.Drawing.Point(0, 56)
        Me.LayoutTarjeta.Name = "LayoutTarjeta"
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Root = Me.LayoutTarjetaRaiz
        Me.LayoutTarjeta.Size = New System.Drawing.Size(453, 154)
        Me.LayoutTarjeta.TabIndex = 0
        Me.LayoutTarjeta.Text = "LayoutTarjeta"
        '
        'FormaTarjeta
        '
        Me.AcceptButton = Me.BotonAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.CancelButton = Me.BotonCancelar
        Me.ClientSize = New System.Drawing.Size(453, 280)
        Me.Controls.Add(Me.LayoutTarjeta)
        Me.Controls.Add(Me.PanelEncabezado)
        Me.Controls.Add(Me.PanelAcciones)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "FormaTarjeta"
        Me.Text = "Tarjeta"
        CType(Me.LayoutAcciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutAcciones.ResumeLayout(False)
        CType(Me.LigaAyuda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutAccionesRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForBotonCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioAcciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForBotonAceptar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForLigaAyuda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioAccionesRelleno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelEncabezado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelEncabezado.ResumeLayout(False)
        CType(Me.LayoutEncabezado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutEncabezado.ResumeLayout(False)
        CType(Me.ImagenEncabezado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutEncabezadoRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForImagenEncabezado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForEtiquetaEncabezado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelAcciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelAcciones.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected Friend WithEvents FuenteDatos As System.Windows.Forms.BindingSource
    Protected Friend WithEvents ManejadorValidacion As DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider
    Friend WithEvents PanelEncabezado As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutEncabezado As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutEncabezadoRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ImagenEncabezado As System.Windows.Forms.PictureBox
    Friend WithEvents ItemForImagenEncabezado As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaEncabezado As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ItemForEtiquetaEncabezado As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents PanelAcciones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutAcciones As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutAccionesRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ItemForBotonCancelar As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EspacioAcciones As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ItemForBotonAceptar As DevExpress.XtraLayout.LayoutControlItem
    Protected Friend WithEvents LayoutTarjeta As DevExpress.XtraDataLayout.DataLayoutControl
    Protected Friend WithEvents LayoutTarjetaRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LigaAyuda As DevExpress.XtraEditors.HyperLinkEdit
    Friend WithEvents ItemForLigaAyuda As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BotonCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BotonAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EspacioAccionesRelleno As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EtiquetaCopyright As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem

End Class
