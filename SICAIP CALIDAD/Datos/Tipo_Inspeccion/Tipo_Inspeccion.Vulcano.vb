﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Tipo de inspección", "Tipos de inspección", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "nombre", 1, "Nombre")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "descripcion", 2, "Descripción")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "TiempoTexto", 3, "Tiempo", ColumnasPosicionesFijas.FijoIzquierda,"Tipos de inspección : {0:n0}" )>
Public Class Tipo_Inspeccion
    Inherits Entidad(Of Tipo_Inspeccion, Tipo_InspeccionCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As Tipo_Inspeccion)
        With EntidadNueva
            .cve_tipo_inspeccion  =cve_tipo_inspeccion  
            .nombre  =nombre
            .descripcion=descripcion 
            .tiempo =tiempo 
             
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
              .cve_tipo_inspeccion  =cve_tipo_inspeccion  
            .nombre  =nombre
            .descripcion=descripcion 
            .tiempo =tiempo 
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            cve_tipo_inspeccion  =.cve_tipo_inspeccion  
            nombre  =.nombre
            descripcion=.descripcion 
            tiempo =.tiempo   
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As Tipo_Inspeccion
        Return New Tipo_Inspeccion
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As Tipo_Inspeccion) As Boolean
        Return Equals(Entidad.descripcion   , descripcion   ) AndAlso
            Equals(Entidad.nombre   , nombre  )  AndAlso
            Equals(Entidad.tiempo    , tiempo   )  
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Return nombre  
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return nombre    
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As Tipo_InspeccionCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case Tipo_InspeccionCampos .NOMBRE    
                If String .IsNullOrEmpty (nombre) Then
                    Resultado.Add(My.Resources.Str_NombreInpeccionRequerido )
                End If

            Case Tipo_InspeccionCampos .tiempo    
                If tiempo=0 Then
                    Resultado.Add(My.Resources.Str_TiempoInspeccionRequerido )
                End If
        End Select
        Return Resultado
    End Function
End Class
