﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Cliente
''' </summary>
Public Enum LABDIMSolicitudCampos As Integer

    cve_solicitud
	cve_usuario
	no_piezas
    no_cotas
    cve_modelo
    cve_modelonew
    descripcion
	comentarios
	Estatus
	Fecha_Solicitud
	Fecha_Entrada
	tipo_inspeccion
	fecha_prevista
	detalle
	Fecha_Salida
    nombreArchivo

End Enum

