﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los tipos de entidades que se pueden mostrar.
''' </summary>
''' <remarks>Esta lista contiene los tipos de entidades que se mostrarán en base a su estatus de borrado.</remarks>
Public Enum EntidadesVisibles As Integer

    ''' <summary>
    ''' No se muestra ninguna entidad.
    ''' </summary>
    Ninguna = 0

    ''' <summary>
    ''' Se muestran únicamente aquellas entidades que no están borradas.
    ''' </summary>
    Existentes = 1

    ''' <summary>
    ''' Se muestran únicamente aquellas entidades que sí están borradas.
    ''' </summary>
    Borradas = 2

    ''' <summary>
    ''' Se muestran todas las entidades, borradas y no borradas.
    ''' </summary>
    Todas = 3

End Enum