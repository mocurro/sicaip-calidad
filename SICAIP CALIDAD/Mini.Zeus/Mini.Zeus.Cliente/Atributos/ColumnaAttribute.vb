﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Atributo para definir las condiciones de una propiedad para ser mostrada en un grid como columna.
''' </summary>
''' <remarks>Se puede aplicar este atributo más de una vez. Cada instancia define las características de la columna para diferentes propiedades y diferentes culturas.</remarks>
<AttributeUsage(AttributeTargets.Class, AllowMultiple:=True, Inherited:=True)> _
Public NotInheritable Class ColumnaAttribute
    Inherits Attribute

    ''' <summary>
    ''' Contiene la cultura a la que corresponde la definición de la columna.
    ''' </summary>
    Private MiCultura As String

    ''' <summary>
    ''' Contiene el título de la columna como aparecerá en el grid.
    ''' </summary>
    Private MiTitulo As String

    ''' <summary>
    ''' Contiene la cadena para formatear el valor de cada celda.
    ''' </summary>
    Private MiCadenaFormato As String

    ''' <summary>
    ''' Determina el tipo de formato que se aplicará en cada celda.
    ''' </summary>
    Private MiTipoFormato As ColumnasTiposFormatos

    ''' <summary>
    ''' Contiene el nombre del campo del objeto asociado a la columna.
    ''' </summary>
    Private MiCampo As String

    ''' <summary>
    ''' Determina si la columna será fija.
    ''' </summary>
    Private MiPosicionFija As ColumnasPosicionesFijas

    ''' <summary>
    ''' Determina si el grid estará agrupado por esta columna.
    ''' </summary>
    Private MiAgrupar As Boolean

    ''' <summary>
    ''' Contiene la cadena para formatear el valor del resumen.
    ''' </summary>
    Private MiCadenaResumen As String

    ''' <summary>
    ''' Determina el tipo de cálculo que se aplicará en el resumen.
    ''' </summary>
    Private MiTipoResumen As ColumnasTiposResumenes

    ''' <summary>
    ''' Contiene el texto que se utilizará como hint en el encabezado de la columna.
    ''' </summary>
    Private MiHint As String

    ''' <summary>
    ''' Indica la posición cardinal de la columna dentro del grid.
    ''' </summary>
    Private MiPosicion As Integer

    ''' <summary>
    ''' Indica si la columna se puede agrupar.
    ''' </summary>
    Private MiPermiteAgrupar As Boolean

    ''' <summary>
    ''' Indica si la columna se puede ordenar.
    ''' </summary>
    Private MiPermiteOrdenar As Boolean

    ''' <summary>
    ''' Indica si la columna se puede filtrar.
    ''' </summary>
    Private MiPermiteFiltar As Boolean

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String)

        Me.New(Cultura, Campo, Posicion, Titulo, ColumnasTiposFormatos.Ninguno, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="CadenaFormato">La cadena para formatear el valor de cada celda.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal CadenaFormato As String)

        Me.New(Cultura, Campo, Posicion, Titulo, CadenaFormato, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoFormato">El tipo de formato que se aplicará en cada celda.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoFormato As ColumnasTiposFormatos)

        Me.New(Cultura, Campo, Posicion, Titulo, TipoFormato, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String)

        Me.New(Cultura, Campo, Posicion, Titulo, ColumnasTiposFormatos.Ninguno, TipoResumen, CadenaResumen, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="CadenaFormato">La cadena para formatear el valor de cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal CadenaFormato As String, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String)

        Me.New(Cultura, Campo, Posicion, Titulo, CadenaFormato, TipoResumen, CadenaResumen, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoFormato">El tipo de formato que se aplicará en cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoFormato As ColumnasTiposFormatos, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String)

        Me.New(Cultura, Campo, Posicion, Titulo, TipoFormato, TipoResumen, CadenaResumen, String.Empty, False, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, ColumnasTiposFormatos.Ninguno, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="CadenaFormato">La cadena para formatear el valor de cada celda.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal CadenaFormato As String, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, CadenaFormato, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoFormato">El tipo de formato que se aplicará en cada celda.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoFormato As ColumnasTiposFormatos, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, TipoFormato, ColumnasTiposResumenes.Ninguno, String.Empty, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, ColumnasTiposFormatos.Ninguno, TipoResumen, CadenaResumen, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="CadenaFormato">La cadena para formatear el valor de cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal CadenaFormato As String, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, CadenaFormato, TipoResumen, CadenaFormato, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoFormato">El tipo de formato que se aplicará en cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoFormato As ColumnasTiposFormatos, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String, ByVal Agrupar As Boolean)

        Me.New(Cultura, Campo, Posicion, Titulo, TipoFormato, TipoResumen, CadenaResumen, String.Empty, Agrupar, ColumnasPosicionesFijas.Libre, True, True, True)

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="CadenaFormato">La cadena para formatear el valor de cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    ''' <param name="Hint">El texto que se utilizará como hint en el encabezado de la columna.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    ''' <param name="PosicionFija">Determina si la columna será fija.</param>
    ''' <param name="PermiteAgrupar">Indica si la columna se puede agrupar.</param>
    ''' <param name="PermiteOrdenar">Indica si la columna se puede ordenar.</param>
    ''' <param name="PermiteFiltrar">Indica si la columna se puede filtrar.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal CadenaFormato As String, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String, ByVal Hint As String, ByVal Agrupar As Boolean, ByVal PosicionFija As ColumnasPosicionesFijas, ByVal PermiteAgrupar As Boolean, ByVal PermiteOrdenar As Boolean, ByVal PermiteFiltrar As Boolean)

        MiCultura = Cultura
        MiCampo = Campo
        MiPosicion = Posicion
        MiTitulo = Titulo
        MiCadenaFormato = CadenaFormato
        MiTipoFormato = ColumnasTiposFormatos.Personalizado
        MiTipoResumen = TipoResumen
        MiCadenaResumen = CadenaResumen
        MiHint = Hint
        MiAgrupar = Agrupar
        MiPosicionFija = PosicionFija
        MiPermiteAgrupar = PermiteAgrupar
        MiPermiteOrdenar = PermiteOrdenar
        MiPermiteFiltar = PermiteFiltrar

    End Sub

    ''' <summary>
    ''' Crea la información de la columna para un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura a la que corresponde la definición de la columna.</param>
    ''' <param name="Campo">El nombre del campo del objeto asociado a la columna.</param>
    ''' <param name="Posicion">La posición cardinal de la columna dentro del grid.</param>
    ''' <param name="Titulo">El título de la columna como aparecerá en el grid.</param>
    ''' <param name="TipoFormato">El tipo de formato que se aplicará en cada celda.</param>
    ''' <param name="TipoResumen">El tipo de cálculo que se aplicará en el resumen.</param>
    ''' <param name="CadenaResumen">La cadena para formatear el valor del resumen.</param>
    ''' <param name="Hint">El texto que se utilizará como hint en el encabezado de la columna.</param>
    ''' <param name="Agrupar">Determina si el grid estará agrupado por esta columna.</param>
    ''' <param name="PosicionFija">Determina si la columna será fija.</param>
    ''' <param name="PermiteAgrupar">Indica si la columna se puede agrupar.</param>
    ''' <param name="PermiteOrdenar">Indica si la columna se puede ordenar.</param>
    ''' <param name="PermiteFiltrar">Indica si la columna se puede filtrar.</param>
    Public Sub New(ByVal Cultura As String, ByVal Campo As String, ByVal Posicion As Integer, ByVal Titulo As String, ByVal TipoFormato As ColumnasTiposFormatos, ByVal TipoResumen As ColumnasTiposResumenes, ByVal CadenaResumen As String, ByVal Hint As String, ByVal Agrupar As Boolean, ByVal PosicionFija As ColumnasPosicionesFijas, ByVal PermiteAgrupar As Boolean, ByVal PermiteOrdenar As Boolean, ByVal PermiteFiltrar As Boolean)

        MiCultura = Cultura
        MiCampo = Campo
        MiPosicion = Posicion
        MiTitulo = Titulo
        MiCadenaFormato = String.Empty
        MiTipoFormato = TipoFormato
        MiTipoResumen = TipoResumen
        MiCadenaResumen = CadenaResumen
        MiHint = Hint
        MiAgrupar = Agrupar
        MiPosicionFija = PosicionFija
        MiPermiteAgrupar = PermiteAgrupar
        MiPermiteOrdenar = PermiteOrdenar
        MiPermiteFiltar = PermiteFiltrar

    End Sub

    ''' <summary>
    ''' Contiene la cultura a la que corresponde la definición de la columna.
    ''' </summary>
    Public ReadOnly Property Cultura() As String
        Get
            Return MiCultura
        End Get
    End Property

    ''' <summary>
    ''' Contiene el título de la columna como aparecerá en el grid.
    ''' </summary>
    Public ReadOnly Property Titulo() As String
        Get
            Return MiTitulo
        End Get
    End Property

    ''' <summary>
    ''' Contiene la cadena para formatear el valor de cada celda.
    ''' </summary>
    Public ReadOnly Property CadenaFormato() As String
        Get
            Return MiCadenaFormato
        End Get
    End Property

    ''' <summary>
    ''' Determina el tipo de formato que se aplicará en cada celda.
    ''' </summary>
    Public ReadOnly Property TipoFormato() As ColumnasTiposFormatos
        Get
            Return MiTipoFormato
        End Get
    End Property

    ''' <summary>
    ''' Contiene el nombre del campo del objeto asociado a la columna.
    ''' </summary>
    Public ReadOnly Property Campo() As String
        Get
            Return MiCampo
        End Get
    End Property

    ''' <summary>
    ''' Determina si la columna será fija.
    ''' </summary>
    Public ReadOnly Property PosicionFija() As ColumnasPosicionesFijas
        Get
            Return MiPosicionFija
        End Get
    End Property

    ''' <summary>
    ''' Determina si el grid estará agrupado por esta columna.
    ''' </summary>
    Public ReadOnly Property Agrupar() As Boolean
        Get
            Return MiAgrupar
        End Get
    End Property

    ''' <summary>
    ''' Contiene la cadena para formatear el valor del resumen.
    ''' </summary>
    Public ReadOnly Property CadenaResumen() As String
        Get
            Return MiCadenaResumen
        End Get
    End Property

    ''' <summary>
    ''' Determina el tipo de cálculo que se aplicará en el resumen.
    ''' </summary>
    Public ReadOnly Property TipoResumen() As ColumnasTiposResumenes
        Get
            Return MiTipoResumen
        End Get
    End Property

    ''' <summary>
    ''' Contiene el texto que se utilizará como hint en el encabezado de la columna.
    ''' </summary>
    Public ReadOnly Property Hint() As String
        Get
            Return MiHint
        End Get
    End Property

    ''' <summary>
    ''' Indica la posición cardinal de la columna dentro del grid.
    ''' </summary>
    Public ReadOnly Property Posicion() As Integer
        Get
            Return MiPosicion
        End Get
    End Property

    ''' <summary>
    ''' Indica si la columna se puede agrupar.
    ''' </summary>
    Public ReadOnly Property PermiteAgrupar() As Boolean
        Get
            Return MiPermiteAgrupar
        End Get
    End Property

    ''' <summary>
    ''' Indica si la columna se puede ordenar.
    ''' </summary>
    Public ReadOnly Property PermiteOrdenar() As Boolean
        Get
            Return MiPermiteOrdenar
        End Get
    End Property

    ''' <summary>
    ''' Indica si la columna se puede filtrar.
    ''' </summary>
    Public ReadOnly Property PermiteFiltar() As Boolean
        Get
            Return MiPermiteFiltar
        End Get
    End Property

End Class