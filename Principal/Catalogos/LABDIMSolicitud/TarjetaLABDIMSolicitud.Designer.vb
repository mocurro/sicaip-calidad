﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaLABDIMSolicitud
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboComponente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteModelos = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemComponente = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtSolicitante = New DevExpress.XtraEditors.TextEdit()
        Me.itemSolicitante = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit()
        Me.ItemDescripción = New DevExpress.XtraLayout.LayoutControlItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.itemDiseño = New DevExpress.XtraLayout.LayoutControlItem()
        Me.combodiseño = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtComentarios = New DevExpress.XtraEditors.TextEdit()
        Me.itemComentarios = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SpinPiezas = New DevExpress.XtraEditors.SpinEdit()
        Me.ItemPiezas = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        CType(Me.FuenteDatos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ManejadorValidacion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutTarjeta,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutTarjeta.SuspendLayout
        CType(Me.LayoutTarjetaRaiz,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboComponente.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.FuenteModelos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemComponente,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtSolicitante.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemSolicitante,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtDescripcion.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ItemDescripción,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemDiseño,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.combodiseño.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtComentarios.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemComentarios,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SpinPiezas.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ItemPiezas,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.BehaviorManager1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.SpinPiezas)
        Me.LayoutTarjeta.Controls.Add(Me.txtComentarios)
        Me.LayoutTarjeta.Controls.Add(Me.txtDescripcion)
        Me.LayoutTarjeta.Controls.Add(Me.txtSolicitante)
        Me.LayoutTarjeta.Controls.Add(Me.ComboComponente)
        Me.LayoutTarjeta.Controls.Add(Me.combodiseño)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = true
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 222)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.itemComponente, Me.itemSolicitante, Me.ItemDescripción, Me.itemDiseño, Me.itemComentarios, Me.ItemPiezas})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 222)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 144)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 58)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboComponente
        '
        Me.ComboComponente.Location = New System.Drawing.Point(101, 36)
        Me.ComboComponente.Name = "ComboComponente"
        Me.ComboComponente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboComponente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("np_gkn", "modelo")})
        Me.ComboComponente.Properties.DataSource = Me.FuenteModelos
        Me.ComboComponente.Properties.DisplayMember = "np_gkn"
        Me.ComboComponente.Properties.NullText = "Seleccione número de parte"
        Me.ComboComponente.Properties.ValueMember = "cve_modelo"
        Me.ComboComponente.Size = New System.Drawing.Size(407, 20)
        Me.ComboComponente.StyleController = Me.LayoutTarjeta
        Me.ComboComponente.TabIndex = 8
        '
        'itemComponente
        '
        Me.itemComponente.Control = Me.ComboComponente
        Me.itemComponente.Location = New System.Drawing.Point(0, 24)
        Me.itemComponente.Name = "itemComponente"
        Me.itemComponente.Size = New System.Drawing.Size(500, 24)
        Me.itemComponente.Text = "Número de parte"
        Me.itemComponente.TextSize = New System.Drawing.Size(85, 13)
        '
        'txtSolicitante
        '
        Me.txtSolicitante.Enabled = False
        Me.txtSolicitante.Location = New System.Drawing.Point(101, 12)
        Me.txtSolicitante.Name = "txtSolicitante"
        Me.txtSolicitante.Size = New System.Drawing.Size(407, 20)
        Me.txtSolicitante.StyleController = Me.LayoutTarjeta
        Me.txtSolicitante.TabIndex = 9
        '
        'itemSolicitante
        '
        Me.itemSolicitante.Control = Me.txtSolicitante
        Me.itemSolicitante.Location = New System.Drawing.Point(0, 0)
        Me.itemSolicitante.Name = "itemSolicitante"
        Me.itemSolicitante.Size = New System.Drawing.Size(500, 24)
        Me.itemSolicitante.Text = "Solicitante"
        Me.itemSolicitante.TextSize = New System.Drawing.Size(85, 13)
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Enabled = False
        Me.txtDescripcion.Location = New System.Drawing.Point(101, 60)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(407, 20)
        Me.txtDescripcion.StyleController = Me.LayoutTarjeta
        Me.txtDescripcion.TabIndex = 11
        '
        'ItemDescripción
        '
        Me.ItemDescripción.Control = Me.txtDescripcion
        Me.ItemDescripción.Location = New System.Drawing.Point(0, 48)
        Me.ItemDescripción.Name = "ItemDescripción"
        Me.ItemDescripción.Size = New System.Drawing.Size(500, 24)
        Me.ItemDescripción.Text = "Descripción"
        Me.ItemDescripción.TextSize = New System.Drawing.Size(85, 13)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'itemDiseño
        '
        Me.itemDiseño.Control = Me.combodiseño
        Me.itemDiseño.Location = New System.Drawing.Point(0, 96)
        Me.itemDiseño.Name = "itemDiseño"
        Me.itemDiseño.Size = New System.Drawing.Size(500, 24)
        Me.itemDiseño.Text = "Diseño"
        Me.itemDiseño.TextSize = New System.Drawing.Size(85, 13)
        '
        'combodiseño
        '
        Me.BehaviorManager1.SetBehaviors(Me.combodiseño, New DevExpress.Utils.Behaviors.Behavior() {CType(DevExpress.Utils.Behaviors.Common.OpenFileBehavior.Create(GetType(DevExpress.XtraEditors.Behaviors.OpenFileBehaviorSourceForButtonEdit), True, DevExpress.Utils.Behaviors.Common.FileIconSize.Small, Nothing, Nothing, DevExpress.Utils.Behaviors.Common.CompletionMode.FilesAndDirectories, Nothing), DevExpress.Utils.Behaviors.Behavior)})
        Me.combodiseño.EditValue = "Seleccionar diseño"
        Me.combodiseño.Location = New System.Drawing.Point(101, 108)
        Me.combodiseño.Name = "combodiseño"
        Me.combodiseño.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.combodiseño.Properties.NullText = "[EditValue is null]"
        Me.combodiseño.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.combodiseño.Size = New System.Drawing.Size(407, 20)
        Me.combodiseño.StyleController = Me.LayoutTarjeta
        Me.combodiseño.TabIndex = 12
        '
        'txtComentarios
        '
        Me.txtComentarios.Location = New System.Drawing.Point(101, 132)
        Me.txtComentarios.Name = "txtComentarios"
        Me.txtComentarios.Size = New System.Drawing.Size(407, 20)
        Me.txtComentarios.StyleController = Me.LayoutTarjeta
        Me.txtComentarios.TabIndex = 13
        '
        'itemComentarios
        '
        Me.itemComentarios.Control = Me.txtComentarios
        Me.itemComentarios.Location = New System.Drawing.Point(0, 120)
        Me.itemComentarios.Name = "itemComentarios"
        Me.itemComentarios.Size = New System.Drawing.Size(500, 24)
        Me.itemComentarios.Text = "Comentarios"
        Me.itemComentarios.TextSize = New System.Drawing.Size(85, 13)
        '
        'SpinPiezas
        '
        Me.SpinPiezas.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinPiezas.Location = New System.Drawing.Point(101, 84)
        Me.SpinPiezas.Name = "SpinPiezas"
        Me.SpinPiezas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinPiezas.Properties.MaxLength = 999
        Me.SpinPiezas.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.SpinPiezas.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinPiezas.Properties.NullText = "1"
        Me.SpinPiezas.Size = New System.Drawing.Size(407, 20)
        Me.SpinPiezas.StyleController = Me.LayoutTarjeta
        Me.SpinPiezas.TabIndex = 14
        '
        'ItemPiezas
        '
        Me.ItemPiezas.Control = Me.SpinPiezas
        Me.ItemPiezas.Location = New System.Drawing.Point(0, 72)
        Me.ItemPiezas.Name = "ItemPiezas"
        Me.ItemPiezas.Size = New System.Drawing.Size(500, 24)
        Me.ItemPiezas.Text = "Número de piezas"
        Me.ItemPiezas.TextSize = New System.Drawing.Size(85, 13)
        '
        'TarjetaLABDIMSolicitud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 348)
        Me.Name = "TarjetaLABDIMSolicitud"
        Me.Text = "TarjetaLabDim"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolicitante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemSolicitante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemDescripción, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemDiseño, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.combodiseño.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComentarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComentarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpinPiezas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemPiezas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboComponente As LookUpEdit
    Friend WithEvents itemComponente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteModelos As BindingSource
    Friend WithEvents txtComentarios As TextEdit
    Friend WithEvents txtDescripcion As TextEdit
    Friend WithEvents txtSolicitante As TextEdit
    Friend WithEvents itemSolicitante As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemDescripción As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemDiseño As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemComentarios As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents SpinPiezas As SpinEdit
    Friend WithEvents ItemPiezas As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents combodiseño As ButtonEdit
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
End Class
