﻿Imports Datos
Imports Mini.Zeus.Cliente

Public Class CatalogoArea_usuarios


    Private MiListaUsuario As List(Of SEGURIDAD_USUARIO)

    Private MiListaSubAreas As List(Of detalle)

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Area_Usuario  ), Datos.Fabricas.Areas_Usuarios , GetType(TarjetaArea_Usuarios )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Area_Usuario  )

    End Function

    Public ReadOnly Property Usuarios() As List(Of SEGURIDAD_USUARIO )
    Get

        If MiListaUsuario Is Nothing Then
           Dim Filtro = New Dictionary(Of String, Object)
           Filtro.Add("Estatus", 1)
           MiListaUsuario = Fabricas.Seguridad_Usuarios .ObtenerExistentesParaSerializar(Filtro)
        End If
        Return MiListaUsuario
    End Get
    End Property

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property SubAreas() As List(Of Datos.detalle)
        Get
            If MiListaSubAreas Is Nothing Then
                'MiListaUnidadNegocio.Clear 
                Dim Filtro = New Dictionary(Of String, Object)
                MiListaSubAreas = Fabricas.SubArea.ObtenerExistentesParaSerializar(Filtro)

            End If
            Return MiListaSubAreas
        End Get
    End Property

End Class