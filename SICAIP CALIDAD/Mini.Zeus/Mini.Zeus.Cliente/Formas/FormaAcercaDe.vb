﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Ventana base para generar dialogos de información de aplicaciones Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana de información (About) para aplicaciones basadas en Zeus. Todas las ventanas de información heredan finalmente de esta ventana.</remarks>
Public Class FormaAcercaDe
    Implements IDialogo

    ''' <summary>
    ''' Preparar contenido de la ventana.
    ''' </summary>
    Private Sub AcercaDe_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Asigna aspectos visuales
        Icon = My.Resources.Icono_GloboInformacion

        'Aplica únicamente durante la ejecución
        'If Not DesignMode Then

        'Rellena los datos automáticos
        EtiquetaTitulo.Text = My.Application.Info.Title
        Dim Version = My.Application.Info.Version
        If Deployment.Application.ApplicationDeployment.IsNetworkDeployed Then
            Version = Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion
        End If
        EtiquetaVersion.Text = String.Format(EtiquetaVersion.Text, Version.ToString())
        EtiquetaTrademark.Text = My.Application.Info.Trademark.Replace("&", "&&")
        EtiquetaCopyright.Text = My.Application.Info.Copyright.Replace("&", "&&")
        ' End If

    End Sub
    ''' <summary>
    ''' Muestra la ventana de información para un usuario en particular.
    ''' </summary>
    ''' <param name="UsuarioActual">Usuario dueño de la ventana de información</param>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Function MostrarVentana(ByVal owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult Implements IDialogo.MostrarVentana


        Return MostrarVentana(owner)

    End Function


End Class