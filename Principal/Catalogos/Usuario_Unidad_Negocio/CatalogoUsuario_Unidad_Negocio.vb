﻿Imports Datos
Imports Mini.Zeus.Cliente

Public Class CatalogoUsuario_Unidad_Negocio


    Private MiListaUsuario As List(Of SEGURIDAD_USUARIO)

    Private MiListaUnidadNegocio As List(Of Unidad_Negocio)

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Usuario_Unidad_Negocio ), Datos.Fabricas.Usuario_Unidad_Negocio, GetType(TarjetaUsuario_Unidad_Negocio )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Usuario_Unidad_Negocio )

    End Function

    Public ReadOnly Property Usuarios() As List(Of SEGURIDAD_USUARIO )
    Get

        If MiListaUsuario Is Nothing Then
           Dim Filtro = New Dictionary(Of String, Object)
           Filtro.Add("Estatus", 1)
           MiListaUsuario = Fabricas.Seguridad_Usuarios .ObtenerExistentesParaSerializar(Filtro)
        End If
        Return MiListaUsuario
    End Get
    End Property

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property UnidadNegocio() As List(Of Datos.Unidad_Negocio )
        Get
            If MiListaUnidadNegocio  Is Nothing Then
                'MiListaUnidadNegocio.Clear 
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Filtro", 0)
                MiListaUnidadNegocio = Fabricas.Unidad_Negocio  .ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaUnidadNegocio
        End Get
    End Property
    
End Class