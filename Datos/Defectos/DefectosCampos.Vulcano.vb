﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Cliente
''' </summary>
Public Enum DefectosCampos As Integer

    cve_defecto
    Nombre
    Descripcion
    Estatus

End Enum

