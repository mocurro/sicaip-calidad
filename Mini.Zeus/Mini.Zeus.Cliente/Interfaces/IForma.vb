﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas de Zeus.
''' </summary>
''' <remarks>Todas las ventanas basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IForma

    ''' <summary>
    ''' Determina si la ventana usa sábanas.
    ''' </summary>
    Property UsaSabana() As Boolean

    ''' <summary>
    ''' Regresa la sábana que usa esta ventana.
    ''' </summary>
    ReadOnly Property Sabana() As Form

    ''' <summary>
    ''' Le pone sábana a esta ventana con sus dimensiones y características iguales.
    ''' </summary>
    Sub PonerSabana()

    ''' <summary>
    ''' Le pone sábana a esta ventana con sus dimensiones y características iguales.
    ''' </summary>
    ''' <param name="Titulo">El título del mensaje.</param>
    ''' <param name="Mensaje">El mensaje a mostrar dentro de la sábana.</param>
    Sub PonerSabana(ByVal Titulo As String, ByVal Mensaje As String)

    ''' <summary>
    ''' Quita la sábana que está utilizando la ventana.
    ''' </summary>
    Sub QuitarSabana()

    '''' <summary>
    '''' Contiene la referencia al usuario que utiliza la forma.
    '''' </summary>
    '''' <remarks>Cuando no se asigna un valor a esta propiedad, el usuario actual se determina de forma dinámica de acuerdo al usuario actual de la sesión.</remarks>
    'Property UsuarioActual() As Usuarios

    ''' <summary>
    ''' Muestra la ventana.
    ''' </summary>
    Sub MostrarVentana()

    '''' <summary>
    '''' Muestra la ventana para un usuario particular.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza la ventana.</param>
    'Sub MostrarVentana(ByVal UsuarioActual As Usuarios)

End Interface
