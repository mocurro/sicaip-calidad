﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Cliente
''' </summary>
Public Enum Unidad_NegocioCampos As Integer

    cve_Unidad_Negocio
    Nombre
    Descripcion
    Estatus

End Enum

