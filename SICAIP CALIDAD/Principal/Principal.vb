﻿Imports DevExpress.XtraReports.UI
Imports Mini.Zeus.Cliente.UX

Public Class Principal
    Private Sub btnClientes_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientes.ItemClick
        MostrarPestaña(GetType(CatalogoCliente))
    End Sub

    Private Sub btnClienteModelo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClienteModelo.ItemClick
        MostrarPestaña(GetType(CatalogoCliente_Modelo))
    End Sub

    Private Sub btnDefectos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDefectos.ItemClick
        MostrarPestaña(GetType(CatalogoDefecto))
    End Sub

    Private Sub btnUnidadNegocio_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUnidadNegocio.ItemClick
        MostrarPestaña(GetType(CatalogoUnidad_Negocio))
    End Sub

    Private Sub btnComponenteUnidad_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnComponenteUnidad.ItemClick
        MostrarPestaña(GetType(CatalogoComponente_Unidad_Negocio))
    End Sub

    Private Sub btnAreasUsuarios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUnidadNegocioUsuarios.ItemClick
        MostrarPestaña(GetType(CatalogoUsuario_Unidad_Negocio))
    End Sub

    Private Sub btnNRFTsys_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNRFTsys.ItemClick
        Dim Dialogo = New NRFTsys
        Dialogo.ShowDialog()
    End Sub

    Private Sub btnReporteReclamos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnReporteReclamos.ItemClick
        MostrarPestaña(GetType(CatalogoReclamo))
    End Sub

    Private Sub btnAreaUsuarios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAreaUsuarios.ItemClick
        MostrarPestaña(GetType(CatalogoArea_usuarios))
    End Sub

    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim usuario = Environment.UserName
            Icon = My.Resources.application
            btnClientes.LargeGlyph = My.Resources.User32x32
            btnClienteModelo.LargeGlyph = My.Resources.ClienteModelo
            btnDefectos.LargeGlyph = My.Resources.defectos32x32
            btnUnidadNegocio.LargeGlyph = My.Resources.UnidadNegocio32x32
            btnComponenteUnidad.LargeGlyph = My.Resources.ComponenteUnidad32x32
            btnUnidadNegocioUsuarios.LargeGlyph = My.Resources.UnidadNegocioUsuarios32x32
            btnAreaUsuarios.LargeGlyph = My.Resources.AreaUsuario32x32
            btnSolicitud.LargeGlyph = My.Resources.LabDimSolicitud
            btnTipoInspeccion.LargeGlyph = My.Resources.TipoInspeccion32x32
            btnRegistro.LargeGlyph = My.Resources.LabDimRegistro
            btnNRFTsys.LargeGlyph = My.Resources.NFRTSYS
            btnReporteReclamos.LargeGlyph = My.Resources.ReporteReclamos
            BtnGraficos.LargeGlyph = My.Resources.Reportes48x48
            Using MiModelo = New Datos.ModeloDatosDataContext(Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer)
                Dim query = From reclamo In MiModelo.SEGURIDAD_USUARIOs
                            Where reclamo.Id_Usuario = usuario
                Datos.SesiónLocal.Usuario = query.FirstOrDefault
            End Using
            If Datos.SesiónLocal.Usuario IsNot Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Clientes)
                Dim MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)

                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnClientes.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Cliente_Modelos)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnClienteModelo.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Defectos)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnDefectos.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Unidad_Negocio)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnUnidadNegocio.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Componente_unidad_negocio)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnComponenteUnidad.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Area_Usuarios)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnAreaUsuarios.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Solicitud)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnSolicitud.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Tipo_Inspeccion)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnTipoInspeccion.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_Registro)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnRegistro.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_NRFTSYS)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnNRFTsys.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Reporte_reclamos)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnReporteReclamos.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If

                Filtro.Clear()
                Filtro.Add("cve_usuario", Datos.SesiónLocal.Usuario.CVE_Usuario)
                Filtro.Add("Permiso", Datos.Constantes.Permiso_Catalogo_unidad_negocio_Usuarios)
                MiListaPermisos = Datos.Fabricas.Permisos.ObtenerExistentesParaSerializar(Filtro)
                If MiListaPermisos IsNot Nothing AndAlso MiListaPermisos.Count > 0 Then
                    btnUnidadNegocioUsuarios.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                    MiListaPermisos.Clear()
                End If



            End If
        Catch ex As Exception

        End Try




    End Sub

    Private Sub btnSolicitud_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSolicitud.ItemClick
        MostrarPestaña(GetType(CatalogoLABDIMSolicitud))
    End Sub

    Private Sub btnTipoInspeccion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnTipoInspeccion.ItemClick
        MostrarPestaña(GetType(CatalogoTipo_Inpeccion))
    End Sub

    Private Sub btnRegistro_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnRegistro.ItemClick

        MostrarPestaña(GetType(CatalogoLABDIMRegistro))
    End Sub

   
   

    Private Sub btnLamDim_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnLamDim.ItemClick

    End Sub

    Private Sub BtnGraficos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnGraficos.ItemClick
        Dim Dialogo = New Grafico
        Dialogo.ShowDialog()
    End Sub
End Class