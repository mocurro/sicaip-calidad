﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los diferentes modos de operación de un catalogo incrustado.
''' </summary>
''' <remarks>Contiene la lista de las diferentes operaciones autorizadas para realizar dentro de un catalogo incrustado.</remarks>
Public Enum ModosCatalogosIncrustados As Integer

    ''' <summary>
    ''' El catálogo no tiene modo alguno definido, por lo que no permite realizar acción alguna.
    ''' </summary>
    Ninguno = 0

    ''' <summary>
    ''' La acción Agregar permite agregar únicamente datos nuevos.
    ''' </summary>
    AgregarNuevo = 1

    ''' <summary>
    ''' La acción Agregar permite agregar únicamente datos existentes.
    ''' </summary>
    AgregarExistente = 2

    ''' <summary>
    ''' La acción Agregar muestra un sub menú para agregar datos nuevos o existentes.
    ''' </summary>
    AgregarNuevoOExistente = 3

    ''' <summary>
    ''' El catálogo no permite ninguna acción de edición (agregar, modificar, borrar).
    ''' </summary>
    SoloConsulta = 4

End Enum