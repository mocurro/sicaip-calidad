﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Namespace Sincronizacion

    ''' <summary>
    ''' Modulo que ofrece extensiones de la capa Zeus.Cliente, sección Sincronización.
    ''' </summary>
    ''' <remarks>Este módulo contiene funciones y propiedades relacionadas al dominio de sincronización, incluyendo la información base para determinar la ubicación de la fuente de datos.</remarks>
    <Microsoft.VisualBasic.HideModuleName()> _
    Public Module ExtensionesSincronizacion

        ''' <summary>
        ''' Contiene la cadena de conexión a la base de datos SQL Server.
        ''' </summary>
        Private MiCadenaConexionSQLServer As String

        ''' <summary>
        ''' Contiene la Url del servicio web que da acceso a la información.
        ''' </summary>
        Private MiUrlServicioWeb As Uri

        ''' <summary>
        ''' Indica la ubicación de la fuente de datos.
        ''' </summary>
        Private MiUbicacionFuenteDatos As UbicacionesFuentesDatos


        Public Delegate Sub DelegadoProcesamiento()

        Public Delegate Sub DelegadoProcesamientoParametrizado(ByVal MiObjecto As Object)

        ''' <summary>
        ''' Contiene la instancia del programador de tareas.
        ''' </summary>
        Private MiProgramadorTareas As ProgramadorTareas

        Public ReadOnly Property Programador() As ProgramadorTareas
            Get
                If MiProgramadorTareas Is Nothing Then
                    MiProgramadorTareas = New ProgramadorTareas(500)
                End If
                Return MiProgramadorTareas
            End Get
        End Property

        ''' <summary>
        ''' Determina la cadena de conexión a la base de datos SQL Server.
        ''' </summary>
        Public Property CadenaConexionSQLServer() As String
            Get
                Return MiCadenaConexionSQLServer
            End Get
            Set(ByVal value As String)
                MiCadenaConexionSQLServer = value
            End Set
        End Property

        ''' <summary>
        ''' Determina la Url del servicio web que da acceso a la información.
        ''' </summary>
        Public Property UrlServicioWeb() As Uri
            Get
                Return MiUrlServicioWeb
            End Get
            Set(ByVal value As Uri)
                MiUrlServicioWeb = value
            End Set
        End Property

        ''' <summary>
        ''' Determina la ubicación de la fuente de datos.
        ''' </summary>
        Public Property UbicacionFuenteDatos() As UbicacionesFuentesDatos
            Get
                Return MiUbicacionFuenteDatos
            End Get
            Set(ByVal value As UbicacionesFuentesDatos)
                MiUbicacionFuenteDatos = value
            End Set
        End Property

        ''' <summary>
        ''' Regresa la representación textual del ambiente actual.
        ''' </summary>
        Public ReadOnly Property AmbienteTexto() As String
            Get
                'Select Case MiAmbiente
                '    Case Ambientes.Desarrollo
                '        Return "Desarrollo"
                '    Case Ambientes.Pruebas
                '        Return "Pruebas"
                '    Case Ambientes.Produccion
                '        Return "Producción"
                '    Case Else
                '        Return "Producción"
                'End Select
                Return "Producción"
            End Get
        End Property

        ''' <summary>
        ''' Configura la conexión para una instancia específica con seguridad integrada.
        ''' </summary>
        ''' <param name="InstanciaSQLServer">Nombre completo de la instancia de SQL Server donde está la base de datos.</param>
        ''' <param name="NombreBaseDatos">El nombre de la base de datos como la conoce SQL Server.</param>
        Public Sub ConfigurarCadenaConexion(ByVal InstanciaSQLServer As String, ByVal NombreBaseDatos As String)

            With New Data.SqlClient.SqlConnectionStringBuilder()
                .IntegratedSecurity = True
                .DataSource = InstanciaSQLServer
                .InitialCatalog = NombreBaseDatos
                MiCadenaConexionSQLServer = .ConnectionString
            End With

        End Sub

        ''' <summary>
        ''' Configura la conexión para una instancia específica con usuario y contraseña.
        ''' </summary>
        ''' <param name="InstanciaSQLServer">Nombre completo de la instancia de SQL Server donde está la base de datos.</param>
        ''' <param name="NombreBaseDatos">El nombre de la base de datos como la conoce SQL Server.</param>
        ''' <param name="Usuario">El usuario que realizará la conexión a la base de datos.</param>
        ''' <param name="Contraseña">La contraseña del usuario.</param>
        Public Sub ConfigurarCadenaConexion(ByVal InstanciaSQLServer As String, ByVal NombreBaseDatos As String, ByVal Usuario As String, ByVal Contraseña As String)

            With New Data.SqlClient.SqlConnectionStringBuilder()
                .IntegratedSecurity = False
                .DataSource = InstanciaSQLServer
                .InitialCatalog = NombreBaseDatos
                .UserID = Usuario
                .Password = Contraseña
                MiCadenaConexionSQLServer = .ConnectionString
            End With

        End Sub

        '''' <summary>
        '''' Configura la conexión para una base de datos SQL Compacta específica.
        '''' </summary>
        '''' <param name="Archivo">La ruta completa al archivo de la base de datos.</param>
        'Public Sub ConfigurarCadenaConexionCompacta(ByVal Archivo As String)

        '    MiCadenaConexionSQLServer = String.Format("Data Source={0};Persist Security Info=False;", Archivo)

        'End Sub

        '''' <summary>
        '''' Configura la conexión para una base de datos SQL Compacta específica que tiene contraseña.
        '''' </summary>
        '''' <param name="Archivo">La ruta completa al archivo de la base de datos.</param>
        '''' <param name="Contraseña">La contraseña de la base de datos.</param>
        'Public Sub ConfigurarCadenaConexionCompacta(ByVal Archivo As String, ByVal Contraseña As String)

        '    MiCadenaConexionSQLServer = String.Format("Data Source={0};Encrypt Database=True;Password={1};Persist Security Info=False;", Archivo, Contraseña)

        'End Sub

    End Module

End Namespace