'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Layout base para cualquier definición de reporte que estará contenido en una venta de reporte de Zeus.
''' </summary>
''' <remarks>Este layout contiene funciones comunes para cualquier definición de reporte que implemente UX de Zeus. Todas las definiciones de reporte heredan finalmente de este layout.</remarks>
Public Class ReporteDefinicion

End Class