﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para entidades de Zeus.
''' </summary>
''' <remarks>Todos los objetos de datos basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IEntidad

    ''' <summary>
    ''' Regresa el identificador de la entidad.
    ''' </summary>
    ''' <remarks>El identificador de la entidad es el mismo valor que corresponde al valor del campo llave en la base de datos.</remarks>
    ReadOnly Property Id() As Guid

    ''' <summary>
    ''' Regresa el identificador de la entidad.
    ''' </summary>
    ''' <remarks>El identificador de la entidad es el mismo valor que corresponde al valor del campo llave en la base de datos.</remarks>
    ReadOnly Property IdTexto() As String

    ''' <summary>
    ''' Obtiene los metadatos de la entidad.
    ''' </summary>
    Property MetadatosEntidad() As Integer

    ''' <summary>
    ''' Determina si la entidad es de solo lectura o no.
    ''' </summary>
    Property EsSoloLectura() As Boolean

    ''' <summary>
    ''' Regresa la cuenta de eliminado del registro tal y como está guardado en la base de datos.
    ''' </summary>
    Property CuentaEliminadoEntidad() As Integer

    ''' <summary>
    ''' Regresa la fecha y hora de creación del registro tal y como está guardado en la base de datos (Hora Universal).
    ''' </summary>
    Property FechaHoraCreacionEntidad() As Date

    ''' <summary>
    ''' Regresa la fecha y hora de la última modificación del registro tal y como está guardado en la base de datos (Hora Universal).
    ''' </summary>
    Property FechaHoraModificacionEntidad() As Date

    ''' <summary>
    ''' Regresa la fecha y hora de creación del registro expresado en la zona horaria local.
    ''' </summary>
    Property FechaHoraLocalCreacion() As Date

    ''' <summary>
    ''' Regresa la fecha y hora de última modificación del registro expresado en la zona horaria local.
    ''' </summary>
    Property FechaHoraLocalModificacion() As Date

    '''' <summary>
    '''' Regresa el usuario que inicializó la entidad.
    '''' </summary>
    '''' <remarks>Esta propiedad contiene el usuario que ejecutó el constructor de la entidad y será utilizado para registrarlo como el usuario de creación o modificación, según sea el caso.</remarks>
    'ReadOnly Property UsuarioActual() As Usuarios

    '''' <summary>
    '''' Regresa el usuario que creó el registro.
    '''' </summary>
    'ReadOnly Property UsuarioCreacion() As Usuarios

    '''' <summary>
    '''' Regresa el usuario que realizó la última modificación del registro.
    '''' </summary>
    'ReadOnly Property UsuarioModificacion() As Usuarios

    ''' <summary>
    ''' Regresa el estatus actual de la entidad.
    ''' </summary>
    ''' <remarks>Este estatus permite conocer si una entidad es nueva, existente o tiene otro estado como resultado de ejectuar algunas de las funciones disponibles.</remarks>
    Property EstatusEntidad() As EstatusEntidades

    ''' <summary>
    ''' Regresa el estatus actual de la entidad.
    ''' </summary>
    ''' <remarks>Este estatus permite conocer si una entidad es nueva, existente o tiene otro estado como resultado de ejectuar algunas de las funciones disponibles. El valor que regresa esta propiedad no incluye nigún estado de tipo "Pendiente".</remarks>
    ReadOnly Property EstatusEntidad(ByVal IgnoraPendientes As Boolean) As EstatusEntidades

    ''' <summary>
    ''' Regresa la representación de la entidad en texto.
    ''' </summary>
    ''' <remarks>Esta propiedad regresa la misma cadena de texto que la función ToString().</remarks>
    ReadOnly Property TextoEntidad() As String

    ''' <summary>
    ''' Regresa la imagen que representa la entidad.
    ''' </summary>
    ''' <remarks>Regresa la imagen con el tamaño por default de 16x16 pixeles.</remarks>
    ReadOnly Property ImagenEntidad() As Drawing.Image

    ''' <summary>
    ''' Regresa la imagen que representa la entidad.
    ''' </summary>
    ''' <param name="Tamaño">Determina el tamaño de la imagen a partir de una lista de tamaños conocidos.</param>
    ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As TamañosImagenes) As Drawing.Image

    ''' <summary>
    ''' Regresa el ícono que representa la entidad.
    ''' </summary>
    ReadOnly Property IconoEntidad() As Drawing.Icon

    ''' <summary>
    ''' Valida la información de todos los campos de la entidad y regresa todos los mensajes posibles cuando algún campo no es válido.
    ''' </summary>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato de algún campo. Si más de un campo es inválido, esta lista contendrá los mensajes de todos los campos inválidos. Si la lista resultante está vacia, se puede interpretar que todos los campos de la entidad son válidos.</remarks>
    Function Validar() As List(Of String)

    ''' <summary>
    ''' Valida la información de un campo específico de la entidad y regresa todos los mensajes posibles cuando el campo no es válido.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a validar.</param>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato del campo. Si la lista resultante está vacia, se puede interpretar que el campo de la entidad es válido.</remarks>
    Function Validar(ByVal Campo As Integer) As List(Of String)

    ''' <summary>
    ''' Indica si la información de todos los campos de la entidad es válida.
    ''' </summary>
    ''' <remarks>El resultado de esta función se basa en los resultados de la función Validar() para determinar la validez de la entidad.</remarks>
    Function EsValido() As Boolean

    ''' <summary>
    ''' Indica si la información de un campo de la entidad es válida.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a verificar su validez.</param>
    ''' <remarks>El resultado de esta función se basa en los resulados de la función Validar(Integer) para determinar la validez del campo.</remarks>
    Function EsValido(ByVal Campo As Integer) As Boolean

    ''' <summary>
    ''' Indica si la información de al menos un campo de la entidad es diferente a la información original.
    ''' </summary>
    ''' <remarks>Para entidades nuevas, esta función regresa True cuando la información de algún campo es diferente a los valores por default de los campos. Para entidades existentes, la función regresa True cuando la información de algún campo es diferente a la información que existía en la base de datos al momento de cargar la información.</remarks>
    Function EstaSucio() As Boolean

    ''' <summary>
    ''' Genera la copia de la información para poder realizar comparaciones entre datos originales y datos actuales.
    ''' </summary>
    ''' <remarks>Esta función es utilizada por fábricas de entidades para mantener sincronizada la información de forma adecuada.</remarks>
    Sub InicializarOriginal()

    ''' <summary>
    ''' Completa la inicialización de la entidad como una nueva.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que fabrico la entidad.</param>
    Sub InicializarNuevo()

    '''' <summary>
    '''' Completa la inicialización de la entidad cargada desde la base de datos.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que fabrico la entidad.</param>
    '''' <param name="FabricaUsuarios">La fábrica de usuarios para obtener los usuarios de creación y modificación.</param>
    Sub InicializarExistente()

    ''' <summary>
    ''' Restablece la información de la entidad a sus valores originales.
    ''' </summary>
    ''' <remarks>Esta función toma la información almacenada como original para convertirla en la información vigente. La información original se actualiza automáticamente al realizar una operación exitosa en la fábrica de entidades.</remarks>
    Sub RestablecerOriginal()

    ''' <summary>
    ''' Define que el estatus actual de datos de la entidad se convierta en el original.
    ''' </summary>
    ''' <remarks>Esta función toma la información almacenada en la entidad y la copia al original para convertirla en información vigente.</remarks>
    Sub AceptarComoOriginal()

    ''' <summary>
    ''' Crea un clon de la entidad.
    ''' </summary>
    ''' <remarks>Esta función crea un nuevo objeto identico.</remarks>
    Function Clonar() As Object

    '''' <summary>
    '''' Regresa información acerca de un permiso en particular de esta entidad.
    '''' </summary>
    '''' <param name="Tipo">El tipo de permiso del cual se requiere información.</param>
    '''' <remarks>Esta función regresa el atributo que define un permiso específico para esta entidad. Si la entidad no tiene un permiso específico definido, regresará un nuevo atributo sin permiso con el estatus de Negado.</remarks>
    'Function Permiso(ByVal Tipo As TiposPermisos) As PermisoAttribute

    ''' <summary>
    ''' Regresa información acerca del texto de esta entidad para una cultura en particular.
    ''' </summary>
    ''' <param name="Cultura">La cultura para la cual se requiere la información.</param>
    ''' <remarks>Esta función regresa el atributo que define el texto específico para una cultura en esta entidad. Si la entidad no tiene un texto específico definido, regresará un nuevo atributo sin textos.</remarks>
    Function Texto(ByVal Cultura As String) As TextoAttribute

    ''' <summary>
    ''' Regresa la lista de todas las propiedades que están marcadas para ser columnas en un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura para la cual se requiere la información.</param>
    ''' <remarks>Esta función regresa la lista de atributos que definen las características de una propiedad para ser mostrada en un grid. Si la entidad no tiene propiedades marcadas con el atributo, regresará una lista vacia.</remarks>
    Function Columnas(ByVal Cultura As String) As List(Of ColumnaAttribute)

    ''' <summary>
    ''' Serializa la entidad para ser transmitida o guardada.
    ''' </summary>
    Function Serializar() As String

End Interface