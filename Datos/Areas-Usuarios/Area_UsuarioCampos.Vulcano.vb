﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Componente_Unidad_Negocio
''' </summary>
Public Enum Area_UsuarioCampos As Integer

    Cve_area_usuario
    Cve_Area
    cve_usuario
    Nombre
    Estatus

End Enum

