﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaCliente_Modelo
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboEstatus = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ItemEstatus = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboCliente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteClientes = New System.Windows.Forms.BindingSource(Me.components)
        Me.ItemCliente = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboComponente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteModelos = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemComponente = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutTarjeta.SuspendLayout()
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.ComboComponente)
        Me.LayoutTarjeta.Controls.Add(Me.ComboCliente)
        Me.LayoutTarjeta.Controls.Add(Me.ComboEstatus)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 102)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.ItemEstatus, Me.ItemCliente, Me.itemComponente})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 102)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 72)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 10)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboEstatus
        '
        Me.ComboEstatus.Location = New System.Drawing.Point(97, 60)
        Me.ComboEstatus.Name = "ComboEstatus"
        Me.ComboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboEstatus.Properties.Items.AddRange(New Object() {"Activo", "Inactivo"})
        Me.ComboEstatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboEstatus.Size = New System.Drawing.Size(411, 20)
        Me.ComboEstatus.StyleController = Me.LayoutTarjeta
        Me.ComboEstatus.TabIndex = 6
        '
        'ItemEstatus
        '
        Me.ItemEstatus.Control = Me.ComboEstatus
        Me.ItemEstatus.Location = New System.Drawing.Point(0, 48)
        Me.ItemEstatus.Name = "ItemEstatus"
        Me.ItemEstatus.Size = New System.Drawing.Size(500, 24)
        Me.ItemEstatus.Text = "Estatus"
        Me.ItemEstatus.TextSize = New System.Drawing.Size(81, 13)
        '
        'ComboCliente
        '
        Me.ComboCliente.Location = New System.Drawing.Point(97, 12)
        Me.ComboCliente.Name = "ComboCliente"
        Me.ComboCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboCliente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Nombre", "Cliente")})
        Me.ComboCliente.Properties.DataSource = Me.FuenteClientes
        Me.ComboCliente.Properties.DisplayMember = "Nombre"
        Me.ComboCliente.Properties.ValueMember = "cve_cliente"
        Me.ComboCliente.Size = New System.Drawing.Size(411, 20)
        Me.ComboCliente.StyleController = Me.LayoutTarjeta
        Me.ComboCliente.TabIndex = 7
        '
        'ItemCliente
        '
        Me.ItemCliente.Control = Me.ComboCliente
        Me.ItemCliente.Location = New System.Drawing.Point(0, 0)
        Me.ItemCliente.Name = "ItemCliente"
        Me.ItemCliente.Size = New System.Drawing.Size(500, 24)
        Me.ItemCliente.Text = "Cliente"
        Me.ItemCliente.TextSize = New System.Drawing.Size(81, 13)
        '
        'ComboComponente
        '
        Me.ComboComponente.Location = New System.Drawing.Point(97, 36)
        Me.ComboComponente.Name = "ComboComponente"
        Me.ComboComponente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboComponente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("np_gkn", "modelo")})
        Me.ComboComponente.Properties.DataSource = Me.FuenteModelos
        Me.ComboComponente.Properties.DisplayMember = "np_gkn"
        Me.ComboComponente.Properties.ValueMember = "cve_modelo"
        Me.ComboComponente.Size = New System.Drawing.Size(411, 20)
        Me.ComboComponente.StyleController = Me.LayoutTarjeta
        Me.ComboComponente.TabIndex = 8
        '
        'itemComponente
        '
        Me.itemComponente.Control = Me.ComboComponente
        Me.itemComponente.Location = New System.Drawing.Point(0, 24)
        Me.itemComponente.Name = "itemComponente"
        Me.itemComponente.Size = New System.Drawing.Size(500, 24)
        Me.itemComponente.Text = "Número de parte"
        Me.itemComponente.TextSize = New System.Drawing.Size(81, 13)
        '
        'TarjetaCliente_Modelo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 228)
        Me.Name = "TarjetaCliente_Modelo"
        Me.Text = "TarjetaClientes"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboEstatus As ComboBoxEdit
    Friend WithEvents ItemEstatus As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ComboComponente As LookUpEdit
    Friend WithEvents ComboCliente As LookUpEdit
    Friend WithEvents ItemCliente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemComponente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteModelos As BindingSource
    Friend WithEvents FuenteClientes As BindingSource
End Class
