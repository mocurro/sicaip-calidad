﻿Imports System.ComponentModel

Public Class TarjetaTipo_Inpeccion
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoTipo_Inpeccion  ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Tipo_InspeccionCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Tipo_InspeccionCampos.Nombre, itemNombre)
            .Add(Datos.Tipo_InspeccionCampos.Descripcion, itemDescripcion)
            .Add(Datos.Tipo_InspeccionCampos.tiempo, itemtiempo )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Tipo_InspeccionCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Tipo_Inspeccion  )()
        Select Case sender.Name
            Case TextoNombre.Name
                Paquete.Nombre = TextoNombre.Text.Trim
                'Return True
            Case TextoDescripcion .Name
                Paquete.Descripcion = TextoDescripcion.Text.Trim
                'Paquete.TipoAplicacion = NUTEC.Comun.TipoAplicacion.Embarques
                'Return True

           Case SpinTiempo  .Name
                Paquete.tiempo  = CInt(SpinTiempo.Value)
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Tipo_Inspeccion )()
        OmitirCambiosPersonalizados =True 
        SpinTiempo .Value =paquete .tiempo 
        OmitirCambiosPersonalizados =False 
        'If paquete.Estatus = 0 Then
        '    ComboEstatus.SelectedIndex = 0
        'Else
        '    ComboEstatus.SelectedIndex = 1
        'End If

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        'Dim Paquete = EntidadDe(Of Datos.Cliente )()
        'Paquete.Estatus  = ComboEstatus.SelectedIndex
    End Sub
End Class