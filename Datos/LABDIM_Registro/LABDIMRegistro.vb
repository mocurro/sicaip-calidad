﻿Public Class LamdimO

      Private MiNombreArchivo As String 
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public  Property NombreArchivo As String
        Get
         Return MiNombreArchivo 
        End Get
        Set(value As String)
            MiNombreArchivo=value 
        End Set
    End Property

    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property Folio() As String
        Get
            Return cve_solicitud.ToString .PadLeft (5,CChar ("0"))
        End Get
    End Property

    
    Public ReadOnly Property user As SEGURIDAD_USUARIO 
        Get
            Dim MiCliente=Datos.Fabricas .Seguridad_Usuarios .ObtenerUno (cve_usuario )
            Return MiCliente 
        End Get
    End Property

    Public ReadOnly Property Archivo As LABDIMArchivo 
    Get
             Dim Filtro = New Dictionary(Of String, Object)
             Filtro.Add("cv_solicitud", cve_solicitud) 
            Dim MiArchivo=Datos.Fabricas .LABDIMArchivos .ObtenerExistentes  (Filtro  )
            If MiArchivo IsNot Nothing AndAlso MiArchivo .Count >0 Then
                Return MiArchivo .SingleOrDefault 
            Else 
                Return Nothing 
            End If
    End Get
    End Property
End Class
