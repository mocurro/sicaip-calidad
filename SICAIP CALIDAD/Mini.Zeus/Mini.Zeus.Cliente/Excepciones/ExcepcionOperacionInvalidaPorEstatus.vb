﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Excepción que ocurre cuando se intenta realizar una operación y el estatus no es válido.
''' </summary>
''' <remarks>Esta excepción la arroja una fábrica cuando el estatus de una entidad no es válido para una operación en particular. Esta excepción ocurre antes de iniciar cualquier acción en la operación.</remarks>
Public Class ExcepcionOperacionInvalidaPorEstatus
    Inherits Excepcion

    ''' <summary>
    ''' Crea la excepción basada en un objeto y método específico.
    ''' </summary>
    ''' <param name="Objeto">El objeto que generó la excepción. Si el objeto implementa IObjeto el mensaje de la excepción contendrá información adicional.</param>
    ''' <param name="Metodo">El método donde ocurrió la excepción.</param>
    Public Sub New(ByVal Objeto As Object, ByVal Metodo As Reflection.MethodBase)

        MyBase.New(Objeto, Metodo)

    End Sub

    ''' <summary>
    ''' Regresa el mensaje de la excepción.
    ''' </summary>
    Protected Overrides Function Mensaje(ByVal NombreObjeto As String, ByVal NombreMetodo As String) As String

        Return String.Format(My.Resources.Str_ExcepcionOperacionInvalidaPorEstatus, NombreObjeto, NombreMetodo)

    End Function

End Class
