﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Ventana base para cualquier inteface visual basada en Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana que implemente UX de Zeus. Todas las ventanas heredan finalmente de esta ventana.</remarks>
Public Class Forma
    Implements IForma

    ''' <summary>
    ''' Determina si la ventana usa sabanas.
    ''' </summary>
    Private MiUsaSabana As Boolean

    ''' <summary>
    ''' Contiene la sabana de esta ventana.
    ''' </summary>
    Private MiSabana As Sabana

    ''' <summary>
    ''' Bandera para controlar proceso de cerrado de ventana.
    ''' </summary>
    Private MiEstaCerrando As Boolean

    '''' <summary>
    '''' Contiene la referencia al usuario que utiliza la forma.
    '''' </summary>
    'Private MiUsuarioActual As Usuarios

    ''' <summary>
    ''' Prepara la ventana.
    ''' </summary>
    Private Sub Forma_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Aplica unicamente cuando está en ejecución
        If Not DesignMode Then

            'Poner sábana en el padre de ser necesario
            If MiUsaSabana AndAlso Owner IsNot Nothing Then
                If TypeOf Owner Is IForma Then
                    With DirectCast(Owner, IForma)
                        .PonerSabana()
                        Owner = .Sabana
                    End With
                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Libera elementos utilizados por la ventana.
    ''' </summary>
    Private Sub Forma_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Quita la sábana en caso de haberla aplicado
            If Not MiEstaCerrando Then
                MiEstaCerrando = True
                Try
                    If MiUsaSabana AndAlso Owner IsNot Nothing AndAlso Owner.Owner IsNot Nothing Then
                        If TypeOf Owner.Owner Is IForma Then
                            With DirectCast(Owner.Owner, IForma)
                                .QuitarSabana()
                            End With
                        End If
                    End If
                Catch ex As Exception
                End Try
                MiEstaCerrando = False
            End If

        End If

    End Sub

    ''' <summary>
    ''' Le pone sábana a esta ventana con sus dimensiones y características iguales.
    ''' </summary>
    Public Sub PonerSabana() Implements IForma.PonerSabana

        If MiSabana Is Nothing Then
            MiSabana = New Sabana()
            Dim BordeIzquierdo = (Width - ClientSize.Width) \ 2
            Dim BordesHorizontales = Height - ClientSize.Height
            With MiSabana
                .Size = ClientSize
                .MinimumSize = ClientSize
                .MaximumSize = ClientSize
                .Left = Left + BordeIzquierdo
                .Top = Top + BordesHorizontales - BordeIzquierdo
                If .Left < Left Then .Left = Left
                If .Top < Top Then .Top = Top
                .WindowState = WindowState
                .MostrarVentana(Me)
                '.BringToFront()
                '.Enabled = False
            End With
        End If

    End Sub

    ''' <summary>
    ''' Le pone sábana a esta ventana con sus dimensiones y características iguales.
    ''' </summary>
    ''' <param name="Titulo">El título del mensaje.</param>
    ''' <param name="Mensaje">El mensaje a mostrar dentro de la sábana.</param>
    Public Sub PonerSabana(ByVal Titulo As String, ByVal Mensaje As String) Implements IForma.PonerSabana

        If MiSabana Is Nothing Then
            MiSabana = New Sabana()
            Dim BordeIzquierdo = (Width - ClientSize.Width) \ 2
            Dim BordesHorizontales = Height - ClientSize.Height
            With MiSabana
                If TypeOf Me Is IPestaña Then
                    Dim FormaPrincipal = CType(ParentForm, FormaPrincipalListon)
                    Dim Area = CType(ParentForm, FormaPrincipalListon).ManejadorPestañas.SelectedPage.TabControl.ViewInfo.PageClientBounds
                    Dim Pestaña = CType(ParentForm, FormaPrincipalListon).ManejadorPestañas.SelectedPage.TabControl.ViewInfo.HeaderInfo
                    .Size = Area.Size
                    'TODO: Zeus.Cliente.Windows.UX.Forma:PonerSabana - Ajustar la posición correcta de la sábana dentro de la pestaña.
                    '.Left = Area.Left + FormaPrincipal.PanelCliente.Left
                    '.Top = Area.Top + Pestaña.Client.Top + Pestaña.Client.Height + FormaPrincipal.PanelCliente.Top
                    .Left = Area.Left
                    .Top = FormaPrincipal.PanelCliente.Top + Area.Top - (Area.Top - Pestaña.Client.Height) * 2 - 1
                Else
                    .Size = ClientSize
                    .Left = Left + BordeIzquierdo
                    .Top = Top + BordesHorizontales - BordeIzquierdo
                    If .Left < Left Then .Left = Left
                    If .Top < Top Then .Top = Top
                End If
                .MinimumSize = ClientSize
                .MaximumSize = ClientSize
                .WindowState = WindowState
                .MostrarVentana(Titulo, Mensaje, Me)
                '.BringToFront()
                '.Enabled = False
            End With
        End If

    End Sub

    ''' <summary>
    ''' Quita la sábana que está utilizando la ventana.
    ''' </summary>
    Public Sub QuitarSabana() Implements IForma.QuitarSabana

        If MiSabana IsNot Nothing Then
            MiSabana.Close()
            MiSabana = Nothing
            BringToFront()
            Focus()
        End If

    End Sub

    ''' <summary>
    ''' Regresa la sábana que usa esta ventana.
    ''' </summary>
    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property Sabana() As System.Windows.Forms.Form Implements IForma.Sabana
        Get
            Return MiSabana
        End Get
    End Property

    ''' <summary>
    ''' Determina si la ventana usa sábanas.
    ''' </summary>
    <System.ComponentModel.Description("Determina si la ventana usa sabanas. La sábana es aplicada sobre la ventana padre cuando esta ventana se abre.")> _
    <System.ComponentModel.Category("Zeus")> _
    <System.ComponentModel.DefaultValue(False)> _
    Public Property UsaSabana() As Boolean Implements IForma.UsaSabana
        Get
            Return MiUsaSabana
        End Get
        Set(ByVal value As Boolean)
            MiUsaSabana = value
        End Set
    End Property

    '''' <summary>
    '''' Contiene la referencia al usuario que utiliza la forma.
    '''' </summary>
    '''' <remarks>Cuando no se asigna un valor a esta propiedad, el usuario actual se determina de forma dinámica de acuerdo al usuario actual de la sesión.</remarks>
    '<System.ComponentModel.Browsable(False)> _
    'Public Property UsuarioActual() As Usuarios Implements IForma.UsuarioActual
    '    Get
    '        If MiUsuarioActual Is Nothing Then
    '            Return Sesion.UsuarioActual
    '        Else
    '            Return MiUsuarioActual
    '        End If
    '    End Get
    '    Set(ByVal value As Usuarios)
    '        MiUsuarioActual = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Regresa la cultura UI del hilo actual de ejecución.
    ''' </summary>
    <System.ComponentModel.Browsable(False)> _
    Protected Shared ReadOnly Property Cultura() As String
        Get
            If Not My.Application.UICulture.Name.Equals("es-MX") Then
                My.Application.ChangeUICulture("es-MX")
            End If
            Return My.Application.UICulture.Name
        End Get
    End Property

    ''' <summary>
    ''' Muestra la ventana en forma normal.
    ''' </summary>
    Public Sub MostrarVentana() Implements IForma.MostrarVentana

        Show()

    End Sub

    '''' <summary>
    '''' Muestra la ventana en forma normal para un usuario particular.
    '''' </summary>
    '''' <param name="UsuarioActual">Usuario de la ventana.</param>
    'Public Sub MostrarVentana() Implements IForma.MostrarVentana

    '    'MiUsuarioActual = UsuarioActual
    '    MostrarVentana()

    'End Sub

End Class