﻿Public Class Solicitud

    Private MiFolio As String

    Private MiFechaIngreso As Date

    Private MiSolicitante As String 

    Private MiDescripcion As String

    Private MiFechaPrevista As Date

    Private MiNoPiezas As Integer
    Public Property Folio As String
        Get
            Return MiFolio
        End Get
        Set(ByVal value As String)
            MiFolio = value
        End Set
    End Property
    Public Property FechaIngreso As Date
        Get
            Return MiFechaIngreso
        End Get
        Set(ByVal value As Date)
            MiFechaIngreso = value
        End Set
    End Property
    Public Property Solicitante As String
        Get
            Return MiSolicitante
        End Get
        Set(ByVal value As String)
            MiSolicitante = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return MiDescripcion
        End Get
        Set(ByVal value As String)
            MiDescripcion = value
        End Set
    End Property
    Public Property FechaPrevista As Date
        Get
            Return MiFechaPrevista
        End Get
        Set(ByVal value As Date)
            MiFechaPrevista = value
        End Set
    End Property
    Public Property NoPiezas As Integer
        Get
            Return MiNoPiezas
        End Get
        Set(ByVal value As Integer)
            MiNoPiezas = value
        End Set
    End Property

End Class
