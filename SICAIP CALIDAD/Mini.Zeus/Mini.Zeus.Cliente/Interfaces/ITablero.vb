﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas tablero (Dashboard) de Zeus.
''' </summary>
''' <remarks>Todas las ventanas tablero basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface ITablero

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Sub Foco()

    ''' <summary>
    ''' Muestra la pestaña dentro de la ventana padre.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Sub MostrarVentana(ByVal owner As IWin32Window)

    '''' <summary>
    '''' Muestra la pestaña para un usuario particular dentro de la ventana padre.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza la pestaña.</param>
    '''' <param name="owner">Ventana padre.</param>
    'Sub MostrarVentana(ByVal UsuarioActual As Usuarios, ByVal owner As IWin32Window)

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    Property VentanaPadre() As IFormaPrincipal

    ''' <summary>
    ''' Actualiza el contenido de la pestaña.
    ''' </summary>
    ''' <remarks>Esta función actualiza la información que se muestra en la pestaña. La función tiene un evento Antes y un evento Despues.</remarks>
    Sub ActualizarVentana()

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Event AntesActualizarVentana As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Event DespuesActualizarVentana As EventHandler

End Interface
