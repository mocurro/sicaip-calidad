﻿Imports Mini.Zeus.Cliente.UX

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class NRFTsys
    Inherits FormaDialogo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NRFTsys))
        Me.Raiz = New DevExpress.XtraLayout.LayoutControl()
        Me.GridReportadas = New DevExpress.XtraGrid.GridControl()
        Me.FuenteDatosReportadas = New System.Windows.Forms.BindingSource(Me.components)
        Me.VistaReportadas = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridRecibidas = New DevExpress.XtraGrid.GridControl()
        Me.FuenteDatosRecibidas = New System.Windows.Forms.BindingSource(Me.components)
        Me.VistaRecibidas = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SeparatorControl1 = New DevExpress.XtraEditors.SeparatorControl()
        Me.EtiquetaRol = New DevExpress.XtraEditors.LabelControl()
        Me.EtiquetaUsuario = New DevExpress.XtraEditors.LabelControl()
        Me.btnReportar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.BotonAtender = New DevExpress.XtraEditors.SimpleButton()
        Me.EtiquetaReportadas = New DevExpress.XtraEditors.LabelControl()
        Me.EtiquetaRecibidas = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Raiz.SuspendLayout()
        CType(Me.GridReportadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatosReportadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VistaReportadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridRecibidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatosRecibidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VistaRecibidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Raiz
        '
        Me.Raiz.Controls.Add(Me.GridReportadas)
        Me.Raiz.Controls.Add(Me.GridRecibidas)
        Me.Raiz.Controls.Add(Me.SeparatorControl1)
        Me.Raiz.Controls.Add(Me.EtiquetaRol)
        Me.Raiz.Controls.Add(Me.EtiquetaUsuario)
        Me.Raiz.Controls.Add(Me.btnReportar)
        Me.Raiz.Controls.Add(Me.LabelControl1)
        Me.Raiz.Controls.Add(Me.BotonAtender)
        Me.Raiz.Controls.Add(Me.EtiquetaReportadas)
        Me.Raiz.Controls.Add(Me.EtiquetaRecibidas)
        Me.Raiz.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Raiz.Location = New System.Drawing.Point(0, 0)
        Me.Raiz.Name = "Raiz"
        Me.Raiz.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1071, 204, 450, 400)
        Me.Raiz.Root = Me.LayoutControlGroup1
        Me.Raiz.Size = New System.Drawing.Size(1014, 694)
        Me.Raiz.TabIndex = 0
        Me.Raiz.Text = "LayoutControl1"
        '
        'GridReportadas
        '
        Me.GridReportadas.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridReportadas.DataSource = Me.FuenteDatosReportadas
        Me.GridReportadas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridReportadas.Location = New System.Drawing.Point(12, 412)
        Me.GridReportadas.MainView = Me.VistaReportadas
        Me.GridReportadas.Name = "GridReportadas"
        Me.GridReportadas.ShowOnlyPredefinedDetails = True
        Me.GridReportadas.Size = New System.Drawing.Size(990, 253)
        Me.GridReportadas.TabIndex = 15
        Me.GridReportadas.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VistaReportadas})
        '
        'FuenteDatosReportadas
        '
        Me.FuenteDatosReportadas.AllowNew = False
        '
        'VistaReportadas
        '
        Me.VistaReportadas.GridControl = Me.GridReportadas
        Me.VistaReportadas.Name = "VistaReportadas"
        Me.VistaReportadas.OptionsBehavior.Editable = False
        '
        'GridRecibidas
        '
        Me.GridRecibidas.DataSource = Me.FuenteDatosRecibidas
        Me.GridRecibidas.Location = New System.Drawing.Point(12, 120)
        Me.GridRecibidas.MainView = Me.VistaRecibidas
        Me.GridRecibidas.Name = "GridRecibidas"
        Me.GridRecibidas.ShowOnlyPredefinedDetails = True
        Me.GridRecibidas.Size = New System.Drawing.Size(990, 248)
        Me.GridRecibidas.TabIndex = 14
        Me.GridRecibidas.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VistaRecibidas})
        '
        'FuenteDatosRecibidas
        '
        Me.FuenteDatosRecibidas.AllowNew = False
        '
        'VistaRecibidas
        '
        Me.VistaRecibidas.GridControl = Me.GridRecibidas
        Me.VistaRecibidas.Name = "VistaRecibidas"
        Me.VistaRecibidas.OptionsBehavior.Editable = False
        '
        'SeparatorControl1
        '
        Me.SeparatorControl1.Location = New System.Drawing.Point(12, 35)
        Me.SeparatorControl1.Name = "SeparatorControl1"
        Me.SeparatorControl1.Size = New System.Drawing.Size(990, 35)
        Me.SeparatorControl1.TabIndex = 13
        '
        'EtiquetaRol
        '
        Me.EtiquetaRol.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaRol.Appearance.Options.UseFont = True
        Me.EtiquetaRol.Location = New System.Drawing.Point(950, 12)
        Me.EtiquetaRol.Name = "EtiquetaRol"
        Me.EtiquetaRol.Size = New System.Drawing.Size(52, 19)
        Me.EtiquetaRol.StyleController = Me.Raiz
        Me.EtiquetaRol.TabIndex = 12
        '
        'EtiquetaUsuario
        '
        Me.EtiquetaUsuario.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaUsuario.Appearance.Options.UseFont = True
        Me.EtiquetaUsuario.Location = New System.Drawing.Point(12, 12)
        Me.EtiquetaUsuario.Name = "EtiquetaUsuario"
        Me.EtiquetaUsuario.Size = New System.Drawing.Size(129, 19)
        Me.EtiquetaUsuario.StyleController = Me.Raiz
        Me.EtiquetaUsuario.TabIndex = 11
        Me.EtiquetaUsuario.Text = "Nombre usuario"
        '
        'btnReportar
        '
        Me.btnReportar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportar.Appearance.Options.UseFont = True
        Me.btnReportar.Location = New System.Drawing.Point(579, 382)
        Me.btnReportar.Name = "btnReportar"
        Me.btnReportar.Size = New System.Drawing.Size(423, 26)
        Me.btnReportar.StyleController = Me.Raiz
        Me.btnReportar.TabIndex = 10
        Me.btnReportar.Text = "Reportar nueva incidencia"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 669)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(990, 13)
        Me.LabelControl1.StyleController = Me.Raiz
        Me.LabelControl1.TabIndex = 9
        '
        'BotonAtender
        '
        Me.BotonAtender.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BotonAtender.Appearance.Options.UseFont = True
        Me.BotonAtender.Enabled = False
        Me.BotonAtender.Location = New System.Drawing.Point(579, 90)
        Me.BotonAtender.Name = "BotonAtender"
        Me.BotonAtender.Size = New System.Drawing.Size(423, 26)
        Me.BotonAtender.StyleController = Me.Raiz
        Me.BotonAtender.TabIndex = 8
        Me.BotonAtender.Text = "Atender / Modificar"
        '
        'EtiquetaReportadas
        '
        Me.EtiquetaReportadas.Appearance.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaReportadas.Appearance.Options.UseFont = True
        Me.EtiquetaReportadas.Location = New System.Drawing.Point(12, 382)
        Me.EtiquetaReportadas.Name = "EtiquetaReportadas"
        Me.EtiquetaReportadas.Size = New System.Drawing.Size(212, 25)
        Me.EtiquetaReportadas.StyleController = Me.Raiz
        Me.EtiquetaReportadas.TabIndex = 7
        Me.EtiquetaReportadas.Text = "Incidencias reportadas"
        '
        'EtiquetaRecibidas
        '
        Me.EtiquetaRecibidas.Appearance.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaRecibidas.Appearance.Options.UseFont = True
        Me.EtiquetaRecibidas.Location = New System.Drawing.Point(12, 90)
        Me.EtiquetaRecibidas.Name = "EtiquetaRecibidas"
        Me.EtiquetaRecibidas.Size = New System.Drawing.Size(195, 25)
        Me.EtiquetaRecibidas.StyleController = Me.Raiz
        Me.EtiquetaRecibidas.TabIndex = 6
        Me.EtiquetaRecibidas.Text = "Incidencias recibidas"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.EmptySpaceItem5, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.EmptySpaceItem6, Me.LayoutControlItem10, Me.EmptySpaceItem4, Me.EmptySpaceItem7, Me.LayoutControlItem11, Me.LayoutControlItem12})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1014, 694)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(199, 78)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(346, 30)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(216, 370)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(329, 30)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 360)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(994, 10)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.EtiquetaRecibidas
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(199, 30)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.EtiquetaReportadas
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 370)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(216, 30)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.BotonAtender
        Me.LayoutControlItem5.Location = New System.Drawing.Point(567, 78)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(427, 30)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.LabelControl1
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 657)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(994, 17)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.btnReportar
        Me.LayoutControlItem7.Location = New System.Drawing.Point(567, 370)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(427, 30)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 62)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(994, 16)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.EtiquetaUsuario
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(133, 23)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.EtiquetaRol
        Me.LayoutControlItem9.Location = New System.Drawing.Point(938, 0)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(56, 23)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(133, 0)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(805, 23)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.SeparatorControl1
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(994, 39)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(545, 78)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(22, 30)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(545, 370)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(22, 30)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.GridRecibidas
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 108)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(994, 252)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.GridReportadas
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 400)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(994, 257)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'NRFTsys
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1014, 694)
        Me.Controls.Add(Me.Raiz)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NRFTsys"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NRFTsys"
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Raiz.ResumeLayout(False)
        CType(Me.GridReportadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatosReportadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VistaReportadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridRecibidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatosRecibidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VistaRecibidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Raiz As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents BotonAtender As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EtiquetaReportadas As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EtiquetaRecibidas As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaRol As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EtiquetaUsuario As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnReportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SeparatorControl1 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridRecibidas As DevExpress.XtraGrid.GridControl
    Friend WithEvents VistaRecibidas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridReportadas As DevExpress.XtraGrid.GridControl
    Friend WithEvents VistaReportadas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Protected Friend WithEvents FuenteDatosReportadas As BindingSource
    Protected Friend WithEvents FuenteDatosRecibidas As BindingSource
End Class
