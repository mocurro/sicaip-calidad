﻿Imports DevExpress.XtraCharts

Public Class Grafico
    Private Sub LABDIM_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Graficar()
        Catch ex As Exception

        End Try


    End Sub


    Private Sub Graficar()

        Dim Filtro = New Dictionary(Of String, Object)

        Dim Solicitudes As New List(Of Datos.LABDIMSolicitud)
        Dim fechaActual = Date.Now
        GraficaCumplimiento.Series(0).Points.Clear()

        Dim totalañoactual As Integer = 0
        Dim cumplidasañoactual As Integer = 0
        For index = 1 To 12
            Dim fecha = New Date(fechaActual.Year, index, 1)
            Filtro.Clear()
            Filtro.Add("Mes", fecha)
            Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)

            Dim total = Solicitudes.Count
            totalañoactual = totalañoactual + total
            Dim listado2 = Solicitudes.FindAll(Function(c) c.fecha_prevista.HasValue Or c.Fecha_Salida.HasValue)

            Dim cumplidas As Integer = 0
            For Each elemento In listado2
                If elemento.Fecha_Salida <= elemento.fecha_prevista Then
                    cumplidas += 1

                End If
            Next
            cumplidasañoactual = cumplidasañoactual + cumplidas

            Dim porcentaje As Double = 0
            If cumplidas > 0 Then
                porcentaje = (cumplidas * 100) / total
            End If

            'End If

            Select Case index
                Case 1
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Enero", porcentaje))
                Case 2
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Febrero", porcentaje))
                Case 3
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Marzo", porcentaje))
                Case 4
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Abril", porcentaje))
                Case 5
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Mayo", porcentaje))
                Case 6
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Junio", porcentaje))
                Case 7
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Julio", porcentaje))
                Case 8
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Agosto", porcentaje))
                Case 9
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Septiembre", porcentaje))
                Case 10
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Octubre", porcentaje))
                Case 11
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Noviembre", porcentaje))
                Case 12
                    GraficaCumplimiento.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint("Diciembre", porcentaje))
            End Select
            GraficaCumplimiento.Series(0).Label.TextPattern = "{A}: {VP:P0}"
        Next

        Dim porcentajeañoactual As Double = 0
        If cumplidasañoactual > 0 Then
            porcentajeañoactual = (cumplidasañoactual * 100) / totalañoactual
        End If

        CumplimientoXAño.Series(0).Points.Clear()
        Dim Año = Date.Today.Year

        CumplimientoXAño.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", Año - 1), graficaXAño(Año)))
        CumplimientoXAño.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", Año), porcentajeañoactual))

        FueraTiempoProgramado.Series(0).Points.Clear()
        Solicitudes.Clear()
        Filtro.Clear()
        Filtro.Add("Mes", fechaActual)
        Filtro.Add("Dia", fechaActual)
        Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)
        Dim sumatoria As Integer = 0
        For x = 24 To 30
            If fechaActual.Day = x Then
                Solicitudes.Clear()
                Filtro.Clear()
                Filtro.Add("Mes", fechaActual)
                Filtro.Add("Dia", fechaActual)
                Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)
                If Solicitudes IsNot Nothing Then
                    Dim listado2 = Solicitudes.FindAll(Function(c) Not c.Fecha_Salida.HasValue)
                    sumatoria += listado2.Count
                End If


                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), sumatoria))

            ElseIf fechaActual.Day < x Then
                Dim fecha = New Date(fechaActual.Year, fechaActual.Month, x)
                Solicitudes.Clear()
                Filtro.Clear()
                Filtro.Add("Mes", fecha)
                Filtro.Add("Dia", fecha)
                Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)

                If Solicitudes IsNot Nothing Then
                    Dim listado2 = Solicitudes.FindAll(Function(c) Not c.Fecha_Salida.HasValue)
                    sumatoria += listado2.Count
                End If

                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), sumatoria))
            Else
                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), 0))
            End If
        Next
        sumatoria = 0
        For x = 1 To 23

            If fechaActual.Day = x Then
                Solicitudes.Clear()
                Filtro.Clear()
                Filtro.Add("Mes", fechaActual)
                Filtro.Add("Dia", fechaActual)
                Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)
                If Solicitudes IsNot Nothing Then
                    Dim listado2 = Solicitudes.FindAll(Function(c) Not c.Fecha_Salida.HasValue)
                    sumatoria += listado2.Count
                End If

                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), sumatoria))

            ElseIf fechaActual.Day < x Then
                Dim fecha = New Date(fechaActual.Year, fechaActual.Month, x)
                Solicitudes.Clear()
                Filtro.Clear()
                Filtro.Add("Mes", fecha)
                Filtro.Add("Dia", fecha)
                Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)

                If Solicitudes IsNot Nothing Then
                    Dim listado2 = Solicitudes.FindAll(Function(c) Not c.Fecha_Salida.HasValue)
                    sumatoria += listado2.Count
                End If

                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), sumatoria))
            Else
                FueraTiempoProgramado.Series(0).Points.Add(New DevExpress.XtraCharts.SeriesPoint(String.Format("'{0}'", x), 0))
            End If
        Next

    End Sub

    Private Sub GraficaCumplimiento_CustomDrawSeriesPoint(sender As Object, e As CustomDrawSeriesPointEventArgs) Handles GraficaCumplimiento.CustomDrawSeriesPoint

    End Sub

    'para saber el ultimo dia del mes
    Function UltimoDiaDelMes(ByVal dtmFecha As Date) As Date
        UltimoDiaDelMes = DateSerial(Year(dtmFecha), Month(dtmFecha) + 1, 0)
    End Function


    Public Function graficaXAño(ByVal año As Integer) As Double
        Dim Filtro = New Dictionary(Of String, Object)
        Dim Solicitudes As New List(Of Datos.LABDIMSolicitud)
        Dim totalañoactual As Integer = 0
        Dim cumplidasañoactual As Integer = 0
        For index = 1 To 12
            Dim fecha = New Date(año - 1, index, 1)
            Filtro.Clear()
            Filtro.Add("Mes", fecha)
            Solicitudes = Datos.Fabricas.LABDIMSolicitud.ObtenerExistentes(Filtro)

            Dim total = Solicitudes.Count
            totalañoactual = totalañoactual + total
            Dim listado2 = Solicitudes.FindAll(Function(c) c.fecha_prevista.HasValue Or c.Fecha_Salida.HasValue)

            Dim cumplidas As Integer = 0
            For Each elemento In listado2
                If elemento.Fecha_Salida <= elemento.fecha_prevista Then
                    cumplidas += 1
                End If
            Next
            cumplidasañoactual = cumplidasañoactual + cumplidas
        Next
        Dim porcentajeañoactual As Double = 0
        If cumplidasañoactual > 0 Then
            porcentajeañoactual = (cumplidasañoactual * 100) / totalañoactual
        End If
        Return porcentajeañoactual
    End Function

End Class