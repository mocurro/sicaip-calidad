﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los metadatos de la tabla Unidad_Negocio
''' </summary>
<Flags()>
Public Enum Unidad_NegocioMetadatos As Integer

    ''' <summary>
    ''' Ninguno
    ''' </summary>
    Ninguno = 0

End Enum