﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================
Imports System.Windows.Forms.Form
''' <summary>
''' Ventana base para cualquier diálogo que forma parte de una aplicación de Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana diálogo que implemente UX de Zeus. Todos los diálogos heredan finalmente de esta ventana.</remarks>
Public Class FormaDialogo
    Implements IDialogo

    ''' <summary>
    ''' Muestra la ventana de diálogo.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Function MostrarVentana(ByVal owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult Implements IDialogo.MostrarVentana
        Try
            Return ShowDialog(owner)
        Catch ex As Exception
            QuitarSabana()
        End Try


    End Function

End Class
