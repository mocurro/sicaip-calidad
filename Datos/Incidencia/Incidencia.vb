﻿Public Class Incidencia
    Private MiFolio As String

    Private MiFechaReporte As Date 

    Private MiSeccion As String 

    Private MiAtributo As String 

    Private MiDetalles As String 

    Private MiComentarios As String
    Public Property Folio As String
        Get
            Return MiFolio
        End Get
        Set(ByVal value As String)
            MiFolio = value
        End Set
    End Property
    Public Property FechaReporte As Date
        Get
            Return MiFechaReporte
        End Get
        Set(ByVal value As Date)
            MiFechaReporte = value
        End Set
    End Property
    Public Property Seccion As String
        Get
            Return MiSeccion
        End Get
        Set(ByVal value As String)
            MiSeccion = value
        End Set
    End Property
    Public Property Atributo As String
        Get
            Return MiAtributo
        End Get
        Set(ByVal value As String)
            MiAtributo = value
        End Set
    End Property
    Public Property Detalles As String
        Get
            Return MiDetalles
        End Get
        Set(ByVal value As String)
            MiDetalles = value
        End Set
    End Property
    Public Property Comentarios As String
        Get
            Return MiComentarios
        End Get
        Set(ByVal value As String)
            MiComentarios = value
        End Set
    End Property
End Class
