﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Sabana.
''' </summary>
''' <remarks>Esta ventana funge como sábana para cubrir ventanas que no están activas, con la intención de orientar al usuario hacia la interface correcta.</remarks>
Public Class Sabana

    ''' <summary>
    ''' Muestra la sábana sin ningún mensaje.
    ''' </summary>
    ''' <param name="owner">Ventana que se cubre con esta sábana.</param>
    ''' <remarks>Esta función muestra la sábana en su forma estandar.</remarks>
    Public Sub MostrarVentana(ByVal owner As IWin32Window)

        GrupoProgreso.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        Show(owner)

    End Sub

    ''' <summary>
    ''' Muestra la sábana con un mensaje.
    ''' </summary>
    ''' <param name="Titulo">El título del mensaje.</param>
    ''' <param name="Mensaje">El mensaje a mostrar dentro de la sábana.</param>
    ''' <param name="owner">Ventana que se cubre con esta sábana.</param>
    ''' <remarks>Esta función muestra la sábana con un mensaje para retroalimentar al usuario.</remarks>
    Public Sub MostrarVentana(ByVal Titulo As String, ByVal Mensaje As String, ByVal owner As IWin32Window)

        GrupoProgreso.Text = Titulo
        EtiquetaMensaje.Text = Mensaje
        GrupoProgreso.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        Show(owner)

    End Sub

End Class