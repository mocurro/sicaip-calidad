﻿Imports System.IO
Imports System.Text

Public Module SesiónLocal
    Private UsuarioActual As SEGURIDAD_USUARIO


    Private MiHilo As System.Threading.Thread

    Public Property Usuario As SEGURIDAD_USUARIO
        Get
            Return UsuarioActual
        End Get
        Set(value As SEGURIDAD_USUARIO)
            UsuarioActual = value
        End Set
    End Property

    ''' <summary>
    ''' Cuerpo del correo
    ''' </summary>
    Private body_notificacion As String

    ''' <summary>
    ''' Correo destinatario
    ''' </summary>
    Private Para As New List(Of String)

    Private ListadoArchivos As New List(Of String)

    'Private FromEmail As String = "sicaip.soporte@gkndriveline.com"
    'Private FromDescEmail As String = "sicaip.soporte@gkndriveline.com"
    'Private CredentialUser As String = "sicaip.soporte@gkndriveline.com"

    'Private CredentialPass As String = "Pass12705"
    'Private HostEmail As String = "SMTP-US.gkn.com"
    'Private PortEmail As String = "25"
    'Private SSLEmail As String = "False"

    Private FromEmail As String = "auto.notificaciones@verifid.net"
    Private FromDescEmail As String = "auto.notificaciones@verifid.net"
    Private CredentialUser As String = "auto.notificaciones@verifid.net"

    Private CredentialPass As String = "AntoraVerifID1"
    Private HostEmail As String = "smtp.office365.com "
    Private PortEmail As String = "587"
    Private SSLEmail As String = "True"

    Public Sub crear_contenido_mensaje_NRFTNueva(ByVal Defecto As Datos.Defecto, ByVal cantidadEncontrada As Integer, ByVal cantidadRechazada As Integer, ByVal detalle As String)
        body_notificacion = Nothing
        'body_notificacion = "Buen día." & vbCrLf & vbCrLf
        body_notificacion += "Se generó un reclamo con la siguiente información:" & vbCrLf
        body_notificacion += String.Format("Defecto: {1}{0}", vbCrLf, Defecto.nombre)
        body_notificacion += String.Format("Cantidad encontrada: {1}{0}", vbCrLf, cantidadEncontrada)
        body_notificacion += String.Format("Cantidad rechazada: {1}{0}", vbCrLf, cantidadRechazada)
        body_notificacion += String.Format("Detalle: {1}{0}", vbCrLf, detalle)
        'body_notificacion += String.Format("Código de Empleado: {1}{0}", vbCrLf, lblCodigoEmpleado.Text)
        'body_notificacion += String.Format("Fecha del Registro: {1:dd-MM-yyyy HH:mm}{0}{0}", vbCrLf, Now)
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\GKNSICAIP_V2\ClickOnce\GKN-SICAIP.application" & vbCrLf & vbCrLf
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\SICAIP_GRAFICOS_TEST\ClickOnce\GKNSICAIP_Test.application" & vbCrLf & vbCrLf
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf

        'subject_notificacion = String.Format("Registro de Paro Planeado Pre-Aprobado en el Equipo: {0} - Línea: {1}", lblNombreEquipo.Text, cbxLinea.Text)
    End Sub


    Public Sub crear_contenido_mensaje_NRFTAtender(ByVal Defecto As Datos.Reclamo)
        body_notificacion = Nothing
        body_notificacion += "Se atendío un reclamo con la siguiente información:" & vbCrLf
        body_notificacion += String.Format("Folio: {1}{0}", vbCrLf, CType(Defecto.cve_reclamo.ToString.PadLeft(5, CChar("0")), String))
        body_notificacion += String.Format("Cantidad encontrada: {1}{0}", vbCrLf, Defecto.cantidad_encontrada)
        body_notificacion += String.Format("Cantidad rechazada: {1}{0}", vbCrLf, Defecto.cantidad_rechazada)
        body_notificacion += String.Format("Detalle: {1}{0}", vbCrLf, Defecto.detalle)
        body_notificacion += String.Format("Comentarios: {1}{0}", vbCrLf, Defecto.comentarios)
        'body_notificacion += String.Format("Código de Empleado: {1}{0}", vbCrLf, lblCodigoEmpleado.Text)
        'body_notificacion += String.Format("Fecha del Registro: {1:dd-MM-yyyy HH:mm}{0}{0}", vbCrLf, Now)
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\GKNSICAIP_V2\ClickOnce\GKN-SICAIP.application" & vbCrLf & vbCrLf
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\SICAIP_GRAFICOS_TEST\ClickOnce\GKNSICAIP_Test.application" & vbCrLf & vbCrLf
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf

        'subject_notificacion = String.Format("Registro de Paro Planeado Pre-Aprobado en el Equipo: {0} - Línea: {1}", lblNombreEquipo.Text, cbxLinea.Text)
    End Sub


    Public Sub crear_contenido_mensaje_NRFTRechazo(ByVal Defecto As Datos.Reclamo, ByVal area As Datos.detalle)
        body_notificacion = Nothing
        body_notificacion += "Se rechazó un reclamo con la siguiente información:" & vbCrLf
        body_notificacion += String.Format("Folio: {1}{0}", vbCrLf, CType(Defecto.cve_reclamo.ToString.PadLeft(5, CChar("0")), String))
        body_notificacion += String.Format("Se reasignó al área: {1}{0}", vbCrLf, area.descripcion)
        body_notificacion += String.Format("Cantidad encontrada: {1}{0}", vbCrLf, Defecto.cantidad_encontrada)
        body_notificacion += String.Format("Cantidad rechazada: {1}{0}", vbCrLf, Defecto.cantidad_rechazada)
        body_notificacion += String.Format("Detalle: {1}{0}", vbCrLf, Defecto.detalle)
        body_notificacion += String.Format("Comentarios: {1}{0}", vbCrLf, Defecto.comentarios)
        'body_notificacion += String.Format("Código de Empleado: {1}{0}", vbCrLf, lblCodigoEmpleado.Text)
        'body_notificacion += String.Format("Fecha del Registro: {1:dd-MM-yyyy HH:mm}{0}{0}", vbCrLf, Now)
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\GKNSICAIP_V2\ClickOnce\GKN-SICAIP.application" & vbCrLf & vbCrLf
        'body_notificacion += "Accesa a SICAIP: \\celsr014\gknsicaip$\SICAIP_GRAFICOS_TEST\ClickOnce\GKNSICAIP_Test.application" & vbCrLf & vbCrLf
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf

        'subject_notificacion = String.Format("Registro de Paro Planeado Pre-Aprobado en el Equipo: {0} - Línea: {1}", lblNombreEquipo.Text, cbxLinea.Text)
    End Sub


    Public Sub crear_contenido_mensaje_LabDimSolicitud(ByVal Solicitud As Datos.LABDIMSolicitud ,byval Usuario As SEGURIDAD_USUARIO )
        body_notificacion = Nothing
        body_notificacion += "Se ha generado una solicitud de inspección para atenderlo accede a LabDim" & vbCrLf
        body_notificacion += String.Format("ID: {1}{0}", vbCrLf,  Solicitud.cve_solicitud.ToString .PadLeft (5,CChar ("0") ))
        body_notificacion += String.Format("Solicitante: {1}{0}", vbCrLf, Usuario.Nombre )
        body_notificacion += String.Format("{0}Número de piezas: {1}", vbCrLf, Solicitud.no_piezas)
        body_notificacion += String.Format("{0}Descripción: {1}{0}", vbCrLf, Solicitud.descripcion)
        Dim TieneSolicitud As Boolean =False 
        If Solicitud IsNot Nothing Then
            TieneSolicitud=True 
        End If
        body_notificacion += String.Format("Diseño: {1} {0}", vbCrLf, TieneSolicitud.ToString)
        body_notificacion += String.Format("Comentarios: {1} {0}{0}", vbCrLf, Solicitud.comentarios)
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf
        
    End Sub

    Public Sub crear_contenido_mensaje_LabDimModificarSolicitud(ByVal Solicitud As Datos.LABDIMSolicitud, ByVal Usuario As SEGURIDAD_USUARIO)
        body_notificacion = Nothing
        body_notificacion += "Se ha modificado una solicitud de inspección para ver el estado accede a LabDim" & vbCrLf
        body_notificacion += String.Format("ID: {1}{0}", vbCrLf, Solicitud.cve_solicitud.ToString.PadLeft(5, CChar("0")))
        'body_notificacion += String.Format("Fecha de registo: {1}{0}", vbCrLf, Solicitud.Fecha_Entrada)
        'body_notificacion += String.Format("Fecha prevista: {1}{0}", vbCrLf, Solicitud.fecha_prevista)
        body_notificacion += String.Format("Solicitante: {1}{0}", vbCrLf, Usuario.Nombre)
        body_notificacion += String.Format("{0}Número de piezas: {1} {0}", vbCrLf, Solicitud.no_piezas)
        body_notificacion += String.Format("Descripción: {1}{0}", vbCrLf, Solicitud.descripcion)
        'body_notificacion += String.Format("{0}Número de cotas por pieza: {1} {0}", vbCrLf, Solicitud.no_cotas)
        body_notificacion += String.Format("Comentarios: {1} {0}", vbCrLf, Solicitud.comentarios)
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf

    End Sub


    Public Sub crear_contenido_mensaje_LabDimFinalizarSolicitud(ByVal Solicitud As Datos.LABDIMSolicitud, ByVal Usuario As SEGURIDAD_USUARIO)
        body_notificacion = Nothing
        body_notificacion += "Se ha finalizado una solicitud de inspección para ver el reporte accede a LabDim" & vbCrLf
        body_notificacion += String.Format("ID: {1}{0}", vbCrLf, Solicitud.cve_solicitud.ToString.PadLeft(5, CChar("0")))
        body_notificacion += String.Format("Fecha de registo: {1}{0}", vbCrLf, Solicitud.Fecha_Entrada)
        body_notificacion += String.Format("Fecha prevista: {1}{0}", vbCrLf, Solicitud.fecha_prevista)
        body_notificacion += String.Format("Fecha de salida: {1}{0}", vbCrLf, Solicitud.Fecha_Salida)
        body_notificacion += String.Format("Solicitante: {1}{0}", vbCrLf, Usuario.Nombre)
        body_notificacion += String.Format("{0}Número de piezas: {1} {0}", vbCrLf, Solicitud.no_piezas)
        body_notificacion += String.Format("Descripción: {1}{0}", vbCrLf, Solicitud.descripcion)
        body_notificacion += String.Format("{0}Número de cotas por pieza: {1} {0}", vbCrLf, Solicitud.no_cotas)
        body_notificacion += String.Format("Comentarios: {1} {0}", vbCrLf, Solicitud.comentarios)
        body_notificacion += "Este documento solo es informativo, y en caso de alguna aclaración favor de reportar de manera oportuna con la persona correspondiente." & vbCrLf & vbCrLf

    End Sub


    Public Sub envia_notificacion_adjunto(ByVal from As List(Of String), ByVal MiListadoArchivos As List(Of String))
        'Para.Clear()
        'ListadoArchivos.Clear()
        Para = from
        ListadoArchivos = MiListadoArchivos
        MiHilo = New System.Threading.Thread(AddressOf NotificarAdjunto)
        MiHilo.Start()
    End Sub

    Private Sub NotificarAdjunto()
        Try

            Dim UseSSLEmail = False
            If SSLEmail = "True" Then
                UseSSLEmail = True
            ElseIf SSLEmail = "False" Then
                UseSSLEmail = False
            Else
                UseSSLEmail = False
            End If
            Dim Message As New System.Net.Mail.MailMessage()
            'CONFIGURACIÓN DEL STMP
            Dim SMTP As New System.Net.Mail.SmtpClient() With {.Credentials = New System.Net.NetworkCredential(CredentialUser, CredentialPass), .Host = HostEmail, .Port = Integer.Parse(PortEmail), .EnableSsl = UseSSLEmail}
            ' CONFIGURACION DEL MENSAJE
            For Each correo In Para
                Message.[To].Add(correo)
            Next
            'Cuenta de Correo al que se le quiere enviar el e-mail
            Message.From = New System.Net.Mail.MailAddress(FromEmail, FromDescEmail, System.Text.Encoding.UTF8) 'Quien lo envía
            Message.Subject = "Notificación SICAIP Calidad" 'subject_notificacion 'Sujeto del e-mail
            Message.SubjectEncoding = System.Text.Encoding.UTF8 'Codificacion
            Message.Body = body_notificacion 'contenido del mail
            Message.BodyEncoding = System.Text.Encoding.UTF8
            Message.Priority = System.Net.Mail.MailPriority.Normal
            Message.IsBodyHtml = False
            Dim attach As System.Net.Mail.Attachment
            For Each file In ListadoArchivos
                attach = New System.Net.Mail.Attachment(file)
                Message.Attachments.Add(attach)
            Next

            'ENVIO
            SMTP.Send(Message)
            If attach IsNot Nothing Then
                attach.Dispose()
            End If
            'Catch ex As System.Net.Mail.SmtpException
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub envia_notificacion_paro_z(ByVal from As List(Of String))
        Para.Clear 
        Para = from
        MiHilo = New System.Threading.Thread(AddressOf Notificar)
        MiHilo.Start()
    End Sub

    Private Sub Notificar
        Try

            Dim UseSSLEmail = False
            If SSLEmail = "True" Then
                UseSSLEmail = True
            ElseIf SSLEmail = "False" Then
                UseSSLEmail = False
            Else
                UseSSLEmail = False
            End If
            Dim Message As New System.Net.Mail.MailMessage()
            'CONFIGURACIÓN DEL STMP
            Dim SMTP As New System.Net.Mail.SmtpClient() With {.Credentials = New System.Net.NetworkCredential(CredentialUser, CredentialPass), .Host = HostEmail, .Port = Integer.Parse(PortEmail), .EnableSsl = UseSSLEmail}
            ' CONFIGURACION DEL MENSAJE
            For Each correo In Para
                Message.[To].Add(correo)
            Next
            'Cuenta de Correo al que se le quiere enviar el e-mail
            Message.From = New System.Net.Mail.MailAddress(FromEmail, FromDescEmail, System.Text.Encoding.UTF8) 'Quien lo envía
            Message.Subject = "Notificación SICAIP Calidad" 'subject_notificacion 'Sujeto del e-mail
            Message.SubjectEncoding = System.Text.Encoding.UTF8 'Codificacion
            Message.Body = body_notificacion 'contenido del mail
            Message.BodyEncoding = System.Text.Encoding.UTF8
            Message.Priority = System.Net.Mail.MailPriority.Normal
            Message.IsBodyHtml = False
            'ENVIO
            SMTP.Send(Message)
            'Catch ex As System.Net.Mail.SmtpException
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    
End Module
