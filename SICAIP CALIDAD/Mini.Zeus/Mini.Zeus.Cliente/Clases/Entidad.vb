﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Objeto base del cual derivan todos los objetos de Zeus que representan un registro de la base de datos.
''' </summary>
''' <typeparam name="TTipoEntidad">Define el tipo específico de entidad.</typeparam>
''' <typeparam name="TEnumeradorCampos">Define el enumerador que contiene los campos de la entidad específica.</typeparam>
''' <remarks>Esta clase incluye el atributo de contrato de datos (DataContract) para poder consumirlo en un WCF.</remarks>
<Runtime.Serialization.DataContract()> _
Public MustInherit Class Entidad(Of TTipoEntidad, TEnumeradorCampos)
    Inherits Objeto
    Implements IEntidad
    Implements IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos)

    ''' <summary>
    ''' Determina si la entidad es de solo lectura o no.
    ''' </summary>
    ''' <remarks>Esta variable se carga de determinar si la entidad es de solo lectura o no.</remarks>
    Private MiEsSoloLectura As Boolean = False

    ''' <summary>
    ''' Contiene la lista de atributos de permisos de la entidad.
    ''' </summary>
    ''' <remarks>Esta lista se carga la primera ves que sea requerida y se utiliza para validar los permisos de una entidad.</remarks>
    Private MisPermisos As List(Of PermisoAttribute)

    ''' <summary>
    ''' Contiene la lista de atributos de texto de la entidad.
    ''' </summary>
    ''' <remarks>Esta lista se carga la primera ves que sea requerida y se utiliza para exponer los textos de una entidad.</remarks>
    Private MisTextos As List(Of TextoAttribute)

    ''' <summary>
    ''' Contiene la lista de atributos de columnas de la entidad.
    ''' </summary>
    ''' <remarks>Esta lista se carga la primera ves que sea requerida y se utiliza para definir las columnas y sus propiedades para un grid.</remarks>
    Private MisColumnas As List(Of ColumnaAttribute)

    ''' <summary>
    ''' Contiene la información original de la entidad.
    ''' </summary>
    ''' <remarks>Esta variable contiene información que permite determinar si la entidad está sucia, es decir, que ha cambiado.</remarks>
    <Runtime.Serialization.DataMember()> _
    Protected MiOriginal As TTipoEntidad
    ''' <summary>
    ''' Contiene el estatus actual de la entidad.
    ''' </summary>
    <Runtime.Serialization.DataMember()> _
    Protected MiEstatusEntidad As EstatusEntidades

    ''' <summary>
    ''' Determina si la entidad ya fue inicializado.
    ''' </summary>
    <Runtime.Serialization.DataMember()> _
    Private MiEstaInicializado As Boolean

    ''' <summary>
    ''' Determina si la entidad ha sido marcada para ser borrada posteriormente.
    ''' </summary>
    ''' <remarks>Este valor puede alterar el resultado obtenido al consultar el estatus de la entidad.</remarks>
    <Runtime.Serialization.DataMember()> _
    Private MiEstaPendienteBorrar As Boolean

    '''' <summary>
    '''' Determina si la entidad ha sido marcada para ser recuperada posteriormente.
    '''' </summary>
    '''' <remarks>Este valor puede alterar el resultado obtenido al consultar el estatus de la entidad.</remarks>
    '<Runtime.Serialization.DataMember()> _
    'Private MiEstaPendienteRecuperar As Boolean

    ''' <summary>
    ''' Determina si la entidad ha sido marcada para ser destruida posteriormente.
    ''' </summary>
    ''' <remarks>Este valor puede alterar el resultado obtenido al consultar el estatus de la entidad.</remarks>
    <Runtime.Serialization.DataMember()> _
    Private MiEstaPendienteDestruir As Boolean




#Region "Propiedades base de cualquier entidad"
    ''' <summary>
    ''' Determina si la entidad es de solo lectura o no.
    ''' </summary>
    Protected Friend Property EsSoloLectura() As Boolean Implements IEntidad.EsSoloLectura
        Get
            Return MiEsSoloLectura
        End Get
        Set(ByVal value As Boolean)
            MiEsSoloLectura = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene los metadatos de la entidad.
    ''' </summary>
    Protected Friend MustOverride Property MetadatosEntidad() As Integer Implements IEntidad.MetadatosEntidad

    ''' <summary>
    ''' Regresa la cuenta de eliminado del registro tal y como está guardado en la base de datos.
    ''' </summary>
    Protected Friend MustOverride Property CuentaEliminadoEntidad() As Integer Implements IEntidad.CuentaEliminadoEntidad


    ''' <summary>
    ''' Regresa la fecha y hora de creación del registro tal y como está guardado en la base de datos (Hora Universal).
    ''' </summary>
    Protected Friend MustOverride Property FechaHoraCreacionEntidad() As Date Implements IEntidad.FechaHoraCreacionEntidad

    ''' <summary>
    ''' Regresa la fecha y hora de la última modificación del registro tal y como está guardado en la base de datos (Hora Universal).
    ''' </summary>
    Protected Friend MustOverride Property FechaHoraModificacionEntidad() As Date Implements IEntidad.FechaHoraModificacionEntidad
#End Region

    ''' <summary>
    ''' Determina si el campo metadatos tiene un valor en particular activado.
    ''' </summary>
    ''' <param name="Valor">El valor que se desea validar.</param>
    Public Function MetadatosContiene(ByVal Valor As Integer) As Boolean

        Return (MetadatosEntidad And Valor) = Valor

    End Function

    ''' <summary>
    ''' Activa un valor en particular para el campo metadatos.
    ''' </summary>
    ''' <param name="Valor">El valor que se desea activar sin alterar el resto del campo metadatos.</param>
    Public Sub MetadatosActivar(ByVal Valor As Integer)

        MetadatosEntidad = MetadatosEntidad Or Valor

    End Sub

    ''' <summary>
    ''' Desactiva un valor en particular para el campo metadatos.
    ''' </summary>
    ''' <param name="Valor">El valor que se desea desactivar sin alterar el resto del campo metadatos.</param>
    Public Sub MetadatosDesactivar(ByVal Valor As Integer)

        MetadatosEntidad = (MetadatosEntidad Or Valor) Xor Valor

    End Sub

    ''' <summary>
    ''' Completa la inicialización de la entidad como una nueva.
    ''' </summary>
    Public Sub InicializarNuevo() Implements IEntidad.InicializarNuevo

        If Not MiEstaInicializado Then

            MiEstatusEntidad = EstatusEntidades.Nuevo

            InicializarNuevoCampos()
            InicializarListasComoNuevas()
            InicializarOriginal()
            MiEstaInicializado = True
        End If

    End Sub

    ''' <summary>
    ''' Permite que entidades heredadas implementen inicialización de campos para entidades nuevas.
    ''' </summary>
    ''' <remarks>Esta función permite que una entidad particular implemente código de inicialización adicional para entidades nuevas.</remarks>
    Protected Overridable Sub InicializarNuevoCampos()

        'No hace nada, la implementación depende enteramente de entidades heredadas

    End Sub

    ''' <summary>
    ''' Inicializa todas las entidades de las listas como nuevas.
    ''' </summary>
    ''' <remarks>Esta función se ejecuta como resultado de una inicialización como nueva de la entidad y que es necesario propagar hacia las listas.</remarks>
    Protected Overridable Sub InicializarListasComoNuevas()

        'Por default, dado que se asume que no hay listas, no se realiza acción alguna

    End Sub

    ''' <summary>
    ''' Completa la inicialización de la entidad cargada desde la base de datos.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que fabrico la entidad.</param>
    ''' <param name="FabricaUsuarios">La fábrica de usuarios para obtener los usuarios de creación y modificación.</param>
    Public Sub InicializarExistente() Implements IEntidad.InicializarExistente

        If Not MiEstaInicializado Then

            If CuentaEliminadoEntidad = 0 Then
                MiEstatusEntidad = EstatusEntidades.Existente
            Else
                MiEstatusEntidad = EstatusEntidades.Borrado
            End If
            InicializarExistenteCampos()
            InicializarListasComoExistentes()
            InicializarOriginal()
            MiEstaInicializado = True
        End If

    End Sub

    ''' <summary>
    ''' Permite que entidades heredadas implementen inicialización de campos para entidades existentes.
    ''' </summary>
    ''' <remarks>Esta función permite que una entidad particular implemente código de inicialización adicional para entidades existentes.</remarks>
    Protected Overridable Sub InicializarExistenteCampos()

        'No hace nada, la implementación depende enteramente de entidades heredadas

    End Sub

    ''' <summary>
    ''' Inicializa todas las entidades de las listas como existentes.
    ''' </summary>
    ''' <remarks>Esta función se ejecuta como resultado de una inicialización como existente de la entidad y que es necesario propagar hacia las listas.</remarks>
    Protected Overridable Sub InicializarListasComoExistentes()

        'Por default, dado que se asume que no hay listas, no se realiza acción alguna

    End Sub

    ''' <summary>
    ''' Genera la copia de la información para poder realizar comparaciones entre datos originales y datos actuales.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub InicializarOriginal() Implements IEntidad.InicializarOriginal

        MiOriginal = CrearNuevoOriginalEntidad()
        With DirectCast(MiOriginal, IEntidad)
            CopiarAOriginalEntidad()
            .MetadatosEntidad = MetadatosEntidad
            .CuentaEliminadoEntidad = CuentaEliminadoEntidad
            .FechaHoraCreacionEntidad = FechaHoraCreacionEntidad
            .FechaHoraModificacionEntidad = FechaHoraModificacionEntidad
        End With

    End Sub

    ''' <summary>
    ''' Restaura la información de la entidad a sus valores originales.
    ''' </summary>
    ''' <remarks>Esta función toma la información almacenada como original para convertirla en la información vigente. La información original se actualiza automáticamente al realizar una operación exitosa en la fábrica de entidades.</remarks>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Nuevo, Existente o PendienteBorrar.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub RestaurarOriginal() Implements IEntidad.RestablecerOriginal

        'Determina si la entidad se puede restaurar
        If EstatusEntidad = EstatusEntidades.Nuevo Or _
           EstatusEntidad = EstatusEntidades.Existente Or _
           EstatusEntidad = EstatusEntidades.PendienteBorrar Or _
           EstatusEntidad = EstatusEntidades.PendienteDestruir Then

            'Restaurar original
            MiEstaPendienteBorrar = False
            MiEstaPendienteDestruir = False
            With DirectCast(MiOriginal, IEntidad)
                CopiarDesdeOriginalEntidad()
                MetadatosEntidad = .MetadatosEntidad
                CuentaEliminadoEntidad = .CuentaEliminadoEntidad
                FechaHoraCreacionEntidad = .FechaHoraCreacionEntidad
                FechaHoraModificacionEntidad = .FechaHoraModificacionEntidad
            End With
            RestaurarListasOriginal()

        Else

            'Estatus inválido, arrojar excepción
            Throw New ExcepcionOperacionInvalidaPorEstatus(Me, Reflection.MethodBase.GetCurrentMethod())

        End If

    End Sub

    ''' <summary>
    ''' Define que el estatus actual de datos de la entidad se convierta en el original.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub AceptarComoOriginal() Implements IEntidad.AceptarComoOriginal

        'Determina si la entidad se puede aceptar como original
        If EstatusEntidad = EstatusEntidades.Nuevo Or _
           EstatusEntidad = EstatusEntidades.Existente Or _
           EstatusEntidad = EstatusEntidades.PendienteBorrar Or _
           EstatusEntidad = EstatusEntidades.PendienteDestruir Then

            'Actualizar información original
            With DirectCast(MiOriginal, IEntidad)
                CopiarAOriginalEntidad()
                .MetadatosEntidad = MetadatosEntidad
                .CuentaEliminadoEntidad = CuentaEliminadoEntidad
                .FechaHoraCreacionEntidad = FechaHoraCreacionEntidad
                .FechaHoraModificacionEntidad = FechaHoraModificacionEntidad
            End With

        Else

            'Estatus inválido, arrojar excepción
            Throw New ExcepcionOperacionInvalidaPorEstatus(Me, Reflection.MethodBase.GetCurrentMethod())

        End If

    End Sub

    ''' <summary>
    ''' Genera un nuevo objeto repositorio de la información original de la entidad.
    ''' </summary>
    Protected MustOverride Function CrearNuevoOriginalEntidad() As TTipoEntidad

    ''' <summary>
    ''' Genera la copia de la información para poder realizar comparaciones entre datos originales y datos actuales.
    ''' </summary>
    Protected MustOverride Sub CopiarAOriginalEntidad()

    ''' <summary>
    ''' Restaura la inforamción de la entidad a sus valores originales.
    ''' </summary>
    Protected MustOverride Sub CopiarDesdeOriginalEntidad()

    ''' <summary>
    ''' Propaga el proceso de restauración de original a las listas.
    ''' </summary>
    Protected Overridable Sub RestaurarListasOriginal()

        'Se asume que no hay listas, por lo que no se ejecuta código alguno

    End Sub

    ''' <summary>
    ''' Indica si la información de al menos un campo de la entidad es diferente a la información original.
    ''' </summary>
    ''' <remarks>Para entidades nuevas, esta función siempre regresa True. Para entidades existentes, la función regresa True cuando la información de algún campo es diferente a la información que existía en la base de datos al momento de cargar la información. Para cualquier otro estatus de entidad, esta función regresa siempre Falso.</remarks>
    Public Function EstaSucio() As Boolean Implements IEntidad.EstaSucio

        'Determinar si la entidad está pendiente para borrar, lo cual se considera como sucio.
        If MiEstaPendienteBorrar Or MiEstaPendienteDestruir Then Return True

        'Selecciona en base al estatus de la entidad
        Select Case MiEstatusEntidad
            Case EstatusEntidades.Nuevo
                'La entidad es nueva, por lo que siempre se considera sucia.
                Return True

            Case EstatusEntidades.Existente
                'La entidad existe, depende de la evaluación de su información.
                Return (Not Equivalente(MiOriginal)) OrElse _
                       EstanSuciosCamposFueraEquivalenciaEntidad() OrElse _
                       (Not Equals(MetadatosEntidad, DirectCast(MiOriginal, IEntidad).MetadatosEntidad)) OrElse _
                       EstanSuciasListasEntidad()

            Case Else
                'La entidad tiene un estatus que se puede asumir no permite manipularla.
                Return False

        End Select

    End Function

    ''' <summary>
    ''' Indica si la información de al menos un campo no contemplado dentro de la función de Equivalente es diferente a la información original.
    ''' </summary>
    ''' <remarks>Esta función sólo se ejecuta cuando el estatus de la entidad lo requiere (Existente).</remarks>
    Protected Overridable Function EstanSuciosCamposFueraEquivalenciaEntidad() As Boolean

        'Por default asumir que todos los campos fueron evaluados, por lo que no están sucios
        Return False

    End Function

    ''' <summary>
    ''' Indica si la información de al menos una entidad de alguna de las listas de esta entidad está sucia.
    ''' </summary>
    ''' <remarks>Esta función sólo se ejecuta cuando el estatus de la entidad lo requiere (Existente).</remarks>
    Protected Overridable Function EstanSuciasListasEntidad() As Boolean

        'Por default, determinar que no hay listas, por lo que no están sucias
        Return False

    End Function

    ''' <summary>
    ''' Regresa el estatus actual de la entidad.
    ''' </summary>
    ''' <remarks>Este estatus permite conocer si una entidad es nueva, existente o tiene otro estado como resultado de ejectuar algunas de las funciones disponibles.</remarks>
    ''' <exception cref="ExcepcionEstatusInvalido">Arroja la excepción cuando se intenta cambiar el estatus de la entidad a un nuevo estatus que no está permitido dado el estatus actual. La información de transiciones válidas de estatus está definida en el diagrama de estado Entidad:Estatus.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Property EstatusEntidad() As EstatusEntidades Implements IEntidad.EstatusEntidad
        Get

            'Determinar si requiere evaluar el caso especial de PendienteBorrar
            Select Case MiEstatusEntidad
                Case EstatusEntidades.Nuevo, EstatusEntidades.Existente
                    'Para el caso de Nuevo o Existente, regresar si está pendiente o no.
                    If MiEstaPendienteBorrar Then
                        Return EstatusEntidades.PendienteBorrar
                    ElseIf MiEstaPendienteDestruir Then
                        Return EstatusEntidades.PendienteDestruir
                    Else
                        Return MiEstatusEntidad
                    End If

                Case Else
                    'Para todos los demás casos, regresar el estatus.
                    Return MiEstatusEntidad

            End Select

        End Get
        Set(ByVal value As EstatusEntidades)

            'Realizar validaciones de cambio para entidades inicializadas
            If MiEstaInicializado Then

                'Determinar si el nuevo valor es válido. 
                'Los valores permitidos son aquellos que permite el diagrama de estado Entidad:Estatus.
                If MiEstaPendienteBorrar Then
                    If MiEstatusEntidad = EstatusEntidades.Nuevo Then
                        Select Case value
                            Case EstatusEntidades.Existente, EstatusEntidades.Destruido, EstatusEntidades.Borrado
                                Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                        End Select
                    ElseIf MiEstatusEntidad = EstatusEntidades.Existente Then
                        Select Case value
                            Case EstatusEntidades.Nuevo, EstatusEntidades.Desconocido
                                Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                        End Select
                    Else
                        Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                    End If
                ElseIf MiEstaPendienteDestruir Then
                    If MiEstatusEntidad = EstatusEntidades.Nuevo Then
                        Select Case value
                            Case EstatusEntidades.Existente, EstatusEntidades.Destruido, EstatusEntidades.Borrado
                                Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                        End Select
                    ElseIf MiEstatusEntidad = EstatusEntidades.Existente Then
                        Select Case value
                            Case EstatusEntidades.Nuevo, EstatusEntidades.Desconocido, EstatusEntidades.Borrado
                                Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                        End Select
                    Else
                        Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                    End If
                Else
                    Select Case MiEstatusEntidad
                        Case EstatusEntidades.Nuevo
                            Select Case value
                                Case EstatusEntidades.Borrado, EstatusEntidades.Destruido
                                    Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                            End Select
                        Case EstatusEntidades.Existente
                            Select Case value
                                Case EstatusEntidades.Nuevo, EstatusEntidades.Desconocido
                                    Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                            End Select
                        Case EstatusEntidades.Borrado
                            Select Case value
                                Case EstatusEntidades.Nuevo, EstatusEntidades.PendienteBorrar, EstatusEntidades.Desconocido
                                    Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                            End Select
                        Case EstatusEntidades.Destruido, EstatusEntidades.Desconocido
                            Throw New ExcepcionEstatusInvalido(Me, Reflection.MethodBase.GetCurrentMethod())
                    End Select
                End If

            End If

            'Realizar el cambio
            MiEstaPendienteBorrar = False
            MiEstaPendienteDestruir = False
            If value = EstatusEntidades.PendienteBorrar Then
                MiEstaPendienteBorrar = True
            ElseIf value = EstatusEntidades.PendienteDestruir Then
                MiEstaPendienteDestruir = True
            Else
                MiEstatusEntidad = value
            End If

        End Set
    End Property

    ''' <summary>
    ''' Regresa el estatus actual de la entidad.
    ''' </summary>
    ''' <remarks>Este estatus permite conocer si una entidad es nueva, existente o tiene otro estado como resultado de ejectuar algunas de las funciones disponibles. El valor que regresa esta propiedad no incluye nigún estado de tipo "Pendiente".</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public ReadOnly Property EstatusEntidad(ByVal IgnoraPendientes As Boolean) As EstatusEntidades Implements IEntidad.EstatusEntidad
        Get
            Return MiEstatusEntidad
        End Get
    End Property

    ''' <summary>
    ''' Indica si la información de todos los campos de la entidad es válida.
    ''' </summary>
    ''' <remarks>El resultado de esta función se basa en los resultados de la función Validar() para determinar la validez de la entidad.</remarks>
    Public Function EsValido() As Boolean Implements IEntidad.EsValido

        Return Validar().Count = 0

    End Function

    ''' <summary>
    ''' Indica si la información de un campo de la entidad es válida.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a verificar su validez.</param>
    ''' <remarks>El resultado de esta función se basa en los resulados de la función Validar(Integer) para determinar la validez del campo.</remarks>
    Public Function EsValido(ByVal Campo As TEnumeradorCampos) As Boolean Implements IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos).EsValido

        Return Validar(Campo).Count = 0

    End Function

    ''' <summary>
    ''' Indica si la información de un campo de la entidad es válida.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a verificar su validez.</param>
    ''' <remarks>El resultado de esta función se basa en los resulados de la función Validar(Integer) para determinar la validez del campo.</remarks>
    Public Function EsValido(ByVal Campo As Integer) As Boolean Implements IEntidad.EsValido

        Return Validar(Campo).Count = 0

    End Function

    ''' <summary>
    ''' Regresa la fecha y hora de creación del registro expresado en la zona horaria local.
    ''' </summary>
    Public Property FechaHoraLocalCreacion() As Date Implements IEntidad.FechaHoraLocalCreacion
        Get
            Return FechaHoraCreacionEntidad.ToLocalTime()
        End Get
        Set(ByVal value As Date)
            FechaHoraCreacionEntidad = value.ToUniversalTime()
        End Set
    End Property

    ''' <summary>
    ''' Regresa la fecha y hora de última modificación del registro expresado en la zona horaria local.
    ''' </summary>
    Public Property FechaHoraLocalModificacion() As Date Implements IEntidad.FechaHoraLocalModificacion
        Get
            Return FechaHoraModificacionEntidad.ToLocalTime()
        End Get
        Set(ByVal value As Date)
            FechaHoraModificacionEntidad = value.ToUniversalTime()
        End Set
    End Property

    ''' <summary>
    ''' Regresa el ícono que representa la entidad.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public MustOverride ReadOnly Property IconoEntidad() As System.Drawing.Icon Implements IEntidad.IconoEntidad

    ''' <summary>
    ''' Regresa el identificador de la entidad.
    ''' </summary>
    ''' <remarks>El identificador de la entidad es el mismo valor que corresponde al valor del campo llave en la base de datos.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)>
    Public MustOverride Overrides ReadOnly Property Id() As System.Guid Implements IEntidad.Id

    ''' <summary>
    ''' Regresa el identificador de la entidad.
    ''' </summary>
    ''' <remarks>El identificador de la entidad es el mismo valor que corresponde al valor del campo llave en la base de datos.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)>
    Public MustOverride Overrides ReadOnly Property IdTexto() As String Implements IEntidad.IdTexto

    ''' <summary>
    ''' Regresa la imagen que representa la entidad.
    ''' </summary>
    ''' <remarks>Regresa la imagen con el tamaño por default de 16x16 pixeles.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public ReadOnly Property ImagenEntidad() As System.Drawing.Image Implements IEntidad.ImagenEntidad
        Get
            Return ImagenEntidadEspecifica(TamañosImagenes.x16)
        End Get
    End Property

    ''' <summary>
    ''' Regresa la imagen que representa la entidad.
    ''' </summary>
    ''' <param name="Tamaño">Determina el tamaño de la imagen a partir de una lista de tamaños conocidos.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public MustOverride ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As TamañosImagenes) As System.Drawing.Image Implements IEntidad.ImagenEntidadEspecifica

    '''' <summary>
    '''' Regresa la imagen que representa la entidad.
    '''' </summary>
    '''' <param name="Tamaño">Determina el tamaño de la imagen a partir de una lista de tamaños conocidos.</param>
    '<ComponentModel.EditorBrowsable(ComponentModel.EditorBrowsableState.Advanced)> _
    'Public ReadOnly Property ImagenEntidadEspecifica2(ByVal Tamaño As TamañosImagenes) As System.Drawing.Image
    '    Get
    '        Select Case Tamaño
    '            Case TamañosImagenes.x16, TamañosImagenes.x32, TamañosImagenes.x48, TamañosImagenes.x256
    '                Dim IcoE = IconoEntidad
    '                Using Icono = New Drawing.Icon(IcoE, Tamaño, Tamaño)
    '                    ''Return Drawing.Bitmap.FromHicon(Icono5.Handle)
    '                    'Dim x = Drawing.Bitmap.FromHicon(Icono.Handle)
    '                    'x.MakeTransparent()
    '                    'Return x

    '                    'Dim Memoria = New IO.MemoryStream()
    '                    'Icono.Save(Memoria)
    '                    'Dim Img = Drawing.Image.FromStream(Memoria)
    '                    'Dim g = Drawing.Graphics.FromImage(Img)
    '                    'Return Img

    '                    'Icono.ToBitmap().

    '                End Using
    '            Case Else
    '                Return Nothing
    '        End Select
    '    End Get
    'End Property

    ''' <summary>
    ''' Regresa la representación de la entidad en texto.
    ''' </summary>
    ''' <remarks>Esta propiedad regresa la misma cadena de texto que la función ToString().</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public ReadOnly Property TextoEntidad() As String Implements IEntidad.TextoEntidad
        Get
            Return ToString()
        End Get
    End Property

    ''' <summary>
    ''' Regresa la representación de la entidad en texto.
    ''' </summary>
    Public MustOverride Overrides Function ToString() As String


    ''' <summary>
    ''' Valida la información de todos los campos de la entidad y regresa todos los mensajes posibles cuando algún campo no es válido.
    ''' </summary>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato de algún campo. Si más de un campo es inválido, esta lista contendrá los mensajes de todos los campos inválidos. Si la lista resultante está vacia, se puede interpretar que todos los campos de la entidad son válidos.</remarks>
    Public Function Validar() As System.Collections.Generic.List(Of String) Implements IEntidad.Validar

        'Crear el resultado vacio
        Dim Resultado = New List(Of String)

        'Determinar si es necesario aplicar la validación de las listas de la entidad
        If MiEstatusEntidad = EstatusEntidades.Nuevo Or MiEstatusEntidad = EstatusEntidades.Existente Then

            'Recorrer los valores del enumerador de campos
            For Each Campo As Integer In [Enum].GetValues(GetType(TEnumeradorCampos))
                For Each Validacion In Validar(Campo)
                    Resultado.Add(Validacion)
                Next
            Next

            'Recorrer el resultado de validar las listas
            For Each Valor In ValidarListasEntidad()
                Resultado.Add(Valor)
            Next

        Else

            'Agregar el mensaje de que la entidad no tiene un estaus válido
            Resultado.Add(My.Resources.Str_EntidadValidarEstatusInvalido)

        End If

        'Regresar el resultado final
        Return Resultado

    End Function

    ''' <summary>
    ''' Valida la información de un campo específico de la entidad y regresa todos los mensajes posibles cuando el campo no es válido.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a validar.</param>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato del campo. Si la lista resultante está vacia, se puede interpretar que el campo de la entidad es válido. La función toma en cuenta el estatus de la entidad de forma que si no es una entidad nueva o existente, esta función siempre regresa False.</remarks>
    Public Function Validar(ByVal Campo As TEnumeradorCampos) As System.Collections.Generic.List(Of String) Implements IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos).Validar

        'Selecciona en base al estatus de la entidad
        Select Case MiEstatusEntidad
            Case EstatusEntidades.Nuevo, EstatusEntidades.Existente
                'La entidad debe ser validada pues tiene un estatus que requiere validación
                Return ValidarEntidad(Campo)

            Case Else
                'La entidad tiene un estatus que se puede asumir no permite manipularla.
                Dim Resultado = New List(Of String)
                Resultado.Add(My.Resources.Str_EntidadValidarEstatusInvalido)
                Return Resultado

        End Select

    End Function

    ''' <summary>
    ''' Valida la información de un campo específico de la entidad y regresa todos los mensajes posibles cuando el campo no es válido.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a validar.</param>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato del campo. Si la lista resultante está vacia, se puede interpretar que el campo de la entidad es válido.</remarks>
    Protected MustOverride Function ValidarEntidad(ByVal Campo As TEnumeradorCampos) As System.Collections.Generic.List(Of String)

    ''' <summary>
    ''' Valida la información de las entidades contenidas en las listas de esta entidad.
    ''' </summary>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan algúna entidad de alguna de las listas. Si la lista resultante está vacia, se puede interpertar que todas las entidades de todas las listas son válidas.</remarks>
    Protected Overridable Function ValidarListasEntidad() As List(Of String)

        'Por default, determinar que no hay listas, por lo que son válidas.
        Return New List(Of String)

    End Function

    ''' <summary>
    ''' Valida la información de un campo específico de la entidad y regresa todos los mensajes posibles cuando el campo no es válido.
    ''' </summary>
    ''' <param name="Campo">Determina el campo a validar.</param>
    ''' <remarks>El resultado de esta función contiene la lista de todos los mensajes que invalidan el dato del campo. Si la lista resultante está vacia, se puede interpretar que el campo de la entidad es válido.</remarks>
    Public Function Validar(ByVal Campo As Integer) As System.Collections.Generic.List(Of String) Implements IEntidad.Validar

        Return Validar(CType(CType(Campo, Object), TEnumeradorCampos))

    End Function

    ''' <summary>
    ''' Determina si otra entidad del mismo tipo es equivalente a esta entidad.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será evaluada para determinar equivalencia.</param>
    ''' <remarks>Esta función determina si dos entidades del mismo tipo son equivalentes, es decir, que la información de los campos significativos contengan la misma información.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public MustOverride Function Equivalente(ByVal Entidad As TTipoEntidad) As Boolean Implements IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos).Equivalente

    ''' <summary>
    ''' Crea un clon de la entidad.
    ''' </summary>
    ''' <remarks>Esta función crea un nuevo objeto identico.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ClonarGenerico() As Object Implements IEntidad.Clonar

        Return Clonar()

    End Function

    ''' <summary>
    ''' Crea un clon de la entidad.
    ''' </summary>
    ''' <remarks>Esta función crea un nuevo objeto identico.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function Clonar() As TTipoEntidad Implements IEntidadGenerico(Of TTipoEntidad, TEnumeradorCampos).Clonar

        Dim NuevaEntidad = CrearNuevoOriginalEntidad()
        ClonarEntidad(NuevaEntidad)
        With DirectCast(NuevaEntidad, IEntidad)
            .MetadatosEntidad = MetadatosEntidad
            .CuentaEliminadoEntidad = CuentaEliminadoEntidad
            .FechaHoraCreacionEntidad = FechaHoraCreacionEntidad
            .FechaHoraModificacionEntidad = FechaHoraModificacionEntidad
            .EsSoloLectura = EsSoloLectura
        End With
        Return NuevaEntidad

    End Function

    ''' <summary>
    ''' Clona la información de la entidad a la nueva entidad.
    ''' </summary>
    ''' <param name="EntidadNueva">La entidad donde serán clonados los datos.</param>
    Protected MustOverride Sub ClonarEntidad(ByVal EntidadNueva As TTipoEntidad)

    '''' <summary>
    '''' Regresa información acerca de un permiso en particular de esta entidad.
    '''' </summary>
    '''' <param name="Tipo">El tipo de permiso del cual se requiere información.</param>
    '''' <remarks>Esta función regresa el atributo que define un permiso específico para esta entidad. Si la entidad no tiene un permiso específico definido, regresará un nuevo atributo sin permiso con el estatus de Negado.</remarks>
    '<System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    'Public Function Permiso(ByVal Tipo As TiposPermisos) As PermisoAttribute Implements IEntidad.Permiso

    '    'Checa si no se ha cargado la lista de permisos
    '    If MisPermisos Is Nothing Then

    '        'Cargar todos los atributos de permisos de esta entidad
    '        MisPermisos = CType(Me.GetType().GetCustomAttributes(GetType(PermisoAttribute), False), PermisoAttribute()).ToList

    '    End If

    '    'Buscar el permiso del tipo definido
    '    Dim Resultado = MisPermisos.Find(Function(Item) Item.Tipo = Tipo)
    '    If Resultado Is Nothing Then
    '        Resultado = New PermisoAttribute(Tipo, Permisos.Desconocido, NivelesPermisos.Negado)
    '    End If

    '    'Regresa el permiso resultante
    '    Return Resultado

    'End Function

    ''' <summary>
    ''' Regresa información acerca del texto de esta entidad para una cultura en particular.
    ''' </summary>
    ''' <param name="Cultura">La cultura para la cual se requiere la información.</param>
    ''' <remarks>Esta función regresa el atributo que define el texto específico para una cultura en esta entidad. Si la entidad no tiene un texto específico definido, regresará un nuevo atributo sin textos.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function Texto(ByVal Cultura As String) As TextoAttribute Implements IEntidad.Texto

        'Checa si no se ha cargado la lista de textos
        If MisTextos Is Nothing Then

            'Cargar todos los atributos de textos de esta entidad
            MisTextos = CType(Me.GetType().GetCustomAttributes(GetType(TextoAttribute), False), TextoAttribute()).ToList

        End If

        'Buscar el texto de la cultura definida
        Dim Resultado = MisTextos.Find(Function(Item) Item.Cultura = Cultura)
        If Resultado Is Nothing Then
            Resultado = New TextoAttribute(Cultura, String.Empty, String.Empty)
        End If

        'Regresa el texto resultante
        Return Resultado

    End Function

    ''' <summary>
    ''' Regresa la lista de todas las propiedades que están marcadas para ser columnas en un grid.
    ''' </summary>
    ''' <param name="Cultura">La cultura para la cual se requiere la información.</param>
    ''' <remarks>Esta función regresa la lista de atributos que definen las características de una propiedad para ser mostrada en un grid. Si la entidad no tiene propiedades marcadas con el atributo, regresará una lista vacia.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function Columnas(ByVal Cultura As String) As System.Collections.Generic.List(Of ColumnaAttribute) Implements IEntidad.Columnas

        'Checa si no se ha cargado la lista de columnas
        If MisColumnas Is Nothing Then

            'Cargar todos los atributos de columnas de esta entidad
            MisColumnas = CType(Me.GetType().GetCustomAttributes(GetType(ColumnaAttribute), True), ColumnaAttribute()).OrderBy(Function(Item) Item.Posicion).ToList()

        End If

        'Buscar las definiciones de columna de la cultura definida
        Dim Resultado = MisColumnas.FindAll(Function(Item) Item.Cultura = Cultura)

        'Regresa la lista resultante
        Return Resultado

    End Function

    ''' <summary>
    ''' Serializa la entidad para ser transmitida o guardada.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Overridable Function Serializar() As String Implements IEntidad.Serializar

        'Prepara el serializador
        Dim Resultado = New System.Text.StringBuilder()
        Dim Serializador = New Xml.Serialization.XmlSerializer(Me.GetType())
        Using Escritor = New IO.StringWriter(Resultado)

            'Serializar entidad
            Serializador.Serialize(Escritor, Me)
            Escritor.Close()

        End Using

        'Regresa resultado serializado
        Return Resultado.ToString()

        ''Prepara el serializador (WCF)
        'Dim Resultado = New Text.StringBuilder()
        'Dim Serializador = New Runtime.Serialization.DataContractSerializer(Me.GetType())
        'Using Escritor = Xml.XmlDictionaryWriter.CreateDictionaryWriter(Xml.XmlWriter.Create(Resultado))

        '    'Serializa la entidad
        '    Serializador.WriteObject(Escritor, Me)
        '    Escritor.Close()

        'End Using

        ''Regresa el resultado serializado
        'Return Resultado.ToString()

    End Function

End Class