﻿
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Clase que extiende las fabricas.
''' </summary>
Public Class Fabricas
    Inherits Mini.Zeus.Cliente.UX.Recursos.Fabricas

    ''' <summary>
    ''' Fábrica de Cliente.
    ''' </summary>
    Private Shared MiFabricaCliente As Datos.ClienteFabricas 

    ''' <summary>
    ''' Fábrica de Cliente.
    ''' </summary>
    Private Shared MiFabricaCliente_Modelo As Datos.Cliente_ModeloFabricas 


     ''' <summary>
    ''' Fábrica de Modelo.
    ''' </summary>
    Private Shared MiFabricaModelo As Datos.ModeloFabricas 

    ''' <summary>
    ''' Fabrica de defectos
    ''' </summary>
    Private Shared MiFabricaDefecto As Datos.DefectosFabricas

    ''' <summary>
    ''' Fabrica de Unidad de negocio
    ''' </summary>
    Private Shared MiFabricaUnidad_Negocio As Datos.Unidad_NegocioFabricas

        ''' <summary>
    ''' Fabrica de Unidad de Componente
    ''' </summary>
    Private Shared MiFabricaComponente As Datos.ComponenteFabricas


            ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Private Shared MiFabricaComponenteUnidadNegocio As Datos.Componente_Unidad_NegocioFabricas

    ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Private Shared MiFabricaUsuarios As Datos.SEGURIDAD_USUARIOFabrica 


        ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Private Shared MiFabricaUsuario_Unidad_Negocio As Datos.Usuario_Unidad_NegocioFabricas

    ''' <summary>
    ''' Fabrica de reclamos
    ''' </summary>
    Private shared MiFabricaReclamo As Datos.ReclamoFabricas

    ''' <summary>
    ''' Fabrica de areas_usuarios
    ''' </summary>
    Private shared MiFabricaAreas_Usuarios As Datos.Area_UsuarioFabricas

    ''' <summary>
    ''' Fabrica de areas
    ''' </summary>
    Private Shared MiFabricaAreas As Datos.AreaFabricas

    ''' <summary>
    ''' Fabrica de líneas
    ''' </summary>
    Private Shared MiFabricaLineas As Datos.LineaFabricas


    Private shared MiFabricaLABDIMSolicitud As Datos.LABDIMSolicitudFabricas

    ''' <summary>
    ''' Fabrica de LABDIMArchivos
    ''' </summary>
    Private Shared MiFabricaLABDIMArchivos As Datos.LABDIMArchivoFabricas

    Private Shared MiFabricaPermisos As Datos.SEGURIDAD_PERMISOFabricas

    ''' <summary>
    ''' Fabrica de subareas
    ''' </summary>
    Private Shared MiFabricaSubArea As Datos.detalleFabricas

    ''' <summary>
    ''' Fabrica de MiFabricaPermisos
    ''' </summary>
    Public Shared Property SubArea As Datos.detalleFabricas
        Get
            If MiFabricaSubArea Is Nothing Then
                MiFabricaSubArea = New detalleFabricas
            End If
            Return MiFabricaSubArea
        End Get
        Set(ByVal value As Datos.detalleFabricas)
            MiFabricaSubArea = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de MiFabricaPermisos
    ''' </summary>
    Public Shared Property Permisos As Datos.SEGURIDAD_PERMISOFabricas
        Get
            If MiFabricaPermisos Is Nothing Then
                MiFabricaPermisos = New SEGURIDAD_PERMISOFabricas
            End If
            Return MiFabricaPermisos
        End Get
        Set(ByVal value As Datos.SEGURIDAD_PERMISOFabricas)
            MiFabricaPermisos = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de LABDIMArchivos
    ''' </summary>
    Public Shared Property LABDIMArchivos As Datos.LABDIMArchivoFabricas
        Get
            If MiFabricaLABDIMArchivos Is Nothing Then
                MiFabricaLABDIMArchivos = New LABDIMArchivoFabricas    
            End If
            Return MiFabricaLABDIMArchivos
        End Get
        Set(ByVal value As Datos.LABDIMArchivoFabricas)
            MiFabricaLABDIMArchivos = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de los tipos de inspección
    ''' </summary>
    Public Shared Property Tipo_Inpeccion As Datos.Tipo_InspeccionFabricas
        Get
            If MiFabricaTipo_Inpeccion Is Nothing Then
                MiFabricaTipo_Inpeccion = New Tipo_InspeccionFabricas   
            End If
            Return MiFabricaTipo_Inpeccion
        End Get
        Set(ByVal value As Datos.Tipo_InspeccionFabricas)
            MiFabricaTipo_Inpeccion = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de los tipos de inspección
    ''' </summary>
    Private shared MiFabricaTipo_Inpeccion As Datos.Tipo_InspeccionFabricas 

    Public Shared Property LABDIMSolicitud As Datos.LABDIMSolicitudFabricas
        Get
             If MiFabricaLABDIMSolicitud Is Nothing Then
                MiFabricaLABDIMSolicitud = New LABDIMSolicitudFabricas   
            End If
            Return MiFabricaLABDIMSolicitud
        End Get
        Set(ByVal value As Datos.LABDIMSolicitudFabricas)
            MiFabricaLABDIMSolicitud = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de líneas
    ''' </summary>
    Public Shared Property Lineas As Datos.LineaFabricas
        Get
            If MiFabricaLineas Is Nothing Then
                MiFabricaLineas = New LineaFabricas   
            End If
            Return MiFabricaLineas
        End Get
        Set(ByVal value As Datos.LineaFabricas)
            MiFabricaLineas = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de areas
    ''' </summary>
    Public Shared Property Areas As Datos.AreaFabricas
        Get
             If MiFabricaAreas Is Nothing Then
                MiFabricaAreas = New AreaFabricas  
            End If
            Return MiFabricaAreas
        End Get
        Set(ByVal value As Datos.AreaFabricas)
            MiFabricaAreas = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de areas_usuarios
    ''' </summary>
    Public Shared Property Areas_Usuarios As Datos.Area_UsuarioFabricas
        Get
            If MiFabricaAreas_Usuarios Is Nothing Then
                MiFabricaAreas_Usuarios = New Area_UsuarioFabricas  
            End If
            Return MiFabricaAreas_Usuarios
        End Get
        Set(ByVal value As Datos.Area_UsuarioFabricas)
            MiFabricaAreas_Usuarios = value
        End Set
    End Property

    Public Shared Property Reclamo As Datos.ReclamoFabricas
        Get
            If MiFabricaReclamo Is Nothing Then
                MiFabricaReclamo = New ReclamoFabricas  
            End If
            Return MiFabricaReclamo
        End Get
        Set(ByVal value As Datos.ReclamoFabricas)
            MiFabricaReclamo = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Public Shared Property Usuario_Unidad_Negocio As Datos.Usuario_Unidad_NegocioFabricas
        Get
              If MiFabricaUsuario_Unidad_Negocio Is Nothing Then
                MiFabricaUsuario_Unidad_Negocio = New Usuario_Unidad_NegocioFabricas 
            End If
            Return MiFabricaUsuario_Unidad_Negocio
        End Get
        Set(ByVal value As Datos.Usuario_Unidad_NegocioFabricas)
            MiFabricaUsuario_Unidad_Negocio = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Public Shared Property Seguridad_Usuarios As Datos.SEGURIDAD_USUARIOFabrica
        Get
             If MiFabricaUsuarios Is Nothing Then
                MiFabricaUsuarios = New SEGURIDAD_USUARIOFabrica 
            End If
            Return MiFabricaUsuarios
        End Get
        Set(ByVal value As Datos.SEGURIDAD_USUARIOFabrica)
            MiFabricaUsuarios = value
        End Set
    End Property
    ''' <summary>
    ''' Fabrica de ComponenteUnidadNegocio
    ''' </summary>
    Public Shared Property ComponenteUnidadNegocio As Datos.Componente_Unidad_NegocioFabricas
        Get
            If MiFabricaComponenteUnidadNegocio Is Nothing Then
                MiFabricaComponenteUnidadNegocio = New Componente_Unidad_NegocioFabricas 
            End If
            Return MiFabricaComponenteUnidadNegocio
        End Get
        Set(ByVal value As Datos.Componente_Unidad_NegocioFabricas)
            MiFabricaComponenteUnidadNegocio = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de Unidad de Componente
    ''' </summary>
    Public Shared Property Componente As Datos.ComponenteFabricas
        Get
            If MiFabricaComponente Is Nothing Then
                MiFabricaComponente = New ComponenteFabricas 
            End If
            Return MiFabricaComponente
        End Get
        Set(ByVal value As Datos.ComponenteFabricas)
            MiFabricaComponente = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de Unidad de negocio
    ''' </summary>
    Public Shared Property Unidad_Negocio As Datos.Unidad_NegocioFabricas
        Get
             If MiFabricaUnidad_Negocio Is Nothing Then
                MiFabricaUnidad_Negocio = New Unidad_NegocioFabricas 
            End If
            Return MiFabricaUnidad_Negocio
        End Get
        Set(ByVal value As Datos.Unidad_NegocioFabricas)
            MiFabricaUnidad_Negocio = value
        End Set
    End Property

    ''' <summary>
    ''' Fabrica de defectos
    ''' </summary>
    Public Shared Property Defecto As Datos.DefectosFabricas 
        Get
            If MiFabricaDefecto Is Nothing Then
                MiFabricaDefecto = New DefectosFabricas
            End If
            Return MiFabricaDefecto
        End Get
        Set(ByVal value As Datos.DefectosFabricas)
            MiFabricaDefecto = value
        End Set
    End Property

    Public Shared Property Modelo As Datos.ModeloFabricas
        Get
            If MiFabricaModelo Is Nothing Then
                MiFabricaModelo = New ModeloFabricas
            End If
            Return MiFabricaModelo
        End Get
        Set(value As Datos.ModeloFabricas)
            MiFabricaModelo = value
        End Set
    End Property

    Public Shared Property Cliente As Datos.ClienteFabricas
        Get
            If MiFabricaCliente Is Nothing Then
                MiFabricaCliente = New ClienteFabricas
            End If
            Return MiFabricaCliente
        End Get
        Set(value As Datos.ClienteFabricas)
            MiFabricaCliente = value
        End Set
    End Property

    
    Public Shared Property Cliente_Modelof As Datos.Cliente_ModeloFabricas 
        Get
            If MiFabricaCliente_Modelo Is Nothing Then
                MiFabricaCliente_Modelo = New Cliente_ModeloFabricas 
            End If
            Return MiFabricaCliente_Modelo
        End Get
        Set(value As Datos.Cliente_ModeloFabricas )
            MiFabricaCliente_Modelo = value
        End Set
    End Property
    
End Class