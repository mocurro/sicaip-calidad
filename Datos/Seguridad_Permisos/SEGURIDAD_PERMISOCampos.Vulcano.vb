﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Cliente
''' </summary>
Public Enum SEGURIDAD_PERMISOCampos As Integer

    CVE_Permiso
    Descripcion
    Nombre_Corto
    CVE_CATEGORIA_PERMISO
    PermisoTipoUsuario

End Enum

