﻿Imports Datos
Imports Mini.Zeus.Cliente
Imports Mini.Zeus.Cliente.UX
Imports System.IO

Public Class CatalogoLABDIMSolicitud

    Private MiListaModelos As List(Of Datos.modelo)

    Private MiListaClientes As List(Of Datos.Cliente)

    Private MiListaArchivos As List(Of Datos.LABDIMArchivo )

    Private ListadoCorreosAtiende As New List(Of String)

    Private ListadoRutas As New List(Of String)

    Private MiListaArchivosNoEditados As List(Of Datos.LABDIMArchivo)

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.LABDIMSolicitud), Datos.Fabricas.LABDIMSolicitud, GetType(TarjetaLABDIMSolicitud)))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.LABDIMSolicitud)

    End Function

    Private Sub CatalogoLABDIMSolicitud_DespuesAgregarNuevo(sender As Object, e As ArgumentosDespuesAccionCatalogo(Of IEntidad)) Handles Me.DespuesAgregarNuevo
        Dim solicitud = CType(e.ElementosInvolucrados, Datos.LABDIMSolicitud)
        ListadoRutas.Clear()
        If Archivos IsNot Nothing Then
            For Each elemento In Archivos
                Using MiModelo = New ModeloDatosDataContext(Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer)
                    elemento.cve_solicitud = solicitud.cve_solicitud
                    If elemento.Nombre.Count > 80 Then
                        elemento.Nombre = elemento.Nombre.Substring(elemento.Nombre.Count - 80, 80)
                        'elemento.Nombre = elemento.Nombre.Substring(elemento.Nombre.Count - 80, 80)
                    End If
                    MiModelo.LABDIMArchivos.InsertOnSubmit(elemento)
                    MiModelo.SubmitChanges()
                End Using
                Dim direcotorioEtiquetas As String = "C:\\GKN\\"

                Dim RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas, elemento.Nombre)
                If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                    System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                End If
                Try
                    If File.Exists(RutaArchivo) Then
                        File.Delete(RutaArchivo)
                    End If

                Catch ex As Exception

                End Try

                File.WriteAllBytes(RutaArchivo, elemento.Archivo.ToArray)

                ListadoRutas.Add(RutaArchivo)
            Next
        End If

        ListadoCorreosAtiende.Clear
        SesiónLocal.crear_contenido_mensaje_LabDimSolicitud(solicitud, solicitud.user)
        ListadoCorreosAtiende.Add(solicitud.user.Email)
        Try
            SesiónLocal.envia_notificacion_adjunto(ListadoCorreosAtiende, ListadoRutas)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al enviar correo", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub CatalogoLABDIMSolicitud_DespuesModificarSeleccionado(sender As Object, e As ArgumentosDespuesAccionCatalogo(Of IEntidad)) Handles Me.DespuesModificarSeleccionado
        Dim solicitud = CType(e.ElementosInvolucrados, Datos.LABDIMSolicitud)
        ListadoRutas.Clear()
        If Archivos IsNot Nothing Then
            For Each elemento In Archivos
                Using Contexto = New ModeloDatosDataContext(Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer)
                    Dim Objeto = Contexto.LABDIMArchivos.Single(Function(p As LABDIMArchivo) p.cve_solicitud = solicitud.cve_solicitud)
                    Objeto.Nombre = elemento.Nombre
                    Objeto.Archivo = elemento.Archivo
                    Contexto.SubmitChanges
                End Using
                Dim direcotorioEtiquetas As String = String.Format("C:\\GKN\\{0}", Guid.NewGuid)
                Dim RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas, elemento.Nombre)
                If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                    System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                End If
                Try
                    If File.Exists(RutaArchivo) Then
                        File.Delete(RutaArchivo)
                    End If
                    File.WriteAllBytes(RutaArchivo, elemento.Archivo.ToArray)

                Catch ex As Exception

                End Try


                ListadoRutas.Add(RutaArchivo)
            Next
        Else
            If MiListaArchivosNoEditados IsNot Nothing Then
                For Each elemento In MiListaArchivosNoEditados

                    Dim direcotorioEtiquetas As String = "C:\\GKN\\"
                    Dim RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas, elemento.Nombre)
                    If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                        System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                    End If
                    Try
                        If File.Exists(RutaArchivo) Then
                            File.Delete(RutaArchivo)
                        End If
                    Catch ex As Exception

                    End Try

                    File.WriteAllBytes(RutaArchivo, elemento.Archivo.ToArray)

                    ListadoRutas.Add(RutaArchivo)
                Next
            End If
        End If
        ListadoCorreosAtiende.Clear()
        SesiónLocal.crear_contenido_mensaje_LabDimModificarSolicitud(solicitud, solicitud.user)
        ListadoCorreosAtiende.Add(solicitud.user.Email)
        Try
            SesiónLocal.envia_notificacion_adjunto(ListadoCorreosAtiende, ListadoRutas)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al enviar correo", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Modelos() As List(Of Datos.modelo)
        Get
            If MiListaModelos Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaModelos = Fabricas.Modelo.ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaModelos
        End Get
    End Property


    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Clientes() As List(Of Datos.Cliente)
        Get
            If MiListaClientes Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaClientes = Fabricas.Cliente.ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaClientes
        End Get
    End Property

    Public Property Archivos As List(Of Datos.LABDIMArchivo)
        Get

            Return MiListaArchivos

        End Get
        Set(value As List(Of Datos.LABDIMArchivo))
            MiListaArchivos = value
        End Set
    End Property

    Public Property ArchivosNoEditados As List(Of Datos.LABDIMArchivo)
        Get

            Return MiListaArchivosNoEditados
        End Get
        Set(value As List(Of Datos.LABDIMArchivo))
            MiListaArchivosNoEditados = value
        End Set
    End Property

    ''' <summary>
    ''' Define el filtro para las consultas de datos.
    ''' </summary>
    ''' <param name="Filtro">El filtro de las consultas.</param>
    Protected Overrides Sub DefinirFiltro(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object))

        Filtro.Clear()
        Filtro.Add("Tipo", 0)

    End Sub

End Class