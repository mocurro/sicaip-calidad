﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Componente - Unidad de negocio", "Componentes - Unidades de negocio", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "component.componente", 1, "Componente",Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Componentes - Unidades de negocio : {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "Unidad_Nego.nombre", 2, "Unidad de negocio")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "fecha_inicio", 3, "Fecha inicio")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "fecha_fin", 4, "Fecha fin")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 5, "Estatus")>
Public Class Componente_Unidad_Negocio
    Inherits Entidad(Of Componente_Unidad_Negocio, Componente_Unidad_NegocioCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As Componente_Unidad_Negocio)
        With EntidadNueva
            .cve_componente_unidad_negocio =cve_componente_unidad_negocio 
            ._cve_componente =cve_componente 
            .cve_unidad_negocio =cve_unidad_negocio
            .estatus =estatus 
            .fecha_inicio =fecha_inicio 
            .fecha_fin =fecha_fin 
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
            .cve_componente_unidad_negocio =cve_componente_unidad_negocio 
            ._cve_componente =cve_componente 
            .cve_unidad_negocio =cve_unidad_negocio
            .estatus =estatus 
            .fecha_inicio =fecha_inicio 
            .fecha_fin =fecha_fin
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            cve_componente_unidad_negocio =.cve_componente_unidad_negocio 
            _cve_componente =.cve_componente 
            cve_unidad_negocio =.cve_unidad_negocio
            estatus =.estatus 
            fecha_inicio =.fecha_inicio 
            fecha_fin =.fecha_fin
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As Componente_Unidad_Negocio
        Return New Componente_Unidad_Negocio
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As Componente_Unidad_Negocio) As Boolean
        'Return false
        Return Equals(Entidad.cve_componente  , _cve_componente) AndAlso
             Equals(Entidad._cve_unidad_negocio, _cve_unidad_negocio) AndAlso
             Equals (Entidad.fecha_inicio , fecha_inicio ) AndAlso
             Equals (Entidad.fecha_fin  , fecha_fin  ) AndAlso
             Equals(Entidad.estatus, estatus)
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return estatus 
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As Componente_Unidad_NegocioCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case Componente_Unidad_NegocioCampos .cve_Componente    
                If cve_Componente= 0 Then
                    Resultado.Add(My.Resources.Str_CveClienteRequerido)
                End If
            Case Componente_Unidad_NegocioCampos .cve_Unidad_negocio    
                If cve_Unidad_negocio= 0 Then
                    Resultado.Add(My.Resources.Str_CveModeloRequerido)
                End If
            Case Componente_Unidad_NegocioCampos .fecha_inicio     
                If fecha_inicio is Nothing  Then
                    Resultado.Add(My.Resources.Str_FechaInicio)
                End If
                If fecha_fin IsNot Nothing Then
                    If fecha_inicio > fecha_fin Then
                        Resultado.Add(My.Resources.Str_FechaInicioError )
                    End If
                End If
        End Select
        Return Resultado
    End Function
End Class
