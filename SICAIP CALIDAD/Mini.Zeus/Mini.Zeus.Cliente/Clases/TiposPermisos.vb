﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista de tipos de permisos conocidos.
''' </summary>
''' <remarks>La lista contiene valores para permisos conocidos, más un tipo especial para permisos desconocidos. Cuando se utiliza el valor desconocido ("Otro"), será responsabilidad del desarrollador darle significado.</remarks>
Public Enum TiposPermisos As Integer

    ''' <summary>
    ''' No representa un permiso.
    ''' </summary>
    Ninguno = 0

    ''' <summary>
    ''' Permiso para ver información general.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de realizar consultas a la base de datos y para desplegar en catálogos.</remarks>
    Ver = 1

    ''' <summary>
    ''' Permiso para ver información detallada de un registro.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de mostrar la información completa de un registro en su formulario.</remarks>
    VerDetalles = 2

    ''' <summary>
    ''' Permiso para agregar información.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intentar guardar información en la base de datos. Depende de contar con el permiso de "Ver".</remarks>
    Agregar = 3

    ''' <summary>
    ''' Permiso para editar información existente.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intentar abrir el formulario de un registro para editarlo. Depende de contar con el permiso de "Ver".</remarks>
    Editar = 4

    ''' <summary>
    ''' Permiso para borrar información existente.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intentar borrar información desde un catálogo. La información no es borrada realmente, sino marcada para excluirla en las consultas. Depende de contar con el permiso de "Ver".</remarks>
    Borrar = 5

    ''' <summary>
    ''' Permiso para exportar información.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intentar exportar la información desde un catálogo. Depende de contar con el permiso de "Ver".</remarks>
    Exportar = 6

    ''' <summary>
    ''' Permiso para recuperar información borrada.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intetar recuperar un registro borrado (marcado para ser excluido de las consultas).</remarks>
    Recuperar = 7

    ''' <summary>
    ''' Permiso para borrar permanentemente información existente.
    ''' </summary>
    ''' <remarks>Este tipo de permiso se aplica al momento de intentar borrar permanentemente información.</remarks>
    Destruir = 8

    ''' <summary>
    ''' Permiso no conocido (depende del contexto).
    ''' </summary>
    ''' <remarks>Este tipo de permiso solo tiene significado dentro de un contexto donde el desarrollador le de función.</remarks>
    Otro = 9999

End Enum