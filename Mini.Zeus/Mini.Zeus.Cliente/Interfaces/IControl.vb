﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para controles de Zeus.
''' </summary>
''' <remarks>Todos los controles basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IControl

    '''' <summary>
    '''' Contiene la referencia al usuario que utiliza el control.
    '''' </summary>
    '''' <remarks>Cuando no se asigna un valor a esta propiedad, el usuario actual se determina de forma dinámica de acuerdo al usuario actual de la sesión.</remarks>
    'Property UsuarioActual() As Usuarios

    ''' <summary>
    ''' Inicializa el control.
    ''' </summary>
    Sub InicializarControl()

    '''' <summary>
    '''' Inicializa el control para un usuario particular.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza el control.</param>
    'Sub InicializarControl(ByVal UsuarioActual As Usuarios)

End Interface
