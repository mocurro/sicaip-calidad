﻿Imports System.Data.Linq
Imports Mini.Zeus.Cliente.UX

'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Fábrica de monedas.
''' </summary>
''' <remarks>Esta fábrica permite administrar entidades de Clientes.</remarks>
Public Class Tipo_InspeccionFabricas
    Inherits Fabrica(Of Tipo_Inspeccion )


    ''' <summary>
    ''' Crea una nueva fábrica dinámica de entidades Zeus.
    ''' </summary>
    ''' <remarks>La información de conexión se obtiene a partir de la información existente en Sincronización. Cada consulta a la información se obtiene en el momento, por lo que la fábrica puede reaccionar a cambios después de haber sido creada sin manipular sus valores de forma directa.</remarks>
    Public Sub New()

        MyBase.New()

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica local de entidades Zeus.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos local.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos local.</remarks>
    Public Sub New(ByVal CadenaConexion As String)

        MyBase.New(CadenaConexion)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica remota de entidades Zeus.
    ''' </summary>
    ''' <param name="UrlServicio">La ruta del servicio remoto.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos remota.</remarks>
    Public Sub New(ByVal UrlServicio As Uri)

        MyBase.New(UrlServicio)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica de entidades Zeus.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos local.</param>
    ''' <param name="UrlServicio">La ruta del servicio remoto.</param>
    ''' <param name="UbicacionFuenteDatos">Determina la ubicación inicial de la fuente de datos de la fábrica. Este valor puede ser modificado posteriormente.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos local.</remarks>
    Public Sub New(ByVal CadenaConexion As String, ByVal UrlServicio As Uri, ByVal UbicacionFuenteDatos As UbicacionesFuentesDatos)

        MyBase.New(CadenaConexion, UrlServicio, UbicacionFuenteDatos)

    End Sub

    ''' <summary>
    ''' Borra la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Moneda a borrar.</param>
    Protected Overrides Sub BorrarSQLServer(ByVal Entidad As Tipo_Inspeccion  )

         Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Dim Objeto = Contexto.Tipo_Inspeccions .Single(Function(p As Tipo_Inspeccion ) p.cve_tipo_inspeccion  = Entidad.cve_tipo_inspeccion)
            Objeto.cuentaeliminado =1 
            Contexto.SubmitChanges
        End Using


    End Sub

    ''' <summary>
    ''' Borra la moneda remota.
    ''' </summary>
    ''' <param name="Entidad">ubicacion a borrar.</param>
    Protected Overrides Sub BorrarServicioWeb(ByVal Entidad As Tipo_Inspeccion )

    End Sub

    ''' <summary>
    ''' Crear un nuevo.
    ''' </summary>
    Protected Overrides Function CrearNuevaEntidad() As Tipo_Inspeccion

        Dim Resultado = New Tipo_Inspeccion()' With {.ID_LineasProductos = Guid.NewGuid()}
        Return Resultado

    End Function

    ''' <summary>
    ''' Destruye la ubicacion local.
    ''' </summary>
    ''' <param name="Entidad">ubicacion a destruir.</param>
    Protected Overrides Sub DestruirSQLServer(ByVal Entidad As Tipo_Inspeccion )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
              Contexto.Tipo_Inspeccions  .DeleteOnSubmit (Entidad)
            Contexto.SubmitChanges 
            'Contexto.DestruirLineasProductosPorGuidLineasProductos(Entidad.ID_LineasProductos)
        End Using

    End Sub

    ''' <summary>
    ''' Destruye la moneda remota.
    ''' </summary>
    ''' <param name="Entidad">Moneda a destruir.</param>
    Protected Overrides Sub DestruirServicioWeb(ByVal Entidad As Tipo_Inspeccion )

    End Sub


    ''' <summary>
    ''' Agrega la información de la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Ubicacion a agregar.</param>
    Protected Overrides Sub GuardarNuevoSQLServer(ByVal Entidad As Tipo_Inspeccion )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto .Tipo_Inspeccions.InsertOnSubmit (Entidad )
            Contexto.SubmitChanges 
            'Contexto.AgregarLineasProductos(Entidad.ID_LineasProductos, Entidad.ID_Linea, Entidad.Id_Codigo, Entidad.Lote, Entidad.Metadatos, Entidad.CuentaEliminado)
        End Using

    End Sub

    ''' <summary>
    ''' Agrega la información de la Ubicacion remota.
    ''' </summary>
    ''' <param name="Entidad">Ubicacion a agregar.</param>
    Protected Overrides Sub GuardarNuevoServicioWeb(ByVal Entidad As Tipo_Inspeccion )

    End Sub

    ''' <summary>
    ''' Define los datos relacionados a cargar al momento de una consulta.
    ''' </summary>
    ''' <param name="DatosRelacionados">La lista de las relaciones.</param>
    Protected Overrides Sub DefinirDatosRelacionados(ByRef DatosRelacionados As System.Data.Linq.DataLoadOptions)

        'With DatosRelacionados
        '    .LoadWith(Of Cliente_Modelo )(Function(Item As modelo) Item.Cliente  )
        '    .LoadWith(Of Cliente_Modelo )(Function(Item As Cliente_Modelo) Item.modelo   )
        'End With

    End Sub

    ''' <summary>
    ''' Obtiene las Ubicacions locales no borradas.
    ''' </summary>
    Protected Overloads Overrides Function ObtenerExistentesSQLServer(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object), ByVal DatosRelacionados As System.Data.Linq.DataLoadOptions) As System.Collections.Generic.List(Of Tipo_Inspeccion )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto.LoadOptions = DatosRelacionados
            Contexto.DeferredLoadingEnabled = False

            Dim Resultado = New List(Of Tipo_Inspeccion )
            If Filtro.Count = 0 Then
                'Obtener Ubicacions
                Dim Query = From clien In Contexto.Tipo_Inspeccions 
                            Where clien .cuentaeliminado =0
                            Order By clien.cve_tipo_inspeccion     Ascending
                            Select clien
                Resultado = Query.ToList()

            Else
                'Arrojar excepción
                Throw New ArgumentException()

            End If

            'Regresar lista resultante
            Return Resultado

        End Using

    End Function
    

    '''' <summary>
    '''' Obtiene una moneda local específico.
    '''' </summary>
    'Protected Overloads Overrides Function ObtenerUnoSQLServer(ByVal Id As Long , ByVal DatosRelacionados As System.Data.Linq.DataLoadOptions) As Cliente 

    '    Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
    '        Contexto.LoadOptions = DatosRelacionados
    '        Contexto.DeferredLoadingEnabled = False

    '        Dim Query = From client In Contexto.Clientes 
    '                    Where client.cve_cliente  = Id
    '                    Select client
    '        Return Query.SingleOrDefault()

    '    End Using

    'End Function

    ''' <summary>
    ''' Recupera la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Moneda a recuperar.</param>
    Protected Overrides Sub RecuperarSQLServer(ByVal Entidad As Tipo_Inspeccion )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            'Contexto.ReciclarLineasPorGuidLinea(Entidad.ID_Linea, False)
        End Using

    End Sub

    Protected Overrides Sub GuardarExistenteServicioWeb(ByVal Entidad As Tipo_Inspeccion )

    End Sub

    Protected Overloads Overrides Sub GuardarExistenteSQLServer(ByVal Entidad As Tipo_Inspeccion )
        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Dim Objeto = Contexto.Tipo_Inspeccions .Single(Function(p As Tipo_Inspeccion ) p.cve_tipo_inspeccion  = Entidad.cve_tipo_inspeccion)
            Objeto.nombre  = Entidad.nombre 
            Objeto.descripcion = Entidad.descripcion
            Objeto.tiempo  = Entidad.tiempo 
            Contexto.SubmitChanges
        End Using

    End Sub

    Protected Overrides Function ObtenerExistentesServicioWeb(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object)) As System.Collections.Generic.List(Of Tipo_Inspeccion )

    End Function

    Protected Overrides Function ObtenerUnoServicioWeb(ByVal Id As System.Guid) As Tipo_Inspeccion 

    End Function

    Protected Overrides Sub RecuperarServicioWeb(ByVal Entidad As Tipo_Inspeccion )

    End Sub

    Protected Overrides Function ObtenerUnoSQLServer(Id As String, DatosRelacionados As DataLoadOptions) As Tipo_Inspeccion 
        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto.LoadOptions = DatosRelacionados
            Contexto.DeferredLoadingEnabled = False

            Dim Query = From model   In Contexto.Tipo_Inspeccions  
                        Where model.cve_tipo_inspeccion   = Id
                        Select model
            Return Query.SingleOrDefault()

        End Using
    End Function

    Protected Overrides Function ObtenerUnoServicioWeb(Id As String) As Tipo_Inspeccion 
        Throw New NotImplementedException()
    End Function

    Protected Overrides Function ObtenerUnoSQLServer(Id As Guid, DatosRelacionados As DataLoadOptions) As Tipo_Inspeccion
        Throw New NotImplementedException()
    End Function
End Class
