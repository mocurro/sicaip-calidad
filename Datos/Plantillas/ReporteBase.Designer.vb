﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ReporteBase
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column7 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression7 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column8 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression8 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column9 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression9 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column10 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression10 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column11 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression11 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column12 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression12 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column13 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression13 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column14 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression14 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column15 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression15 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column16 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression16 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column17 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression17 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column18 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression18 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column19 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression19 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReporteBase))
        Dim SelectQuery2 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column20 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression20 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column21 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression21 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column22 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression22 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column23 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression23 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column24 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression24 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column25 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression25 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery3 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column26 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression26 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column27 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression27 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column28 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression28 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column29 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression29 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column30 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression30 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column31 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression31 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column32 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression32 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column33 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression33 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column34 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression34 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column35 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression35 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column36 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression36 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column37 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression37 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column38 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression38 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery4 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column39 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression39 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column40 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression40 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column41 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression41 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column42 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression42 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column43 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression43 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column44 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression44 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column45 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression45 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column46 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression46 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery5 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column47 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression47 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table5 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column48 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression48 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column49 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression49 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column50 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression50 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column51 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression51 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column52 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression52 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery6 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column53 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression53 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table6 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column54 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression54 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column55 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression55 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column56 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression56 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column57 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression57 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column58 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression58 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column59 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression59 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column60 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression60 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery7 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column61 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression61 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table7 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column62 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression62 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column63 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression63 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column64 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression64 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column65 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression65 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column66 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression66 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column67 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression67 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column68 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression68 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column69 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression69 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column70 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression70 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column71 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression71 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column72 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression72 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column73 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression73 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery8 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column74 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression74 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table8 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column75 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression75 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column76 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression76 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column77 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression77 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery9 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column78 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression78 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table9 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column79 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression79 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column80 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression80 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column81 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression81 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column82 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression82 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column83 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression83 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column84 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression84 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column85 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression85 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column86 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression86 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery10 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column87 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression87 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table10 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column88 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression88 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column89 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression89 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column90 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression90 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column91 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression91 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column92 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression92 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery11 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column93 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression93 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table11 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column94 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression94 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column95 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression95 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column96 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression96 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column97 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression97 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column98 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression98 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery12 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column99 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression99 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table12 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column100 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression100 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column101 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression101 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column102 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression102 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery13 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column103 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression103 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table13 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column104 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression104 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column105 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression105 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column106 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression106 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column107 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression107 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column108 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression108 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column109 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression109 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column110 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression110 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery14 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column111 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression111 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table14 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column112 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression112 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column113 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression113 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column114 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression114 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column115 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression115 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column116 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression116 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery15 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column117 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression117 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table15 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column118 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression118 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column119 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression119 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column120 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression120 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column121 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression121 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column122 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression122 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column123 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression123 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column124 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression124 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column125 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression125 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery16 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column126 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression126 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table16 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column127 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression127 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column128 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression128 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column129 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression129 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column130 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression130 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column131 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression131 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column132 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression132 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column133 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression133 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery17 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column134 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression134 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table17 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column135 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression135 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column136 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression136 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column137 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression137 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column138 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression138 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column139 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression139 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column140 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression140 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column141 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression141 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column142 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression142 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column143 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression143 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery18 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column144 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression144 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table18 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column145 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression145 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column146 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression146 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column147 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression147 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column148 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression148 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column149 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression149 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column150 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression150 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery19 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column151 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression151 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table19 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column152 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression152 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column153 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression153 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column154 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression154 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery20 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column155 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression155 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table20 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column156 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression156 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column157 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression157 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column158 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression158 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column159 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression159 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column160 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression160 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery21 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column161 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression161 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table21 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column162 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression162 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column163 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression163 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column164 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression164 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column165 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression165 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column166 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression166 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column167 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression167 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column168 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression168 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column169 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression169 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column170 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression170 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column171 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression171 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column172 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression172 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column173 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression173 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column174 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression174 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column175 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression175 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column176 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression176 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column177 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression177 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column178 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression178 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column179 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression179 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery22 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column180 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression180 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table22 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column181 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression181 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column182 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression182 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column183 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression183 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column184 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression184 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column185 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression185 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column186 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression186 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column187 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression187 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column188 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression188 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery23 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column189 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression189 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table23 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column190 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression190 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column191 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression191 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column192 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression192 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery24 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column193 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression193 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table24 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column194 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression194 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column195 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression195 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column196 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression196 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery25 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column197 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression197 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table25 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column198 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression198 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column199 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression199 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column200 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression200 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column201 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression201 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column202 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression202 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column203 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression203 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column204 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression204 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column205 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression205 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery26 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column206 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression206 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table26 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column207 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression207 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column208 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression208 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column209 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression209 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column210 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression210 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column211 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression211 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery27 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column212 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression212 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table27 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column213 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression213 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column214 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression214 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column215 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression215 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column216 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression216 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column217 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression217 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column218 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression218 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column219 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression219 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery28 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column220 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression220 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table28 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column221 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression221 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column222 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression222 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column223 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression223 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column224 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression224 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column225 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression225 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column226 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression226 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column227 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression227 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column228 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression228 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery29 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column229 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression229 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table29 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column230 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression230 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column231 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression231 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column232 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression232 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column233 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression233 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column234 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression234 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column235 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression235 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery30 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column236 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression236 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table30 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column237 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression237 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column238 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression238 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column239 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression239 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery31 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column240 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression240 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table31 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column241 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression241 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column242 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression242 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column243 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression243 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column244 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression244 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column245 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression245 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column246 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression246 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column247 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression247 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column248 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression248 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column249 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression249 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column250 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression250 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery32 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column251 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression251 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table32 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column252 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression252 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column253 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression253 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column254 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression254 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column255 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression255 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column256 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression256 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column257 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression257 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column258 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression258 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery33 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column259 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression259 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table33 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column260 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression260 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column261 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression261 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column262 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression262 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery34 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column263 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression263 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table34 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column264 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression264 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column265 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression265 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column266 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression266 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column267 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression267 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column268 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression268 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column269 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression269 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column270 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression270 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column271 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression271 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column272 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression272 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery35 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column273 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression273 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table35 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column274 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression274 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column275 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression275 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column276 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression276 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column277 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression277 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column278 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression278 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column279 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression279 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column280 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression280 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery36 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column281 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression281 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table36 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column282 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression282 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column283 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression283 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column284 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression284 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim SelectQuery37 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column285 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression285 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table37 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column286 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression286 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column287 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression287 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.FuenteDatos = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource()
        Me.SqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource()
        Me.FuenteDatos2 = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatos2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Expanded = False
        Me.Detail.HeightF = 100.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 100.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 100.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1})
        Me.DetailReport.DataMember = "DetalleProceso"
        Me.DetailReport.DataSource = Me.FuenteDatos2
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.HeightF = 100.0!
        Me.Detail1.Name = "Detail1"
        '
        'FuenteDatos
        '
        Me.FuenteDatos.DataSource = GetType(AppFin.ProduccionBase)
        Me.FuenteDatos.Name = "FuenteDatos"
        '
        'SqlDataSource2
        '
        Me.SqlDataSource2.ConnectionName = "des05n\localdb#7fd3358f.ControlCantidades_WAA.dbo"
        Me.SqlDataSource2.Name = "SqlDataSource2"
        ColumnExpression1.ColumnName = "Oid"
        Table1.Name = "Producción"
        ColumnExpression1.Table = Table1
        Column1.Expression = ColumnExpression1
        ColumnExpression2.ColumnName = "Usuario"
        ColumnExpression2.Table = Table1
        Column2.Expression = ColumnExpression2
        ColumnExpression3.ColumnName = "Maquina"
        ColumnExpression3.Table = Table1
        Column3.Expression = ColumnExpression3
        ColumnExpression4.ColumnName = "FechaProducion"
        ColumnExpression4.Table = Table1
        Column4.Expression = ColumnExpression4
        ColumnExpression5.ColumnName = "Estatus"
        ColumnExpression5.Table = Table1
        Column5.Expression = ColumnExpression5
        ColumnExpression6.ColumnName = "Lote"
        ColumnExpression6.Table = Table1
        Column6.Expression = ColumnExpression6
        ColumnExpression7.ColumnName = "NoParte"
        ColumnExpression7.Table = Table1
        Column7.Expression = ColumnExpression7
        ColumnExpression8.ColumnName = "Contenedor"
        ColumnExpression8.Table = Table1
        Column8.Expression = ColumnExpression8
        ColumnExpression9.ColumnName = "NoPiezas"
        ColumnExpression9.Table = Table1
        Column9.Expression = ColumnExpression9
        ColumnExpression10.ColumnName = "OptimisticLockField"
        ColumnExpression10.Table = Table1
        Column10.Expression = ColumnExpression10
        ColumnExpression11.ColumnName = "GCRecord"
        ColumnExpression11.Table = Table1
        Column11.Expression = ColumnExpression11
        ColumnExpression12.ColumnName = "Almacen"
        ColumnExpression12.Table = Table1
        Column12.Expression = ColumnExpression12
        ColumnExpression13.ColumnName = "FechaCaducidad"
        ColumnExpression13.Table = Table1
        Column13.Expression = ColumnExpression13
        ColumnExpression14.ColumnName = "LoteCliente"
        ColumnExpression14.Table = Table1
        Column14.Expression = ColumnExpression14
        ColumnExpression15.ColumnName = "NoContenedor"
        ColumnExpression15.Table = Table1
        Column15.Expression = ColumnExpression15
        ColumnExpression16.ColumnName = "DescripcionProducto"
        ColumnExpression16.Table = Table1
        Column16.Expression = ColumnExpression16
        ColumnExpression17.ColumnName = "ClaveUsuario"
        ColumnExpression17.Table = Table1
        Column17.Expression = ColumnExpression17
        ColumnExpression18.ColumnName = "Estado"
        ColumnExpression18.Table = Table1
        Column18.Expression = ColumnExpression18
        ColumnExpression19.ColumnName = "EstandarPack"
        ColumnExpression19.Table = Table1
        Column19.Expression = ColumnExpression19
        SelectQuery1.Columns.Add(Column1)
        SelectQuery1.Columns.Add(Column2)
        SelectQuery1.Columns.Add(Column3)
        SelectQuery1.Columns.Add(Column4)
        SelectQuery1.Columns.Add(Column5)
        SelectQuery1.Columns.Add(Column6)
        SelectQuery1.Columns.Add(Column7)
        SelectQuery1.Columns.Add(Column8)
        SelectQuery1.Columns.Add(Column9)
        SelectQuery1.Columns.Add(Column10)
        SelectQuery1.Columns.Add(Column11)
        SelectQuery1.Columns.Add(Column12)
        SelectQuery1.Columns.Add(Column13)
        SelectQuery1.Columns.Add(Column14)
        SelectQuery1.Columns.Add(Column15)
        SelectQuery1.Columns.Add(Column16)
        SelectQuery1.Columns.Add(Column17)
        SelectQuery1.Columns.Add(Column18)
        SelectQuery1.Columns.Add(Column19)
        SelectQuery1.Name = "Producción"
        SelectQuery1.Tables.Add(Table1)
        Me.SqlDataSource2.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
        Me.SqlDataSource2.ResultSchemaSerializable = resources.GetString("SqlDataSource2.ResultSchemaSerializable")
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionName = "des05n\localdb#7fd3358f.ControlCantidades_WAA.dbo"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        ColumnExpression20.ColumnName = "Oid"
        Table2.Name = "Almacen"
        ColumnExpression20.Table = Table2
        Column20.Expression = ColumnExpression20
        ColumnExpression21.ColumnName = "Nombre"
        ColumnExpression21.Table = Table2
        Column21.Expression = ColumnExpression21
        ColumnExpression22.ColumnName = "Descripcion"
        ColumnExpression22.Table = Table2
        Column22.Expression = ColumnExpression22
        ColumnExpression23.ColumnName = "Ubicacion"
        ColumnExpression23.Table = Table2
        Column23.Expression = ColumnExpression23
        ColumnExpression24.ColumnName = "OptimisticLockField"
        ColumnExpression24.Table = Table2
        Column24.Expression = ColumnExpression24
        ColumnExpression25.ColumnName = "GCRecord"
        ColumnExpression25.Table = Table2
        Column25.Expression = ColumnExpression25
        SelectQuery2.Columns.Add(Column20)
        SelectQuery2.Columns.Add(Column21)
        SelectQuery2.Columns.Add(Column22)
        SelectQuery2.Columns.Add(Column23)
        SelectQuery2.Columns.Add(Column24)
        SelectQuery2.Columns.Add(Column25)
        SelectQuery2.Name = "Almacen"
        SelectQuery2.Tables.Add(Table2)
        ColumnExpression26.ColumnName = "Oid"
        Table3.Name = "Bitacora"
        ColumnExpression26.Table = Table3
        Column26.Expression = ColumnExpression26
        ColumnExpression27.ColumnName = "Almacen"
        ColumnExpression27.Table = Table3
        Column27.Expression = ColumnExpression27
        ColumnExpression28.ColumnName = "NoParte"
        ColumnExpression28.Table = Table3
        Column28.Expression = ColumnExpression28
        ColumnExpression29.ColumnName = "Estandar"
        ColumnExpression29.Table = Table3
        Column29.Expression = ColumnExpression29
        ColumnExpression30.ColumnName = "Usuario"
        ColumnExpression30.Table = Table3
        Column30.Expression = ColumnExpression30
        ColumnExpression31.ColumnName = "FechaCaducidad"
        ColumnExpression31.Table = Table3
        Column31.Expression = ColumnExpression31
        ColumnExpression32.ColumnName = "LoteInterno"
        ColumnExpression32.Table = Table3
        Column32.Expression = ColumnExpression32
        ColumnExpression33.ColumnName = "LoteCliente"
        ColumnExpression33.Table = Table3
        Column33.Expression = ColumnExpression33
        ColumnExpression34.ColumnName = "Maquina"
        ColumnExpression34.Table = Table3
        Column34.Expression = ColumnExpression34
        ColumnExpression35.ColumnName = "FechaCreacion"
        ColumnExpression35.Table = Table3
        Column35.Expression = ColumnExpression35
        ColumnExpression36.ColumnName = "NoContenedor"
        ColumnExpression36.Table = Table3
        Column36.Expression = ColumnExpression36
        ColumnExpression37.ColumnName = "OptimisticLockField"
        ColumnExpression37.Table = Table3
        Column37.Expression = ColumnExpression37
        ColumnExpression38.ColumnName = "GCRecord"
        ColumnExpression38.Table = Table3
        Column38.Expression = ColumnExpression38
        SelectQuery3.Columns.Add(Column26)
        SelectQuery3.Columns.Add(Column27)
        SelectQuery3.Columns.Add(Column28)
        SelectQuery3.Columns.Add(Column29)
        SelectQuery3.Columns.Add(Column30)
        SelectQuery3.Columns.Add(Column31)
        SelectQuery3.Columns.Add(Column32)
        SelectQuery3.Columns.Add(Column33)
        SelectQuery3.Columns.Add(Column34)
        SelectQuery3.Columns.Add(Column35)
        SelectQuery3.Columns.Add(Column36)
        SelectQuery3.Columns.Add(Column37)
        SelectQuery3.Columns.Add(Column38)
        SelectQuery3.Name = "Bitacora"
        SelectQuery3.Tables.Add(Table3)
        ColumnExpression39.ColumnName = "Oid"
        Table4.Name = "Condiciones"
        ColumnExpression39.Table = Table4
        Column39.Expression = ColumnExpression39
        ColumnExpression40.ColumnName = "Nombre"
        ColumnExpression40.Table = Table4
        Column40.Expression = ColumnExpression40
        ColumnExpression41.ColumnName = "RangoInferior"
        ColumnExpression41.Table = Table4
        Column41.Expression = ColumnExpression41
        ColumnExpression42.ColumnName = "RangoSuperior"
        ColumnExpression42.Table = Table4
        Column42.Expression = ColumnExpression42
        ColumnExpression43.ColumnName = "UnidadMedida"
        ColumnExpression43.Table = Table4
        Column43.Expression = ColumnExpression43
        ColumnExpression44.ColumnName = "MaquinaCondicion"
        ColumnExpression44.Table = Table4
        Column44.Expression = ColumnExpression44
        ColumnExpression45.ColumnName = "OptimisticLockField"
        ColumnExpression45.Table = Table4
        Column45.Expression = ColumnExpression45
        ColumnExpression46.ColumnName = "GCRecord"
        ColumnExpression46.Table = Table4
        Column46.Expression = ColumnExpression46
        SelectQuery4.Columns.Add(Column39)
        SelectQuery4.Columns.Add(Column40)
        SelectQuery4.Columns.Add(Column41)
        SelectQuery4.Columns.Add(Column42)
        SelectQuery4.Columns.Add(Column43)
        SelectQuery4.Columns.Add(Column44)
        SelectQuery4.Columns.Add(Column45)
        SelectQuery4.Columns.Add(Column46)
        SelectQuery4.Name = "Condiciones"
        SelectQuery4.Tables.Add(Table4)
        ColumnExpression47.ColumnName = "Oid"
        Table5.Name = "ConfiguracionPuertos"
        ColumnExpression47.Table = Table5
        Column47.Expression = ColumnExpression47
        ColumnExpression48.ColumnName = "Maquina"
        ColumnExpression48.Table = Table5
        Column48.Expression = ColumnExpression48
        ColumnExpression49.ColumnName = "PuertoAPPInicio"
        ColumnExpression49.Table = Table5
        Column49.Expression = ColumnExpression49
        ColumnExpression50.ColumnName = "PuertoAPPFin"
        ColumnExpression50.Table = Table5
        Column50.Expression = ColumnExpression50
        ColumnExpression51.ColumnName = "OptimisticLockField"
        ColumnExpression51.Table = Table5
        Column51.Expression = ColumnExpression51
        ColumnExpression52.ColumnName = "GCRecord"
        ColumnExpression52.Table = Table5
        Column52.Expression = ColumnExpression52
        SelectQuery5.Columns.Add(Column47)
        SelectQuery5.Columns.Add(Column48)
        SelectQuery5.Columns.Add(Column49)
        SelectQuery5.Columns.Add(Column50)
        SelectQuery5.Columns.Add(Column51)
        SelectQuery5.Columns.Add(Column52)
        SelectQuery5.Name = "ConfiguracionPuertos"
        SelectQuery5.Tables.Add(Table5)
        ColumnExpression53.ColumnName = "Oid"
        Table6.Name = "Contenedores"
        ColumnExpression53.Table = Table6
        Column53.Expression = ColumnExpression53
        ColumnExpression54.ColumnName = "Codigo"
        ColumnExpression54.Table = Table6
        Column54.Expression = ColumnExpression54
        ColumnExpression55.ColumnName = "Descripcion"
        ColumnExpression55.Table = Table6
        Column55.Expression = ColumnExpression55
        ColumnExpression56.ColumnName = "PesoMin"
        ColumnExpression56.Table = Table6
        Column56.Expression = ColumnExpression56
        ColumnExpression57.ColumnName = "PesoMax"
        ColumnExpression57.Table = Table6
        Column57.Expression = ColumnExpression57
        ColumnExpression58.ColumnName = "Imagen"
        ColumnExpression58.Table = Table6
        Column58.Expression = ColumnExpression58
        ColumnExpression59.ColumnName = "OptimisticLockField"
        ColumnExpression59.Table = Table6
        Column59.Expression = ColumnExpression59
        ColumnExpression60.ColumnName = "GCRecord"
        ColumnExpression60.Table = Table6
        Column60.Expression = ColumnExpression60
        SelectQuery6.Columns.Add(Column53)
        SelectQuery6.Columns.Add(Column54)
        SelectQuery6.Columns.Add(Column55)
        SelectQuery6.Columns.Add(Column56)
        SelectQuery6.Columns.Add(Column57)
        SelectQuery6.Columns.Add(Column58)
        SelectQuery6.Columns.Add(Column59)
        SelectQuery6.Columns.Add(Column60)
        SelectQuery6.Name = "Contenedores"
        SelectQuery6.Tables.Add(Table6)
        ColumnExpression61.ColumnName = "Oid"
        Table7.Name = "DatosPlantilla"
        ColumnExpression61.Table = Table7
        Column61.Expression = ColumnExpression61
        ColumnExpression62.ColumnName = "Almacen"
        ColumnExpression62.Table = Table7
        Column62.Expression = ColumnExpression62
        ColumnExpression63.ColumnName = "NoParte"
        ColumnExpression63.Table = Table7
        Column63.Expression = ColumnExpression63
        ColumnExpression64.ColumnName = "Estandar"
        ColumnExpression64.Table = Table7
        Column64.Expression = ColumnExpression64
        ColumnExpression65.ColumnName = "Usuario"
        ColumnExpression65.Table = Table7
        Column65.Expression = ColumnExpression65
        ColumnExpression66.ColumnName = "FechaCaducidad"
        ColumnExpression66.Table = Table7
        Column66.Expression = ColumnExpression66
        ColumnExpression67.ColumnName = "LoteInterno"
        ColumnExpression67.Table = Table7
        Column67.Expression = ColumnExpression67
        ColumnExpression68.ColumnName = "LoteCliente"
        ColumnExpression68.Table = Table7
        Column68.Expression = ColumnExpression68
        ColumnExpression69.ColumnName = "Maquina"
        ColumnExpression69.Table = Table7
        Column69.Expression = ColumnExpression69
        ColumnExpression70.ColumnName = "FechaCreacion"
        ColumnExpression70.Table = Table7
        Column70.Expression = ColumnExpression70
        ColumnExpression71.ColumnName = "OptimisticLockField"
        ColumnExpression71.Table = Table7
        Column71.Expression = ColumnExpression71
        ColumnExpression72.ColumnName = "GCRecord"
        ColumnExpression72.Table = Table7
        Column72.Expression = ColumnExpression72
        ColumnExpression73.ColumnName = "NoContenedor"
        ColumnExpression73.Table = Table7
        Column73.Expression = ColumnExpression73
        SelectQuery7.Columns.Add(Column61)
        SelectQuery7.Columns.Add(Column62)
        SelectQuery7.Columns.Add(Column63)
        SelectQuery7.Columns.Add(Column64)
        SelectQuery7.Columns.Add(Column65)
        SelectQuery7.Columns.Add(Column66)
        SelectQuery7.Columns.Add(Column67)
        SelectQuery7.Columns.Add(Column68)
        SelectQuery7.Columns.Add(Column69)
        SelectQuery7.Columns.Add(Column70)
        SelectQuery7.Columns.Add(Column71)
        SelectQuery7.Columns.Add(Column72)
        SelectQuery7.Columns.Add(Column73)
        SelectQuery7.Name = "DatosPlantilla"
        SelectQuery7.Tables.Add(Table7)
        ColumnExpression74.ColumnName = "Oid"
        Table8.Name = "HorariosCondiciones"
        ColumnExpression74.Table = Table8
        Column74.Expression = ColumnExpression74
        ColumnExpression75.ColumnName = "Horario"
        ColumnExpression75.Table = Table8
        Column75.Expression = ColumnExpression75
        ColumnExpression76.ColumnName = "OptimisticLockField"
        ColumnExpression76.Table = Table8
        Column76.Expression = ColumnExpression76
        ColumnExpression77.ColumnName = "GCRecord"
        ColumnExpression77.Table = Table8
        Column77.Expression = ColumnExpression77
        SelectQuery8.Columns.Add(Column74)
        SelectQuery8.Columns.Add(Column75)
        SelectQuery8.Columns.Add(Column76)
        SelectQuery8.Columns.Add(Column77)
        SelectQuery8.Name = "HorariosCondiciones"
        SelectQuery8.Tables.Add(Table8)
        ColumnExpression78.ColumnName = "Oid"
        Table9.Name = "Maquinas"
        ColumnExpression78.Table = Table9
        Column78.Expression = ColumnExpression78
        ColumnExpression79.ColumnName = "Nombre"
        ColumnExpression79.Table = Table9
        Column79.Expression = ColumnExpression79
        ColumnExpression80.ColumnName = "Descripcion"
        ColumnExpression80.Table = Table9
        Column80.Expression = ColumnExpression80
        ColumnExpression81.ColumnName = "MaquinaAlmacen"
        ColumnExpression81.Table = Table9
        Column81.Expression = ColumnExpression81
        ColumnExpression82.ColumnName = "TrabajaNoParteSeleccionado"
        ColumnExpression82.Table = Table9
        Column82.Expression = ColumnExpression82
        ColumnExpression83.ColumnName = "TipoNotificacion"
        ColumnExpression83.Table = Table9
        Column83.Expression = ColumnExpression83
        ColumnExpression84.ColumnName = "Estatus"
        ColumnExpression84.Table = Table9
        Column84.Expression = ColumnExpression84
        ColumnExpression85.ColumnName = "OptimisticLockField"
        ColumnExpression85.Table = Table9
        Column85.Expression = ColumnExpression85
        ColumnExpression86.ColumnName = "GCRecord"
        ColumnExpression86.Table = Table9
        Column86.Expression = ColumnExpression86
        SelectQuery9.Columns.Add(Column78)
        SelectQuery9.Columns.Add(Column79)
        SelectQuery9.Columns.Add(Column80)
        SelectQuery9.Columns.Add(Column81)
        SelectQuery9.Columns.Add(Column82)
        SelectQuery9.Columns.Add(Column83)
        SelectQuery9.Columns.Add(Column84)
        SelectQuery9.Columns.Add(Column85)
        SelectQuery9.Columns.Add(Column86)
        SelectQuery9.Name = "Maquinas"
        SelectQuery9.Tables.Add(Table9)
        ColumnExpression87.ColumnName = "Oid"
        Table10.Name = "ModelDifference"
        ColumnExpression87.Table = Table10
        Column87.Expression = ColumnExpression87
        ColumnExpression88.ColumnName = "UserId"
        ColumnExpression88.Table = Table10
        Column88.Expression = ColumnExpression88
        ColumnExpression89.ColumnName = "ContextId"
        ColumnExpression89.Table = Table10
        Column89.Expression = ColumnExpression89
        ColumnExpression90.ColumnName = "Version"
        ColumnExpression90.Table = Table10
        Column90.Expression = ColumnExpression90
        ColumnExpression91.ColumnName = "OptimisticLockField"
        ColumnExpression91.Table = Table10
        Column91.Expression = ColumnExpression91
        ColumnExpression92.ColumnName = "GCRecord"
        ColumnExpression92.Table = Table10
        Column92.Expression = ColumnExpression92
        SelectQuery10.Columns.Add(Column87)
        SelectQuery10.Columns.Add(Column88)
        SelectQuery10.Columns.Add(Column89)
        SelectQuery10.Columns.Add(Column90)
        SelectQuery10.Columns.Add(Column91)
        SelectQuery10.Columns.Add(Column92)
        SelectQuery10.Name = "ModelDifference"
        SelectQuery10.Tables.Add(Table10)
        ColumnExpression93.ColumnName = "Oid"
        Table11.Name = "ModelDifferenceAspect"
        ColumnExpression93.Table = Table11
        Column93.Expression = ColumnExpression93
        ColumnExpression94.ColumnName = "Name"
        ColumnExpression94.Table = Table11
        Column94.Expression = ColumnExpression94
        ColumnExpression95.ColumnName = "Xml"
        ColumnExpression95.Table = Table11
        Column95.Expression = ColumnExpression95
        ColumnExpression96.ColumnName = "Owner"
        ColumnExpression96.Table = Table11
        Column96.Expression = ColumnExpression96
        ColumnExpression97.ColumnName = "OptimisticLockField"
        ColumnExpression97.Table = Table11
        Column97.Expression = ColumnExpression97
        ColumnExpression98.ColumnName = "GCRecord"
        ColumnExpression98.Table = Table11
        Column98.Expression = ColumnExpression98
        SelectQuery11.Columns.Add(Column93)
        SelectQuery11.Columns.Add(Column94)
        SelectQuery11.Columns.Add(Column95)
        SelectQuery11.Columns.Add(Column96)
        SelectQuery11.Columns.Add(Column97)
        SelectQuery11.Columns.Add(Column98)
        SelectQuery11.Name = "ModelDifferenceAspect"
        SelectQuery11.Tables.Add(Table11)
        ColumnExpression99.ColumnName = "Oid"
        Table12.Name = "NoParteNoContinuos"
        ColumnExpression99.Table = Table12
        Column99.Expression = ColumnExpression99
        ColumnExpression100.ColumnName = "NoParteNo"
        ColumnExpression100.Table = Table12
        Column100.Expression = ColumnExpression100
        ColumnExpression101.ColumnName = "OptimisticLockField"
        ColumnExpression101.Table = Table12
        Column101.Expression = ColumnExpression101
        ColumnExpression102.ColumnName = "GCRecord"
        ColumnExpression102.Table = Table12
        Column102.Expression = ColumnExpression102
        SelectQuery12.Columns.Add(Column99)
        SelectQuery12.Columns.Add(Column100)
        SelectQuery12.Columns.Add(Column101)
        SelectQuery12.Columns.Add(Column102)
        SelectQuery12.Name = "NoParteNoContinuos"
        SelectQuery12.Tables.Add(Table12)
        ColumnExpression103.ColumnName = "Oid"
        Table13.Name = "PermissionPolicyMemberPermissionsObject"
        ColumnExpression103.Table = Table13
        Column103.Expression = ColumnExpression103
        ColumnExpression104.ColumnName = "Members"
        ColumnExpression104.Table = Table13
        Column104.Expression = ColumnExpression104
        ColumnExpression105.ColumnName = "ReadState"
        ColumnExpression105.Table = Table13
        Column105.Expression = ColumnExpression105
        ColumnExpression106.ColumnName = "WriteState"
        ColumnExpression106.Table = Table13
        Column106.Expression = ColumnExpression106
        ColumnExpression107.ColumnName = "Criteria"
        ColumnExpression107.Table = Table13
        Column107.Expression = ColumnExpression107
        ColumnExpression108.ColumnName = "TypePermissionObject"
        ColumnExpression108.Table = Table13
        Column108.Expression = ColumnExpression108
        ColumnExpression109.ColumnName = "OptimisticLockField"
        ColumnExpression109.Table = Table13
        Column109.Expression = ColumnExpression109
        ColumnExpression110.ColumnName = "GCRecord"
        ColumnExpression110.Table = Table13
        Column110.Expression = ColumnExpression110
        SelectQuery13.Columns.Add(Column103)
        SelectQuery13.Columns.Add(Column104)
        SelectQuery13.Columns.Add(Column105)
        SelectQuery13.Columns.Add(Column106)
        SelectQuery13.Columns.Add(Column107)
        SelectQuery13.Columns.Add(Column108)
        SelectQuery13.Columns.Add(Column109)
        SelectQuery13.Columns.Add(Column110)
        SelectQuery13.Name = "PermissionPolicyMemberPermissionsObject"
        SelectQuery13.Tables.Add(Table13)
        ColumnExpression111.ColumnName = "Oid"
        Table14.Name = "PermissionPolicyNavigationPermissionsObject"
        ColumnExpression111.Table = Table14
        Column111.Expression = ColumnExpression111
        ColumnExpression112.ColumnName = "ItemPath"
        ColumnExpression112.Table = Table14
        Column112.Expression = ColumnExpression112
        ColumnExpression113.ColumnName = "NavigateState"
        ColumnExpression113.Table = Table14
        Column113.Expression = ColumnExpression113
        ColumnExpression114.ColumnName = "Role"
        ColumnExpression114.Table = Table14
        Column114.Expression = ColumnExpression114
        ColumnExpression115.ColumnName = "OptimisticLockField"
        ColumnExpression115.Table = Table14
        Column115.Expression = ColumnExpression115
        ColumnExpression116.ColumnName = "GCRecord"
        ColumnExpression116.Table = Table14
        Column116.Expression = ColumnExpression116
        SelectQuery14.Columns.Add(Column111)
        SelectQuery14.Columns.Add(Column112)
        SelectQuery14.Columns.Add(Column113)
        SelectQuery14.Columns.Add(Column114)
        SelectQuery14.Columns.Add(Column115)
        SelectQuery14.Columns.Add(Column116)
        SelectQuery14.Name = "PermissionPolicyNavigationPermissionsObject"
        SelectQuery14.Tables.Add(Table14)
        ColumnExpression117.ColumnName = "Oid"
        Table15.Name = "PermissionPolicyObjectPermissionsObject"
        ColumnExpression117.Table = Table15
        Column117.Expression = ColumnExpression117
        ColumnExpression118.ColumnName = "Criteria"
        ColumnExpression118.Table = Table15
        Column118.Expression = ColumnExpression118
        ColumnExpression119.ColumnName = "ReadState"
        ColumnExpression119.Table = Table15
        Column119.Expression = ColumnExpression119
        ColumnExpression120.ColumnName = "WriteState"
        ColumnExpression120.Table = Table15
        Column120.Expression = ColumnExpression120
        ColumnExpression121.ColumnName = "DeleteState"
        ColumnExpression121.Table = Table15
        Column121.Expression = ColumnExpression121
        ColumnExpression122.ColumnName = "NavigateState"
        ColumnExpression122.Table = Table15
        Column122.Expression = ColumnExpression122
        ColumnExpression123.ColumnName = "TypePermissionObject"
        ColumnExpression123.Table = Table15
        Column123.Expression = ColumnExpression123
        ColumnExpression124.ColumnName = "OptimisticLockField"
        ColumnExpression124.Table = Table15
        Column124.Expression = ColumnExpression124
        ColumnExpression125.ColumnName = "GCRecord"
        ColumnExpression125.Table = Table15
        Column125.Expression = ColumnExpression125
        SelectQuery15.Columns.Add(Column117)
        SelectQuery15.Columns.Add(Column118)
        SelectQuery15.Columns.Add(Column119)
        SelectQuery15.Columns.Add(Column120)
        SelectQuery15.Columns.Add(Column121)
        SelectQuery15.Columns.Add(Column122)
        SelectQuery15.Columns.Add(Column123)
        SelectQuery15.Columns.Add(Column124)
        SelectQuery15.Columns.Add(Column125)
        SelectQuery15.Name = "PermissionPolicyObjectPermissionsObject"
        SelectQuery15.Tables.Add(Table15)
        ColumnExpression126.ColumnName = "Oid"
        Table16.Name = "PermissionPolicyRole"
        ColumnExpression126.Table = Table16
        Column126.Expression = ColumnExpression126
        ColumnExpression127.ColumnName = "Name"
        ColumnExpression127.Table = Table16
        Column127.Expression = ColumnExpression127
        ColumnExpression128.ColumnName = "IsAdministrative"
        ColumnExpression128.Table = Table16
        Column128.Expression = ColumnExpression128
        ColumnExpression129.ColumnName = "CanEditModel"
        ColumnExpression129.Table = Table16
        Column129.Expression = ColumnExpression129
        ColumnExpression130.ColumnName = "PermissionPolicy"
        ColumnExpression130.Table = Table16
        Column130.Expression = ColumnExpression130
        ColumnExpression131.ColumnName = "OptimisticLockField"
        ColumnExpression131.Table = Table16
        Column131.Expression = ColumnExpression131
        ColumnExpression132.ColumnName = "GCRecord"
        ColumnExpression132.Table = Table16
        Column132.Expression = ColumnExpression132
        ColumnExpression133.ColumnName = "ObjectType"
        ColumnExpression133.Table = Table16
        Column133.Expression = ColumnExpression133
        SelectQuery16.Columns.Add(Column126)
        SelectQuery16.Columns.Add(Column127)
        SelectQuery16.Columns.Add(Column128)
        SelectQuery16.Columns.Add(Column129)
        SelectQuery16.Columns.Add(Column130)
        SelectQuery16.Columns.Add(Column131)
        SelectQuery16.Columns.Add(Column132)
        SelectQuery16.Columns.Add(Column133)
        SelectQuery16.Name = "PermissionPolicyRole"
        SelectQuery16.Tables.Add(Table16)
        ColumnExpression134.ColumnName = "Oid"
        Table17.Name = "PermissionPolicyTypePermissionsObject"
        ColumnExpression134.Table = Table17
        Column134.Expression = ColumnExpression134
        ColumnExpression135.ColumnName = "Role"
        ColumnExpression135.Table = Table17
        Column135.Expression = ColumnExpression135
        ColumnExpression136.ColumnName = "TargetType"
        ColumnExpression136.Table = Table17
        Column136.Expression = ColumnExpression136
        ColumnExpression137.ColumnName = "ReadState"
        ColumnExpression137.Table = Table17
        Column137.Expression = ColumnExpression137
        ColumnExpression138.ColumnName = "WriteState"
        ColumnExpression138.Table = Table17
        Column138.Expression = ColumnExpression138
        ColumnExpression139.ColumnName = "CreateState"
        ColumnExpression139.Table = Table17
        Column139.Expression = ColumnExpression139
        ColumnExpression140.ColumnName = "DeleteState"
        ColumnExpression140.Table = Table17
        Column140.Expression = ColumnExpression140
        ColumnExpression141.ColumnName = "NavigateState"
        ColumnExpression141.Table = Table17
        Column141.Expression = ColumnExpression141
        ColumnExpression142.ColumnName = "OptimisticLockField"
        ColumnExpression142.Table = Table17
        Column142.Expression = ColumnExpression142
        ColumnExpression143.ColumnName = "GCRecord"
        ColumnExpression143.Table = Table17
        Column143.Expression = ColumnExpression143
        SelectQuery17.Columns.Add(Column134)
        SelectQuery17.Columns.Add(Column135)
        SelectQuery17.Columns.Add(Column136)
        SelectQuery17.Columns.Add(Column137)
        SelectQuery17.Columns.Add(Column138)
        SelectQuery17.Columns.Add(Column139)
        SelectQuery17.Columns.Add(Column140)
        SelectQuery17.Columns.Add(Column141)
        SelectQuery17.Columns.Add(Column142)
        SelectQuery17.Columns.Add(Column143)
        SelectQuery17.Name = "PermissionPolicyTypePermissionsObject"
        SelectQuery17.Tables.Add(Table17)
        ColumnExpression144.ColumnName = "Oid"
        Table18.Name = "PermissionPolicyUser"
        ColumnExpression144.Table = Table18
        Column144.Expression = ColumnExpression144
        ColumnExpression145.ColumnName = "StoredPassword"
        ColumnExpression145.Table = Table18
        Column145.Expression = ColumnExpression145
        ColumnExpression146.ColumnName = "ChangePasswordOnFirstLogon"
        ColumnExpression146.Table = Table18
        Column146.Expression = ColumnExpression146
        ColumnExpression147.ColumnName = "UserName"
        ColumnExpression147.Table = Table18
        Column147.Expression = ColumnExpression147
        ColumnExpression148.ColumnName = "IsActive"
        ColumnExpression148.Table = Table18
        Column148.Expression = ColumnExpression148
        ColumnExpression149.ColumnName = "OptimisticLockField"
        ColumnExpression149.Table = Table18
        Column149.Expression = ColumnExpression149
        ColumnExpression150.ColumnName = "GCRecord"
        ColumnExpression150.Table = Table18
        Column150.Expression = ColumnExpression150
        SelectQuery18.Columns.Add(Column144)
        SelectQuery18.Columns.Add(Column145)
        SelectQuery18.Columns.Add(Column146)
        SelectQuery18.Columns.Add(Column147)
        SelectQuery18.Columns.Add(Column148)
        SelectQuery18.Columns.Add(Column149)
        SelectQuery18.Columns.Add(Column150)
        SelectQuery18.Name = "PermissionPolicyUser"
        SelectQuery18.Tables.Add(Table18)
        ColumnExpression151.ColumnName = "Roles"
        Table19.Name = "PermissionPolicyUserUsers_PermissionPolicyRoleRoles"
        ColumnExpression151.Table = Table19
        Column151.Expression = ColumnExpression151
        ColumnExpression152.ColumnName = "Users"
        ColumnExpression152.Table = Table19
        Column152.Expression = ColumnExpression152
        ColumnExpression153.ColumnName = "OID"
        ColumnExpression153.Table = Table19
        Column153.Expression = ColumnExpression153
        ColumnExpression154.ColumnName = "OptimisticLockField"
        ColumnExpression154.Table = Table19
        Column154.Expression = ColumnExpression154
        SelectQuery19.Columns.Add(Column151)
        SelectQuery19.Columns.Add(Column152)
        SelectQuery19.Columns.Add(Column153)
        SelectQuery19.Columns.Add(Column154)
        SelectQuery19.Name = "PermissionPolicyUserUsers_PermissionPolicyRoleRoles"
        SelectQuery19.Tables.Add(Table19)
        ColumnExpression155.ColumnName = "Oid"
        Table20.Name = "PlantillasImpresion"
        ColumnExpression155.Table = Table20
        Column155.Expression = ColumnExpression155
        ColumnExpression156.ColumnName = "Nombre"
        ColumnExpression156.Table = Table20
        Column156.Expression = ColumnExpression156
        ColumnExpression157.ColumnName = "Tipo"
        ColumnExpression157.Table = Table20
        Column157.Expression = ColumnExpression157
        ColumnExpression158.ColumnName = "Datos"
        ColumnExpression158.Table = Table20
        Column158.Expression = ColumnExpression158
        ColumnExpression159.ColumnName = "OptimisticLockField"
        ColumnExpression159.Table = Table20
        Column159.Expression = ColumnExpression159
        ColumnExpression160.ColumnName = "GCRecord"
        ColumnExpression160.Table = Table20
        Column160.Expression = ColumnExpression160
        SelectQuery20.Columns.Add(Column155)
        SelectQuery20.Columns.Add(Column156)
        SelectQuery20.Columns.Add(Column157)
        SelectQuery20.Columns.Add(Column158)
        SelectQuery20.Columns.Add(Column159)
        SelectQuery20.Columns.Add(Column160)
        SelectQuery20.Name = "PlantillasImpresion"
        SelectQuery20.Tables.Add(Table20)
        ColumnExpression161.ColumnName = "Oid"
        Table21.Name = "Producción"
        ColumnExpression161.Table = Table21
        Column161.Expression = ColumnExpression161
        ColumnExpression162.ColumnName = "Usuario"
        ColumnExpression162.Table = Table21
        Column162.Expression = ColumnExpression162
        ColumnExpression163.ColumnName = "Maquina"
        ColumnExpression163.Table = Table21
        Column163.Expression = ColumnExpression163
        ColumnExpression164.ColumnName = "FechaProducion"
        ColumnExpression164.Table = Table21
        Column164.Expression = ColumnExpression164
        ColumnExpression165.ColumnName = "Estatus"
        ColumnExpression165.Table = Table21
        Column165.Expression = ColumnExpression165
        ColumnExpression166.ColumnName = "Lote"
        ColumnExpression166.Table = Table21
        Column166.Expression = ColumnExpression166
        ColumnExpression167.ColumnName = "NoParte"
        ColumnExpression167.Table = Table21
        Column167.Expression = ColumnExpression167
        ColumnExpression168.ColumnName = "Contenedor"
        ColumnExpression168.Table = Table21
        Column168.Expression = ColumnExpression168
        ColumnExpression169.ColumnName = "NoPiezas"
        ColumnExpression169.Table = Table21
        Column169.Expression = ColumnExpression169
        ColumnExpression170.ColumnName = "OptimisticLockField"
        ColumnExpression170.Table = Table21
        Column170.Expression = ColumnExpression170
        ColumnExpression171.ColumnName = "GCRecord"
        ColumnExpression171.Table = Table21
        Column171.Expression = ColumnExpression171
        ColumnExpression172.ColumnName = "Almacen"
        ColumnExpression172.Table = Table21
        Column172.Expression = ColumnExpression172
        ColumnExpression173.ColumnName = "FechaCaducidad"
        ColumnExpression173.Table = Table21
        Column173.Expression = ColumnExpression173
        ColumnExpression174.ColumnName = "LoteCliente"
        ColumnExpression174.Table = Table21
        Column174.Expression = ColumnExpression174
        ColumnExpression175.ColumnName = "NoContenedor"
        ColumnExpression175.Table = Table21
        Column175.Expression = ColumnExpression175
        ColumnExpression176.ColumnName = "DescripcionProducto"
        ColumnExpression176.Table = Table21
        Column176.Expression = ColumnExpression176
        ColumnExpression177.ColumnName = "ClaveUsuario"
        ColumnExpression177.Table = Table21
        Column177.Expression = ColumnExpression177
        ColumnExpression178.ColumnName = "Estado"
        ColumnExpression178.Table = Table21
        Column178.Expression = ColumnExpression178
        ColumnExpression179.ColumnName = "EstandarPack"
        ColumnExpression179.Table = Table21
        Column179.Expression = ColumnExpression179
        SelectQuery21.Columns.Add(Column161)
        SelectQuery21.Columns.Add(Column162)
        SelectQuery21.Columns.Add(Column163)
        SelectQuery21.Columns.Add(Column164)
        SelectQuery21.Columns.Add(Column165)
        SelectQuery21.Columns.Add(Column166)
        SelectQuery21.Columns.Add(Column167)
        SelectQuery21.Columns.Add(Column168)
        SelectQuery21.Columns.Add(Column169)
        SelectQuery21.Columns.Add(Column170)
        SelectQuery21.Columns.Add(Column171)
        SelectQuery21.Columns.Add(Column172)
        SelectQuery21.Columns.Add(Column173)
        SelectQuery21.Columns.Add(Column174)
        SelectQuery21.Columns.Add(Column175)
        SelectQuery21.Columns.Add(Column176)
        SelectQuery21.Columns.Add(Column177)
        SelectQuery21.Columns.Add(Column178)
        SelectQuery21.Columns.Add(Column179)
        SelectQuery21.Name = "Producción"
        SelectQuery21.Tables.Add(Table21)
        ColumnExpression180.ColumnName = "Oid"
        Table22.Name = "Productos"
        ColumnExpression180.Table = Table22
        Column180.Expression = ColumnExpression180
        ColumnExpression181.ColumnName = "Codigo"
        ColumnExpression181.Table = Table22
        Column181.Expression = ColumnExpression181
        ColumnExpression182.ColumnName = "Descripcion"
        ColumnExpression182.Table = Table22
        Column182.Expression = ColumnExpression182
        ColumnExpression183.ColumnName = "Cliente"
        ColumnExpression183.Table = Table22
        Column183.Expression = ColumnExpression183
        ColumnExpression184.ColumnName = "PesoMin"
        ColumnExpression184.Table = Table22
        Column184.Expression = ColumnExpression184
        ColumnExpression185.ColumnName = "PesoMax"
        ColumnExpression185.Table = Table22
        Column185.Expression = ColumnExpression185
        ColumnExpression186.ColumnName = "Imagen"
        ColumnExpression186.Table = Table22
        Column186.Expression = ColumnExpression186
        ColumnExpression187.ColumnName = "OptimisticLockField"
        ColumnExpression187.Table = Table22
        Column187.Expression = ColumnExpression187
        ColumnExpression188.ColumnName = "GCRecord"
        ColumnExpression188.Table = Table22
        Column188.Expression = ColumnExpression188
        SelectQuery22.Columns.Add(Column180)
        SelectQuery22.Columns.Add(Column181)
        SelectQuery22.Columns.Add(Column182)
        SelectQuery22.Columns.Add(Column183)
        SelectQuery22.Columns.Add(Column184)
        SelectQuery22.Columns.Add(Column185)
        SelectQuery22.Columns.Add(Column186)
        SelectQuery22.Columns.Add(Column187)
        SelectQuery22.Columns.Add(Column188)
        SelectQuery22.Name = "Productos"
        SelectQuery22.Tables.Add(Table22)
        ColumnExpression189.ColumnName = "Maquina"
        Table23.Name = "ProductosProducto_MaquinasMaquina"
        ColumnExpression189.Table = Table23
        Column189.Expression = ColumnExpression189
        ColumnExpression190.ColumnName = "Producto"
        ColumnExpression190.Table = Table23
        Column190.Expression = ColumnExpression190
        ColumnExpression191.ColumnName = "OID"
        ColumnExpression191.Table = Table23
        Column191.Expression = ColumnExpression191
        ColumnExpression192.ColumnName = "OptimisticLockField"
        ColumnExpression192.Table = Table23
        Column192.Expression = ColumnExpression192
        SelectQuery23.Columns.Add(Column189)
        SelectQuery23.Columns.Add(Column190)
        SelectQuery23.Columns.Add(Column191)
        SelectQuery23.Columns.Add(Column192)
        SelectQuery23.Name = "ProductosProducto_MaquinasMaquina"
        SelectQuery23.Tables.Add(Table23)
        ColumnExpression193.ColumnName = "NoParteNoContinuo"
        Table24.Name = "ProductosProductosNoContinuos_NoParteNoContinuosNoParteNoContinuo"
        ColumnExpression193.Table = Table24
        Column193.Expression = ColumnExpression193
        ColumnExpression194.ColumnName = "ProductosNoContinuos"
        ColumnExpression194.Table = Table24
        Column194.Expression = ColumnExpression194
        ColumnExpression195.ColumnName = "OID"
        ColumnExpression195.Table = Table24
        Column195.Expression = ColumnExpression195
        ColumnExpression196.ColumnName = "OptimisticLockField"
        ColumnExpression196.Table = Table24
        Column196.Expression = ColumnExpression196
        SelectQuery24.Columns.Add(Column193)
        SelectQuery24.Columns.Add(Column194)
        SelectQuery24.Columns.Add(Column195)
        SelectQuery24.Columns.Add(Column196)
        SelectQuery24.Name = "ProductosProductosNoContinuos_NoParteNoContinuosNoParteNoContinuo"
        SelectQuery24.Tables.Add(Table24)
        ColumnExpression197.ColumnName = "Oid"
        Table25.Name = "ReportDataV2"
        ColumnExpression197.Table = Table25
        Column197.Expression = ColumnExpression197
        ColumnExpression198.ColumnName = "ObjectTypeName"
        ColumnExpression198.Table = Table25
        Column198.Expression = ColumnExpression198
        ColumnExpression199.ColumnName = "Content"
        ColumnExpression199.Table = Table25
        Column199.Expression = ColumnExpression199
        ColumnExpression200.ColumnName = "Name"
        ColumnExpression200.Table = Table25
        Column200.Expression = ColumnExpression200
        ColumnExpression201.ColumnName = "ParametersObjectTypeName"
        ColumnExpression201.Table = Table25
        Column201.Expression = ColumnExpression201
        ColumnExpression202.ColumnName = "IsInplaceReport"
        ColumnExpression202.Table = Table25
        Column202.Expression = ColumnExpression202
        ColumnExpression203.ColumnName = "PredefinedReportType"
        ColumnExpression203.Table = Table25
        Column203.Expression = ColumnExpression203
        ColumnExpression204.ColumnName = "OptimisticLockField"
        ColumnExpression204.Table = Table25
        Column204.Expression = ColumnExpression204
        ColumnExpression205.ColumnName = "GCRecord"
        ColumnExpression205.Table = Table25
        Column205.Expression = ColumnExpression205
        SelectQuery25.Columns.Add(Column197)
        SelectQuery25.Columns.Add(Column198)
        SelectQuery25.Columns.Add(Column199)
        SelectQuery25.Columns.Add(Column200)
        SelectQuery25.Columns.Add(Column201)
        SelectQuery25.Columns.Add(Column202)
        SelectQuery25.Columns.Add(Column203)
        SelectQuery25.Columns.Add(Column204)
        SelectQuery25.Columns.Add(Column205)
        SelectQuery25.Name = "ReportDataV2"
        SelectQuery25.Tables.Add(Table25)
        ColumnExpression206.ColumnName = "Oid"
        Table26.Name = "Reproceso"
        ColumnExpression206.Table = Table26
        Column206.Expression = ColumnExpression206
        ColumnExpression207.ColumnName = "Nombre"
        ColumnExpression207.Table = Table26
        Column207.Expression = ColumnExpression207
        ColumnExpression208.ColumnName = "Descripcion"
        ColumnExpression208.Table = Table26
        Column208.Expression = ColumnExpression208
        ColumnExpression209.ColumnName = "Estatus"
        ColumnExpression209.Table = Table26
        Column209.Expression = ColumnExpression209
        ColumnExpression210.ColumnName = "OptimisticLockField"
        ColumnExpression210.Table = Table26
        Column210.Expression = ColumnExpression210
        ColumnExpression211.ColumnName = "GCRecord"
        ColumnExpression211.Table = Table26
        Column211.Expression = ColumnExpression211
        SelectQuery26.Columns.Add(Column206)
        SelectQuery26.Columns.Add(Column207)
        SelectQuery26.Columns.Add(Column208)
        SelectQuery26.Columns.Add(Column209)
        SelectQuery26.Columns.Add(Column210)
        SelectQuery26.Columns.Add(Column211)
        SelectQuery26.Name = "Reproceso"
        SelectQuery26.Tables.Add(Table26)
        ColumnExpression212.ColumnName = "Oid"
        Table27.Name = "SecuritySystemMemberPermissionsObject"
        ColumnExpression212.Table = Table27
        Column212.Expression = ColumnExpression212
        ColumnExpression213.ColumnName = "Members"
        ColumnExpression213.Table = Table27
        Column213.Expression = ColumnExpression213
        ColumnExpression214.ColumnName = "AllowRead"
        ColumnExpression214.Table = Table27
        Column214.Expression = ColumnExpression214
        ColumnExpression215.ColumnName = "AllowWrite"
        ColumnExpression215.Table = Table27
        Column215.Expression = ColumnExpression215
        ColumnExpression216.ColumnName = "Criteria"
        ColumnExpression216.Table = Table27
        Column216.Expression = ColumnExpression216
        ColumnExpression217.ColumnName = "Owner"
        ColumnExpression217.Table = Table27
        Column217.Expression = ColumnExpression217
        ColumnExpression218.ColumnName = "OptimisticLockField"
        ColumnExpression218.Table = Table27
        Column218.Expression = ColumnExpression218
        ColumnExpression219.ColumnName = "GCRecord"
        ColumnExpression219.Table = Table27
        Column219.Expression = ColumnExpression219
        SelectQuery27.Columns.Add(Column212)
        SelectQuery27.Columns.Add(Column213)
        SelectQuery27.Columns.Add(Column214)
        SelectQuery27.Columns.Add(Column215)
        SelectQuery27.Columns.Add(Column216)
        SelectQuery27.Columns.Add(Column217)
        SelectQuery27.Columns.Add(Column218)
        SelectQuery27.Columns.Add(Column219)
        SelectQuery27.Name = "SecuritySystemMemberPermissionsObject"
        SelectQuery27.Tables.Add(Table27)
        ColumnExpression220.ColumnName = "Oid"
        Table28.Name = "SecuritySystemObjectPermissionsObject"
        ColumnExpression220.Table = Table28
        Column220.Expression = ColumnExpression220
        ColumnExpression221.ColumnName = "Criteria"
        ColumnExpression221.Table = Table28
        Column221.Expression = ColumnExpression221
        ColumnExpression222.ColumnName = "AllowRead"
        ColumnExpression222.Table = Table28
        Column222.Expression = ColumnExpression222
        ColumnExpression223.ColumnName = "AllowWrite"
        ColumnExpression223.Table = Table28
        Column223.Expression = ColumnExpression223
        ColumnExpression224.ColumnName = "AllowDelete"
        ColumnExpression224.Table = Table28
        Column224.Expression = ColumnExpression224
        ColumnExpression225.ColumnName = "AllowNavigate"
        ColumnExpression225.Table = Table28
        Column225.Expression = ColumnExpression225
        ColumnExpression226.ColumnName = "Owner"
        ColumnExpression226.Table = Table28
        Column226.Expression = ColumnExpression226
        ColumnExpression227.ColumnName = "OptimisticLockField"
        ColumnExpression227.Table = Table28
        Column227.Expression = ColumnExpression227
        ColumnExpression228.ColumnName = "GCRecord"
        ColumnExpression228.Table = Table28
        Column228.Expression = ColumnExpression228
        SelectQuery28.Columns.Add(Column220)
        SelectQuery28.Columns.Add(Column221)
        SelectQuery28.Columns.Add(Column222)
        SelectQuery28.Columns.Add(Column223)
        SelectQuery28.Columns.Add(Column224)
        SelectQuery28.Columns.Add(Column225)
        SelectQuery28.Columns.Add(Column226)
        SelectQuery28.Columns.Add(Column227)
        SelectQuery28.Columns.Add(Column228)
        SelectQuery28.Name = "SecuritySystemObjectPermissionsObject"
        SelectQuery28.Tables.Add(Table28)
        ColumnExpression229.ColumnName = "Oid"
        Table29.Name = "SecuritySystemRole"
        ColumnExpression229.Table = Table29
        Column229.Expression = ColumnExpression229
        ColumnExpression230.ColumnName = "OptimisticLockField"
        ColumnExpression230.Table = Table29
        Column230.Expression = ColumnExpression230
        ColumnExpression231.ColumnName = "GCRecord"
        ColumnExpression231.Table = Table29
        Column231.Expression = ColumnExpression231
        ColumnExpression232.ColumnName = "ObjectType"
        ColumnExpression232.Table = Table29
        Column232.Expression = ColumnExpression232
        ColumnExpression233.ColumnName = "Name"
        ColumnExpression233.Table = Table29
        Column233.Expression = ColumnExpression233
        ColumnExpression234.ColumnName = "IsAdministrative"
        ColumnExpression234.Table = Table29
        Column234.Expression = ColumnExpression234
        ColumnExpression235.ColumnName = "CanEditModel"
        ColumnExpression235.Table = Table29
        Column235.Expression = ColumnExpression235
        SelectQuery29.Columns.Add(Column229)
        SelectQuery29.Columns.Add(Column230)
        SelectQuery29.Columns.Add(Column231)
        SelectQuery29.Columns.Add(Column232)
        SelectQuery29.Columns.Add(Column233)
        SelectQuery29.Columns.Add(Column234)
        SelectQuery29.Columns.Add(Column235)
        SelectQuery29.Name = "SecuritySystemRole"
        SelectQuery29.Tables.Add(Table29)
        ColumnExpression236.ColumnName = "ChildRoles"
        Table30.Name = "SecuritySystemRoleParentRoles_SecuritySystemRoleChildRoles"
        ColumnExpression236.Table = Table30
        Column236.Expression = ColumnExpression236
        ColumnExpression237.ColumnName = "ParentRoles"
        ColumnExpression237.Table = Table30
        Column237.Expression = ColumnExpression237
        ColumnExpression238.ColumnName = "OID"
        ColumnExpression238.Table = Table30
        Column238.Expression = ColumnExpression238
        ColumnExpression239.ColumnName = "OptimisticLockField"
        ColumnExpression239.Table = Table30
        Column239.Expression = ColumnExpression239
        SelectQuery30.Columns.Add(Column236)
        SelectQuery30.Columns.Add(Column237)
        SelectQuery30.Columns.Add(Column238)
        SelectQuery30.Columns.Add(Column239)
        SelectQuery30.Name = "SecuritySystemRoleParentRoles_SecuritySystemRoleChildRoles"
        SelectQuery30.Tables.Add(Table30)
        ColumnExpression240.ColumnName = "Oid"
        Table31.Name = "SecuritySystemTypePermissionsObject"
        ColumnExpression240.Table = Table31
        Column240.Expression = ColumnExpression240
        ColumnExpression241.ColumnName = "TargetType"
        ColumnExpression241.Table = Table31
        Column241.Expression = ColumnExpression241
        ColumnExpression242.ColumnName = "AllowRead"
        ColumnExpression242.Table = Table31
        Column242.Expression = ColumnExpression242
        ColumnExpression243.ColumnName = "AllowWrite"
        ColumnExpression243.Table = Table31
        Column243.Expression = ColumnExpression243
        ColumnExpression244.ColumnName = "AllowCreate"
        ColumnExpression244.Table = Table31
        Column244.Expression = ColumnExpression244
        ColumnExpression245.ColumnName = "AllowDelete"
        ColumnExpression245.Table = Table31
        Column245.Expression = ColumnExpression245
        ColumnExpression246.ColumnName = "AllowNavigate"
        ColumnExpression246.Table = Table31
        Column246.Expression = ColumnExpression246
        ColumnExpression247.ColumnName = "OptimisticLockField"
        ColumnExpression247.Table = Table31
        Column247.Expression = ColumnExpression247
        ColumnExpression248.ColumnName = "GCRecord"
        ColumnExpression248.Table = Table31
        Column248.Expression = ColumnExpression248
        ColumnExpression249.ColumnName = "ObjectType"
        ColumnExpression249.Table = Table31
        Column249.Expression = ColumnExpression249
        ColumnExpression250.ColumnName = "Owner"
        ColumnExpression250.Table = Table31
        Column250.Expression = ColumnExpression250
        SelectQuery31.Columns.Add(Column240)
        SelectQuery31.Columns.Add(Column241)
        SelectQuery31.Columns.Add(Column242)
        SelectQuery31.Columns.Add(Column243)
        SelectQuery31.Columns.Add(Column244)
        SelectQuery31.Columns.Add(Column245)
        SelectQuery31.Columns.Add(Column246)
        SelectQuery31.Columns.Add(Column247)
        SelectQuery31.Columns.Add(Column248)
        SelectQuery31.Columns.Add(Column249)
        SelectQuery31.Columns.Add(Column250)
        SelectQuery31.Name = "SecuritySystemTypePermissionsObject"
        SelectQuery31.Tables.Add(Table31)
        ColumnExpression251.ColumnName = "Oid"
        Table32.Name = "SecuritySystemUser"
        ColumnExpression251.Table = Table32
        Column251.Expression = ColumnExpression251
        ColumnExpression252.ColumnName = "StoredPassword"
        ColumnExpression252.Table = Table32
        Column252.Expression = ColumnExpression252
        ColumnExpression253.ColumnName = "ChangePasswordOnFirstLogon"
        ColumnExpression253.Table = Table32
        Column253.Expression = ColumnExpression253
        ColumnExpression254.ColumnName = "UserName"
        ColumnExpression254.Table = Table32
        Column254.Expression = ColumnExpression254
        ColumnExpression255.ColumnName = "IsActive"
        ColumnExpression255.Table = Table32
        Column255.Expression = ColumnExpression255
        ColumnExpression256.ColumnName = "OptimisticLockField"
        ColumnExpression256.Table = Table32
        Column256.Expression = ColumnExpression256
        ColumnExpression257.ColumnName = "GCRecord"
        ColumnExpression257.Table = Table32
        Column257.Expression = ColumnExpression257
        ColumnExpression258.ColumnName = "ObjectType"
        ColumnExpression258.Table = Table32
        Column258.Expression = ColumnExpression258
        SelectQuery32.Columns.Add(Column251)
        SelectQuery32.Columns.Add(Column252)
        SelectQuery32.Columns.Add(Column253)
        SelectQuery32.Columns.Add(Column254)
        SelectQuery32.Columns.Add(Column255)
        SelectQuery32.Columns.Add(Column256)
        SelectQuery32.Columns.Add(Column257)
        SelectQuery32.Columns.Add(Column258)
        SelectQuery32.Name = "SecuritySystemUser"
        SelectQuery32.Tables.Add(Table32)
        ColumnExpression259.ColumnName = "Roles"
        Table33.Name = "SecuritySystemUserUsers_SecuritySystemRoleRoles"
        ColumnExpression259.Table = Table33
        Column259.Expression = ColumnExpression259
        ColumnExpression260.ColumnName = "Users"
        ColumnExpression260.Table = Table33
        Column260.Expression = ColumnExpression260
        ColumnExpression261.ColumnName = "OID"
        ColumnExpression261.Table = Table33
        Column261.Expression = ColumnExpression261
        ColumnExpression262.ColumnName = "OptimisticLockField"
        ColumnExpression262.Table = Table33
        Column262.Expression = ColumnExpression262
        SelectQuery33.Columns.Add(Column259)
        SelectQuery33.Columns.Add(Column260)
        SelectQuery33.Columns.Add(Column261)
        SelectQuery33.Columns.Add(Column262)
        SelectQuery33.Name = "SecuritySystemUserUsers_SecuritySystemRoleRoles"
        SelectQuery33.Tables.Add(Table33)
        ColumnExpression263.ColumnName = "Oid"
        Table34.Name = "StandarPack"
        ColumnExpression263.Table = Table34
        Column263.Expression = ColumnExpression263
        ColumnExpression264.ColumnName = "NoParte"
        ColumnExpression264.Table = Table34
        Column264.Expression = ColumnExpression264
        ColumnExpression265.ColumnName = "CodContenedor"
        ColumnExpression265.Table = Table34
        Column265.Expression = ColumnExpression265
        ColumnExpression266.ColumnName = "Maquina"
        ColumnExpression266.Table = Table34
        Column266.Expression = ColumnExpression266
        ColumnExpression267.ColumnName = "NoPiezas"
        ColumnExpression267.Table = Table34
        Column267.Expression = ColumnExpression267
        ColumnExpression268.ColumnName = "RelacionConteendoresEntrada"
        ColumnExpression268.Table = Table34
        Column268.Expression = ColumnExpression268
        ColumnExpression269.ColumnName = "RelaciónContenedoresSalida"
        ColumnExpression269.Table = Table34
        Column269.Expression = ColumnExpression269
        ColumnExpression270.ColumnName = "OptimisticLockField"
        ColumnExpression270.Table = Table34
        Column270.Expression = ColumnExpression270
        ColumnExpression271.ColumnName = "GCRecord"
        ColumnExpression271.Table = Table34
        Column271.Expression = ColumnExpression271
        ColumnExpression272.ColumnName = "PesoContenedor"
        ColumnExpression272.Table = Table34
        Column272.Expression = ColumnExpression272
        SelectQuery34.Columns.Add(Column263)
        SelectQuery34.Columns.Add(Column264)
        SelectQuery34.Columns.Add(Column265)
        SelectQuery34.Columns.Add(Column266)
        SelectQuery34.Columns.Add(Column267)
        SelectQuery34.Columns.Add(Column268)
        SelectQuery34.Columns.Add(Column269)
        SelectQuery34.Columns.Add(Column270)
        SelectQuery34.Columns.Add(Column271)
        SelectQuery34.Columns.Add(Column272)
        SelectQuery34.Name = "StandarPack"
        SelectQuery34.Tables.Add(Table34)
        ColumnExpression273.ColumnName = "Oid"
        Table35.Name = "SystemObjects"
        ColumnExpression273.Table = Table35
        Column273.Expression = ColumnExpression273
        ColumnExpression274.ColumnName = "NoSerie"
        ColumnExpression274.Table = Table35
        Column274.Expression = ColumnExpression274
        ColumnExpression275.ColumnName = "FechaInicio"
        ColumnExpression275.Table = Table35
        Column275.Expression = ColumnExpression275
        ColumnExpression276.ColumnName = "FechaFin"
        ColumnExpression276.Table = Table35
        Column276.Expression = ColumnExpression276
        ColumnExpression277.ColumnName = "Dias"
        ColumnExpression277.Table = Table35
        Column277.Expression = ColumnExpression277
        ColumnExpression278.ColumnName = "Estatus"
        ColumnExpression278.Table = Table35
        Column278.Expression = ColumnExpression278
        ColumnExpression279.ColumnName = "OptimisticLockField"
        ColumnExpression279.Table = Table35
        Column279.Expression = ColumnExpression279
        ColumnExpression280.ColumnName = "GCRecord"
        ColumnExpression280.Table = Table35
        Column280.Expression = ColumnExpression280
        SelectQuery35.Columns.Add(Column273)
        SelectQuery35.Columns.Add(Column274)
        SelectQuery35.Columns.Add(Column275)
        SelectQuery35.Columns.Add(Column276)
        SelectQuery35.Columns.Add(Column277)
        SelectQuery35.Columns.Add(Column278)
        SelectQuery35.Columns.Add(Column279)
        SelectQuery35.Columns.Add(Column280)
        SelectQuery35.Name = "SystemObjects"
        SelectQuery35.Tables.Add(Table35)
        ColumnExpression281.ColumnName = "Oid"
        Table36.Name = "Usuario"
        ColumnExpression281.Table = Table36
        Column281.Expression = ColumnExpression281
        ColumnExpression282.ColumnName = "ClaveEmpleado"
        ColumnExpression282.Table = Table36
        Column282.Expression = ColumnExpression282
        ColumnExpression283.ColumnName = "Nombre"
        ColumnExpression283.Table = Table36
        Column283.Expression = ColumnExpression283
        ColumnExpression284.ColumnName = "EsAdminAPP"
        ColumnExpression284.Table = Table36
        Column284.Expression = ColumnExpression284
        SelectQuery36.Columns.Add(Column281)
        SelectQuery36.Columns.Add(Column282)
        SelectQuery36.Columns.Add(Column283)
        SelectQuery36.Columns.Add(Column284)
        SelectQuery36.Name = "Usuario"
        SelectQuery36.Tables.Add(Table36)
        ColumnExpression285.ColumnName = "OID"
        Table37.Name = "XPObjectType"
        ColumnExpression285.Table = Table37
        Column285.Expression = ColumnExpression285
        ColumnExpression286.ColumnName = "TypeName"
        ColumnExpression286.Table = Table37
        Column286.Expression = ColumnExpression286
        ColumnExpression287.ColumnName = "AssemblyName"
        ColumnExpression287.Table = Table37
        Column287.Expression = ColumnExpression287
        SelectQuery37.Columns.Add(Column285)
        SelectQuery37.Columns.Add(Column286)
        SelectQuery37.Columns.Add(Column287)
        SelectQuery37.Name = "XPObjectType"
        SelectQuery37.Tables.Add(Table37)
        Me.SqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery2, SelectQuery3, SelectQuery4, SelectQuery5, SelectQuery6, SelectQuery7, SelectQuery8, SelectQuery9, SelectQuery10, SelectQuery11, SelectQuery12, SelectQuery13, SelectQuery14, SelectQuery15, SelectQuery16, SelectQuery17, SelectQuery18, SelectQuery19, SelectQuery20, SelectQuery21, SelectQuery22, SelectQuery23, SelectQuery24, SelectQuery25, SelectQuery26, SelectQuery27, SelectQuery28, SelectQuery29, SelectQuery30, SelectQuery31, SelectQuery32, SelectQuery33, SelectQuery34, SelectQuery35, SelectQuery36, SelectQuery37})
        Me.SqlDataSource1.ResultSchemaSerializable = resources.GetString("SqlDataSource1.ResultSchemaSerializable")
        '
        'FuenteDatos2
        '
        Me.FuenteDatos2.DataSource = GetType(AppFin.ProduccionBase)
        Me.FuenteDatos2.Name = "FuenteDatos2"
        '
        'ReporteBase
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.DetailReport})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.SqlDataSource1, Me.SqlDataSource2, Me.FuenteDatos, Me.FuenteDatos2})
        Me.DataSource = Me.FuenteDatos2
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "17.1"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatos2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents SqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Public WithEvents FuenteDatos As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents FuenteDatos2 As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
End Class
