﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Control base para cualquier inteface visual basada en Zeus.
''' </summary>
''' <remarks>Este control contiene funciones comunes para cualquier control que implemente UX de Zeus. Todos los controles heredan finalmente de este control.</remarks>
Public Class Control
    Implements IControl


    ''' <summary>
    ''' Evento para indicar que el valor del control ha cambiado.
    ''' </summary>
    Public Event CambioValor As EventHandler

    ''' <summary>
    ''' Regresa la cultura UI del hilo actual de ejecución.
    ''' </summary>
    <System.ComponentModel.Browsable(False)> _
    Protected Shared ReadOnly Property Cultura() As String
        Get
            Return My.Application.UICulture.Name
        End Get
    End Property

    Public Sub LevantarCambioValor(ByVal sender As Object, ByVal e As EventArgs)

        RaiseEvent CambioValor(sender, e)

    End Sub

    ''' <summary>
    ''' Inicializa el control.
    ''' </summary>
    Public Sub InicializarControl() Implements IControl.InicializarControl

        'No hace nada, es responsabilidad del control heredado implementar código funcional

    End Sub

    '''' <summary>
    '''' Inicializa el control para un usuario particular.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza el control.</param>
    'Public Sub InicializarControl(ByVal UsuarioActual As Usuarios) Implements IControl.InicializarControl

    '    'No hace nada, es responsabilidad del control heredado implementar código funcional
    '    MiUsuarioActual = UsuarioActual

    'End Sub

End Class
