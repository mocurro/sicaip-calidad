﻿Imports System.ComponentModel
Imports System.IO

Public Class TarjetaLABDIMRegistro

    Private client As Datos.Defecto

    Private component As Datos.modelo

    Private MiRutaArchivo As String

    Private MiLabDimArchivo As Datos.LABDIMArchivo
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoLABDIMRegistro))
        TiposVentanasAfectadas.Add(GetType(CatalogoLABDIMSolicitud))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.LABDIMSolicitudCampos, DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.LABDIMSolicitudCampos.no_piezas , ItemNoPiezas )
            .Add(Datos.LABDIMSolicitudCampos.no_cotas , itemNoCotas )
            .Add(Datos.LABDIMSolicitudCampos.tipo_inspeccion , itemtipoinspeccion )
            .Add(Datos.LABDIMSolicitudCampos.comentarios, itemComentarios)
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.LABDIMSolicitudCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        '
        Select Case sender.Name
            Case SpinNoPiezas .Name  
                Paquete .no_piezas = CInt(SpinNoPiezas.Value)
            Case SpinNoCotas .Name 
                Paquete .no_cotas = CType(SpinNoCotas .Value, Integer?)
            Case txtComentarios .Name
                Paquete .comentarios =txtComentarios .Text .Trim 
            Case ComboComponente.Name
                If  ComboComponente.EditValue IsNot Nothing Then
                    Dim MiGUID As Long = CInt(ComboComponente.EditValue.ToString)
                    component = Datos.Fabricas.Modelo.ObtenerUno(MiGUID)
                    txtDescripcion.Text = component.descripcion
                    Paquete.cve_modelo = MiGUID
                    Paquete.descripcion = component.descripcion
                End If
                
                Return True

            Case ComboTipoinspeccion .Name 

                Paquete .tipo_inspeccion =ComboTipoinspeccion .Text 
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        'Cargar los combos
        With EntidadDe(Of Datos.LABDIMSolicitud)()
            FuenteModelos.DataSource = CatalogoDe(Of CatalogoLABDIMRegistro).Modelos  '.FindAll(Function(Item) Item )
        End With
        'Cargar los combos
        With EntidadDe(Of Datos.LABDIMSolicitud)()
            FuenteTipoInspeccion .DataSource = CatalogoDe(Of CatalogoLABDIMRegistro ).TiposInspección  '.FindAll(Function(Item) Item )
        End With

        OmitirCambiosPersonalizados = True
        If paquete.cve_usuario = 0 Then
            paquete.cve_usuario = Datos.SesiónLocal.Usuario.CVE_Usuario
            txtSolicitante.Text = Datos.SesiónLocal.Usuario.Nombre
        Else
            txtSolicitante.Text = Datos.SesiónLocal.Usuario.Nombre
        End If
        txtDescripcion .Text =paquete.descripcion 
        SpinNoPiezas .Value  =paquete.no_piezas 
        txtComentarios .Text =paquete .comentarios 
        If paquete .no_cotas .HasValue Then
            SpinNoCotas.Value = CDec(paquete .no_cotas)
        End If
        
        ComboComponente.EditValue = paquete.cve_modelo
        
        ComboTipoinspeccion.Text =paquete.tipo_inspeccion 
        ComboTipoinspeccion.EditValue =paquete.tipo_inspeccion 

        ComboTipoinspeccion.Text =String .Empty  
        ComboTipoinspeccion.EditValue =paquete.tipo_inspeccion
        
        OmitirCambiosPersonalizados = False

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        'Cargar los combos
        Dim paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        paquete.Fecha_Entrada = Date.Now
        paquete.Estatus = 1
        paquete.Fecha_Entrada = Date.Now
        paquete.fecha_prevista = Date.Now.AddDays(1)
        If paquete.no_cotas Is Nothing Then
            paquete.no_cotas = 1
        End If
        Dim Milista As New List(Of Datos.LABDIMArchivo )
        If MiLabDimArchivo IsNot Nothing Then
            Milista.Add (MiLabDimArchivo)
             With EntidadDe(Of Datos.LABDIMSolicitud)()
            CatalogoDe(Of CatalogoLABDIMSolicitud).Archivos = Milista
        End With
        End If
        
       
    End Sub

    Private Sub LookUpEdit1_MouseClick(sender As Object, e As MouseEventArgs) 
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        ' Test result.
        If result = Windows.Forms.DialogResult.OK Then

            ' Get the file name.
            MiRutaArchivo = OpenFileDialog1.FileName
            'combodiseño.Text = MiRutaArchivo

            MiLabDimArchivo = New Datos.LABDIMArchivo
            MiLabDimArchivo.Nombre = OpenFileDialog1.SafeFileName
            MiLabDimArchivo.Tipo = 0
            If File.Exists(MiRutaArchivo) Then
                Dim fs As New FileStream(MiRutaArchivo, FileMode.Open)
                Dim data(fs.Length) As Byte
                fs.Read(data, 0, Convert.ToInt32(fs.Length))
                MiLabDimArchivo.Archivo = data
                fs.Close()
            Else
                MessageBox.Show("El archivo seleccionado no existe, favor de verificarlo")
            End If

        End If
    End Sub

    Private Sub TarjetaLABDIMSolicitud_Closed(sender As Object, e As EventArgs) Handles Me.Closed

    End Sub
End Class