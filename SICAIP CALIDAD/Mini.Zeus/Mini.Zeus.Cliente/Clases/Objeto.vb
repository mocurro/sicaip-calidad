﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Objeto base del cual derivan todos los objetos de Zeus.
''' </summary>
''' <remarks>Esta clase incluye el atributo de contrato de datos (DataContract) para poder consumirlo en un WCF.</remarks>
<Runtime.Serialization.DataContract()> _
Public MustInherit Class Objeto
    Implements IDisposable
    Implements IObjeto
    Implements IEquatable(Of Objeto)

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' Free other state (managed objects).
            End If

            ' Free your own state (unmanaged objects).
            ' Set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    ''' <summary>
    ''' Contiene el Id que identifica el objeto.
    ''' </summary>
    <Runtime.Serialization.DataMember()>
    Private MiId As Guid


    ''' <summary>
    ''' Contiene el Id que identifica el objeto.
    ''' </summary>
    <Runtime.Serialization.DataMember()>
    Private MiIdTexto As Guid

    ''' <summary>
    ''' Crea un nuevo objeto Zeus.
    ''' </summary>
    ''' <remarks>Este constructor crea un objeto y le asigna un Id único.</remarks>
    Public Sub New()

        MiId = Guid.NewGuid()

    End Sub
    ''' <summary>
    '''  Regresa el Id que identifica el objeto.
    ''' </summary>
    Public Overridable ReadOnly Property Id() As System.Guid Implements IObjeto.Id
        Get
            Return MiId
        End Get
    End Property

    Public Overridable ReadOnly Property IdTexto() As String Implements IObjeto.IdTexto

    ''' <summary>
    ''' Regresa la representación en texto del Id del objeto.
    ''' </summary>
    Public Overrides Function ToString() As String

        Return Id.ToString()

    End Function

#Region " Lógica de igualdad "
    ''' <summary>
    ''' Determina si un objeto dado es igual a este objeto.
    ''' </summary>
    ''' <param name="other">El objeto con el cual se compara este objeto.</param>
    ''' <remarks>Dos objetos son iguales cuando sus Id son iguales.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Overloads Function Equals(ByVal other As Objeto) As Boolean Implements System.IEquatable(Of Objeto).Equals

        Return Id.Equals(other.Id)

    End Function

    ''' <summary>
    ''' Determina si un objeto dado (que herede de Objeto) es igual a este objeto.
    ''' </summary>
    ''' <param name="obj">El objeto (que hereda Objeto) con el cual se compara este objeto.</param>
    ''' <remarks>Dos objeteos son iguales cuando su Id es igual.</remarks>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando el parámetro 'obj' no hereda de Objeto.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Overrides Function Equals(ByVal obj As Object) As Boolean

        'Utilizar la lógica de la base cuando el objeto externo este vacio.
        If obj Is Nothing Then Return MyBase.Equals(obj)

        'Determinar si el objeto externo hereda de Objeto
        If Not TypeOf obj Is Objeto Then

            'Utiliza la lógca base para comparar
            Return MyBase.Equals(obj)

        Else

            'Regresa el resultado de la comparación
            Return Equals(DirectCast(obj, Objeto))

        End If

    End Function

    ''' <summary>
    ''' Regresa un código Hash para el objeto.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Overrides Function GetHashCode() As Integer

        Return Id.GetHashCode()

    End Function

    ''' <summary>
    ''' Operación binara para determinar si dos objetos son iguales.
    ''' </summary>
    Public Shared Operator =(ByVal obj1 As Objeto, ByVal obj2 As Objeto) As Boolean

        Return obj1.Equals(obj2)

    End Operator

    ''' <summary>
    ''' Operación binara para determinar si dos objetos son diferentes.
    ''' </summary>
    Public Shared Operator <>(ByVal obj1 As Objeto, ByVal obj2 As Objeto) As Boolean

        Return Not obj1.Equals(obj2)

    End Operator
#End Region

End Class