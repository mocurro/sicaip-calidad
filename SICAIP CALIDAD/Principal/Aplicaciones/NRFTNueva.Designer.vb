﻿Imports Mini.Zeus.Cliente.UX

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NRFTNueva
    Inherits FormaDialogo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NRFTNueva))
        Me.Raiz = New DevExpress.XtraLayout.LayoutControl()
        Me.EditRechazada = New DevExpress.XtraEditors.SpinEdit()
        Me.EditCantidadEncontrada = New DevExpress.XtraEditors.SpinEdit()
        Me.ComboArea = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosArea = New System.Windows.Forms.BindingSource(Me.components)
        Me.RutaArchivo = New System.Windows.Forms.Label()
        Me.ComboSeccion = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosSeccion = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboParte = New DevExpress.XtraEditors.LookUpEdit()
        Me.DatosNoParte = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnReportar = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckUnidadNegocio = New DevExpress.XtraEditors.CheckEdit()
        Me.btnSelecArchivo = New DevExpress.XtraEditors.SimpleButton()
        Me.linkRemover = New DevExpress.XtraEditors.HyperlinkLabelControl()
        Me.editDetalle = New DevExpress.XtraEditors.MemoEdit()
        Me.txtDescripciónDefecto = New DevExpress.XtraEditors.TextEdit()
        Me.txtEstampada = New DevExpress.XtraEditors.TextEdit()
        Me.txtUnidadNegocio = New DevExpress.XtraEditors.TextEdit()
        Me.txtCliente = New DevExpress.XtraEditors.TextEdit()
        Me.txtModelo = New DevExpress.XtraEditors.TextEdit()
        Me.txtComponente = New DevExpress.XtraEditors.TextEdit()
        Me.EtiquetaUsuario = New DevExpress.XtraEditors.LabelControl()
        Me.EditAtiende = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemNoPARTE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.itemSeccion = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemArea = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemEncontrada = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemRechazada = New DevExpress.XtraLayout.LayoutControlItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Raiz.SuspendLayout()
        CType(Me.EditRechazada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EditCantidadEncontrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboSeccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosSeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboParte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosNoParte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.editDetalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripciónDefecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstampada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComponente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EditAtiende.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemNoPARTE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemSeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemEncontrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemRechazada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Raiz
        '
        Me.Raiz.Controls.Add(Me.EditRechazada)
        Me.Raiz.Controls.Add(Me.EditCantidadEncontrada)
        Me.Raiz.Controls.Add(Me.ComboArea)
        Me.Raiz.Controls.Add(Me.RutaArchivo)
        Me.Raiz.Controls.Add(Me.ComboSeccion)
        Me.Raiz.Controls.Add(Me.ComboParte)
        Me.Raiz.Controls.Add(Me.btnReportar)
        Me.Raiz.Controls.Add(Me.CheckUnidadNegocio)
        Me.Raiz.Controls.Add(Me.btnSelecArchivo)
        Me.Raiz.Controls.Add(Me.linkRemover)
        Me.Raiz.Controls.Add(Me.editDetalle)
        Me.Raiz.Controls.Add(Me.txtDescripciónDefecto)
        Me.Raiz.Controls.Add(Me.txtEstampada)
        Me.Raiz.Controls.Add(Me.txtUnidadNegocio)
        Me.Raiz.Controls.Add(Me.txtCliente)
        Me.Raiz.Controls.Add(Me.txtModelo)
        Me.Raiz.Controls.Add(Me.txtComponente)
        Me.Raiz.Controls.Add(Me.EtiquetaUsuario)
        Me.Raiz.Controls.Add(Me.EditAtiende)
        Me.Raiz.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Raiz.Location = New System.Drawing.Point(0, 0)
        Me.Raiz.Name = "Raiz"
        Me.Raiz.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(935, 217, 450, 400)
        Me.Raiz.Root = Me.LayoutControlGroup1
        Me.Raiz.Size = New System.Drawing.Size(754, 560)
        Me.Raiz.TabIndex = 0
        Me.Raiz.Text = "LayoutControl1"
        '
        'EditRechazada
        '
        Me.EditRechazada.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.EditRechazada.Location = New System.Drawing.Point(598, 418)
        Me.EditRechazada.Name = "EditRechazada"
        Me.EditRechazada.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.EditRechazada.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.EditRechazada.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.EditRechazada.Properties.NullText = "1"
        Me.EditRechazada.Size = New System.Drawing.Size(144, 20)
        Me.EditRechazada.StyleController = Me.Raiz
        Me.EditRechazada.TabIndex = 27
        '
        'EditCantidadEncontrada
        '
        Me.EditCantidadEncontrada.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.EditCantidadEncontrada.Location = New System.Drawing.Point(598, 378)
        Me.EditCantidadEncontrada.Name = "EditCantidadEncontrada"
        Me.EditCantidadEncontrada.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.EditCantidadEncontrada.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.EditCantidadEncontrada.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.EditCantidadEncontrada.Properties.NullText = "1"
        Me.EditCantidadEncontrada.Size = New System.Drawing.Size(144, 20)
        Me.EditCantidadEncontrada.StyleController = Me.Raiz
        Me.EditCantidadEncontrada.TabIndex = 26
        '
        'ComboArea
        '
        Me.ComboArea.Location = New System.Drawing.Point(153, 338)
        Me.ComboArea.Name = "ComboArea"
        Me.ComboArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboArea.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Sub área")})
        Me.ComboArea.Properties.DataSource = Me.DatosArea
        Me.ComboArea.Properties.DisplayMember = "descripcion"
        Me.ComboArea.Properties.NullText = "Seleccione sub área"
        Me.ComboArea.Properties.ValueMember = "cve_detalle"
        Me.ComboArea.Size = New System.Drawing.Size(589, 20)
        Me.ComboArea.StyleController = Me.Raiz
        Me.ComboArea.TabIndex = 25
        '
        'RutaArchivo
        '
        Me.RutaArchivo.Location = New System.Drawing.Point(105, 491)
        Me.RutaArchivo.Name = "RutaArchivo"
        Me.RutaArchivo.Size = New System.Drawing.Size(197, 57)
        Me.RutaArchivo.TabIndex = 24
        '
        'ComboSeccion
        '
        Me.ComboSeccion.Enabled = False
        Me.ComboSeccion.Location = New System.Drawing.Point(153, 290)
        Me.ComboSeccion.Name = "ComboSeccion"
        Me.ComboSeccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboSeccion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("nombre", "Defecto")})
        Me.ComboSeccion.Properties.DataSource = Me.DatosSeccion
        Me.ComboSeccion.Properties.DisplayMember = "nombre"
        Me.ComboSeccion.Properties.NullText = "Seleccione defecto"
        Me.ComboSeccion.Properties.ValueMember = "cve_defecto"
        Me.ComboSeccion.Size = New System.Drawing.Size(589, 20)
        Me.ComboSeccion.StyleController = Me.Raiz
        Me.ComboSeccion.TabIndex = 23
        '
        'ComboParte
        '
        Me.ComboParte.Location = New System.Drawing.Point(153, 39)
        Me.ComboParte.Name = "ComboParte"
        Me.ComboParte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboParte.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("np_gkn", "Número de parte")})
        Me.ComboParte.Properties.DataSource = Me.DatosNoParte
        Me.ComboParte.Properties.DisplayMember = "np_gkn"
        Me.ComboParte.Properties.NullText = "Seleccione un elemento"
        Me.ComboParte.Properties.ValueMember = "cve_modelo"
        Me.ComboParte.Size = New System.Drawing.Size(589, 20)
        Me.ComboParte.StyleController = Me.Raiz
        Me.ComboParte.TabIndex = 22
        '
        'btnReportar
        '
        Me.btnReportar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportar.Appearance.Options.UseFont = True
        Me.btnReportar.Enabled = False
        Me.btnReportar.Location = New System.Drawing.Point(598, 489)
        Me.btnReportar.Name = "btnReportar"
        Me.btnReportar.Size = New System.Drawing.Size(144, 26)
        Me.btnReportar.StyleController = Me.Raiz
        Me.btnReportar.TabIndex = 21
        Me.btnReportar.Text = "Reportar incidencia"
        '
        'CheckUnidadNegocio
        '
        Me.CheckUnidadNegocio.Location = New System.Drawing.Point(306, 491)
        Me.CheckUnidadNegocio.Name = "CheckUnidadNegocio"
        Me.CheckUnidadNegocio.Properties.Caption = "Copiar a mi unidad de negocio"
        Me.CheckUnidadNegocio.Size = New System.Drawing.Size(266, 19)
        Me.CheckUnidadNegocio.StyleController = Me.Raiz
        Me.CheckUnidadNegocio.TabIndex = 18
        '
        'btnSelecArchivo
        '
        Me.btnSelecArchivo.Location = New System.Drawing.Point(12, 491)
        Me.btnSelecArchivo.Name = "btnSelecArchivo"
        Me.btnSelecArchivo.Size = New System.Drawing.Size(89, 22)
        Me.btnSelecArchivo.StyleController = Me.Raiz
        Me.btnSelecArchivo.TabIndex = 17
        Me.btnSelecArchivo.Text = "Adjuntar archivo"
        '
        'linkRemover
        '
        Me.linkRemover.Cursor = System.Windows.Forms.Cursors.Hand
        Me.linkRemover.Location = New System.Drawing.Point(12, 535)
        Me.linkRemover.Name = "linkRemover"
        Me.linkRemover.Size = New System.Drawing.Size(82, 13)
        Me.linkRemover.StyleController = Me.Raiz
        Me.linkRemover.TabIndex = 16
        Me.linkRemover.Text = "Remover Archivo"
        '
        'editDetalle
        '
        Me.editDetalle.Location = New System.Drawing.Point(153, 362)
        Me.editDetalle.Name = "editDetalle"
        Me.editDetalle.Size = New System.Drawing.Size(419, 125)
        Me.editDetalle.StyleController = Me.Raiz
        Me.editDetalle.TabIndex = 15
        '
        'txtDescripciónDefecto
        '
        Me.txtDescripciónDefecto.Enabled = False
        Me.txtDescripciónDefecto.Location = New System.Drawing.Point(153, 314)
        Me.txtDescripciónDefecto.Name = "txtDescripciónDefecto"
        Me.txtDescripciónDefecto.Size = New System.Drawing.Size(589, 20)
        Me.txtDescripciónDefecto.StyleController = Me.Raiz
        Me.txtDescripciónDefecto.TabIndex = 13
        '
        'txtEstampada
        '
        Me.txtEstampada.Location = New System.Drawing.Point(153, 266)
        Me.txtEstampada.Name = "txtEstampada"
        Me.txtEstampada.Size = New System.Drawing.Size(589, 20)
        Me.txtEstampada.StyleController = Me.Raiz
        Me.txtEstampada.TabIndex = 11
        '
        'txtUnidadNegocio
        '
        Me.txtUnidadNegocio.Enabled = False
        Me.txtUnidadNegocio.Location = New System.Drawing.Point(153, 153)
        Me.txtUnidadNegocio.Name = "txtUnidadNegocio"
        Me.txtUnidadNegocio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnidadNegocio.Properties.Appearance.Options.UseFont = True
        Me.txtUnidadNegocio.Size = New System.Drawing.Size(589, 26)
        Me.txtUnidadNegocio.StyleController = Me.Raiz
        Me.txtUnidadNegocio.TabIndex = 9
        '
        'txtCliente
        '
        Me.txtCliente.Enabled = False
        Me.txtCliente.Location = New System.Drawing.Point(153, 123)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.Properties.Appearance.Options.UseFont = True
        Me.txtCliente.Size = New System.Drawing.Size(589, 26)
        Me.txtCliente.StyleController = Me.Raiz
        Me.txtCliente.TabIndex = 8
        '
        'txtModelo
        '
        Me.txtModelo.Enabled = False
        Me.txtModelo.Location = New System.Drawing.Point(153, 93)
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModelo.Properties.Appearance.Options.UseFont = True
        Me.txtModelo.Size = New System.Drawing.Size(589, 26)
        Me.txtModelo.StyleController = Me.Raiz
        Me.txtModelo.TabIndex = 7
        '
        'txtComponente
        '
        Me.txtComponente.Enabled = False
        Me.txtComponente.Location = New System.Drawing.Point(153, 63)
        Me.txtComponente.Name = "txtComponente"
        Me.txtComponente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComponente.Properties.Appearance.Options.UseFont = True
        Me.txtComponente.Size = New System.Drawing.Size(589, 26)
        Me.txtComponente.StyleController = Me.Raiz
        Me.txtComponente.TabIndex = 6
        '
        'EtiquetaUsuario
        '
        Me.EtiquetaUsuario.Location = New System.Drawing.Point(12, 12)
        Me.EtiquetaUsuario.Name = "EtiquetaUsuario"
        Me.EtiquetaUsuario.Size = New System.Drawing.Size(36, 13)
        Me.EtiquetaUsuario.StyleController = Me.Raiz
        Me.EtiquetaUsuario.TabIndex = 4
        Me.EtiquetaUsuario.Text = "Usuario"
        '
        'EditAtiende
        '
        Me.EditAtiende.Enabled = False
        Me.EditAtiende.Location = New System.Drawing.Point(153, 183)
        Me.EditAtiende.Name = "EditAtiende"
        Me.EditAtiende.Size = New System.Drawing.Size(589, 79)
        Me.EditAtiende.StyleController = Me.Raiz
        Me.EditAtiende.TabIndex = 10
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem1, Me.EmptySpaceItem3, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.EmptySpaceItem4, Me.LayoutControlItem12, Me.EmptySpaceItem5, Me.LayoutControlItem11, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.EmptySpaceItem7, Me.EmptySpaceItem8, Me.LayoutControlItem17, Me.ItemNoPARTE, Me.itemSeccion, Me.LayoutControlItem2, Me.ItemArea, Me.ItemEncontrada, Me.ItemRechazada})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(754, 560)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 505)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(93, 18)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(40, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(694, 17)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.EtiquetaUsuario
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(40, 17)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 17)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(734, 10)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtComponente
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 51)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem3.Text = "Componente"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtModelo
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 81)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem4.Text = "Modelo"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtCliente
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 111)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem5.Text = "Cliente"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtUnidadNegocio
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 141)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(734, 30)
        Me.LayoutControlItem6.Text = "Unidad de negocio"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.EditAtiende
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 171)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(734, 83)
        Me.LayoutControlItem7.Text = "Atiende"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txtEstampada
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 254)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem8.Text = "Estampado /  Cargo / Colada"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtDescripciónDefecto
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 302)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(734, 24)
        Me.LayoutControlItem10.Text = "Atributo"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(138, 13)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(586, 507)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(148, 33)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.editDetalle
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 350)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(564, 129)
        Me.LayoutControlItem12.Text = "Detalles"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(138, 13)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(294, 502)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(270, 38)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.linkRemover
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 523)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(93, 17)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.btnSelecArchivo
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 479)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(93, 26)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.CheckUnidadNegocio
        Me.LayoutControlItem14.Location = New System.Drawing.Point(294, 479)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(270, 23)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(564, 350)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(22, 190)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = False
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(586, 430)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(148, 47)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.btnReportar
        Me.LayoutControlItem17.Location = New System.Drawing.Point(586, 477)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(148, 30)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'ItemNoPARTE
        '
        Me.ItemNoPARTE.Control = Me.ComboParte
        Me.ItemNoPARTE.Location = New System.Drawing.Point(0, 27)
        Me.ItemNoPARTE.Name = "ItemNoPARTE"
        Me.ItemNoPARTE.Size = New System.Drawing.Size(734, 24)
        Me.ItemNoPARTE.Text = "Número de parte"
        Me.ItemNoPARTE.TextSize = New System.Drawing.Size(138, 13)
        '
        'itemSeccion
        '
        Me.itemSeccion.Control = Me.ComboSeccion
        Me.itemSeccion.Location = New System.Drawing.Point(0, 278)
        Me.itemSeccion.Name = "itemSeccion"
        Me.itemSeccion.Size = New System.Drawing.Size(734, 24)
        Me.itemSeccion.Text = "Sección"
        Me.itemSeccion.TextSize = New System.Drawing.Size(138, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.RutaArchivo
        Me.LayoutControlItem2.Location = New System.Drawing.Point(93, 479)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(201, 61)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'ItemArea
        '
        Me.ItemArea.Control = Me.ComboArea
        Me.ItemArea.Location = New System.Drawing.Point(0, 326)
        Me.ItemArea.Name = "ItemArea"
        Me.ItemArea.Size = New System.Drawing.Size(734, 24)
        Me.ItemArea.Text = "Sub Área"
        Me.ItemArea.TextSize = New System.Drawing.Size(138, 13)
        '
        'ItemEncontrada
        '
        Me.ItemEncontrada.Control = Me.EditCantidadEncontrada
        Me.ItemEncontrada.Location = New System.Drawing.Point(586, 350)
        Me.ItemEncontrada.Name = "ItemEncontrada"
        Me.ItemEncontrada.Size = New System.Drawing.Size(148, 40)
        Me.ItemEncontrada.Text = "Cantidad encontrada"
        Me.ItemEncontrada.TextLocation = DevExpress.Utils.Locations.Top
        Me.ItemEncontrada.TextSize = New System.Drawing.Size(138, 13)
        '
        'ItemRechazada
        '
        Me.ItemRechazada.Control = Me.EditRechazada
        Me.ItemRechazada.Location = New System.Drawing.Point(586, 390)
        Me.ItemRechazada.Name = "ItemRechazada"
        Me.ItemRechazada.Size = New System.Drawing.Size(148, 40)
        Me.ItemRechazada.Text = "Cantidad rechazada"
        Me.ItemRechazada.TextLocation = DevExpress.Utils.Locations.Top
        Me.ItemRechazada.TextSize = New System.Drawing.Size(138, 13)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'NRFTNueva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 560)
        Me.Controls.Add(Me.Raiz)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NRFTNueva"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NRFTNueva"
        CType(Me.Raiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Raiz.ResumeLayout(False)
        CType(Me.EditRechazada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EditCantidadEncontrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboSeccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosSeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboParte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosNoParte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.editDetalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripciónDefecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstampada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnidadNegocio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EditAtiende.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemNoPARTE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemSeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEncontrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemRechazada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Raiz As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents btnReportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckUnidadNegocio As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnSelecArchivo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents linkRemover As DevExpress.XtraEditors.HyperlinkLabelControl
    Friend WithEvents editDetalle As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtDescripciónDefecto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEstampada As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUnidadNegocio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCliente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtModelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtComponente As DevExpress.XtraEditors.TextEdit
    Friend WithEvents EtiquetaUsuario As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EditAtiende As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ComboParte As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ItemNoPARTE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DatosNoParte As BindingSource
    Friend WithEvents ComboSeccion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents itemSeccion As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DatosSeccion As BindingSource
    Friend WithEvents RutaArchivo As Label
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents ComboArea As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ItemArea As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DatosArea As BindingSource
    Friend WithEvents EditCantidadEncontrada As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ItemEncontrada As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EditRechazada As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ItemRechazada As DevExpress.XtraLayout.LayoutControlItem
End Class
