﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Modulo que ofrece extensiones generales de la capa Zeus.Cliente.Windows.UX.
''' </summary>
''' <remarks>Este módulo contiene funciones y propiedades relacionadas al dominio de UX, incluyendo extensiones formales de controles de DevExpress.</remarks>
<Microsoft.VisualBasic.HideModuleName()> _
Public Module Extensiones

    ''' <summary>
    ''' Manipula la visibilidad del control.
    ''' </summary>
    ''' <param name="Control">Control a manipular.</param>
    ''' <param name="Visible">Determina si el control está visible o no.</param>
    <System.Runtime.CompilerServices.Extension()> _
    Sub Visibilidad(ByVal Control As DevExpress.XtraBars.BarButtonItem, ByVal Visible As Boolean)

        If Visible Then
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

    End Sub

    ''' <summary>
    ''' Manipula la visibilidad del control.
    ''' </summary>
    ''' <param name="Control">Control a manipular.</param>
    ''' <param name="Visible">Determina si el control está visible o no.</param>
    <System.Runtime.CompilerServices.Extension()> _
    Sub Visibilidad(ByVal Control As DevExpress.XtraBars.BarStaticItem, ByVal Visible As Boolean)

        If Visible Then
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

    End Sub

    ''' <summary>
    ''' Manipula la visibilidad del control.
    ''' </summary>
    ''' <param name="Control">Control a manipular.</param>
    ''' <param name="Visible">Determina si el control está visible o no.</param>
    <System.Runtime.CompilerServices.Extension()> _
    Sub Visibilidad(ByVal Control As DevExpress.XtraBars.BarSubItem, ByVal Visible As Boolean)

        If Visible Then
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            Control.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

    End Sub

    ''' <summary>
    ''' Manipula la visiblidad del control.
    ''' </summary>
    ''' <param name="Control">Control a manipular.</param>
    ''' <param name="Visible">Determina si el control está visible o no.</param>
    <System.Runtime.CompilerServices.Extension()> _
    Sub Visibilidad(ByVal Control As DevExpress.XtraLayout.LayoutControlItem, ByVal Visible As Boolean)

        If Visible Then
            Control.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        Else
            Control.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        End If

    End Sub

End Module
