﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas tarjeta de Zeus.
''' </summary>
''' <remarks>Todas las ventanas tarjeta basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface ITarjeta

    ''' <summary>
    ''' Abre la tarjeta.
    ''' </summary>
    ''' <param name="Entidad">La entidad que se editará con la tarjeta.</param>
    ''' <param name="owner">Ventana padre.</param>
    Function MostrarVentana(ByVal Entidad As IEntidad, ByVal owner As IWin32Window) As DialogResult

    '''' <summary>
    '''' Abre la tarjeta para un usuario particular.
    '''' </summary>
    '''' <param name="Entidad">La entidad que se editará con la tarjeta.</param>
    '''' <param name="UsuarioActual">El usuario que utiliza la tarjeta.</param>
    '''' <param name="owner">Ventana padre.</param>
    'Function MostrarVentana(ByVal Entidad As IEntidad, ByVal UsuarioActual As Usuarios, ByVal owner As IWin32Window) As DialogResult

    ''' <summary>
    ''' Contiene la entidad que se edita con esta tarjeta.
    ''' </summary>
    Property Entidad() As IEntidad

    ''' <summary>
    ''' Regresa la entidad que se edita en esta tarjeta.
    ''' </summary>
    ''' <typeparam name="TEntidad">El tipo de entidad.</typeparam>
    ''' <remarks>Esta función regresa el mismo objeto que la propiedad Entidad, pero convirtiendo la entidad en un tipo específico.</remarks>
    Function EntidadDe(Of TEntidad)() As TEntidad

    ''' <summary>
    ''' Contiene el catalogo que ejecuta esta tarjeta.
    ''' </summary>
    Property Catalogo() As ICatalogo

    ''' <summary>
    ''' Regresa el catálogo que mandó llamar esta tarjeta.
    ''' </summary>
    ''' <typeparam name="TCatalogo">El tipo del catálgoo.</typeparam>
    ''' <remarks>Esta función regresa el objeto que original mente se determinó como ventana padre, pero convirtiendolo al tipo de catálogo específico.</remarks>
    Function CatalogoDe(Of TCatalogo)() As TCatalogo

    ''' <summary>
    ''' Contiene la referencia a la ventana principal
    ''' </summary>
    Property VentanaPrincipal() As IFormaPrincipal

    ''' <summary>
    ''' Regresa la lista de los tipos de ventanas que se ven afectadas por cambios en esta tarjeta.
    ''' </summary>
    ReadOnly Property VentanasAfectadas() As List(Of Type)

    ''' <summary>
    ''' Abre la tarjeta.
    ''' </summary>
    Function MostrarInformacion(ByVal Entidad As IEntidad, ByVal owner As IWin32Window)

End Interface