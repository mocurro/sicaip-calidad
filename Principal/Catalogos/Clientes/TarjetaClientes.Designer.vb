﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaCliente
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.TextoNombre = New DevExpress.XtraEditors.TextEdit()
        Me.itemNombre = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TextoDescripcion = New DevExpress.XtraEditors.TextEdit()
        Me.itemDescripcion = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ComboEstatus = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ItemEstatus = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ManejadorValidacion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutTarjeta,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutTarjeta.SuspendLayout
        CType(Me.LayoutTarjetaRaiz,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextoNombre.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemNombre,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextoDescripcion.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.itemDescripcion,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboEstatus.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ItemEstatus,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.ComboEstatus)
        Me.LayoutTarjeta.Controls.Add(Me.TextoDescripcion)
        Me.LayoutTarjeta.Controls.Add(Me.TextoNombre)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 104)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.itemNombre, Me.itemDescripcion, Me.ItemEstatus})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 104)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 72)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 12)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'TextoNombre
        '
        Me.TextoNombre.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.FuenteDatos, "Nombre", True))
        Me.TextoNombre.Location = New System.Drawing.Point(70, 12)
        Me.TextoNombre.Name = "TextoNombre"
        Me.TextoNombre.Size = New System.Drawing.Size(438, 20)
        Me.TextoNombre.StyleController = Me.LayoutTarjeta
        Me.TextoNombre.TabIndex = 4
        '
        'itemNombre
        '
        Me.itemNombre.Control = Me.TextoNombre
        Me.itemNombre.Location = New System.Drawing.Point(0, 0)
        Me.itemNombre.Name = "itemNombre"
        Me.itemNombre.Size = New System.Drawing.Size(500, 24)
        Me.itemNombre.Text = "Nombre"
        Me.itemNombre.TextSize = New System.Drawing.Size(54, 13)
        '
        'TextoDescripcion
        '
        Me.TextoDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.FuenteDatos, "Descripcion", True))
        Me.TextoDescripcion.Location = New System.Drawing.Point(70, 36)
        Me.TextoDescripcion.Name = "TextoDescripcion"
        Me.TextoDescripcion.Size = New System.Drawing.Size(438, 20)
        Me.TextoDescripcion.StyleController = Me.LayoutTarjeta
        Me.TextoDescripcion.TabIndex = 5
        '
        'itemDescripcion
        '
        Me.itemDescripcion.Control = Me.TextoDescripcion
        Me.itemDescripcion.Location = New System.Drawing.Point(0, 24)
        Me.itemDescripcion.Name = "itemDescripcion"
        Me.itemDescripcion.Size = New System.Drawing.Size(500, 24)
        Me.itemDescripcion.Text = "Descripción"
        Me.itemDescripcion.TextSize = New System.Drawing.Size(54, 13)
        '
        'ComboEstatus
        '
        Me.ComboEstatus.Location = New System.Drawing.Point(70, 60)
        Me.ComboEstatus.Name = "ComboEstatus"
        Me.ComboEstatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboEstatus.Properties.Items.AddRange(New Object() {"Activo", "Inactivo"})
        Me.ComboEstatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboEstatus.Size = New System.Drawing.Size(438, 20)
        Me.ComboEstatus.StyleController = Me.LayoutTarjeta
        Me.ComboEstatus.TabIndex = 6
        '
        'ItemEstatus
        '
        Me.ItemEstatus.Control = Me.ComboEstatus
        Me.ItemEstatus.Location = New System.Drawing.Point(0, 48)
        Me.ItemEstatus.Name = "ItemEstatus"
        Me.ItemEstatus.Size = New System.Drawing.Size(500, 24)
        Me.ItemEstatus.Text = "Estatus"
        Me.ItemEstatus.TextSize = New System.Drawing.Size(54, 13)
        '
        'TarjetaCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 230)
        Me.Name = "TarjetaCliente"
        Me.Text = "TarjetaClientes"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextoNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemNombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextoDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboEstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemEstatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextoNombre As TextEdit
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents itemNombre As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ComboEstatus As ComboBoxEdit
    Friend WithEvents TextoDescripcion As TextEdit
    Friend WithEvents itemDescripcion As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemEstatus As DevExpress.XtraLayout.LayoutControlItem
End Class
