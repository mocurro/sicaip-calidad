﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Atributo para definir el texto de una clase, método o propiedad.
''' </summary>
''' <remarks>Se puede aplicar este atributo más de una vez. Cada instancia define el texto para diferentes culturas y el género.</remarks>
<AttributeUsage(AttributeTargets.Class Or AttributeTargets.Property, AllowMultiple:=True, Inherited:=False)> _
Public NotInheritable Class TextoAttribute
    Inherits Attribute

    ''' <summary>
    ''' Contiene el código de la cultura al cual pertenece este texto.
    ''' </summary>
    Private MiCultura As String

    ''' <summary>
    ''' Define el género del texto.
    ''' </summary>
    Private MiGenero As Generos

    ''' <summary>
    ''' Contiene el texto en singular.
    ''' </summary>
    Private MiTextoSingular As String

    ''' <summary>
    ''' Contiene el texto en plural.
    ''' </summary>
    Private MiTextoPlural As String

    ''' <summary>
    ''' Crea un texto en singular sin género.
    ''' </summary>
    ''' <param name="Cultura">Indica la cultura a la cual pertenece el texto.</param>
    ''' <param name="TextoSingular">El texto en singular.</param>
    ''' <remarks>Este constructor define el texto en plural como vacio (String.Empty) y fija el género como "Indefinido".</remarks>
    Public Sub New(ByVal Cultura As String, ByVal TextoSingular As String)

        Me.New(Cultura, TextoSingular, String.Empty, Generos.Indefinido)

    End Sub

    ''' <summary>
    ''' Crea un texto en singular y plural sin género.
    ''' </summary>
    ''' <param name="Cultura">Indica la cultura a la cual pertenece el texto.</param>
    ''' <param name="TextoSingular">El texto en singular.</param>
    ''' <param name="TextoPlural">El texto en plural.</param>
    ''' <remarks>Este constructor fija el género como "Indefinido".</remarks>
    Public Sub New(ByVal Cultura As String, ByVal TextoSingular As String, ByVal TextoPlural As String)

        Me.New(Cultura, TextoSingular, TextoPlural, Generos.Indefinido)

    End Sub

    ''' <summary>
    ''' Crea un texto en singular sin género.
    ''' </summary>
    ''' <param name="Cultura">Indica la cultura a la cual pertenece el texto.</param>
    ''' <param name="TextoSingular">El texto en singular.</param>
    ''' <param name="Genero">Define el género del texto singular.</param>
    ''' <remarks>Este constructor define el texto en plural como vacio (String.Empty).</remarks>
    Public Sub New(ByVal Cultura As String, ByVal TextoSingular As String, ByVal Genero As Generos)

        Me.New(Cultura, TextoSingular, String.Empty, Genero)

    End Sub

    ''' <summary>
    ''' Crea un texto en singular y plural sin género.
    ''' </summary>
    ''' <param name="Cultura">Indica la cultura a la cual pertenece el texto.</param>
    ''' <param name="TextoSingular">El texto en singular.</param>
    ''' <param name="TextoPlural">El texto en plural.</param>
    ''' <param name="Genero">Define el género del texto singular y plural.</param>
    Public Sub New(ByVal Cultura As String, ByVal TextoSingular As String, ByVal TextoPlural As String, ByVal Genero As Generos)

        MiCultura = Cultura
        MiTextoSingular = TextoSingular
        MiTextoPlural = TextoPlural
        MiGenero = Genero

    End Sub

    ''' <summary>
    ''' Regresa la cultura a la cual pertence el texto.
    ''' </summary>
    Public ReadOnly Property Cultura() As String
        Get
            Return MiCultura
        End Get
    End Property

    ''' <summary>
    ''' Regersa el texto en singular.
    ''' </summary>
    Public ReadOnly Property Singular() As String
        Get
            Return MiTextoSingular
        End Get
    End Property

    ''' <summary>
    ''' Regresa el texto en plural.
    ''' </summary>
    ''' <remarks>Si no se definió un texto en plural, esta propiedad regresa String.Empty.</remarks>
    Public ReadOnly Property Plural() As String
        Get
            Return MiTextoPlural
        End Get
    End Property

    ''' <summary>
    ''' Regresa el genero del texto.
    ''' </summary>
    ''' <remarks>Si no se definió un género, esta propiedad regresa "Indefinido".</remarks>
    Public ReadOnly Property Genero() As Generos
        Get
            Return MiGenero
        End Get
    End Property

End Class
