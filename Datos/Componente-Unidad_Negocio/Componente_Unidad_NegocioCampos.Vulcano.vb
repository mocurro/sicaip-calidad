﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Componente_Unidad_Negocio
''' </summary>
Public Enum Componente_Unidad_NegocioCampos As Integer

    cve_Componente_Unidad_Negocio
    cve_Componente
    cve_Unidad_negocio
    Estatus
    fecha_inicio
    Fecha_fin

End Enum

