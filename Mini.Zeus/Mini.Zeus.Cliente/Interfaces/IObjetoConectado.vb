﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para objetos de Zeus que tienen conexión a una fuente de datos.
''' </summary>
''' <remarks>Todos los objetos que tienen conexión a una fuente de datos basados en la plataforma Zeus implementan finalmente esta interface, incluyendo las fábricas.</remarks>
Public Interface IObjetoConectado

    ''' <summary>
    ''' Regresa el identificador del objeto conectado.
    ''' </summary>
    ReadOnly Property Id() As Guid

    ''' <summary>
    ''' Determina la ubicación de la fuente de datos para la entidad conectada.
    ''' </summary>
    ''' <remarks>Este valor determina si todas las operaciones se ejecutan directamente en la base de datos local, o dependen de un servicio Web remoto.</remarks>
    Property UbicacionFuenteDatos() As UbicacionesFuentesDatos

    ''' <summary>
    ''' Regresa la cadena de conexión a la base de datos.
    ''' </summary>
    ''' <remarks>Este valor contiene la cadena de conexión a la base de datos cuando la ubicación de la fuente de datos es SQLServer; en otro caso siempre regresa una cadena vacía.</remarks>
    Property CadenaConexionSQLServer() As String

    ''' <summary>
    ''' Regresa la ruta al servicio web.
    ''' </summary>
    ''' <remarks>Este valor contiene la ruta Url al servicio web que da acceso a la información cuando la ubicación de la fuente de datos es ServicioWeb; en otro caso siempre regresa una cadena vacía.</remarks>
    ReadOnly Property UrlServicioWeb() As Uri

End Interface
