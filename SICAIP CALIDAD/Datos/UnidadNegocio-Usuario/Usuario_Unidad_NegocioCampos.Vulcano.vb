﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Componente_Unidad_Negocio
''' </summary>
Public Enum Usuario_Unidad_NegocioCampos As Integer

    cve_usuario_unidad_negocio
    Cve_Componente_Unidad_Negocio
    CVE_Usuario
    Nombre
    Estatus

End Enum

