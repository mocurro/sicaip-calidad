﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Excepción que ocurre cuando se intenta cambiar el estatus de una entidad a un nuevo estatos que no es válido.
''' </summary>
''' <remarks>Esta excepción la arroja una entidad cuando el estatus actual se intenta modificar a uno nuevo que no está permitido. Esta excepción ocurre antes de cambiar el estatus.</remarks>
Public Class ExcepcionEstatusInvalido
    Inherits Excepcion

    ''' <summary>
    ''' Crea la excepción basada en un objeto y método específico.
    ''' </summary>
    ''' <param name="Objeto">El objeto que generó la excepción. Si el objeto implementa IObjeto el mensaje de la excepción contendrá información adicional.</param>
    ''' <param name="Metodo">El método donde ocurrió la excepción.</param>
    Public Sub New(ByVal Objeto As Object, ByVal Metodo As Reflection.MethodBase)

        MyBase.New(Objeto, Metodo)

    End Sub

    ''' <summary>
    ''' Regresa el mensaje de la excepción.
    ''' </summary>
    Protected Overrides Function Mensaje(ByVal NombreObjeto As String, ByVal NombreMetodo As String) As String

        Return String.Format(My.Resources.Str_ExcepcionEstatusInvalido, NombreObjeto)

    End Function

End Class
