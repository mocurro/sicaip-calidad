﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista de géneros.
''' </summary>
''' <remarks>La lista contiene los generos que pueden aplicar tanto para personas como para redacción.</remarks>
<Flags()> _
Public Enum Generos As Integer

    ''' <summary>
    ''' El genero no está definido o no aplica.
    ''' </summary>
    Indefinido = 0

    ''' <summary>
    ''' Es masculino.
    ''' </summary>
    Masculino = 1

    ''' <summary>
    ''' Es femenino.
    ''' </summary>
    Femenino = 2

    ''' <summary>
    ''' Es neutro.
    ''' </summary>
    Neutro = 4

End Enum