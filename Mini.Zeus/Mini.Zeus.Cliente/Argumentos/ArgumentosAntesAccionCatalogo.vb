﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Argumentos para acciones unitarias antes de que ocurran.
''' </summary>
''' <typeparam name="TElementosInvolucrados">El tipo de dato involucrado en la acción.</typeparam>
Public Class ArgumentosAntesAccionCatalogo(Of TElementosInvolucrados)
    Inherits System.ComponentModel.CancelEventArgs

    ''' <summary>
    ''' Contiene los elementos involucrado en la acción.
    ''' </summary>
    Private MisElementosInvolucrados As TElementosInvolucrados

    ''' <summary>
    ''' Crea un nuevo argumento.
    ''' </summary>
    ''' <param name="ElementosInvolucrados">Los elementos involucrados en la acción.</param>
    Public Sub New(ByVal ElementosInvolucrados As TElementosInvolucrados)

        MyBase.New()
        MisElementosInvolucrados = ElementosInvolucrados

    End Sub

    ''' <summary>
    ''' Crea un nuevo argumento.
    ''' </summary>
    ''' <param name="ElementosInvolucrados">Los elementos involucrados en la acción.</param>
    ''' <param name="Cancelar">Determina si la acción se deberá cancelar.</param>
    Public Sub New(ByVal ElementosInvolucrados As TElementosInvolucrados, ByVal Cancelar As Boolean)

        MyBase.New(Cancelar)
        MisElementosInvolucrados = ElementosInvolucrados

    End Sub

    ''' <summary>
    ''' Regresa los elementos que están involucrados en la acción.
    ''' </summary>
    Public ReadOnly Property ElementosInvolucrados() As TElementosInvolucrados
        Get
            Return MisElementosInvolucrados
        End Get
    End Property

End Class
