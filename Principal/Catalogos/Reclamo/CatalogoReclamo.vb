﻿Imports Mini.Zeus.Cliente
Imports DevExpress.XtraScheduler
Imports DevExpress.XtraEditors.Controls

Public Class CatalogoReclamo

    Private fechaInicio As Date

    Private fechaFin As Date

    ''' <summary>
    ''' Indica si el catálogo está trabajando.
    ''' </summary>
    Private MiTrabajando As Boolean

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Reclamo), Datos.Fabricas.Reclamo, GetType(TarjetaUnidad_Negocio)))

    End Sub

    'Private Sub CatalogoReclamo_Load(sender As Object, e As EventArgs) Handles Me.Load

    '    ComboFechaInicio.DateTime = Date.Now
    '    ComboFechaFin.DateTime = Date.Now
    'End Sub

    ''' <summary>
    ''' Ejectuar acciones antes de la primera carga de datos.
    ''' </summary>
    Private Sub CatalogoCortesBoletos_AntesPrimerActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.AntesPrimerActualizarVentana

        'Asignar rango de fechas para el dia de hoy
        MiTrabajando = True
        ComboFechaInicio.EditValue = Date.Now.Date
        ComboFechaFin.EditValue = Date.Now.Date
        MiTrabajando = False

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Reclamo)

    End Function
    ''' <summary>
    ''' Define el filtro para las consultas de datos.
    ''' </summary>
    ''' <param name="Filtro">El filtro de las consultas.</param>
    Protected Overrides Sub DefinirFiltro(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object))

        Filtro.Clear()
        Filtro.Add("fechaInicio", fechaInicio)
        Filtro.Add("fechaFin", fechaFin)

    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs)
        If fechaInicio <= fechaFin Then

        Else
            MessageBox.Show("La fecha fin debe de ser mayor a la fecha inicio", "Filtro inválido", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    Private Sub ComboFechaInicio_EditValueChanged(sender As Object, e As EventArgs) Handles ComboFechaInicio.EditValueChanged
        fechaInicio=ComboFechaInicio.DateTime

        'Ejecutar solo cuando no esté trabajando
        If Not MiTrabajando Then

            'Validar rangos de fechas
            If ComboFechaInicio.EditValue IsNot Nothing AndAlso ComboFechaFin.EditValue IsNot Nothing Then
                fechaInicio = CDate(ComboFechaInicio.EditValue).Date
                fechaFin = CDate(ComboFechaFin.EditValue).Date
                'ManejadorErrores.ClearErrors()
                If fechaFin < fechaInicio Then

                    'Indicar problema
                    ManejadorErrores.SetError(ComboFechaFin, "La fecha de fin debe ser igual o mayor a la fecha de inicio.", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning)

                Else

                    'Actualizar en automático
                    MiTrabajando = True
                    'ComboRangosFechasDefinidas.SelectedIndex = -1
                    ActualizarVentana()
                    MiTrabajando = False

                End If

            End If

        End If
    End Sub

    Private Sub ComboFechaFin_EditValueChanged(sender As Object, e As EventArgs) Handles ComboFechaFin.EditValueChanged
        fechaFin = ComboFechaFin.DateTime
        'Ejecutar solo cuando no esté trabajando
        If Not MiTrabajando Then

            'Validar rangos de fechas
            If ComboFechaInicio.EditValue IsNot Nothing AndAlso ComboFechaFin.EditValue IsNot Nothing Then
                fechaInicio = CDate(ComboFechaInicio.EditValue).Date
                fechaFin = CDate(ComboFechaFin.EditValue).Date
                'ManejadorErrores.ClearErrors()
                If fechaFin < fechaInicio Then

                    'Indicar problema
                    ManejadorErrores.SetError(ComboFechaFin, "La fecha de fin debe ser igual o mayor a la fecha de inicio.", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning)

                Else

                    'Actualizar en automático
                    MiTrabajando = True
                    'ComboRangosFechasDefinidas.SelectedIndex = -1
                    ActualizarVentana()
                    MiTrabajando = False

                End If

            End If

        End If
    End Sub
End Class