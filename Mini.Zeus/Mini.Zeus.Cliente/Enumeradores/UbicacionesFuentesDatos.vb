﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista las ubicaciones para fuentes de datos.
''' </summary>
''' <remarks>La lista contiene valores para determinar la ubicación de una fuente de datos. Estos valores tienen significancia para una fábrica de entidades pues determina si la información se obtiene de forma local o remota. Para cada caso el mecanismo utilizado para obtener información es radicalmente diferente.</remarks>
Public Enum UbicacionesFuentesDatos As Integer

    ''' <summary>
    ''' La ubicación no está especificada.
    ''' </summary>
    Desconocida = 0

    ''' <summary>
    ''' La fuente de datos es una base de datos SQL Server y se puede accesar directamente.
    ''' </summary>
    SQLServer = 1

    ''' <summary>
    ''' La fuente de datos está envuelta en un servicio web y depende de contar con comunicación a él.
    ''' </summary>
    ServicioWeb = 2

End Enum