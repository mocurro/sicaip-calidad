﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports Microsoft.WindowsAPICodePack
Imports Microsoft.WindowsAPICodePack.Dialogs

''' <summary>
''' Ventana base para cualquier tarjeta que forma parte de una aplicación de Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana tarjeta que implemente UX de Zeus. Todas las ventanas tarjeta heredan finalmente de esta ventana.</remarks>
Public Class FormaTarjeta
    Implements ITarjeta

    ''' <summary>
    ''' Contiene la lista de tipos de pestañas que deben actualizar su contenido como resultado de un cambio realizado por esta tarjeta.
    ''' </summary>
    Private MisTiposVentanasAfectadas As List(Of Type)

    ''' <summary>
    ''' Contiene la lista de campos que se validan en automático, asociadas con el control correspondiente.
    ''' </summary>
    Private MisCamposValidar As Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

    ''' <summary>
    ''' Bandera para controlar el proceso de cerrado de ventana.
    ''' </summary>
    Private MiEstaCerrando As Boolean

    ''' <summary>
    ''' Contiene el texto formateado con la pregunta de cerrar cuando se requieren guardar los cambios.
    ''' </summary>
    Private MiPreguntaCerrar As String

    ''' <summary>
    ''' Determina si se debe omitir el procesamiento de cambios personalizados.
    ''' </summary>
    Private MiOmitirCambioPersonalizado As Boolean

    ''' <summary>
    ''' Contiene la ventana catálogo que mandó abrir la tarjeta.
    ''' </summary>
    Private MiCatalogo As ICatalogo

    ''' <summary>
    ''' Contiene la referencia de la ventana principal.
    ''' </summary>
    Private MiVentanaPrincipal As IFormaPrincipal

    ''' <summary>
    ''' Evento que permite realizar acciones antes de completar el proceso de aceptación de la tarjeta, habilitando la capacidad de cancelar el proceso.
    ''' </summary>
    Protected Event AntesAceptar As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>
    ''' Permite definir si la tarjeta es de tamaño fijo, o si el usuario puede modificar el tamaño.
    ''' </summary>
    Public Property EsTamañoFijo() As Boolean
        Get
            Return (FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle)
        End Get
        Set(ByVal value As Boolean)
            If value Then
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
                MaximizeBox = False
            Else
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
                MaximizeBox = True
            End If
        End Set
    End Property

    ''' <summary>
    ''' Muestra la tarjeta dentro de la ventana padre.
    ''' </summary>
    ''' <param name="Entidad">La entidad que se editará con la tarjeta.</param>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Function MostrarVentana(ByVal Entidad As IEntidad, ByVal owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult Implements ITarjeta.MostrarVentana
        Entidad.InicializarExistente()
        Me.Entidad = Entidad
        If TypeOf owner Is ICatalogo Then
            MiCatalogo = CType(owner, ICatalogo)
        Else
            MiCatalogo = Nothing
        End If
        Return MyBase.MostrarVentana(owner)

    End Function

    '''' <summary>
    '''' Muestra la tarjeta dentro de la ventana padre.
    '''' </summary>
    '''' <param name="owner">Ventana padre.</param>
    '''' <param name="Entidad">La entidad que se editará con la tarjeta.</param>
    '''' <param name="UsuarioActual">Usuario dueño de la tarjeta.</param>
    'Public Shadows Function MostrarVentana(ByVal Entidad As IEntidad, ByVal UsuarioActual As Usuarios, ByVal owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult Implements ITarjeta.MostrarVentana

    '    Me.UsuarioActual = UsuarioActual
    '    Return MostrarVentana(Entidad, owner)

    'End Function

    ''' <summary>
    ''' Contiene la entidad que se edita con esta tarjeta.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public Property Entidad() As IEntidad Implements ITarjeta.Entidad
        Get
            Return TryCast(FuenteDatos.DataSource, IEntidad)
        End Get
        Set(ByVal value As IEntidad)
            FuenteDatos.DataSource = value
        End Set
    End Property

    ''' <summary>
    ''' Contiene el catálogo que mandó llamar esta tarjeta.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public Property Catalogo() As ICatalogo Implements ITarjeta.Catalogo
        Get
            Return MiCatalogo
        End Get
        Set(ByVal value As ICatalogo)
            MiCatalogo = value
        End Set
    End Property

    ''' <summary>
    ''' Contiene la referencia a la ventana principal
    ''' </summary>
    Public Property VentanaPrincipal() As IFormaPrincipal Implements ITarjeta.VentanaPrincipal
        Get
            Return MiVentanaPrincipal
        End Get
        Set(ByVal value As IFormaPrincipal)
            MiVentanaPrincipal = value
        End Set
    End Property

    ''' <summary>
    ''' Regresa la entidad que se edita en esta tarjeta.
    ''' </summary>
    ''' <typeparam name="TEntidad">El tipo de entidad.</typeparam>
    ''' <remarks>Esta función regresa el mismo objeto que la propiedad Entidad, pero convirtiendo la entidad en un tipo específico.</remarks>
    Public Function EntidadDe(Of TEntidad)() As TEntidad Implements ITarjeta.EntidadDe

        Return DirectCast(Entidad, TEntidad)

    End Function

    ''' <summary>
    ''' Regresa el catálogo que mandó llamar esta tarjeta.
    ''' </summary>
    ''' <typeparam name="TCatalogo">El tipo del catálgoo.</typeparam>
    ''' <remarks>Esta función regresa el objeto que original mente se determinó como ventana padre, pero convirtiendolo al tipo de catálogo específico.</remarks>
    Public Function CatalogoDe(Of TCatalogo)() As TCatalogo Implements ITarjeta.CatalogoDe

        Return DirectCast(MiCatalogo, TCatalogo)

    End Function

    ''' <summary>
    ''' Regresa la lista de los tipos de pestañas que se ven afectadas por cambios en esta tarjeta.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Public ReadOnly Property VentanasAfectadas() As System.Collections.Generic.List(Of System.Type) Implements ITarjeta.VentanasAfectadas
        Get
            If MisTiposVentanasAfectadas Is Nothing Then
                MisTiposVentanasAfectadas = New List(Of Type)
                DefinirTiposVentanasAfectadas(MisTiposVentanasAfectadas)
            End If
            Return MisTiposVentanasAfectadas
        End Get
    End Property

    ''' <summary>
    ''' Regresa la lista de campos para validación visual.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Private ReadOnly Property CamposValidar() As Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)
        Get
            If MisCamposValidar Is Nothing Then
                MisCamposValidar = DefinirCamposValidacion()
            End If
            Return MisCamposValidar
        End Get
    End Property

    ''' <summary>
    ''' Determina si se debe omitir el procesamiento de cambios personalizados.
    ''' </summary>
    <System.ComponentModel.Browsable(False)>
    Protected Property OmitirCambiosPersonalizados() As Boolean
        Get
            Return MiOmitirCambioPersonalizado
        End Get
        Set(ByVal value As Boolean)
            MiOmitirCambioPersonalizado = value
        End Set
    End Property

    ''' <summary>
    ''' Prepara la tarjeta.
    ''' </summary>
    ''' <exception cref="ArgumentNullException">Arroja la excepción cuando en la lista de pestañas afectadas no existe al menos un tipo.</exception>
    Private Sub FormaTarjeta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Definir colores en base al tema
            PanelAcciones.BackColor = MiVentanaPrincipal.ColorVentana
            PanelEncabezado.Appearance.BackColor = MiVentanaPrincipal.ColorControl
            PanelEncabezado.Appearance.BackColor2 = MiVentanaPrincipal.ColorControlDeshabilitado
            EtiquetaEncabezado.ForeColor = MiVentanaPrincipal.ColorControlTexto
            EtiquetaCopyright.ForeColor = MiVentanaPrincipal.ColorVentanaTexto

            'Modifica aspectos visuales
            Dim Texto = Entidad.Texto(Cultura)
            Icon = Entidad.IconoEntidad
            ImagenEncabezado.Image = Entidad.ImagenEntidadEspecifica(TamañosImagenes.x32)
            EtiquetaEncabezado.Text = Texto.Singular
            EtiquetaCopyright.Text = String.Format("{0} | {1}", My.Application.Info.Trademark.Replace("&", "&&"), My.Application.Info.Copyright.Replace("&", "&&"))
            Dim Titulo = String.Empty
            Dim Pregunta = String.Empty
            Select Case Texto.Genero
                Case Generos.Masculino
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoMasculino
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteMasculino
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarMasculino
                Case Generos.Femenino
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoFemenino
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteFemenino
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarFemenino
                Case Generos.Neutro
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoNeutro
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteNeutro
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarNeutro
            End Select
            Text = String.Format(Titulo, Texto.Singular.ToLower())
            MiPreguntaCerrar = String.Format(Pregunta, Texto.Singular.ToLower())

        End If

    End Sub

    ''' <summary>
    ''' Determina si se puede cancelar el cerrado por cuestión de estar sucio.
    ''' </summary>
    Private Sub FormaTarjeta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Ejectuar únicamente una vez
            If (Not MiEstaCerrando) And DialogResult <> System.Windows.Forms.DialogResult.OK Then
                MiEstaCerrando = True

                'Determinar si se requiere confirmar
                Dim PuedeCerrar As Boolean = True
                If e.CloseReason = CloseReason.UserClosing Then
                    If Entidad.EstaSucio AndAlso Entidad.EsValido Then
                        Select Case MessageBox.Show(MiPreguntaCerrar, My.Resources.Str_TarjetaCerrarSinGuardarTitulo, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)
                            Case System.Windows.Forms.DialogResult.Yes
                                'Marcar como OK para guardar
                                DialogResult = System.Windows.Forms.DialogResult.OK

                            Case System.Windows.Forms.DialogResult.Cancel
                                'Cambiar la bandera para prevenir cerrar la ventana
                                e.Cancel = True

                        End Select
                    End If
                End If

                MiEstaCerrando = False
            End If

        End If

    End Sub

    ''' <summary>
    ''' Dibuja el borde del encabezado.
    ''' </summary>
    Private Sub PanelEncabezado_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelEncabezado.Paint

        Using Pluma = New Drawing.Pen(SystemColors.ActiveBorder)
            e.Graphics.DrawLine(Pluma, 0, PanelEncabezado.Height - 1, PanelEncabezado.Width, PanelEncabezado.Height - 1)
        End Using

    End Sub

    ''' <summary>
    ''' Dibuja el borde de las acciones.
    ''' </summary>
    Private Sub PanelAcciones_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelAcciones.Paint

        Using Pluma = New Drawing.Pen(SystemColors.ActiveBorder)
            e.Graphics.DrawLine(Pluma, 0, 0, PanelAcciones.Width, 0)
        End Using

    End Sub

    ''' <summary>
    ''' Liga el evento de cambio de contenido para automatizar el proceso de validación.
    ''' </summary>
    Private Sub LayoutTarjeta_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles LayoutTarjeta.ControlAdded

        'Liga el evento de cambio de datos
        If TypeOf e.Control Is DevExpress.XtraEditors.BaseEdit Then
            With DirectCast(e.Control, DevExpress.XtraEditors.BaseEdit)
                AddHandler .EditValueChanged, AddressOf CambioInformacion
            End With
        End If

        'Liga el evento de cambio de datos especial para controles de Zeus
        If TypeOf e.Control Is Control Then
            With DirectCast(e.Control, Control)
                AddHandler .CambioValor, AddressOf CambioInformacion
            End With
        End If

        'En caso de ser posible, liga el evento de clic para botones dentro del control
        If TypeOf e.Control Is DevExpress.XtraEditors.ButtonEdit Then
            With DirectCast(e.Control, DevExpress.XtraEditors.ButtonEdit)
                AddHandler .ButtonClick, AddressOf BotonClick
            End With
        End If

        'Ligar controles especiales a eventos de cambio personalizado
        If TypeOf e.Control Is DevExpress.XtraEditors.BaseCheckedListBoxControl Then
            With DirectCast(e.Control, DevExpress.XtraEditors.BaseCheckedListBoxControl)
                AddHandler .ItemCheck, AddressOf CambioInformacion
            End With
        End If

    End Sub

    ''' <summary>
    ''' Acepta los cambios y cierra la ventana.
    ''' </summary>
    Private Sub BotonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BotonAceptar.Click

        Dim Argumentos = New System.ComponentModel.CancelEventArgs(False)
        RaiseEvent AntesAceptar(Me, Argumentos)
        If Not Argumentos.Cancel Then
            DialogResult = System.Windows.Forms.DialogResult.OK
            Close()
        End If

    End Sub

    ''' <summary>
    ''' Cancela los cambios y cierra la ventana.
    ''' </summary>
    Private Sub BotonCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BotonCancelar.Click

        DialogResult = System.Windows.Forms.DialogResult.Cancel
        Close()

    End Sub

    ''' <summary>
    ''' Analiza la entidad y determina si puede aceptar los cambios.
    ''' </summary>
    Private Sub FuenteDatos_CurrentItemChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FuenteDatos.CurrentItemChanged

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Inicia proceso de validación
            LayoutTarjeta.BeginUpdate()

            'Realiza validación visual de cada campo
            ManejadorValidacion.ClearErrors()
            For Each Campo As KeyValuePair(Of Integer, DevExpress.XtraLayout.LayoutControlItem) In CamposValidar
                ValidarCampo(Campo.Key, Campo.Value)
            Next

            'Realiza validaciones personalizadas y actualiza la interface
            BotonAceptar.Enabled = ValidacionPersonalizada() AndAlso Entidad.EstaSucio AndAlso Entidad.EsValido
            ItemForLigaAyuda.Visibilidad(False) 'Not BotonAceptar.Enabled)

            'Termina proceso de validación
            LayoutTarjeta.EndUpdate()

        End If

    End Sub

    ''' <summary>
    ''' Propaga los cambios a la fuente de datos para levantar el proceso de validación.
    ''' </summary>
    Protected Sub CambioInformacion(ByVal sender As Object, ByVal e As EventArgs)

        'Aplica únicamente durante la ejecución
        If Not DesignMode AndAlso Not IsInitializing Then

            FuenteDatos.EndEdit()
            If Not MiOmitirCambioPersonalizado Then
                If CambioInformacionPersonalizada(DirectCast(sender, System.Windows.Forms.Control), e) Then
                    FuenteDatos_CurrentItemChanged(FuenteDatos, EventArgs.Empty)
                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Notifica que ocurrieron cambios y se requiere una acción personalizada.
    ''' </summary>
    Protected Sub CambioInformacion(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs)

        'Aplica únicamente durante la ejecución
        If Not DesignMode AndAlso Not IsInitializing Then

            'Ejectuar cuando no se deba omitir
            If Not MiOmitirCambioPersonalizado Then
                If CambioInformacionPersonalizada(DirectCast(sender, System.Windows.Forms.Control), e) Then
                    FuenteDatos_CurrentItemChanged(FuenteDatos, EventArgs.Empty)
                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Notifica que ocurrieron cambios y se requiere una acción personalizada.
    ''' </summary>
    Protected Sub CambioInformacion(ByVal sender As String)

        'Aplica únicamente durante la ejecución
        If Not DesignMode AndAlso Not IsInitializing Then

            'Ejectuar cuando no se deba omitir
            If Not MiOmitirCambioPersonalizado Then
                If CambioInformacionPersonalizada(sender, EventArgs.Empty) Then
                    FuenteDatos_CurrentItemChanged(FuenteDatos, EventArgs.Empty)
                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Realiza la validación visual de un campo en particular.
    ''' </summary>
    ''' <param name="Campo">El campo que se desea validar.</param>
    ''' <param name="Control">El control de layout que contiene el campo a validar.</param>
    Private Sub ValidarCampo(ByVal Campo As Integer, ByVal Control As DevExpress.XtraLayout.LayoutControlItem)

        ValidarCampo(Entidad.Validar(Campo), Control)

    End Sub

    ''' <summary>
    ''' Realiza la validación visual de un campo en particular.
    ''' </summary>
    ''' <param name="Mensajes">La lista de mensajes de error de un campo.</param>
    ''' <param name="Control">El control de layout que contiene el campo a validar.</param>
    Protected Sub ValidarCampo(ByVal Mensajes As List(Of String), ByVal Control As DevExpress.XtraLayout.LayoutControlItem)

        'Construir mensaje final
        Dim MensajeFinal = String.Empty
        For Each Mensaje In Mensajes
            MensajeFinal &= Mensaje & Environment.NewLine
        Next
        MensajeFinal = MensajeFinal.Trim(Environment.NewLine.ToCharArray())

        'Actualizar visualmente el control
        If Not String.IsNullOrEmpty(MensajeFinal) Then
            ManejadorValidacion.SetError(Control.Control, MensajeFinal, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning)
        End If

    End Sub

    ''' <summary>
    ''' Realiza acciones comunes para tipos de botones conocidos al momento de hacer clic.
    ''' </summary>
    Private Sub BotonClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)

        'Determina la accion conocida
        Select Case e.Button.Kind
            Case DevExpress.XtraEditors.Controls.ButtonPredefines.Delete
                'Remueve el valor actual del campo
                With CType(sender, DevExpress.XtraEditors.ButtonEdit)
                    .EditValue = Nothing
                End With

        End Select

    End Sub

    ''' <summary>
    ''' Define la lista de tipos de pestañas que deben actualizar su contenido como resultado de un cambio realizado por esta tarjeta.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">La lista donde se guardan los tipos de ventanas que se ven afectadas por esta tarjeta.</param>
    ''' <remarks>Esta función rellena una lista donde se indica los tipos de ventanas que requieren actualización una vez que una tarjeta realiza una modificación a la entidad mediante el botón de aceptar.</remarks>
    Protected Overridable Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As List(Of Type))

        Throw New NotImplementedException()

    End Sub

    ''' <summary>
    ''' Permite realizar acciones cuando ocurrió un cambio que requiere código personalizado.
    ''' </summary>
    ''' <param name="sender">Contiene el control que está causando el cambio.</param>
    ''' <param name="e">Los argumentos originales del cambio.</param>
    ''' <remarks>Esta función reacciona ante el cambio de elementos cuya liga de datos es tan completa que requiere un proceso personalizado. Regresa verdadero en aquellos casos donde manejo el caso.</remarks>
    Protected Overridable Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As EventArgs) As Boolean

        'No hace nada, es responsabilidad de la ventana heredada implementar el código

    End Function

    ''' <summary>
    ''' Permite realizar acciones cuando ocurrió un cambio que requiere código personalizado.
    ''' </summary>
    ''' <param name="sender">Contiene el nombre del control que está causando el cambio.</param>
    ''' <param name="e">Los argumentos originales del cambio.</param>
    ''' <remarks>Esta función reacciona ante el cambio de elementos cuya liga de datos es tan completa que requiere un proceso personalizado. Regresa verdadero en aquellos casos donde manejo el caso.</remarks>
    Protected Overridable Function CambioInformacionPersonalizada(ByVal sender As String, ByVal e As EventArgs) As Boolean

        'No hace nada, es responsabilidad de la ventana heredada implementar el código

    End Function

    ''' <summary>
    ''' Permite realizar una validación personalizada no cubierta por la validación de campos, regresando si dicha validación es correcta o no.
    ''' </summary>
    ''' <remarks>Esta función sirve para realizar validaciones especiales que no son cubiertas en la validación campo por campo.</remarks>
    Protected Overridable Function ValidacionPersonalizada() As Boolean

        'No hace nada, es responsabilidad de la ventana heredada implementar el código
        Return True

    End Function

    ''' <summary>
    ''' Regresa una lista que contiene los valores de los campos de la entidad asociados al control visual para su validación.
    ''' </summary>
    ''' <remarks>Esta función regresa un diccionario que asocia un valor numérico (identificador de un campo particular de la entidad) al control visual de la tarjeta. Con esta información la tarjeta automatiza el proceso de validación visual de dicho campo.</remarks>
    Protected Overridable Function DefinirCamposValidacion() As Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        'Es responsabilidad de la ventana heredada implementar un código funcional
        Return New Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

    End Function

    ''' <summary>
    ''' Convierte la parte llave del diccionario generado para validar campos.
    ''' </summary>
    ''' <typeparam name="TEnumeradorCampos">El enumerador de campos utilizad para rellenar el diccionario.</typeparam>
    ''' <param name="Diccionario">El diccionario a convertir.</param>
    ''' <remarks>Esta función convierte el diccionario de manera que pueda ser utilizado en el proceso automático de validación visual.</remarks>
    Protected Shared Function ConvertirDiccionarioCamposValidacion(Of TEnumeradorCampos)(ByVal Diccionario As Dictionary(Of TEnumeradorCampos, DevExpress.XtraLayout.LayoutControlItem)) As Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Return Diccionario.ToDictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)(Function(Item) CInt(DirectCast(Item.Key, Object)), Function(Item) Item.Value)

    End Function

    ''' <summary>
    ''' Muestra información del estado de la entidad.
    ''' </summary>
    Private Sub LigaAyuda_OpenLink(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.OpenLinkEventArgs) Handles LigaAyuda.OpenLink

        'Armar el mensaje correcto
        Dim Mensaje = String.Empty
        Dim TextoEntidad = Entidad.Texto(Cultura)
        If Not Entidad.EstaSucio() Then
            Select Case TextoEntidad.Genero
                Case Generos.Masculino
                    Mensaje &= String.Format(My.Resources.Str_TarjetaEntidadSuciaMasculino, TextoEntidad.Singular.ToLower()) & Environment.NewLine
                Case Generos.Femenino
                    Mensaje &= String.Format(My.Resources.Str_TarjetaEntidadSuciaFemenino, TextoEntidad.Singular.ToLower()) & Environment.NewLine
                Case Generos.Neutro
                    Mensaje &= String.Format(My.Resources.Str_TarjetaEntidadSuciaNeutro, TextoEntidad.Singular.ToLower()) & Environment.NewLine
            End Select
        End If
        For Each Invalidez In Entidad.Validar()
            Mensaje &= String.Format("• {0}{1}", Invalidez, Environment.NewLine)
        Next
        Mensaje = Mensaje.Trim(Environment.NewLine.ToCharArray())

        'Mostrar el mensaje
        If TaskDialog.IsPlatformSupported Then
            Using Dialogo = New TaskDialog()
                Dialogo.Icon = TaskDialogStandardIcon.Information
                Dialogo.Caption = "¿Por qué no puedo guardar?"
                Dialogo.InstructionText = "No se puede guardar la información por las siguientes razones:"
                Dialogo.Text = Mensaje
                Dialogo.StandardButtons = TaskDialogStandardButtons.Close
                Dialogo.OwnerWindowHandle = Handle
                Dialogo.StartupLocation = TaskDialogStartupLocation.CenterOwner
                Dialogo.Show()
            End Using
        Else
            MessageBox.Show(Mensaje, "¿Por qué no puedo guardar?", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Public Function MostrarInformacion(ByVal Entidad As IEntidad, ByVal owner As IWin32Window) As Object Implements ITarjeta.MostrarInformacion
        'Aplica únicamente durante la ejecución
        If Not DesignMode Then
            If TypeOf owner Is IForma Then
                With DirectCast(owner, IForma)
                    .PonerSabana()
                    owner = .Sabana
                End With
            End If
            'Definir colores en base al tema
            PanelAcciones.BackColor = MiVentanaPrincipal.ColorVentana
            PanelEncabezado.Appearance.BackColor = MiVentanaPrincipal.ColorControl
            PanelEncabezado.Appearance.BackColor2 = MiVentanaPrincipal.ColorControlDeshabilitado
            EtiquetaEncabezado.ForeColor = MiVentanaPrincipal.ColorControlTexto
            EtiquetaCopyright.ForeColor = MiVentanaPrincipal.ColorVentanaTexto

            'Modifica aspectos visuales
            Dim Texto = Entidad.Texto(Cultura)
            Icon = Entidad.IconoEntidad
            ImagenEncabezado.Image = Entidad.ImagenEntidadEspecifica(TamañosImagenes.x32)
            EtiquetaEncabezado.Text = Texto.Singular
            EtiquetaCopyright.Text = String.Format("{0} | {1}", My.Application.Info.Trademark.Replace("&", "&&"), My.Application.Info.Copyright.Replace("&", "&&"))
            Dim Titulo = String.Empty
            Dim Pregunta = String.Empty
            Select Case Texto.Genero
                Case Generos.Masculino
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoMasculino
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteMasculino
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarMasculino
                Case Generos.Femenino
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoFemenino
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteFemenino
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarFemenino
                Case Generos.Neutro
                    If Entidad.EstatusEntidad = EstatusEntidades.Nuevo Then
                        Titulo = My.Resources.Str_TarjetaTituloNuevoNeutro
                    Else
                        Titulo = My.Resources.Str_TarjetaTituloExistenteNeutro
                    End If
                    Pregunta = My.Resources.Str_TarjetaCerrarSinGuardarNeutro
            End Select
            Text = String.Format(Titulo, Texto.Singular.ToLower())
            MiPreguntaCerrar = String.Format(Pregunta, Texto.Singular.ToLower())

        End If
    End Function
End Class