﻿Imports Mini.Zeus.Cliente
Imports DevExpress.XtraScheduler
Imports DevExpress.XtraEditors.Controls
Imports System.ComponentModel
Imports System.IO

Public Class CatalogoReclamo

    Private fechaInicio As Date

    Private fechaFin As Date

    ''' <summary>
    ''' Contiene el botón especial de exportación avanzada.
    ''' </summary>
    Private MiBotonArchivo As DevExpress.XtraBars.BarButtonItem

    ''' <summary>
    ''' Indica si el catálogo está trabajando.
    ''' </summary>
    Private MiTrabajando As Boolean

    Private EstaAbriendoArchivo As Boolean = False

    Private RutaArchivo As String

    Private MiCadenaConexión As String

    Private MiReclamoArchivo As Datos.Reclamo_Archivo

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Reclamo), Datos.Fabricas.Reclamo, GetType(TarjetaUnidad_Negocio)))

    End Sub

    'Private Sub CatalogoReclamo_Load(sender As Object, e As EventArgs) Handles Me.Load

    '    ComboFechaInicio.DateTime = Date.Now
    '    ComboFechaFin.DateTime = Date.Now
    'End Sub

    ''' <summary>
    ''' Ejectuar acciones antes de la primera carga de datos.
    ''' </summary>
    Private Sub CatalogoCortesBoletos_AntesPrimerActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.AntesPrimerActualizarVentana

        'Asignar rango de fechas para el dia de hoy
        MiTrabajando = True
        ComboFechaInicio.EditValue = Date.Now.Date
        ComboFechaFin.EditValue = Date.Now.Date
        MiTrabajando = False

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Reclamo)

    End Function
    ''' <summary>
    ''' Define el filtro para las consultas de datos.
    ''' </summary>
    ''' <param name="Filtro">El filtro de las consultas.</param>
    Protected Overrides Sub DefinirFiltro(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object))

        Filtro.Clear()
        Filtro.Add("fechaInicio", fechaInicio)
        Filtro.Add("fechaFin", fechaFin)

    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs)
        If fechaInicio <= fechaFin Then

        Else
            MessageBox.Show("La fecha fin debe de ser mayor a la fecha inicio", "Filtro inválido", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    Private Sub ComboFechaInicio_EditValueChanged(sender As Object, e As EventArgs) Handles ComboFechaInicio.EditValueChanged
        fechaInicio = ComboFechaInicio.DateTime

        'Ejecutar solo cuando no esté trabajando
        If Not MiTrabajando Then

            'Validar rangos de fechas
            If ComboFechaInicio.EditValue IsNot Nothing AndAlso ComboFechaFin.EditValue IsNot Nothing Then
                fechaInicio = CDate(ComboFechaInicio.EditValue).Date
                fechaFin = CDate(ComboFechaFin.EditValue).Date
                'ManejadorErrores.ClearErrors()
                If fechaFin < fechaInicio Then

                    'Indicar problema
                    ManejadorErrores.SetError(ComboFechaFin, "La fecha de fin debe ser igual o mayor a la fecha de inicio.", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning)

                Else

                    'Actualizar en automático
                    MiTrabajando = True
                    'ComboRangosFechasDefinidas.SelectedIndex = -1
                    ActualizarVentana()
                    MiTrabajando = False

                End If

            End If

        End If
    End Sub

    Private Sub ComboFechaFin_EditValueChanged(sender As Object, e As EventArgs) Handles ComboFechaFin.EditValueChanged
        fechaFin = ComboFechaFin.DateTime
        'Ejecutar solo cuando no esté trabajando
        If Not MiTrabajando Then

            'Validar rangos de fechas
            If ComboFechaInicio.EditValue IsNot Nothing AndAlso ComboFechaFin.EditValue IsNot Nothing Then
                fechaInicio = CDate(ComboFechaInicio.EditValue).Date
                fechaFin = CDate(ComboFechaFin.EditValue).Date
                'ManejadorErrores.ClearErrors()
                If fechaFin < fechaInicio Then

                    'Indicar problema
                    ManejadorErrores.SetError(ComboFechaFin, "La fecha de fin debe ser igual o mayor a la fecha de inicio.", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning)

                Else

                    'Actualizar en automático
                    MiTrabajando = True
                    'ComboRangosFechasDefinidas.SelectedIndex = -1
                    ActualizarVentana()
                    MiTrabajando = False

                End If

            End If

        End If
    End Sub

    Private Sub CatalogoReclamo_AntesActualizarVentana(sender As Object, e As CancelEventArgs) Handles Me.AntesActualizarVentana
        If MiBotonArchivo IsNot Nothing Then
            'MiBotonArchivo.Enabled = False
        End If
    End Sub

    Private Sub CatalogoReclamo_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Crear botón de exportación avanzada
        MiBotonArchivo = Barras.Items.CreateButton("Obtener archivo")
        'MiBotonFinalizar.Glyph = Zeus.Cliente.Windows.UX.My.Resources.Imagen16_Exportar
        MiBotonArchivo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        AddHandler MiBotonArchivo.ItemClick, AddressOf MiBotonArchivo_Click
        BarraHerramientas.AddItem(MiBotonArchivo)
        MiCadenaConexión = Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer
    End Sub



    ''' <summary>
    ''' Realiza la descarga del archivo adjunto.
    ''' </summary>
    Private Sub MiBotonArchivo_Click(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        EstaAbriendoArchivo = True
        Try
            MiReclamoArchivo = Nothing
            Dim registro = EntidadesSeleccionadasDe(Of Datos.Reclamo)()

            If registro IsNot Nothing AndAlso registro.Count > 0 Then

                Dim RutaArchivo = String.Empty
                Using MiModelo = New Datos.ModeloDatosDataContext(MiCadenaConexión)
                    MiReclamoArchivo = (From ra In MiModelo.Reclamo_Archivos
                                        Where ra.cve_reclamo = registro(0).cve_reclamo
                                        Select ra).FirstOrDefault
                End Using

                If MiReclamoArchivo IsNot Nothing Then
                    Dim direcotorioEtiquetas As String = "C:\\GKN\\"
                    RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas, MiReclamoArchivo.Nombre)
                    If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                        System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                    End If
                    If File.Exists(RutaArchivo) Then
                        File.Delete(RutaArchivo)
                    End If
                    File.WriteAllBytes(RutaArchivo, MiReclamoArchivo.archivo.ToArray)
                    Dim proceso As New System.Diagnostics.Process
                    With proceso
                        .StartInfo.FileName = RutaArchivo
                        .Start()
                    End With

                Else
                    MessageBox.Show("No hay archivos cargados al reclamo.", "Advertencia.", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
        Catch ex As Exception
        Finally
            EstaAbriendoArchivo = False
        End Try

    End Sub

End Class