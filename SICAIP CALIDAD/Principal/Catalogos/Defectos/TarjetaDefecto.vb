﻿Imports System.ComponentModel

Public Class TarjetaDefecto
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoDefecto ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.DefectosCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.DefectosCampos.Nombre, itemNombre)
            .Add(Datos.DefectosCampos.Descripcion, itemDescripcion)
            .Add(Datos.DefectosCampos.Estatus , ItemEstatus )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.DefectosCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Defecto)()
        Select Case sender.Name
            Case TextoNombre.Name
                Paquete.Nombre = TextoNombre.Text.Trim
                'Return True
            Case TextoNombre.Name
                Paquete.Descripcion = TextoDescripcion.Text.Trim

            Case ComboEstatus .Name
                Paquete .Estatus = CByte(ComboEstatus .SelectedIndex)
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Defecto)()
        If paquete.Estatus = 0 Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Defecto)()
        'Paquete.Estatus  = CByte(ComboEstatus.SelectedIndex)
    End Sub
End Class