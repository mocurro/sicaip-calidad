﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormaAcercaDe
    Inherits Forma

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DistribucionPrincipal = New DevExpress.XtraLayout.LayoutControl
        Me.EtiquetaCopyright = New DevExpress.XtraEditors.LabelControl
        Me.EtiquetaTrademark = New DevExpress.XtraEditors.LabelControl
        Me.EtiquetaVersion = New DevExpress.XtraEditors.LabelControl
        Me.EtiquetaTitulo = New DevExpress.XtraEditors.LabelControl
        Me.BotonCerrar = New DevExpress.XtraEditors.SimpleButton
        Me.DistribucionGrupoPrincipal = New DevExpress.XtraLayout.LayoutControlGroup
        Me.EspacioVacio1 = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EspacioVacio2 = New DevExpress.XtraLayout.EmptySpaceItem
        Me.EspacioParaBotonCerrar = New DevExpress.XtraLayout.LayoutControlItem
        Me.EspacioParaEtiquetaTitulo = New DevExpress.XtraLayout.LayoutControlItem
        Me.EspacioParaEtiquetaVersion = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
        CType(Me.DistribucionPrincipal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DistribucionPrincipal.SuspendLayout()
        CType(Me.DistribucionGrupoPrincipal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioVacio1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioVacio2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioParaBotonCerrar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioParaEtiquetaTitulo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EspacioParaEtiquetaVersion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DistribucionPrincipal
        '
        Me.DistribucionPrincipal.AllowCustomizationMenu = False
        Me.DistribucionPrincipal.Controls.Add(Me.EtiquetaCopyright)
        Me.DistribucionPrincipal.Controls.Add(Me.EtiquetaTrademark)
        Me.DistribucionPrincipal.Controls.Add(Me.EtiquetaVersion)
        Me.DistribucionPrincipal.Controls.Add(Me.EtiquetaTitulo)
        Me.DistribucionPrincipal.Controls.Add(Me.BotonCerrar)
        Me.DistribucionPrincipal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DistribucionPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.DistribucionPrincipal.Name = "DistribucionPrincipal"
        Me.DistribucionPrincipal.Root = Me.DistribucionGrupoPrincipal
        Me.DistribucionPrincipal.Size = New System.Drawing.Size(284, 148)
        Me.DistribucionPrincipal.TabIndex = 0
        Me.DistribucionPrincipal.Text = "DistribucionPrincipal"
        '
        'EtiquetaCopyright
        '
        Me.EtiquetaCopyright.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaCopyright.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaCopyright.Location = New System.Drawing.Point(12, 97)
        Me.EtiquetaCopyright.Name = "EtiquetaCopyright"
        Me.EtiquetaCopyright.Size = New System.Drawing.Size(260, 13)
        Me.EtiquetaCopyright.StyleController = Me.DistribucionPrincipal
        Me.EtiquetaCopyright.TabIndex = 8
        Me.EtiquetaCopyright.Text = "[Copyright]"
        '
        'EtiquetaTrademark
        '
        Me.EtiquetaTrademark.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaTrademark.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaTrademark.Location = New System.Drawing.Point(12, 80)
        Me.EtiquetaTrademark.Name = "EtiquetaTrademark"
        Me.EtiquetaTrademark.Size = New System.Drawing.Size(260, 13)
        Me.EtiquetaTrademark.StyleController = Me.DistribucionPrincipal
        Me.EtiquetaTrademark.TabIndex = 7
        Me.EtiquetaTrademark.Text = "[Marcas]"
        '
        'EtiquetaVersion
        '
        Me.EtiquetaVersion.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaVersion.Location = New System.Drawing.Point(12, 29)
        Me.EtiquetaVersion.Name = "EtiquetaVersion"
        Me.EtiquetaVersion.Size = New System.Drawing.Size(54, 13)
        Me.EtiquetaVersion.StyleController = Me.DistribucionPrincipal
        Me.EtiquetaVersion.TabIndex = 6
        Me.EtiquetaVersion.Text = "Versión {0}"
        '
        'EtiquetaTitulo
        '
        Me.EtiquetaTitulo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaTitulo.Location = New System.Drawing.Point(12, 12)
        Me.EtiquetaTitulo.Name = "EtiquetaTitulo"
        Me.EtiquetaTitulo.Size = New System.Drawing.Size(34, 13)
        Me.EtiquetaTitulo.StyleController = Me.DistribucionPrincipal
        Me.EtiquetaTitulo.TabIndex = 5
        Me.EtiquetaTitulo.Text = "[Titulo]"
        '
        'BotonCerrar
        '
        Me.BotonCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BotonCerrar.Location = New System.Drawing.Point(201, 114)
        Me.BotonCerrar.Name = "BotonCerrar"
        Me.BotonCerrar.Size = New System.Drawing.Size(71, 22)
        Me.BotonCerrar.StyleController = Me.DistribucionPrincipal
        Me.BotonCerrar.TabIndex = 4
        Me.BotonCerrar.Text = "Cerrar"
        '
        'DistribucionGrupoPrincipal
        '
        Me.DistribucionGrupoPrincipal.CustomizationFormText = "DistribucionPrincipal"
        Me.DistribucionGrupoPrincipal.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.DistribucionGrupoPrincipal.GroupBordersVisible = False
        Me.DistribucionGrupoPrincipal.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EspacioVacio1, Me.EspacioVacio2, Me.EspacioParaBotonCerrar, Me.EspacioParaEtiquetaTitulo, Me.EspacioParaEtiquetaVersion, Me.LayoutControlItem1, Me.LayoutControlItem2})
        Me.DistribucionGrupoPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.DistribucionGrupoPrincipal.Name = "DistribucionGrupoPrincipal"
        Me.DistribucionGrupoPrincipal.Size = New System.Drawing.Size(284, 148)
        Me.DistribucionGrupoPrincipal.Text = "DistribucionGrupoPrincipal"
        Me.DistribucionGrupoPrincipal.TextVisible = False
        '
        'EspacioVacio1
        '
        Me.EspacioVacio1.AllowHotTrack = False
        Me.EspacioVacio1.CustomizationFormText = "EspacioVacio1"
        Me.EspacioVacio1.Location = New System.Drawing.Point(0, 34)
        Me.EspacioVacio1.Name = "EspacioVacio1"
        Me.EspacioVacio1.Size = New System.Drawing.Size(264, 34)
        Me.EspacioVacio1.Text = "EspacioVacio1"
        Me.EspacioVacio1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EspacioVacio2
        '
        Me.EspacioVacio2.AllowHotTrack = False
        Me.EspacioVacio2.CustomizationFormText = "EspacioVacio2"
        Me.EspacioVacio2.Location = New System.Drawing.Point(0, 102)
        Me.EspacioVacio2.Name = "EspacioVacio2"
        Me.EspacioVacio2.Size = New System.Drawing.Size(189, 26)
        Me.EspacioVacio2.Text = "EspacioVacio2"
        Me.EspacioVacio2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EspacioParaBotonCerrar
        '
        Me.EspacioParaBotonCerrar.Control = Me.BotonCerrar
        Me.EspacioParaBotonCerrar.CustomizationFormText = "EspacioParaBotonCerrar"
        Me.EspacioParaBotonCerrar.Location = New System.Drawing.Point(189, 102)
        Me.EspacioParaBotonCerrar.MaxSize = New System.Drawing.Size(75, 26)
        Me.EspacioParaBotonCerrar.MinSize = New System.Drawing.Size(75, 26)
        Me.EspacioParaBotonCerrar.Name = "EspacioParaBotonCerrar"
        Me.EspacioParaBotonCerrar.Size = New System.Drawing.Size(75, 26)
        Me.EspacioParaBotonCerrar.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EspacioParaBotonCerrar.Text = "EspacioParaBotonCerrar"
        Me.EspacioParaBotonCerrar.TextSize = New System.Drawing.Size(0, 0)
        Me.EspacioParaBotonCerrar.TextToControlDistance = 0
        Me.EspacioParaBotonCerrar.TextVisible = False
        '
        'EspacioParaEtiquetaTitulo
        '
        Me.EspacioParaEtiquetaTitulo.Control = Me.EtiquetaTitulo
        Me.EspacioParaEtiquetaTitulo.CustomizationFormText = "EspacioParaEtiquetaTitulo"
        Me.EspacioParaEtiquetaTitulo.Location = New System.Drawing.Point(0, 0)
        Me.EspacioParaEtiquetaTitulo.Name = "EspacioParaEtiquetaTitulo"
        Me.EspacioParaEtiquetaTitulo.Size = New System.Drawing.Size(264, 17)
        Me.EspacioParaEtiquetaTitulo.Text = "EspacioParaEtiquetaTitulo"
        Me.EspacioParaEtiquetaTitulo.TextSize = New System.Drawing.Size(0, 0)
        Me.EspacioParaEtiquetaTitulo.TextToControlDistance = 0
        Me.EspacioParaEtiquetaTitulo.TextVisible = False
        '
        'EspacioParaEtiquetaVersion
        '
        Me.EspacioParaEtiquetaVersion.Control = Me.EtiquetaVersion
        Me.EspacioParaEtiquetaVersion.CustomizationFormText = "EspacioParaEtiquetaVersion"
        Me.EspacioParaEtiquetaVersion.Location = New System.Drawing.Point(0, 17)
        Me.EspacioParaEtiquetaVersion.Name = "EspacioParaEtiquetaVersion"
        Me.EspacioParaEtiquetaVersion.Size = New System.Drawing.Size(264, 17)
        Me.EspacioParaEtiquetaVersion.Text = "EspacioParaEtiquetaVersion"
        Me.EspacioParaEtiquetaVersion.TextSize = New System.Drawing.Size(0, 0)
        Me.EspacioParaEtiquetaVersion.TextToControlDistance = 0
        Me.EspacioParaEtiquetaVersion.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.EtiquetaTrademark
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(264, 17)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.EtiquetaCopyright
        Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 85)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(264, 17)
        Me.LayoutControlItem2.Text = "LayoutControlItem2"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextToControlDistance = 0
        Me.LayoutControlItem2.TextVisible = False
        '
        'FormaAcercaDe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.CancelButton = Me.BotonCerrar
        Me.ClientSize = New System.Drawing.Size(284, 148)
        Me.Controls.Add(Me.DistribucionPrincipal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormaAcercaDe"
        Me.ShowInTaskbar = False
        Me.Text = "Acerca de ..."
        Me.UsaSabana = True
        CType(Me.DistribucionPrincipal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DistribucionPrincipal.ResumeLayout(False)
        CType(Me.DistribucionGrupoPrincipal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioVacio1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioVacio2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioParaBotonCerrar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioParaEtiquetaTitulo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EspacioParaEtiquetaVersion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DistribucionPrincipal As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DistribucionGrupoPrincipal As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EspacioVacio1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EspacioVacio2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents BotonCerrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EspacioParaBotonCerrar As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaVersion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EtiquetaTitulo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EspacioParaEtiquetaTitulo As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EspacioParaEtiquetaVersion As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaTrademark As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaCopyright As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem

End Class
