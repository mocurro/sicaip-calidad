﻿Public Class Reclamo
    Private MiFolio As String

    Private MiNombreArea As String

    Public Property Folio As String
        Get
            Return CType(cve_reclamo.ToString.PadLeft(5, CChar("0")), String)
        End Get
        Set(value As String)
            MiFolio = value
        End Set
    End Property

    Public Property NombreArea As String
        Get
            Return MiNombreArea
        End Get
        Set(value As String)
            MiNombreArea = value
        End Set
    End Property
    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case estatus
                Case ClienteEstatus.Activo
                    Return "Generado"
                Case ClienteEstatus.Inactivo
                    Return "Atendido"

                Case 3
                    Return "Rechazado"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property

    Public ReadOnly Property Client As Cliente
        Get
            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("cve_cliente_modelo", cve_cliente_modelo)
            Dim MiCliente = Datos.Fabricas.Cliente.ObtenerExistentesParaSerializar(Filtro)
            Return MiCliente.SingleOrDefault
        End Get
    End Property

    Public ReadOnly Property Model As Modelo
        Get
            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("cve_cliente_modelo", cve_cliente_modelo)
            Dim Mimodelo = Datos.Fabricas.Modelo.ObtenerExistentesParaSerializar(Filtro)
            Return Mimodelo.SingleOrDefault
        End Get
    End Property

    Public ReadOnly Property Component As componente
        Get
            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("cve_cliente_modelo", cve_cliente_modelo)
            Dim Mimodelo = Datos.Fabricas.Componente.ObtenerExistentesParaSerializar(Filtro)
            Return Mimodelo.SingleOrDefault
        End Get
    End Property

    Public ReadOnly Property Defect As Defecto
        Get
            Dim MiDefect = Datos.Fabricas.Defecto.ObtenerUno(cve_defecto)
            Return MiDefect
        End Get
    End Property

    Public ReadOnly Property User As SEGURIDAD_USUARIO
        Get
            Dim MiUser = Datos.Fabricas.Seguridad_Usuarios.ObtenerUno(cve_usuario_reporta)
            Return MiUser
        End Get
    End Property

    ' Public ReadOnly Property Unidad_Nego As Unidad_Negocio 
    'Get
    '        Dim Unidad_Neg=Datos.Fabricas .Unidad_Negocio   .ObtenerUno (cve_unidad_negocio   )
    '        Return Unidad_Neg 
    'End Get
    'End Property

End Class
