﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface genérica para fábrica de entidades de Zeus.
''' </summary>
''' <typeparam name="TTipoEntidad">Define el tipo específico de entidad que maneja la fábrica.</typeparam>
''' <remarks>Todas las fábricas de objetos de datos basados en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IFabricaGenerico(Of TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    Sub Destruir(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    Sub Borrar(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    Sub Recuperar(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Sub Guardar(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Function ObtenerUno(ByVal Id As Guid) As TTipoEntidad

        ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Function ObtenerUno(ByVal Id As Integer ) As TTipoEntidad

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Function ObtenerUnoParaSerializar(ByVal Id As Guid) As TTipoEntidad

    Function ObtenerUnoParaSerializar(ByVal Id As String) As TTipoEntidad

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes.
    ''' </summary>
    Function ObtenerExistentes() As List(Of TTipoEntidad)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Function ObtenerExistentes(ByVal Filtro As Dictionary(Of String, Object)) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes sin traer datos relacionados
    ''' </summary>
    Function ObtenerExistentesParaSerializar() As List(Of TTipoEntidad)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro sin traer datos relacionados
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Function ObtenerExistentesParaSerializar(ByVal Filtro As Dictionary(Of String, Object)) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    Function CrearNuevo() As TTipoEntidad

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    Function Deserializar(ByVal EntidadSerializada As String) As TTipoEntidad

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se inicializará como nueva o como existente.</param>
    Function Deserializar(ByVal EntidadSerializada As String, ByVal InicializarComoNuevo As Boolean) As TTipoEntidad

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    Function DeserializarLista(ByVal ListaSerializada As List(Of String)) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se inicializará como nueva o como existente.</param>
    Function DeserializarLista(ByVal ListaSerializada As List(Of String), ByVal InicializarComoNuevo As Boolean) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    Function DeserializarLista(ByVal ListaSerializada() As String) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se inicializará como nueva o como existente.</param>
    Function DeserializarLista(ByVal ListaSerializada() As String, ByVal InicializarComoNuevo As Boolean) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Serializa una lista de entidades en su información correspondiente.
    ''' </summary>
    ''' <param name="Lista">La lista de entidades a serializar.</param>
    Function SerializarLista(ByVal Lista As List(Of TTipoEntidad)) As List(Of String)

End Interface