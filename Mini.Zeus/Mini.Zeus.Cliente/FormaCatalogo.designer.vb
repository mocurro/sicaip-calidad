﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormaCatalogo
    Inherits FormaPestaña

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Barras = New DevExpress.XtraBars.BarManager()
        Me.BarraHerramientas = New DevExpress.XtraBars.Bar()
        Me.BotonAgregar = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonModificar = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonBorrar = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonActualizar = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonExportar = New DevExpress.XtraBars.BarSubItem()
        Me.BotonExportarXLSX = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonExportarCSV = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonExportarHTML = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonExportarPDF = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonVerDetalle = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonDestruir = New DevExpress.XtraBars.BarButtonItem()
        Me.BotonRecuperar = New DevExpress.XtraBars.BarButtonItem()
        Me.BarraMenu = New DevExpress.XtraBars.Bar()
        Me.BarraEstado = New DevExpress.XtraBars.Bar()
        Me.BarraEtiquetaCopyright = New DevExpress.XtraBars.BarStaticItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BotonExportarXLS = New DevExpress.XtraBars.BarButtonItem()
        Me.GridCatalogo = New DevExpress.XtraGrid.GridControl()
        Me.FuenteDatos = New System.Windows.Forms.BindingSource()
        Me.VistaCatalogo = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DialogoExportar = New System.Windows.Forms.SaveFileDialog()
        Me.PanelFiltro = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutFiltro = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutFiltroRaiz = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.PanelMensaje = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutMensaje = New DevExpress.XtraLayout.LayoutControl()
        Me.EtiquetaTiempoMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.ProgresoDefinidoMensaje = New DevExpress.XtraEditors.ProgressBarControl()
        Me.ProgresoMensaje = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.EtiquetaMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.ImagenMensaje = New System.Windows.Forms.PictureBox()
        Me.LayoutMensajeRaiz = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.ItemForImagenMensaje = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemForEtiquetaMensaje = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemForProgresoMensaje = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ItemForProgresoDefinidoMensaje = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.HiloActualizarVentana = New System.ComponentModel.BackgroundWorker()
        Me.HiloAgregar = New System.ComponentModel.BackgroundWorker()
        Me.HiloEditar = New System.ComponentModel.BackgroundWorker()
        Me.HiloBorrar = New System.ComponentModel.BackgroundWorker()
        Me.HiloExportar = New System.ComponentModel.BackgroundWorker()
        Me.HiloVerDetalles = New System.ComponentModel.BackgroundWorker()
        Me.HiloDestruir = New System.ComponentModel.BackgroundWorker()
        Me.RelojProgreso = New System.Windows.Forms.Timer()
        CType(Me.Barras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridCatalogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VistaCatalogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelFiltro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelFiltro.SuspendLayout()
        CType(Me.LayoutFiltro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutFiltroRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelMensaje.SuspendLayout()
        CType(Me.LayoutMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutMensaje.SuspendLayout()
        CType(Me.ProgresoDefinidoMensaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgresoMensaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImagenMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMensajeRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForImagenMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForEtiquetaMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForProgresoMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForProgresoDefinidoMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Barras
        '
        Me.Barras.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.BarraHerramientas, Me.BarraMenu, Me.BarraEstado})
        Me.Barras.DockControls.Add(Me.barDockControlTop)
        Me.Barras.DockControls.Add(Me.barDockControlBottom)
        Me.Barras.DockControls.Add(Me.barDockControlLeft)
        Me.Barras.DockControls.Add(Me.barDockControlRight)
        Me.Barras.Form = Me
        Me.Barras.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BotonAgregar, Me.BotonModificar, Me.BotonBorrar, Me.BotonVerDetalle, Me.BotonActualizar, Me.BotonDestruir, Me.BotonRecuperar, Me.BarraEtiquetaCopyright, Me.BotonExportar, Me.BotonExportarXLSX, Me.BotonExportarXLS, Me.BotonExportarCSV, Me.BotonExportarPDF, Me.BotonExportarHTML})
        Me.Barras.MainMenu = Me.BarraMenu
        Me.Barras.MaxItemId = 15
        Me.Barras.StatusBar = Me.BarraEstado
        '
        'BarraHerramientas
        '
        Me.BarraHerramientas.BarName = "BarraHerramientas"
        Me.BarraHerramientas.DockCol = 0
        Me.BarraHerramientas.DockRow = 0
        Me.BarraHerramientas.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.BarraHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonAgregar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonModificar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonBorrar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonActualizar, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.BotonExportar), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonVerDetalle, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonDestruir, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BotonRecuperar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.BarraHerramientas.OptionsBar.AllowQuickCustomization = False
        Me.BarraHerramientas.OptionsBar.DisableClose = True
        Me.BarraHerramientas.OptionsBar.DisableCustomization = True
        Me.BarraHerramientas.OptionsBar.DrawDragBorder = False
        Me.BarraHerramientas.OptionsBar.MultiLine = True
        Me.BarraHerramientas.OptionsBar.UseWholeRow = True
        Me.BarraHerramientas.Text = "BarraHerramientas"
        '
        'BotonAgregar
        '
        Me.BotonAgregar.Caption = "&Agregar"
        Me.BotonAgregar.Id = 0
        Me.BotonAgregar.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Insert)
        Me.BotonAgregar.Name = "BotonAgregar"
        '
        'BotonModificar
        '
        Me.BotonModificar.Caption = "&Modificar"
        Me.BotonModificar.Id = 1
        Me.BotonModificar.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2)
        Me.BotonModificar.Name = "BotonModificar"
        '
        'BotonBorrar
        '
        Me.BotonBorrar.Caption = "&Borrar"
        Me.BotonBorrar.Id = 2
        Me.BotonBorrar.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete)
        Me.BotonBorrar.Name = "BotonBorrar"
        '
        'BotonActualizar
        '
        Me.BotonActualizar.Caption = "A&ctualizar"
        Me.BotonActualizar.Id = 4
        Me.BotonActualizar.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5)
        Me.BotonActualizar.Name = "BotonActualizar"
        '
        'BotonExportar
        '
        Me.BotonExportar.Caption = "E&xportar"
        Me.BotonExportar.Id = 9
        Me.BotonExportar.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BotonExportarXLSX), New DevExpress.XtraBars.LinkPersistInfo(Me.BotonExportarCSV), New DevExpress.XtraBars.LinkPersistInfo(Me.BotonExportarHTML), New DevExpress.XtraBars.LinkPersistInfo(Me.BotonExportarPDF)})
        Me.BotonExportar.Name = "BotonExportar"
        Me.BotonExportar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BotonExportarXLSX
        '
        Me.BotonExportarXLSX.Caption = "&Excel 2007"
        Me.BotonExportarXLSX.Id = 10
        Me.BotonExportarXLSX.Name = "BotonExportarXLSX"
        '
        'BotonExportarCSV
        '
        Me.BotonExportarCSV.Caption = "&Texto separado por comas"
        Me.BotonExportarCSV.Id = 12
        Me.BotonExportarCSV.Name = "BotonExportarCSV"
        '
        'BotonExportarHTML
        '
        Me.BotonExportarHTML.Caption = "&Página web"
        Me.BotonExportarHTML.Id = 14
        Me.BotonExportarHTML.Name = "BotonExportarHTML"
        '
        'BotonExportarPDF
        '
        Me.BotonExportarPDF.Caption = "&PDF"
        Me.BotonExportarPDF.Id = 13
        Me.BotonExportarPDF.Name = "BotonExportarPDF"
        '
        'BotonVerDetalle
        '
        Me.BotonVerDetalle.Caption = "&Ver detalles"
        Me.BotonVerDetalle.Id = 3
        Me.BotonVerDetalle.Name = "BotonVerDetalle"
        Me.BotonVerDetalle.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BotonDestruir
        '
        Me.BotonDestruir.Caption = "&Destruir"
        Me.BotonDestruir.Id = 6
        Me.BotonDestruir.Name = "BotonDestruir"
        Me.BotonDestruir.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BotonRecuperar
        '
        Me.BotonRecuperar.Caption = "&Recuperar"
        Me.BotonRecuperar.Id = 7
        Me.BotonRecuperar.Name = "BotonRecuperar"
        Me.BotonRecuperar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarraMenu
        '
        Me.BarraMenu.BarName = "BarraMenu"
        Me.BarraMenu.DockCol = 0
        Me.BarraMenu.DockRow = 1
        Me.BarraMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.BarraMenu.OptionsBar.MultiLine = True
        Me.BarraMenu.OptionsBar.UseWholeRow = True
        Me.BarraMenu.Text = "BarraMenu"
        Me.BarraMenu.Visible = False
        '
        'BarraEstado
        '
        Me.BarraEstado.BarName = "BarraEstado"
        Me.BarraEstado.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.BarraEstado.DockCol = 0
        Me.BarraEstado.DockRow = 0
        Me.BarraEstado.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.BarraEstado.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarraEtiquetaCopyright)})
        Me.BarraEstado.OptionsBar.AllowQuickCustomization = False
        Me.BarraEstado.OptionsBar.DrawDragBorder = False
        Me.BarraEstado.OptionsBar.UseWholeRow = True
        Me.BarraEstado.Text = "BarraEstado"
        '
        'BarraEtiquetaCopyright
        '
        Me.BarraEtiquetaCopyright.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarraEtiquetaCopyright.Caption = "[Trademark | Copyright]"
        Me.BarraEtiquetaCopyright.Id = 8
        Me.BarraEtiquetaCopyright.Name = "BarraEtiquetaCopyright"
        Me.BarraEtiquetaCopyright.Size = New System.Drawing.Size(32, 0)
        Me.BarraEtiquetaCopyright.Width = 32
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.Barras
        Me.barDockControlTop.Size = New System.Drawing.Size(624, 49)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 417)
        Me.barDockControlBottom.Manager = Me.Barras
        Me.barDockControlBottom.Size = New System.Drawing.Size(624, 25)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 49)
        Me.barDockControlLeft.Manager = Me.Barras
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 368)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(624, 49)
        Me.barDockControlRight.Manager = Me.Barras
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 368)
        '
        'BotonExportarXLS
        '
        Me.BotonExportarXLS.Caption = "E&xcel 2003"
        Me.BotonExportarXLS.Id = 11
        Me.BotonExportarXLS.Name = "BotonExportarXLS"
        '
        'GridCatalogo
        '
        Me.GridCatalogo.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridCatalogo.DataSource = Me.FuenteDatos
        Me.GridCatalogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridCatalogo.Location = New System.Drawing.Point(0, 169)
        Me.GridCatalogo.MainView = Me.VistaCatalogo
        Me.GridCatalogo.MenuManager = Me.Barras
        Me.GridCatalogo.Name = "GridCatalogo"
        Me.GridCatalogo.ShowOnlyPredefinedDetails = True
        Me.GridCatalogo.Size = New System.Drawing.Size(624, 248)
        Me.GridCatalogo.TabIndex = 0
        Me.GridCatalogo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VistaCatalogo})
        '
        'FuenteDatos
        '
        Me.FuenteDatos.AllowNew = False
        '
        'VistaCatalogo
        '
        Me.VistaCatalogo.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.VistaCatalogo.GridControl = Me.GridCatalogo
        Me.VistaCatalogo.GroupPanelText = "Arrastra el encabezado de una columna aquí para agrupar en base a dicha columna"
        Me.VistaCatalogo.Name = "VistaCatalogo"
        Me.VistaCatalogo.OptionsBehavior.AutoExpandAllGroups = True
        Me.VistaCatalogo.OptionsBehavior.AutoPopulateColumns = False
        Me.VistaCatalogo.OptionsBehavior.Editable = False
        Me.VistaCatalogo.OptionsBehavior.ReadOnly = True
        Me.VistaCatalogo.OptionsCustomization.AllowRowSizing = True
        Me.VistaCatalogo.OptionsFilter.UseNewCustomFilterDialog = True
        Me.VistaCatalogo.OptionsMenu.EnableFooterMenu = False
        Me.VistaCatalogo.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.VistaCatalogo.OptionsSelection.MultiSelect = True
        Me.VistaCatalogo.OptionsView.EnableAppearanceEvenRow = True
        Me.VistaCatalogo.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button
        Me.VistaCatalogo.OptionsView.RowAutoHeight = True
        Me.VistaCatalogo.OptionsView.ShowAutoFilterRow = True
        Me.VistaCatalogo.OptionsView.ShowFooter = True
        '
        'DialogoExportar
        '
        Me.DialogoExportar.DefaultExt = "xlsx"
        Me.DialogoExportar.Filter = "Archivos de Microsoft Excel|*.xls;*.xlsx|Todos los archivos|*.*"
        Me.DialogoExportar.SupportMultiDottedExtensions = True
        Me.DialogoExportar.Title = "Ubicación para exportar datos"
        '
        'PanelFiltro
        '
        Me.PanelFiltro.Controls.Add(Me.LayoutFiltro)
        Me.PanelFiltro.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelFiltro.Location = New System.Drawing.Point(0, 121)
        Me.PanelFiltro.Name = "PanelFiltro"
        Me.PanelFiltro.Size = New System.Drawing.Size(624, 48)
        Me.PanelFiltro.TabIndex = 5
        Me.PanelFiltro.Visible = False
        '
        'LayoutFiltro
        '
        Me.LayoutFiltro.AllowCustomization = False
        Me.LayoutFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutFiltro.Location = New System.Drawing.Point(2, 2)
        Me.LayoutFiltro.Name = "LayoutFiltro"
        Me.LayoutFiltro.OptionsView.AllowHotTrack = True
        Me.LayoutFiltro.Root = Me.LayoutFiltroRaiz
        Me.LayoutFiltro.Size = New System.Drawing.Size(620, 44)
        Me.LayoutFiltro.TabIndex = 0
        Me.LayoutFiltro.Text = "LayoutControl1"
        '
        'LayoutFiltroRaiz
        '
        Me.LayoutFiltroRaiz.CustomizationFormText = "LayoutFiltroRaiz"
        Me.LayoutFiltroRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutFiltroRaiz.GroupBordersVisible = False
        Me.LayoutFiltroRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutFiltroRaiz.Name = "LayoutFiltroRaiz"
        Me.LayoutFiltroRaiz.Size = New System.Drawing.Size(620, 44)
        Me.LayoutFiltroRaiz.TextVisible = False
        '
        'PanelMensaje
        '
        Me.PanelMensaje.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.PanelMensaje.Appearance.Options.UseBackColor = True
        Me.PanelMensaje.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelMensaje.Controls.Add(Me.LayoutMensaje)
        Me.PanelMensaje.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelMensaje.Location = New System.Drawing.Point(0, 49)
        Me.PanelMensaje.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelMensaje.LookAndFeel.UseWindowsXPTheme = True
        Me.PanelMensaje.Name = "PanelMensaje"
        Me.PanelMensaje.Size = New System.Drawing.Size(624, 72)
        Me.PanelMensaje.TabIndex = 10
        Me.PanelMensaje.Visible = False
        '
        'LayoutMensaje
        '
        Me.LayoutMensaje.AllowCustomization = False
        Me.LayoutMensaje.Controls.Add(Me.EtiquetaTiempoMensaje)
        Me.LayoutMensaje.Controls.Add(Me.ProgresoDefinidoMensaje)
        Me.LayoutMensaje.Controls.Add(Me.ProgresoMensaje)
        Me.LayoutMensaje.Controls.Add(Me.EtiquetaMensaje)
        Me.LayoutMensaje.Controls.Add(Me.ImagenMensaje)
        Me.LayoutMensaje.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutMensaje.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMensaje.Name = "LayoutMensaje"
        Me.LayoutMensaje.Root = Me.LayoutMensajeRaiz
        Me.LayoutMensaje.Size = New System.Drawing.Size(624, 72)
        Me.LayoutMensaje.TabIndex = 0
        Me.LayoutMensaje.Text = "LayoutControl1"
        '
        'EtiquetaTiempoMensaje
        '
        Me.EtiquetaTiempoMensaje.Location = New System.Drawing.Point(567, 54)
        Me.EtiquetaTiempoMensaje.Name = "EtiquetaTiempoMensaje"
        Me.EtiquetaTiempoMensaje.Size = New System.Drawing.Size(52, 13)
        Me.EtiquetaTiempoMensaje.StyleController = Me.LayoutMensaje
        Me.EtiquetaTiempoMensaje.TabIndex = 8
        Me.EtiquetaTiempoMensaje.Text = "[00:00:00]"
        '
        'ProgresoDefinidoMensaje
        '
        Me.ProgresoDefinidoMensaje.Location = New System.Drawing.Point(62, 44)
        Me.ProgresoDefinidoMensaje.MenuManager = Me.Barras
        Me.ProgresoDefinidoMensaje.Name = "ProgresoDefinidoMensaje"
        Me.ProgresoDefinidoMensaje.Size = New System.Drawing.Size(495, 18)
        Me.ProgresoDefinidoMensaje.StyleController = Me.LayoutMensaje
        Me.ProgresoDefinidoMensaje.TabIndex = 7
        '
        'ProgresoMensaje
        '
        Me.ProgresoMensaje.EditValue = 0
        Me.ProgresoMensaje.Location = New System.Drawing.Point(62, 28)
        Me.ProgresoMensaje.MenuManager = Me.Barras
        Me.ProgresoMensaje.Name = "ProgresoMensaje"
        Me.ProgresoMensaje.Size = New System.Drawing.Size(495, 6)
        Me.ProgresoMensaje.StyleController = Me.LayoutMensaje
        Me.ProgresoMensaje.TabIndex = 6
        '
        'EtiquetaMensaje
        '
        Me.EtiquetaMensaje.AllowHtmlString = True
        Me.EtiquetaMensaje.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaMensaje.Appearance.ForeColor = System.Drawing.SystemColors.InfoText
        Me.EtiquetaMensaje.Appearance.Options.UseFont = True
        Me.EtiquetaMensaje.Appearance.Options.UseForeColor = True
        Me.EtiquetaMensaje.Appearance.Options.UseTextOptions = True
        Me.EtiquetaMensaje.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.EtiquetaMensaje.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaMensaje.Location = New System.Drawing.Point(62, 5)
        Me.EtiquetaMensaje.Name = "EtiquetaMensaje"
        Me.EtiquetaMensaje.Size = New System.Drawing.Size(495, 13)
        Me.EtiquetaMensaje.StyleController = Me.LayoutMensaje
        Me.EtiquetaMensaje.TabIndex = 5
        Me.EtiquetaMensaje.Text = "[Mensaje]"
        '
        'ImagenMensaje
        '
        Me.ImagenMensaje.Location = New System.Drawing.Point(5, 5)
        Me.ImagenMensaje.Name = "ImagenMensaje"
        Me.ImagenMensaje.Size = New System.Drawing.Size(42, 42)
        Me.ImagenMensaje.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ImagenMensaje.TabIndex = 4
        Me.ImagenMensaje.TabStop = False
        '
        'LayoutMensajeRaiz
        '
        Me.LayoutMensajeRaiz.CustomizationFormText = "LayoutMensajeRaiz"
        Me.LayoutMensajeRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutMensajeRaiz.GroupBordersVisible = False
        Me.LayoutMensajeRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.ItemForImagenMensaje, Me.ItemForEtiquetaMensaje, Me.ItemForProgresoMensaje, Me.ItemForProgresoDefinidoMensaje, Me.LayoutControlItem1, Me.EmptySpaceItem1})
        Me.LayoutMensajeRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMensajeRaiz.Name = "LayoutMensajeRaiz"
        Me.LayoutMensajeRaiz.Size = New System.Drawing.Size(624, 72)
        Me.LayoutMensajeRaiz.TextVisible = False
        '
        'ItemForImagenMensaje
        '
        Me.ItemForImagenMensaje.Control = Me.ImagenMensaje
        Me.ItemForImagenMensaje.CustomizationFormText = "ItemForImagenMensaje"
        Me.ItemForImagenMensaje.Location = New System.Drawing.Point(0, 0)
        Me.ItemForImagenMensaje.MaxSize = New System.Drawing.Size(57, 52)
        Me.ItemForImagenMensaje.MinSize = New System.Drawing.Size(57, 52)
        Me.ItemForImagenMensaje.Name = "ItemForImagenMensaje"
        Me.ItemForImagenMensaje.Size = New System.Drawing.Size(57, 72)
        Me.ItemForImagenMensaje.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForImagenMensaje.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 5, 0, 0)
        Me.ItemForImagenMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForImagenMensaje.TextVisible = False
        '
        'ItemForEtiquetaMensaje
        '
        Me.ItemForEtiquetaMensaje.Control = Me.EtiquetaMensaje
        Me.ItemForEtiquetaMensaje.CustomizationFormText = "ItemForEtiquetaMensaje"
        Me.ItemForEtiquetaMensaje.Location = New System.Drawing.Point(57, 0)
        Me.ItemForEtiquetaMensaje.Name = "ItemForEtiquetaMensaje"
        Me.ItemForEtiquetaMensaje.Size = New System.Drawing.Size(505, 23)
        Me.ItemForEtiquetaMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForEtiquetaMensaje.TextVisible = False
        '
        'ItemForProgresoMensaje
        '
        Me.ItemForProgresoMensaje.Control = Me.ProgresoMensaje
        Me.ItemForProgresoMensaje.CustomizationFormText = "ItemForProgresoMensaje"
        Me.ItemForProgresoMensaje.Location = New System.Drawing.Point(57, 23)
        Me.ItemForProgresoMensaje.MaxSize = New System.Drawing.Size(0, 16)
        Me.ItemForProgresoMensaje.MinSize = New System.Drawing.Size(54, 16)
        Me.ItemForProgresoMensaje.Name = "ItemForProgresoMensaje"
        Me.ItemForProgresoMensaje.Size = New System.Drawing.Size(505, 16)
        Me.ItemForProgresoMensaje.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForProgresoMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForProgresoMensaje.TextVisible = False
        '
        'ItemForProgresoDefinidoMensaje
        '
        Me.ItemForProgresoDefinidoMensaje.Control = Me.ProgresoDefinidoMensaje
        Me.ItemForProgresoDefinidoMensaje.CustomizationFormText = "LayoutControlItem1"
        Me.ItemForProgresoDefinidoMensaje.Location = New System.Drawing.Point(57, 39)
        Me.ItemForProgresoDefinidoMensaje.Name = "ItemForProgresoDefinidoMensaje"
        Me.ItemForProgresoDefinidoMensaje.Size = New System.Drawing.Size(505, 33)
        Me.ItemForProgresoDefinidoMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForProgresoDefinidoMensaje.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.EtiquetaTiempoMensaje
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(562, 49)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(62, 23)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(562, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(62, 49)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'HiloActualizarVentana
        '
        '
        'HiloAgregar
        '
        '
        'HiloEditar
        '
        '
        'HiloBorrar
        '
        '
        'HiloExportar
        '
        '
        'HiloVerDetalles
        '
        '
        'HiloDestruir
        '
        '
        'RelojProgreso
        '
        Me.RelojProgreso.Interval = 1000
        '
        'FormaCatalogo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(624, 442)
        Me.Controls.Add(Me.GridCatalogo)
        Me.Controls.Add(Me.PanelFiltro)
        Me.Controls.Add(Me.PanelMensaje)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "FormaCatalogo"
        Me.Text = "Catalogo"
        CType(Me.Barras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridCatalogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VistaCatalogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelFiltro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelFiltro.ResumeLayout(False)
        CType(Me.LayoutFiltro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutFiltroRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelMensaje.ResumeLayout(False)
        CType(Me.LayoutMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutMensaje.ResumeLayout(False)
        CType(Me.ProgresoDefinidoMensaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgresoMensaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImagenMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMensajeRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForImagenMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForEtiquetaMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForProgresoMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForProgresoDefinidoMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Protected Friend WithEvents Barras As DevExpress.XtraBars.BarManager
    Protected Friend WithEvents BarraHerramientas As DevExpress.XtraBars.Bar
    Protected Friend WithEvents BarraMenu As DevExpress.XtraBars.Bar
    Protected Friend WithEvents BarraEstado As DevExpress.XtraBars.Bar
    Protected Friend WithEvents GridCatalogo As DevExpress.XtraGrid.GridControl
    Protected Friend WithEvents FuenteDatos As System.Windows.Forms.BindingSource
    Protected Friend WithEvents BotonAgregar As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonModificar As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonBorrar As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonVerDetalle As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonActualizar As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonDestruir As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents BotonRecuperar As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents VistaCatalogo As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DialogoExportar As System.Windows.Forms.SaveFileDialog
    Protected Friend WithEvents PanelFiltro As DevExpress.XtraEditors.PanelControl
    Protected Friend WithEvents LayoutFiltro As DevExpress.XtraLayout.LayoutControl
    Protected Friend WithEvents LayoutFiltroRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents PanelMensaje As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutMensaje As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutMensajeRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ImagenMensaje As System.Windows.Forms.PictureBox
    Friend WithEvents ItemForImagenMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ItemForEtiquetaMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ProgresoMensaje As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents ItemForProgresoMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents HiloActualizarVentana As System.ComponentModel.BackgroundWorker
    Friend WithEvents ProgresoDefinidoMensaje As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents ItemForProgresoDefinidoMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents HiloAgregar As System.ComponentModel.BackgroundWorker
    Friend WithEvents HiloEditar As System.ComponentModel.BackgroundWorker
    Friend WithEvents HiloBorrar As System.ComponentModel.BackgroundWorker
    Friend WithEvents HiloExportar As System.ComponentModel.BackgroundWorker
    Friend WithEvents HiloVerDetalles As System.ComponentModel.BackgroundWorker
    Friend WithEvents HiloDestruir As System.ComponentModel.BackgroundWorker
    Friend WithEvents BarraEtiquetaCopyright As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BotonExportar As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BotonExportarXLSX As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BotonExportarXLS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BotonExportarCSV As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BotonExportarPDF As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BotonExportarHTML As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents EtiquetaTiempoMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents RelojProgreso As System.Windows.Forms.Timer

End Class
