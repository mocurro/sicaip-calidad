﻿Imports System.ComponentModel
Imports System.IO
Imports Datos

Public Class TarjetaLABDIMSolicitud

    Private client As Datos.Defecto

    Private component As Datos.modelo

    Private MiRutaArchivo As String

    Private MiLabDimArchivo As Datos.LABDIMArchivo


    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoLABDIMRegistro))
        TiposVentanasAfectadas.Add(GetType(CatalogoLABDIMSolicitud))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.LABDIMSolicitudCampos, DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.LABDIMSolicitudCampos.cve_solicitud, itemSolicitante)
            .Add(Datos.LABDIMSolicitudCampos.cve_modelo, itemComponente)
            .Add(Datos.LABDIMSolicitudCampos.descripcion, ItemDescripción)
            .Add(Datos.LABDIMSolicitudCampos.no_piezas, ItemPiezas)
            .Add(Datos.LABDIMSolicitudCampos.comentarios, itemComentarios)
            .Add(Datos.LABDIMSolicitudCampos.nombreArchivo , itemDiseño )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.LABDIMSolicitudCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        '
        Select Case sender.Name
            Case SpinPiezas.Name
                Paquete.no_piezas = CInt(SpinPiezas.Value)
            Case txtComentarios.Name
                Paquete.comentarios = txtComentarios.Text.Trim
            Case ComboComponente.Name

                If ComboComponente.EditValue Isnot Nothing Then
                    Dim MiGUID As Long = CInt(ComboComponente.EditValue.ToString)
                    component = Datos.Fabricas.Modelo.ObtenerUno(MiGUID)
                    txtDescripcion.Text = component.descripcion
                    Paquete.cve_modelo = MiGUID
                    Paquete.descripcion = component.descripcion
                End If
               
                Return True

           Case combodiseño.Name 
                Paquete.NombreArchivo =combodiseño .EditValue .ToString 
                Return True 

        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        'Cargar los combos
        With EntidadDe(Of Datos.LABDIMSolicitud)()
            FuenteModelos.DataSource = CatalogoDe(Of CatalogoLABDIMSolicitud).Modelos  '.FindAll(Function(Item) Item )
        End With

        OmitirCambiosPersonalizados = True
        If paquete.cve_usuario = 0 Then
            paquete.cve_usuario = Datos.SesiónLocal.Usuario.CVE_Usuario
            txtSolicitante.Text = Datos.SesiónLocal.Usuario.Nombre
        Else
            txtSolicitante.Text = Datos.SesiónLocal.Usuario.Nombre
        End If
        txtDescripcion.Text = paquete.descripcion
        SpinPiezas.Value = paquete.no_piezas
        txtComentarios.Text = paquete.comentarios

        Dim Filtro = New Dictionary(Of String, Object)
        Filtro.Add("cv_solicitud", paquete.cve_solicitud)
        Dim MiListaArchivos = Fabricas.LABDIMArchivos.ObtenerExistentesParaSerializar(Filtro)

        If MiListaArchivos IsNot Nothing AndAlso MiListaArchivos.Count > 0 Then
            combodiseño.Text = MiListaArchivos(0).Nombre
            With EntidadDe(Of Datos.LABDIMSolicitud)()
                CatalogoDe(Of CatalogoLABDIMSolicitud).ArchivosNoEditados = MiListaArchivos
            End With
        Else
            With EntidadDe(Of Datos.LABDIMSolicitud)()
                If CatalogoDe(Of CatalogoLABDIMSolicitud).ArchivosNoEditados IsNot Nothing Then
                    CatalogoDe(Of CatalogoLABDIMSolicitud).ArchivosNoEditados.Clear()
                End If

            End With
        End If


        ComboComponente.EditValue = paquete.cve_modelo
        OmitirCambiosPersonalizados = False

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        'Cargar los combos
        Dim paquete = EntidadDe(Of Datos.LABDIMSolicitud)()
        paquete.Fecha_Solicitud = Date.Now

        Dim Milista As New List(Of Datos.LABDIMArchivo)
        If MiLabDimArchivo IsNot Nothing Then
            Milista.Add(MiLabDimArchivo)
            With EntidadDe(Of Datos.LABDIMSolicitud)()
                CatalogoDe(Of CatalogoLABDIMSolicitud).Archivos = Milista
            End With
        End If
    End Sub

    Private Sub LookUpEdit1_MouseClick(sender As Object, e As MouseEventArgs) Handles combodiseño.MouseClick
        Try
            Dim result As DialogResult = OpenFileDialog1.ShowDialog()
            ' Test result.
            If result = Windows.Forms.DialogResult.OK Then

                ' Get the file name.
                MiRutaArchivo = OpenFileDialog1.FileName
                combodiseño.ResetText

                combodiseño.Text = MiRutaArchivo
                combodiseño.Refresh

                MiLabDimArchivo = New Datos.LABDIMArchivo
                MiLabDimArchivo.Nombre = OpenFileDialog1.SafeFileName

                MiLabDimArchivo.Tipo = 0
                If File.Exists(MiRutaArchivo) Then
                    Dim fs As New FileStream(MiRutaArchivo, FileMode.Open)
                    Dim data(fs.Length) As Byte
                    fs.Read(data, 0, Convert.ToInt32(fs.Length))
                    MiLabDimArchivo.Archivo = data
                    fs.Close
                Else
                    MessageBox.Show("El archivo seleccionado no existe, favor de verificarlo")
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(String.Format("No se pudo cargar el archivo seleccionado por la siguiente razón: {0}", ex.Message))
        End Try

    End Sub

    Private Sub TarjetaLABDIMSolicitud_Closed(sender As Object, e As EventArgs) Handles Me.Closed

    End Sub

End Class