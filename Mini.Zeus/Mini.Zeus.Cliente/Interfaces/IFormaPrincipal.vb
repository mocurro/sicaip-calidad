﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas principales de Zeus.
''' </summary>
''' <remarks>Todas las ventanas principales basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface IFormaPrincipal

    ''' <summary>
    ''' Actualiza todas las pestañas que correspondan a un listado de pestañas afectadas.
    ''' </summary>
    ''' <param name="PestañasAfectadas">La lista de tipos de pestañas que deben de ser actualizadas.</param>
    Sub ActualizaPestañasAfectadas(ByVal PestañasAfectadas As List(Of Type))

    ''' <summary>
    ''' Muestra una ventana tipo pestaña en la pantalla.
    ''' </summary>
    ''' <param name="TipoPestaña">El tipo del catálogo a mostrar.</param>
    ''' <remarks>Esta función permite mostrar cualquier tipo de ventana que finalmente representa una pestaña. Esta función puede determinar si la pestaña ya está abierta, en cuyo caso le da el foco en lugar de abrir una nueva.</remarks>
    Sub MostrarPestaña(ByVal TipoPestaña As Type)

    ''' <summary>
    ''' Indica que una pestaña está iniciando un trabajo indeterminado.
    ''' </summary>
    Sub IniciaTrabajoPestaña()

    ''' <summary>
    ''' Indica que una pestaña ha terminado un trabajo indeterminado.
    ''' </summary>
    Sub TerminaTrabajoPestaña()

    ''' <summary>
    ''' Regresa el color para información según el tema.
    ''' </summary>
    ReadOnly Property ColorInfo() As Color

    ''' <summary>
    ''' Regresa el color del texto para información según el tema.
    ''' </summary>
    ReadOnly Property ColorInfoTexto() As Color

    ''' <summary>
    ''' Regresa el color de la ventana según el tema.
    ''' </summary>
    ReadOnly Property ColorVentana() As Color

    ''' <summary>
    ''' Regresa el color del texto para una ventana según el tema.
    ''' </summary>
    ReadOnly Property ColorVentanaTexto() As Color

    ''' <summary>
    ''' Regresa el color del control según el tema.
    ''' </summary>
    ReadOnly Property ColorControl() As Color

    ''' <summary>
    ''' Regresa el color del control deshabilitado según el tema.
    ''' </summary>
    ReadOnly Property ColorControlDeshabilitado() As Color

    ''' <summary>
    ''' Regresa el color del texto para un control según el tema.
    ''' </summary>
    ReadOnly Property ColorControlTexto() As Color

End Interface
