﻿Imports System.ComponentModel

Public Class TarjetaUsuario_Unidad_Negocio
    
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoUsuario_Unidad_Negocio ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Usuario_Unidad_NegocioCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Usuario_Unidad_NegocioCampos .Cve_Componente_Unidad_Negocio, ItemUnidadNegocio )
            .Add(Datos.Usuario_Unidad_NegocioCampos.CVE_Usuario, itemUsuario )
            .Add(Datos.Usuario_Unidad_NegocioCampos.Estatus , ItemEstatus  )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Usuario_Unidad_NegocioCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Usuario_Unidad_Negocio  )()
        
        Select Case sender.Name
            Case ComboUnidadNegocio .Name
                If ComboUnidadNegocio.EditValue IsNot Nothing Then
                    Dim MiGUID As Integer = CInt (ComboUnidadNegocio.EditValue.ToString )
                Paquete.cve_componente_unidad_negocio   = MiGUID
                End If
                
                Return True 
                
            Case ComboUsuario .Name
                If ComboUsuario.EditValue IsNot Nothing Then
                    Dim MiGUID As Long = CInt (ComboUsuario.EditValue.ToString )
                    Paquete.cve_usuario   = MiGUID
                    Paquete .estatus=CType(ComboEstatus .SelectedIndex, Byte?)
                End If
                
                Return True 

            Case ComboEstatus .Name 
                Paquete .estatus = CType(ComboEstatus .SelectedIndex, Byte?)

        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Usuario_Unidad_Negocio )()
        'Cargar los combos
        With EntidadDe(Of Datos.Usuario_Unidad_Negocio  )()
            FuenteUsuarios.DataSource = CatalogoDe(Of CatalogoUsuario_Unidad_Negocio  ).Usuarios ' .FindAll(Function(Item) Item.Estatus  = 0)
        End With
         'Cargar los combos
        With EntidadDe(Of Datos.Usuario_Unidad_Negocio )()
            FuenteUnidadNegocio .DataSource = CatalogoDe(Of CatalogoUsuario_Unidad_Negocio ).UnidadNegocio '.FindAll(Function(Item) Item )
        End With
        ComboUnidadNegocio .EditValue = paquete.cve_componente_unidad_negocio 
        OmitirCambiosPersonalizados = True
        If paquete.Estatus = 0 or paquete.estatus is Nothing  Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If
        
        Dim MiUnidad= CatalogoDe(Of CatalogoUsuario_Unidad_Negocio ).UnidadNegocio.FindAll(Function(Item) Item.cve_componente_unidad_negocio =paquete.cve_componente_unidad_negocio  )
        If MiUnidad IsNot Nothing AndAlso MiUnidad .Count >0 Then
            ComboUnidadNegocio.Text =MiUnidad (0).nombre 
            ComboUnidadNegocio .EditValue = paquete.cve_componente_unidad_negocio 
             ComboUnidadNegocio.Text =String .Empty 
            ComboUnidadNegocio .EditValue = paquete.cve_componente_unidad_negocio 
        End If
        

        ComboUsuario.EditValue = paquete.cve_usuario  
        
        OmitirCambiosPersonalizados = False 
        ComboUnidadNegocio.Refresh 

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Usuario_Unidad_Negocio )()
        
    End Sub
End Class