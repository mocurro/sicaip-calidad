﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Argumentos para eventos que indican el progreso de un proceso.
''' </summary>
''' <remarks>Estos argumentos permiten actualizar la interface visual para notificar progreso de procesos.</remarks>
Public Class ArgumentosProgresoProceso
    Inherits EventArgs

    ''' <summary>
    ''' Contiene el número de paso actual.
    ''' </summary>
    Private MiPasoActual As Integer

    ''' <summary>
    ''' Contiene el número de pasos totales.
    ''' </summary>
    Private MiPasosTotales As Integer

    ''' <summary>
    ''' Crea un nuevo argumento.
    ''' </summary>
    Public Sub New(ByVal PasoActual As Integer, ByVal PasosTotales As Integer)

        MyBase.New()
        MiPasoActual = PasoActual
        MiPasosTotales = PasosTotales

    End Sub

    ''' <summary>
    ''' El número de paso actual.
    ''' </summary>
    Public ReadOnly Property PasoActual() As Integer
        Get
            Return MiPasoActual
        End Get
    End Property

    ''' <summary>
    ''' El número de pasos totales.
    ''' </summary>
    Public ReadOnly Property PasosTotales() As Integer
        Get
            Return MiPasosTotales
        End Get
    End Property

    ''' <summary>
    ''' Regresa el porcentaje de avance que tiene el proceso.
    ''' </summary>
    ''' <remarks>Esta propiedad regresa un valor entero entre 0 y 100 que representa valores entre 0% y 100%</remarks>
    Public ReadOnly Property PorcentajeAvance() As Integer
        Get
            Return CInt((MiPasoActual / MiPasosTotales) * 100)
        End Get
    End Property

End Class