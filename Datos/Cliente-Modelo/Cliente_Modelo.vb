﻿Public Class Cliente_Modelo
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case ClienteEstatus.Activo 
                    Return "Activo"
                Case ClienteEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property

    Public ReadOnly Property Client As Cliente
    Get
            Dim MiCliente=Datos.Fabricas .Cliente .ObtenerUno (cve_cliente )
            Return MiCliente 
    End Get
    End Property

     Public ReadOnly Property Model As Modelo
    Get
            Dim MiModelo=Datos.Fabricas .Modelo  .ObtenerUno (cve_modelo  )
            Return MiModelo 
    End Get
    End Property

End Class
