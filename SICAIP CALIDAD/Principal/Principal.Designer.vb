﻿Imports Mini.Zeus.Cliente.UX

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits FormaPrincipalListon


    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.PestañaCatalogos = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnClientes = New DevExpress.XtraBars.BarButtonItem()
        Me.btnClienteModelo = New DevExpress.XtraBars.BarButtonItem()
        Me.btnDefectos = New DevExpress.XtraBars.BarButtonItem()
        Me.btnUnidadNegocio = New DevExpress.XtraBars.BarButtonItem()
        Me.btnComponenteUnidad = New DevExpress.XtraBars.BarButtonItem()
        Me.btnUnidadNegocioUsuarios = New DevExpress.XtraBars.BarButtonItem()
        Me.btnAreaUsuarios = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnSolicitud = New DevExpress.XtraBars.BarButtonItem()
        Me.btnTipoInspeccion = New DevExpress.XtraBars.BarButtonItem()
        Me.btnRegistro = New DevExpress.XtraBars.BarButtonItem()
        Me.PestañaAplicaciones = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnNRFTsys = New DevExpress.XtraBars.BarButtonItem()
        Me.btnLamDim = New DevExpress.XtraBars.BarButtonItem()
        Me.PestañaReportes = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnReporteReclamos = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnGraficos = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.ManejadorPestañas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Liston, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MenuAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolTipDefault
        '
        '
        '
        '
        Me.ToolTipDefault.DefaultController.AllowHtmlText = True
        Me.ToolTipDefault.DefaultController.AutoPopDelay = 10000
        Me.ToolTipDefault.DefaultController.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip
        '
        'MenuAplicacionSesionUsuario
        '
        Me.MenuAplicacionSesionUsuario.ImageOptions.LargeImage = CType(resources.GetObject("MenuAplicacionSesionUsuario.ImageOptions.LargeImage"), System.Drawing.Image)
        '
        'Liston
        '
        Me.Liston.ExpandCollapseItem.Id = 0
        Me.Liston.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnClientes, Me.btnClienteModelo, Me.btnDefectos, Me.btnUnidadNegocio, Me.btnComponenteUnidad, Me.btnUnidadNegocioUsuarios, Me.btnNRFTsys, Me.btnLamDim, Me.btnReporteReclamos, Me.btnAreaUsuarios, Me.btnSolicitud, Me.btnTipoInspeccion, Me.btnRegistro, Me.BtnGraficos})
        Me.Liston.MaxItemId = 23
        Me.Liston.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.PestañaCatalogos, Me.PestañaAplicaciones, Me.PestañaReportes})
        Me.Liston.Size = New System.Drawing.Size(742, 143)
        Me.Liston.Toolbar.ShowCustomizeItem = False
        '
        'BarraEstado
        '
        Me.BarraEstado.Location = New System.Drawing.Point(0, 475)
        Me.BarraEstado.Size = New System.Drawing.Size(742, 31)
        '
        'PanelCliente
        '
        Me.PanelCliente.Location = New System.Drawing.Point(0, 143)
        Me.PanelCliente.Size = New System.Drawing.Size(742, 332)
        '
        'PestañaCatalogos
        '
        Me.PestañaCatalogos.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup4})
        Me.PestañaCatalogos.Name = "PestañaCatalogos"
        Me.PestañaCatalogos.Text = "Catálogos"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnClientes)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnClienteModelo)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnDefectos)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnUnidadNegocio)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnComponenteUnidad)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnUnidadNegocioUsuarios)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnAreaUsuarios)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "NRFTSys"
        '
        'btnClientes
        '
        Me.btnClientes.Caption = "Clientes"
        Me.btnClientes.Id = 9
        Me.btnClientes.Name = "btnClientes"
        Me.btnClientes.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnClienteModelo
        '
        Me.btnClienteModelo.Caption = "Cliente - Modelo"
        Me.btnClienteModelo.Id = 10
        Me.btnClienteModelo.Name = "btnClienteModelo"
        Me.btnClienteModelo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnDefectos
        '
        Me.btnDefectos.Caption = "Defectos"
        Me.btnDefectos.Id = 11
        Me.btnDefectos.Name = "btnDefectos"
        Me.btnDefectos.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnUnidadNegocio
        '
        Me.btnUnidadNegocio.Caption = "Unidad de negocio"
        Me.btnUnidadNegocio.Id = 12
        Me.btnUnidadNegocio.Name = "btnUnidadNegocio"
        Me.btnUnidadNegocio.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnComponenteUnidad
        '
        Me.btnComponenteUnidad.Caption = "Componente - Unidad de negocio"
        Me.btnComponenteUnidad.Id = 13
        Me.btnComponenteUnidad.Name = "btnComponenteUnidad"
        Me.btnComponenteUnidad.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnUnidadNegocioUsuarios
        '
        Me.btnUnidadNegocioUsuarios.Caption = "Unidad de negocio - Usuarios"
        Me.btnUnidadNegocioUsuarios.Id = 14
        Me.btnUnidadNegocioUsuarios.Name = "btnUnidadNegocioUsuarios"
        Me.btnUnidadNegocioUsuarios.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnAreaUsuarios
        '
        Me.btnAreaUsuarios.Caption = "Sub área-Usuarios"
        Me.btnAreaUsuarios.Id = 18
        Me.btnAreaUsuarios.Name = "btnAreaUsuarios"
        Me.btnAreaUsuarios.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.btnSolicitud)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.btnTipoInspeccion)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.btnRegistro)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "LabDim"
        '
        'btnSolicitud
        '
        Me.btnSolicitud.Caption = "Solicitud"
        Me.btnSolicitud.Id = 19
        Me.btnSolicitud.Name = "btnSolicitud"
        Me.btnSolicitud.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnTipoInspeccion
        '
        Me.btnTipoInspeccion.Caption = "Tipo de inspección"
        Me.btnTipoInspeccion.Id = 20
        Me.btnTipoInspeccion.Name = "btnTipoInspeccion"
        Me.btnTipoInspeccion.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnRegistro
        '
        Me.btnRegistro.Caption = "Registro"
        Me.btnRegistro.Id = 21
        Me.btnRegistro.Name = "btnRegistro"
        Me.btnRegistro.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'PestañaAplicaciones
        '
        Me.PestañaAplicaciones.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.PestañaAplicaciones.Name = "PestañaAplicaciones"
        Me.PestañaAplicaciones.Text = "Aplicaciones"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.btnNRFTsys)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.btnLamDim)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Aplicaciones"
        '
        'btnNRFTsys
        '
        Me.btnNRFTsys.Caption = "NRFTsys"
        Me.btnNRFTsys.Id = 15
        Me.btnNRFTsys.Name = "btnNRFTsys"
        Me.btnNRFTsys.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'btnLamDim
        '
        Me.btnLamDim.Caption = "LabDim"
        Me.btnLamDim.Id = 16
        Me.btnLamDim.Name = "btnLamDim"
        Me.btnLamDim.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'PestañaReportes
        '
        Me.PestañaReportes.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup3})
        Me.PestañaReportes.Name = "PestañaReportes"
        Me.PestañaReportes.Text = "Reportes"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.btnReporteReclamos)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BtnGraficos)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Reportes"
        '
        'btnReporteReclamos
        '
        Me.btnReporteReclamos.Caption = "Reclamos"
        Me.btnReporteReclamos.Id = 17
        Me.btnReporteReclamos.Name = "btnReporteReclamos"
        Me.btnReporteReclamos.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BtnGraficos
        '
        Me.BtnGraficos.Caption = "Gráfico"
        Me.BtnGraficos.Id = 22
        Me.BtnGraficos.Name = "BtnGraficos"
        '
        'Principal
        '
        Me.ToolTipDefault.SetAllowHtmlText(Me, DevExpress.Utils.DefaultBoolean.[Default])
        Me.Appearance.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BaseDeDatos = "GKNSICAIP_V2"
        Me.ClientSize = New System.Drawing.Size(742, 506)
        Me.Contraseña = "VerifID2"
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Instancia = "desarrollo\des"
        Me.Name = "Principal"
        Me.Text = "Principal"
        Me.Usuario = "desarrollo"
        CType(Me.ManejadorPestañas,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Liston,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PanelCliente,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MenuAplicacion,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents btnClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PestañaCatalogos As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnClienteModelo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDefectos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUnidadNegocio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnComponenteUnidad As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUnidadNegocioUsuarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNRFTsys As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnLamDim As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PestañaAplicaciones As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnReporteReclamos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PestañaReportes As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnAreaUsuarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSolicitud As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnTipoInspeccion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnRegistro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnGraficos As DevExpress.XtraBars.BarButtonItem
End Class
