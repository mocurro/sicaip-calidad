﻿Imports System.Data.Linq
Imports Mini.Zeus.Cliente.UX

'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Fábrica de Defecto.
''' </summary>
''' <remarks>Esta fábrica permite administrar entidades de Clientes.</remarks>
Public Class Unidad_NegocioFabricas
    Inherits Fabrica(Of Unidad_Negocio)


    ''' <summary>
    ''' Crea una nueva fábrica dinámica de entidades Zeus.
    ''' </summary>
    ''' <remarks>La información de conexión se obtiene a partir de la información existente en Sincronización. Cada consulta a la información se obtiene en el momento, por lo que la fábrica puede reaccionar a cambios después de haber sido creada sin manipular sus valores de forma directa.</remarks>
    Public Sub New()

        MyBase.New()

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica local de entidades Zeus.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos local.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos local.</remarks>
    Public Sub New(ByVal CadenaConexion As String)

        MyBase.New(CadenaConexion)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica remota de entidades Zeus.
    ''' </summary>
    ''' <param name="UrlServicio">La ruta del servicio remoto.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos remota.</remarks>
    Public Sub New(ByVal UrlServicio As Uri)

        MyBase.New(UrlServicio)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica de entidades Zeus.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos local.</param>
    ''' <param name="UrlServicio">La ruta del servicio remoto.</param>
    ''' <param name="UbicacionFuenteDatos">Determina la ubicación inicial de la fuente de datos de la fábrica. Este valor puede ser modificado posteriormente.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos local.</remarks>
    Public Sub New(ByVal CadenaConexion As String, ByVal UrlServicio As Uri, ByVal UbicacionFuenteDatos As UbicacionesFuentesDatos)

        MyBase.New(CadenaConexion, UrlServicio, UbicacionFuenteDatos)

    End Sub

    ''' <summary>
    ''' Borra la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Defectos a borrar.</param>
    Protected Overrides Sub BorrarSQLServer(ByVal Entidad As Unidad_Negocio )

         Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Dim Objeto=Contexto .Unidad_Negocios .Single(Function(p As Unidad_Negocio ) p.cve_unidad_negocio    = Entidad.cve_unidad_negocio )
            Objeto.cuentaeliminado =1 
            Contexto.SubmitChanges 
        End Using

    End Sub

    ''' <summary>
    ''' Borra la moneda remota.
    ''' </summary>
    ''' <param name="Entidad">ubicacion a borrar.</param>
    Protected Overrides Sub BorrarServicioWeb(ByVal Entidad As Unidad_Negocio )

    End Sub

    ''' <summary>
    ''' Crear un nuevo.
    ''' </summary>
    Protected Overrides Function CrearNuevaEntidad() As Unidad_Negocio

        Dim Resultado = New Unidad_Negocio()' With {.ID_LineasProductos = Guid.NewGuid()}
        Return Resultado

    End Function

    ''' <summary>
    ''' Destruye la ubicacion local.
    ''' </summary>
    ''' <param name="Entidad">ubicacion a destruir.</param>
    Protected Overrides Sub DestruirSQLServer(ByVal Entidad As Unidad_Negocio )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
              Contexto.Unidad_Negocios   .DeleteOnSubmit (Entidad)
            Contexto.SubmitChanges 
            'Contexto.DestruirLineasProductosPorGuidLineasProductos(Entidad.ID_LineasProductos)
        End Using

    End Sub

    ''' <summary>
    ''' Destruye la moneda remota.
    ''' </summary>
    ''' <param name="Entidad">Moneda a destruir.</param>
    Protected Overrides Sub DestruirServicioWeb(ByVal Entidad As Unidad_Negocio )

    End Sub


    ''' <summary>
    ''' Agrega la información de la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Ubicacion a agregar.</param>
    Protected Overrides Sub GuardarNuevoSQLServer(ByVal Entidad As Unidad_Negocio )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto .Unidad_Negocios .InsertOnSubmit (Entidad )
            Contexto.SubmitChanges 
            'Contexto.AgregarLineasProductos(Entidad.ID_LineasProductos, Entidad.ID_Linea, Entidad.Id_Codigo, Entidad.Lote, Entidad.Metadatos, Entidad.CuentaEliminado)
        End Using

    End Sub

    ''' <summary>
    ''' Agrega la información de la Ubicacion remota.
    ''' </summary>
    ''' <param name="Entidad">Ubicacion a agregar.</param>
    Protected Overrides Sub GuardarNuevoServicioWeb(ByVal Entidad As Unidad_Negocio)

    End Sub

    ''' <summary>
    ''' Define los datos relacionados a cargar al momento de una consulta.
    ''' </summary>
    ''' <param name="DatosRelacionados">La lista de las relaciones.</param>
    Protected Overrides Sub DefinirDatosRelacionados(ByRef DatosRelacionados As System.Data.Linq.DataLoadOptions)

        'With DatosRelacionados
        '    .LoadWith(Of LineasProductos)(Function(Item As LineasProductos) Item.Lineas)
        '    .LoadWith(Of LineasProductos)(Function(Item As LineasProductos) Item.Productos)
        'End With

    End Sub

    ''' <summary>
    ''' Obtiene las Ubicacions locales no borradas.
    ''' </summary>
    Protected Overloads Overrides Function ObtenerExistentesSQLServer(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object), ByVal DatosRelacionados As System.Data.Linq.DataLoadOptions) As System.Collections.Generic.List(Of Unidad_Negocio)

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto.LoadOptions = DatosRelacionados
            Contexto.DeferredLoadingEnabled = False

            Dim Resultado = New List(Of Unidad_Negocio)
            If Filtro.Count = 0 Then
                'Obtener Ubicacions
                Dim Query = From UN In Contexto.Unidad_Negocios
                            where un .cuentaeliminado =0
                            Order By UN.cve_unidad_negocio    Ascending
                            Select UN
                Resultado = Query.ToList()

            ElseIf Filtro.ContainsKey("Estatus") Then
                Dim Query = From defect In Contexto.Unidad_Negocios  
                            Where defect.Estatus  = CType(Filtro("Estatus"), Integer) and defect .cuentaeliminado =0
                            Order By defect.cve_unidad_negocio   Ascending 
                            Select defect
                Resultado = Query.ToList()

            ElseIf Filtro.ContainsKey("Filtro") Then

                Dim Query = (From un In Contexto.Unidad_Negocios
                             Join cun In Contexto.Componente_Unidad_Negocios On cun.cve_unidad_negocio Equals un.cve_unidad_negocio
                             Where un.cuentaeliminado = 0
                             Select un, cun) '.Distinct 
                Dim Resultado2 = Query.ToList()
                
                For Each dato In Resultado2 
                    Dim NuevoUnidadNegocio=Datos .Fabricas .Unidad_Negocio .CrearNuevo
                    
                    NuevoUnidadNegocio =dato.un
                    'dato.un.Cve_Componente_Unidad_Negocio =dato.cun .cve_componente_unidad_negocio
                     
                    dim nuevo=NuevoUnidadNegocio.Clonar 
                    nuevo.Cve_Componente_Unidad_Negocio =dato.cun .cve_componente_unidad_negocio 
                    Resultado.Add (nuevo )
                Next                 

           ElseIf Filtro.ContainsKey("cve_usuario") Then
                 Dim Query = (From un In Contexto.Unidad_Negocios  
                            Join cun In Contexto . Componente_Unidad_Negocios On cun.cve_unidad_negocio Equals  un.cve_unidad_negocio _ 
                            Join uun In Contexto .Usuario_Unidad_Negocios On uun.cve_componente_unidad_negocio Equals cun.cve_componente_unidad_negocio 
                            Where uun.cve_usuario= CType(Filtro("cve_usuario"), Integer)
                            Select un)  
                  Resultado = Query.ToList()

            Else
                'Arrojar excepción
                Throw New ArgumentException()

            End If

            'Regresar lista resultante
            Return Resultado

        End Using

    End Function
    
    ''' <summary>
    ''' Recupera la moneda local.
    ''' </summary>
    ''' <param name="Entidad">Moneda a recuperar.</param>
    Protected Overrides Sub RecuperarSQLServer(ByVal Entidad As Unidad_Negocio )

        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            'Contexto.ReciclarLineasPorGuidLinea(Entidad.ID_Linea, False)
        End Using

    End Sub

    Protected Overrides Sub GuardarExistenteServicioWeb(ByVal Entidad As Unidad_Negocio )

    End Sub

    Protected Overloads Overrides Sub GuardarExistenteSQLServer(ByVal Entidad As Unidad_Negocio )
        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Dim Objeto=Contexto .Unidad_Negocios .Single(Function(p As Unidad_Negocio ) p.cve_unidad_negocio    = Entidad.cve_unidad_negocio )
            Objeto.Nombre =Entidad .Nombre 
            Objeto .Descripcion =Entidad .Descripcion
            Objeto .Estatus =Entidad .Estatus
            Contexto.SubmitChanges 
        End Using

    End Sub

    Protected Overrides Function ObtenerExistentesServicioWeb(ByVal Filtro As System.Collections.Generic.Dictionary(Of String, Object)) As System.Collections.Generic.List(Of Unidad_Negocio )

    End Function

    Protected Overrides Function ObtenerUnoServicioWeb(ByVal Id As System.Guid) As Unidad_Negocio 

    End Function

    Protected Overrides Sub RecuperarServicioWeb(ByVal Entidad As Unidad_Negocio )

    End Sub

    Protected Overrides Function ObtenerUnoSQLServer(Id As String, DatosRelacionados As DataLoadOptions) As Unidad_Negocio 
        
        Using Contexto = New ModeloDatosDataContext(CadenaConexionSQLServer)
            Contexto.LoadOptions = DatosRelacionados
            Contexto.DeferredLoadingEnabled = False

            Dim Query = From defect  In Contexto.Unidad_Negocios  
                        Where defect.cve_unidad_negocio    = Id
                        Select defect
            Return Query.SingleOrDefault()

        End Using
    End Function

    Protected Overrides Function ObtenerUnoServicioWeb(Id As String) As Unidad_Negocio 
        Throw New NotImplementedException()
    End Function

    Protected Overrides Function ObtenerUnoSQLServer(Id As Guid, DatosRelacionados As DataLoadOptions) As Unidad_Negocio
        Throw New NotImplementedException()
    End Function
End Class
