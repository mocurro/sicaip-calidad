﻿Public Class Constantes
    Public Const Permiso_Catalogo_Clientes As String = "CATÁLOGO CLIENTES"

    Public Const Permiso_Catalogo_Cliente_Modelos As String = "CATÁLOGO CLIENTES - MODELOS"

    Public Const Permiso_Catalogo_Defectos As String = "CATÁLOGO DEFECTOS"

    Public Const Permiso_Catalogo_Unidad_Negocio As String = "CATÁLOGO DE UNIDAD DE NEGOCIO"

    Public Const Permiso_Catalogo_Componente_unidad_negocio As String = "CATÁLOGO DE COMPONENTE - UNIDAD DE NEGOCIO"

    Public Const Permiso_Catalogo_unidad_negocio_Usuarios As String = "CATÁLOGO DE UNIDAD DE NEGOCIO - USUARIOS"

    Public Const Permiso_Catalogo_Area_Usuarios As String = "CATÁLOGO DE AREA- USUARIOS"

    Public Const Permiso_Catalogo_Tipo_Inspeccion As String = "CATÁLOGO DE TIPO DE INSPECCIÓN"

    Public Const Permiso_Catalogo_Solicitud As String = "ACCESO A CATÁLOGO DE SOLICITUD"

    Public Const Permiso_Catalogo_Registro As String = "CATÁLOGO DE REGISTRO"

    Public Const Permiso_Catalogo_NRFTSYS As String = "APLICACIÓN NRFTSYS"

    Public Const Permiso_Reporte_reclamos As String = "REPORTE DE RECLAMOS"

End Class
