﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Ventana base para cualquier reporte que estará contenido en una venta principal de Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier reporte que implemente UX de Zeus. Todas las ventanas reporte heredan finalmente de esta ventana.</remarks>
Public Class FormaReporte
    Implements IReporte


    ''' <summary>
    ''' Determina si el catálogo está ocupado en base al número de operaciones concurrentes. 0 significa que no está ocupado.
    ''' </summary>
    Private MiEstaOcupado As Integer

    ''' <summary>
    ''' Contiene la definición del reporte.
    ''' </summary>
    Private MiReporte As ReporteDefinicion

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Public Shadows Event AntesActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Implements IReporte.AntesActualizarVentana

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Public Shadows Event DespuesActualizarVentana(ByVal sender As Object, ByVal e As System.EventArgs) Implements IReporte.DespuesActualizarVentana

    ''' <summary>
    ''' Determina si el reporte se basa en el navegador de filtros.
    ''' </summary>
    Public Property UsaFiltro() As Boolean
        Get
            Return (SplitContenido.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both)
        End Get
        Set(ByVal value As Boolean)
            If value Then
                SplitContenido.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both
            Else
                SplitContenido.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2
            End If
        End Set
    End Property

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Public Overrides Sub Foco() Implements IReporte.Foco

        MyBase.Foco()

    End Sub

    ''' <summary>
    ''' Muestra la pestaña dentro de la ventana padre.
    ''' </summary>
    ''' <param name="owner">Ventana padre.</param>
    Public Shadows Sub MostrarVentana(ByVal owner As System.Windows.Forms.IWin32Window) Implements IReporte.MostrarVentana

        MyBase.MostrarVentana(owner)

    End Sub

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    <System.ComponentModel.Browsable(False)> _
    Public Overrides Property VentanaPadre() As IFormaPrincipal Implements IReporte.VentanaPadre
        Get
            Return MyBase.VentanaPadre
        End Get
        Set(ByVal value As IFormaPrincipal)
            MyBase.VentanaPadre = value
        End Set
    End Property

    ''' <summary>
    ''' Regresa la imagen de la entidad.
    ''' </summary>
    Public Overrides ReadOnly Property Imagen() As System.Drawing.Image
        Get
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' Prepara la ventana.
    ''' </summary>
    Private Sub FormaReporte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            'Definir colores en base al tema
            PanelMensaje.BackColor = VentanaPadre.ColorInfo
            EtiquetaMensaje.ForeColor = VentanaPadre.ColorInfoTexto
            EtiquetaCopyright.Text = String.Format("{0} | {1}", My.Application.Info.Trademark.Replace("&", "&&"), My.Application.Info.Copyright.Replace("&", "&&"))

        End If

    End Sub

    ''' <summary>
    ''' Genera el reporte a mostrar.
    ''' </summary>
    Private Sub FormaReporte_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        'Aplica únicamente durante la ejecución
        If Not DesignMode Then

            If SplitContenido.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2 Then
                GenerarReporte()
            End If

        End If

    End Sub

    ''' <summary>
    ''' Genera el reporte con su información para desplegar en la ventana.
    ''' </summary>
    Public Sub GenerarReporte() Implements IReporte.GenerarReporte

        MiReporte = CrearReporteDefinicion()
        AreaImpresion.PrintingSystem = MiReporte.PrintingSystem
        MiReporte.CreateDocument(True)

    End Sub

    Public Sub LimpiarArea()
        AreaImpresion.PrintingSystem = Nothing
    End Sub

    ''' <summary>
    ''' Crea una instancia de la definición del reporte.
    ''' </summary>
    Protected Overridable Function CrearReporteDefinicion() As ReporteDefinicion

        Return New ReporteDefinicion()

    End Function

    ''' <summary>
    ''' Actualiza el contenido de la pestaña.
    ''' </summary>
    ''' <remarks>Esta función actualiza la información que se muestra en la pestaña. La función tiene un evento Antes y un evento Despues.</remarks>
    Public Overrides Sub ActualizarVentana() Implements IReporte.ActualizarVentana

        Throw New NotImplementedException()

    End Sub

    Public Sub ActualizarReporte()
        'Inicia evento y continua si no se canceló
        Dim Argumentos = New System.ComponentModel.CancelEventArgs(False)
        RaiseEvent AntesActualizarVentana(Me, Argumentos)
        If Not Argumentos.Cancel Then

            'Inicia proceso
            UseWaitCursor = True
            MostrarMensaje(String.Format("<color=HotTrack><size=11><b>Cargando {0}</b></size></color><br><size=8>Espere un momento por favor ...</size>", Text.ToLower()), My.Resources.Imagen48_Actualizar)
        Else
            OcultarMensaje()
        End If
    End Sub

    ''' <summary>
    ''' Dibuja la línea inferior divisora.
    ''' </summary>
    Private Sub PanelMensaje_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelMensaje.Paint

        Using Pluma = New Drawing.Pen(SystemColors.ControlDark)
            e.Graphics.DrawLine(Pluma, 0, PanelMensaje.Height - 1, PanelMensaje.Width, PanelMensaje.Height - 1)
        End Using

    End Sub

    ''' <summary>
    ''' Muestra un mensaje en la tira de mensajes del catálogo.
    ''' </summary>
    ''' <param name="Mensaje">El mensaje específico a mostrar.</param>
    ''' <param name="Imagen">La imagen a utilizar.</param>
    Public Sub MostrarMensaje(ByVal Mensaje As String, ByVal Imagen As Image)

        'Definir mensaje e imagen
        EtiquetaMensaje.Text = Mensaje
        ImagenMensaje.Image = Imagen
        MiEstaOcupado += 1

        'Si es el primer proceso que está ocupado, modificar la interface visual
        If MiEstaOcupado = 1 Then
            VentanaPadre.IniciaTrabajoPestaña()
            DefinirProgresoMensaje(0)
            PanelMensaje.Visible = True
            PanelFiltro.Enabled = False
            SplitContenido.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Oculta el mensaje en la tira de mensajes del catálogo.
    ''' </summary>
    ''' <remarks>Si el catálogo estaba ocupado, quedará desocupado.</remarks>
    Public Sub OcultarMensaje()

        'Determinar si es el último proceso, para modificar la interface visual
        MiEstaOcupado -= 1
        If MiEstaOcupado = 0 Then
            PanelMensaje.Visible = False
            PanelFiltro.Enabled = True
            SplitContenido.Enabled = True
            VentanaPadre.TerminaTrabajoPestaña()
        End If

    End Sub

    ''' <summary>
    ''' Delegado para definir el valor máximo del progreso conocido.
    ''' </summary>
    ''' <param name="ValorMaximo">El valor máximo de progreso. Si el valor es 0, la barra de progreso no es visible. Al definir este dato, el valor de la barra de progreso se reinicia en 0.</param>
    Protected Delegate Sub DefinirProgresoMensajeDelegado(ByVal ValorMaximo As Integer)

    ''' <summary>
    ''' Define el valor máximo del progreso conocido.
    ''' </summary>
    ''' <param name="ValorMaximo">El valor máximo de progreso. Si el valor es 0, la barra de progreso no es visible. Al definir este dato, el valor de la barra de progreso se reinicia en 0.</param>
    Public Sub DefinirProgresoMensaje(ByVal ValorMaximo As Integer)

        'Determina si se requiere invocar
        If ProgresoDefinidoMensaje.InvokeRequired Then
            ProgresoDefinidoMensaje.Invoke(New DefinirProgresoMensajeDelegado(AddressOf DefinirProgresoMensaje), New Object() {ValorMaximo})
            Exit Sub
        End If

        'Realiza los cambios en caso de ser necesario
        If MiEstaOcupado > 0 Then
            With ProgresoDefinidoMensaje
                .EditValue = 0
                .Properties.Maximum = ValorMaximo
                ItemForProgresoDefinidoMensaje.Visibilidad(ValorMaximo > 0)
                ItemForProgresoMensaje.Visibilidad(ValorMaximo = 0)
            End With
        End If

    End Sub

    ''' <summary>
    ''' Aumenta el avance de la barra de progreso conocida.
    ''' </summary>
    Public Sub IncrementarProgresoMensaje()

        IncrementarProgresoMensaje(1)

    End Sub

    ''' <summary>
    ''' Delegado para aumentar el avance de la barra de progreso conocida.
    ''' </summary>
    ''' <param name="Cantidad">La cantidad de "pasos" a incrementar.</param>
    Protected Delegate Sub IncrementarProgresoMensajeDelegado(ByVal Cantidad As Integer)

    ''' <summary>
    ''' Aumenta el avance de la barra de progreso conocida.
    ''' </summary>
    ''' <param name="Cantidad">La cantidad de "pasos" a incrementar.</param>
    Public Sub IncrementarProgresoMensaje(ByVal Cantidad As Integer)

        'Determina si se requiere invocar
        If ProgresoDefinidoMensaje.InvokeRequired Then
            ProgresoDefinidoMensaje.Invoke(New IncrementarProgresoMensajeDelegado(AddressOf IncrementarProgresoMensaje), New Object() {Cantidad})
            Exit Sub
        End If

        'Realiza el incremento de ser necesario
        If MiEstaOcupado > 0 Then
            With ProgresoDefinidoMensaje
                .Increment(Cantidad)
            End With
        End If

    End Sub
End Class
