﻿Public Class Area_Usuario
         ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case ClienteEstatus.Activo 
                    Return "Activo"
                Case ClienteEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property

    Public ReadOnly Property user As SEGURIDAD_USUARIO 
        Get
            Dim MiCliente=Datos.Fabricas .Seguridad_Usuarios .ObtenerUno (cve_usuario )
            Return MiCliente 
        End Get
    End Property

    Public ReadOnly Property Are As detalle
        Get
            Dim MiModelo = Datos.Fabricas.SubArea.ObtenerUno(Cve_Sub_Area)
            Return MiModelo
        End Get
    End Property
End Class
