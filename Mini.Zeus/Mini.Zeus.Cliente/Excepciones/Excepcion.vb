﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Objeto base del cual derivan todas las excepciones de Zeus.
''' </summary>
Public MustInherit Class Excepcion
    Inherits Exception

    ''' <summary>
    ''' Contiene el objeto de la excepción.
    ''' </summary>
    Private MiObjeto As Object

    ''' <summary>
    ''' Contiene el método donde ocurrió la excepción.
    ''' </summary>
    Private MiMetodo As Reflection.MethodBase

    ''' <summary>
    ''' Crea la excepción basada en un objeto y método específico.
    ''' </summary>
    ''' <param name="Objeto">El objeto que generó la excepción. Si el objeto implementa IObjeto el mensaje de la excepción contendrá información adicional.</param>
    ''' <param name="Metodo">El método donde ocurrió la excepción.</param>
    Public Sub New(ByVal Objeto As Object, ByVal Metodo As Reflection.MethodBase)

        MiObjeto = Objeto
        MiMetodo = Metodo

    End Sub

    ''' <summary>
    ''' Regresa el objeto de la excepción.
    ''' </summary>
    Public ReadOnly Property Objeto() As Object
        Get
            Return MiObjeto
        End Get
    End Property

    ''' <summary>
    ''' Regresa el método donde ocurrió la excepción.
    ''' </summary>
    Public ReadOnly Property Metodo() As Reflection.MethodBase
        Get
            Return MiMetodo
        End Get
    End Property

    ''' <summary>
    ''' Regresa el mensaje de la excepción.
    ''' </summary>
    Public Overrides ReadOnly Property Message() As String
        Get

            'Preparar valroes
            Dim NombreObjeto = String.Empty
            If MiObjeto IsNot Nothing Then
                NombreObjeto = MiObjeto.GetType().Name
                If TypeOf MiObjeto Is IObjeto Then
                    NombreObjeto = String.Format("{0} [{1}]", NombreObjeto, DirectCast(MiObjeto, IObjeto).Id)
                End If
            End If

            'Regresar el mensaje
            Return Mensaje(NombreObjeto, MiMetodo.Name)

        End Get
    End Property

    ''' <summary>
    ''' Obtiene el mensaje de la excepción.
    ''' </summary>
    ''' <param name="NombreObjeto">El nombre del objeto de la excepción. El nombre puede incluir el Id si el objeto implementa IObjeto.</param>
    ''' <param name="NombreMetodo">El nombre del método donde ocurrió la excepción.</param>
    ''' <remarks>El texto que regresa esta función está ligado a la cultura actual.</remarks>
    Protected MustOverride Function Mensaje(ByVal NombreObjeto As String, ByVal NombreMetodo As String) As String

End Class
