﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para controles catálogo de Zeus.
''' </summary>
''' <remarks>Todos los controles catálogo basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface ICatalogoIncrustado

    ''' <summary>
    ''' Inicializa el catálogo para una entidad particular.
    ''' </summary>
    ''' <param name="EntidadPadre">Entidad padre.</param>
    Sub InicializarControl(ByVal EntidadPadre As IEntidad)

    '''' <summary>
    '''' Inicializa el catálogo para una entidad y usuario particulares.
    '''' </summary>
    '''' <param name="UsuarioActual">El usuario que utiliza la ventana.</param>
    '''' <param name="EntidadPadre">Entidad padre.</param>
    'Sub InicializarControl(ByVal UsuarioActual As Usuarios, ByVal EntidadPadre As IEntidad)

    ''' <summary>
    ''' Determina la entidad padre que contiene la información de este catálogo.
    ''' </summary>
    ReadOnly Property EntidadPadre() As IEntidad

    ''' <summary>
    ''' Regresa el tipo de entidad raíz que se muestra en el catálogo.
    ''' </summary>
    ReadOnly Property TipoEntidadRaiz() As Type

    ''' <summary>
    ''' Determina cuales entidades serán visibles en el catálogo.
    ''' </summary>
    Property EntidadesVisibles() As EntidadesVisibles

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Event AntesActualizarControl As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Event DespuesActualizarControl As EventHandler

    ''' <summary>
    ''' Actualiza la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <remarks>Esta función utiliza la fábrica de la entidad raíz y el filtro pre-definido para realizar la consulta. La función tiene un evento Antes y un evento Despues.</remarks>
    Sub ActualizarControl()

    ''' <summary>
    ''' Define la modalidad del catálogo en relación a las acciones disponibles.
    ''' </summary>
    Property ModoCatalogo() As ModosCatalogosIncrustados

End Interface
