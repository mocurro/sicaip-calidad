﻿Imports Mini.Zeus.Cliente

Public Class CatalogoCliente

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))


        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Cliente  ), Datos.Fabricas .Cliente , GetType(TarjetaCliente  )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Cliente )

    End Function
End Class