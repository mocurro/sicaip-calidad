﻿Imports System.Data.SqlClient
Imports System.IO
Imports Datos

Public Class NRFTNueva
    ''' <summary>
    ''' Listado de números de parte
    ''' </summary>
    Private MiListaNoParte As List(Of Datos.modelo) '.modeloNP)

    ''' <summary>
    ''' Número de parte seleccionado
    ''' </summary>
    Private MiNoParte As Datos.modelo

    ''' <summary>
    ''' Cadena de conexión
    ''' </summary>
    Private MiCadenaConexión As String

    Private MiComponente As Datos.componente

    Private MiCliente As Datos.Cliente

    Private MiUnidadNegocio As Datos.Unidad_Negocio

    Private MiListaUsuarios As New List(Of Datos.SEGURIDAD_USUARIO)

    Private MiListaDefectos As New List(Of Datos.Defecto)

    Private MiDefecto As Datos.Defecto

    Private MiClienteModelo As Datos.Cliente_Modelo ' Cliente_Modelo
    
    Private MiRutaArchivo As String =String.Empty

    Private MiSubArea As Datos.detalle

    Private MandarCorreoUnidadNegocio As Boolean =False 

    Private MiReclamoArchivo As Datos.Reclamo_Archivo 

    Private DefectoSeccionado As Boolean =False 

    Private PasoValidacionCantidad As Boolean =true

    Private SeleccionoArea As Boolean =false

    Private ListadoCorreosAtiende As new  List(Of String)

    Private MiMandarCorreoUnidad As Boolean =False
    
    Private ContieneEstampada As Boolean =false 


    Private Sub NRFTNueva_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        MiCadenaConexión = Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer
        If Datos.SesiónLocal .Usuario IsNot Nothing Then
            EtiquetaUsuario .Text =Datos.SesiónLocal .Usuario.Nombre 
            'EtiquetaRol .Text =Datos.SesiónLocal .Usuario.TipoUsuario .Nombre_Tipo_Usuario 
        End If

        Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
            Dim MiListaNoParte2 = From NoParte In MiModelo.modelos
                                  Where CInt(NoParte.Estatus) = 0
            MiListaNoParte = MiListaNoParte2.ToList
        End Using
        DatosNoParte.DataSource = MiListaNoParte
        'For Each noparte In MiListaNoParte
        '    ComboNoPARTE.Properties.Items.Add(New DevExpress.XtraEditors.Controls.ImageComboBoxItem( noparte.np_gkn  , noparte ))

        'Next

        Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
            Dim MiListaDefectos2 = From midefect In MiModelo.Defectos
                                   Where midefect.Estatus = 0 And midefect .cuentaeliminado =0
            MiListaDefectos = MiListaDefectos2.ToList
        End Using
        DatosSeccion.DataSource = MiListaDefectos
        'For Each defect In MiListaDefectos
        '    ComboDefectos.Properties.Items.Add(New DevExpress.XtraEditors.Controls.ImageComboBoxItem(defect.descripcion, defect, 0))
        'Next
         Dim Filtro = New Dictionary(Of String, Object)
        Dim MiListaAreas = Fabricas.SubArea.ObtenerExistentesParaSerializar(Filtro)
        DatosArea.DataSource =MiListaAreas
    End Sub


    Private Sub ProcesoNoPARTE()
        MiComponente = Nothing
        MiCliente = Nothing
        MiUnidadNegocio = Nothing
        MiListaUsuarios.Clear()
        ListadoCorreosAtiende.Clear

        'MiNoParte = CType(ComboNoPARTE.SelectedItem, Datos.modelo )
        Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
            MiComponente = (From component In MiModelo.componentes
                            Where component.cve_componente = MiNoPARTE.cve_componente
                            Select component).FirstOrDefault
        End Using
        If MiComponente IsNot Nothing Then
            txtComponente.Text = MiComponente.componente
            Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
                MiUnidadNegocio = (From unidad In MiModelo.Unidad_Negocios
                                   Join componenteunidad In MiModelo.Componente_Unidad_Negocios
                                   On unidad.cve_unidad_negocio Equals componenteunidad.cve_unidad_negocio
                                   Where componenteunidad.cve_componente = MiComponente.cve_componente
                                   Select unidad).FirstOrDefault
            End Using
            If MiUnidadNegocio IsNot Nothing Then
                txtUnidadNegocio.Text = MiUnidadNegocio.nombre
            Else
                txtUnidadNegocio.Text = "No hay unidad de negocio asignada"
            End If

            Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
                Dim MiListaUsuarios2 = From usuario In MiModelo.SEGURIDAD_USUARIOs
                                       Join usuariounidad In MiModelo.Usuario_Unidad_Negocios On usuario.CVE_Usuario Equals usuariounidad.cve_usuario
                                       Join componente In MiModelo.Componente_Unidad_Negocios On componente.cve_componente_unidad_negocio Equals usuariounidad.cve_componente_unidad_negocio
                                       Where componente.cve_componente = MiComponente.cve_componente AndAlso usuariounidad.cuentaeliminado = 0 AndAlso componente.cuentaeliminado = 0
                                       Select usuario
                MiListaUsuarios = MiListaUsuarios2.ToList
            End Using
            If MiListaUsuarios.Count > 0 Then
                Dim listadoAtiende As String = String.Empty
                For Each user In MiListaUsuarios
                    listadoAtiende += user.Nombre + ","
                    ListadoCorreosAtiende.Add (user .Email )
                Next
                EditAtiende.Text = listadoAtiende
                ComboSeccion.Enabled = True
            Else
                EditAtiende.Text = "No se han asignado personas que atienden"
            End If

        Else
            txtComponente.Text = "No hay componente asignado"
            txtUnidadNegocio.Text = "No hay unidad de negocio asignada"
        End If
        txtModelo.Text = MiNoPARTE.descripcion

        Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
            MiCliente = (From cliente In MiModelo.Clientes
                         Join modelo In MiModelo.Cliente_Modelos
                         On cliente.cve_cliente Equals modelo.cve_cliente
                         Where modelo.cve_modelo = MiNoPARTE.cve_modelo
                         Select cliente).FirstOrDefault
        End Using
        If MiCliente IsNot Nothing Then
            txtCliente.Text = MiCliente.Nombre
            Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
                MiClienteModelo = (From clientemod In MiModelo.Cliente_Modelos
                                   Where clientemod.cve_modelo = MiNoPARTE.cve_modelo And _
                                   clientemod.cve_cliente = MiCliente.cve_cliente
                                   Select clientemod).FirstOrDefault
            End Using

        Else
            txtCliente.Text = "No se tiene un cliente asignado"
        End If
    End Sub
    Private Sub btnReportar_Click(sender As System.Object, e As System.EventArgs) Handles btnReportar.Click
        dim x=SesiónLocal .Usuario
        Dim IdReclamo As integer
        Try
           Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
                Dim MiReclamo = New Reclamo
                MiReclamo.cve_cliente_modelo = MiClienteModelo.cve_cliente_modelo
                MiReclamo.cve_defecto = MiDefecto.cve_defecto
                MiReclamo.fecha_registro = Date.Now
                MiReclamo.cve_usuario_reporta = SesiónLocal .Usuario .CVE_Usuario 
                MiReclamo.estampada = txtEstampada.Text.Trim
                MiReclamo.cantidad_encontrada = CInt(EditCantidadEncontrada.Value) ' txtEncontrada.Text.Trim
                MiReclamo.cantidad_rechazada = CInt(EditRechazada .Value) ' txtRechazada.Text.Trim
                MiReclamo.detalle = editDetalle.Text.Trim
                MiReclamo.estatus = 0
                If MiSubArea IsNot Nothing Then
                    MiReclamo.Cve_Sub_Area = MiSubArea.cve_detalle
                End If

                MiModelo.Reclamos.InsertOnSubmit(MiReclamo)
            
                MiModelo.SubmitChanges()
            End Using
            'var result = (from item incontext.NombreTabla
            '            orderby item.NombreCampo desc
            '           select item).First();

             If MiReclamoArchivo IsNot Nothing  Then
                 Using MiModelo = New ModeloDatosDataContext(MiCadenaConexión)
                    Dim Query=(From item in MiModelo.Reclamos 
                                 Order by item.cve_reclamo Descending 
                                 Select item )
                    dim reclamo=Query.FirstOrDefault  
                    MiReclamoArchivo.cve_reclamo =reclamo.cve_reclamo 
                    MiModelo .Reclamo_Archivos .InsertOnSubmit (MiReclamoArchivo)
                    MiModelo.SubmitChanges()
                 End Using
             End If
            SesiónLocal .crear_contenido_mensaje_NRFTNueva (MiDefecto ,EditCantidadEncontrada .Value  ,EditRechazada .Value ,editDetalle.Text.Trim)
            'For Each correo In ListadoCorreosAtiende

            'Next
            If MiMandarCorreoUnidad Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("cve_usuario", SesiónLocal .Usuario.CVE_Usuario )
                Dim Unidad=Fabricas.Unidad_Negocio .ObtenerExistentes (Filtro)
                If Unidad IsNot Nothing AndAlso Unidad .Count >0 Then
                    Filtro.Clear 
                    Filtro.Add("cve_usuarioUnidad", Unidad(0).cve_unidad_negocio )
                    Dim ListUser=Fabricas .Seguridad_Usuarios .ObtenerExistentes (Filtro)
                     If ListUser IsNot Nothing AndAlso ListUser .Count >0 Then
                        For Each user In ListUser 
                            ListadoCorreosAtiende.Add (user.Email )
                        Next
                     Else
                        ListadoCorreosAtiende.Add (SesiónLocal .Usuario .Email)
                     End If
                Else
                    ListadoCorreosAtiende.Add (SesiónLocal .Usuario .Email)
                End If
            Else
                ListadoCorreosAtiende.Add (SesiónLocal .Usuario .Email)
            End If


            SesiónLocal .envia_notificacion_paro_z (ListadoCorreosAtiende ) 
            
        Catch ex As Exception
            MessageBox.Show(String.Format ("No se gurado el reclamo por la siguiente razón: {0}",ex.Message ),"Error al guardar el reclamo")
         Finally
            Me.Close()
        End Try
        
    End Sub


    Private Sub ComboParte_EditValueChanged(sender As Object, e As EventArgs) Handles ComboParte.EditValueChanged
        ComboParte.Enabled = False
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiNoParte = CType(combo.GetSelectedDataRow, Datos.modelo)
        If MiNoParte IsNot Nothing Then
            ProcesoNoPARTE()
        End If
        ComboParte.Enabled = True
    End Sub

    Private Sub ComboSeccion_EditValueChanged(sender As Object, e As EventArgs) Handles ComboSeccion.EditValueChanged
        ComboSeccion.Enabled = False
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiDefecto= CType(combo.GetSelectedDataRow, Datos.Defecto)
        txtDescripciónDefecto.Text = MiDefecto.descripcion
        DefectoSeccionado=True 
        ValidarBoton
        ComboSeccion.Enabled = True

    End Sub

    Private Sub ComboArea_EditValueChanged(sender As Object, e As EventArgs) Handles ComboArea.EditValueChanged
        ComboArea.Enabled =False
        Dim combo = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        MiSubArea = CType(combo.GetSelectedDataRow, Datos.detalle)
        If MiSubArea IsNot Nothing Then
            ComboArea.Enabled = True
        End If
        SeleccionoArea =True 
        ValidarBoton
        
    End Sub

    Private Sub btnSelecArchivo_Click(sender As Object, e As EventArgs) Handles btnSelecArchivo.Click
        ' Call ShowDialog.
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()
        btnSelecArchivo.Enabled =False
        ' Test result.
        If result = Windows.Forms.DialogResult.OK Then

            ' Get the file name.
            MiRutaArchivo = OpenFileDialog1.FileName
            RutaArchivo.Text = MiRutaArchivo

            MiReclamoArchivo = New Reclamo_Archivo
            MiReclamoArchivo.Nombre = OpenFileDialog1.SafeFileName

            If MiReclamoArchivo.Nombre.Count > 80 Then
                MiReclamoArchivo.Nombre = MiReclamoArchivo.Nombre.Substring(MiReclamoArchivo.Nombre.Count - 80, 80)
                'MiReclamoArchivo.Nombre = MiReclamoArchivo.Nombre.Substring(MiReclamoArchivo.Nombre.Count - 80, MiReclamoArchivo.Nombre.Count)
            End If
            If File.Exists(MiRutaArchivo) Then
                Dim fs As New FileStream(MiRutaArchivo, FileMode.Open)
                Dim data(fs.Length) As Byte
                fs.Read(data, 0, Convert.ToInt32(fs.Length))
                MiReclamoArchivo.archivo = data
            Else
                MessageBox.Show("El archivo seleccionado no existe, favor de verificarlo")
            End If
            btnSelecArchivo.Enabled = False
        Else
            btnSelecArchivo.Enabled = True
        End If


    End Sub
    

    Private Sub HyperlinkLabelControl1_Click(sender As Object, e As EventArgs) Handles linkRemover.Click
        MiRutaArchivo=String.Empty 
        RutaArchivo.Text=MiRutaArchivo 
        MiReclamoArchivo=Nothing 
         btnSelecArchivo.Enabled =true
    End Sub

    Private Sub CheckUnidadNegocio_CheckedChanged(sender As Object, e As EventArgs) Handles CheckUnidadNegocio.CheckedChanged
        MiMandarCorreoUnidad=CheckUnidadNegocio.Checked 
    End Sub

    Private Sub EditCantidadEncontrada_TextChanged(sender As Object, e As EventArgs) Handles EditCantidadEncontrada.TextChanged
        If EditRechazada .Value >=EditCantidadEncontrada .Value Then
            PasoValidacionCantidad=True
        Else
            PasoValidacionCantidad=false
        End If
        ValidarBoton
    End Sub

    Private Sub EditRechazada_TextChanged(sender As Object, e As EventArgs) Handles EditRechazada.TextChanged
        If EditRechazada .Value >=EditCantidadEncontrada .Value Then
            PasoValidacionCantidad=True
        Else
            PasoValidacionCantidad=false
        End If
        ValidarBoton
    End Sub

    Private sub ValidarBoton
        If DefectoSeccionado and PasoValidacionCantidad and SeleccionoArea and ContieneEstampada Then
            btnReportar .Enabled =True 
        Else
            btnReportar .Enabled =False  
        End If

    End sub

    Private Sub txtEstampada_TextChanged(sender As Object, e As EventArgs) Handles txtEstampada.TextChanged
        If Not  String .IsNullOrEmpty ( txtEstampada .Text.Trim  ) Then
            ContieneEstampada=true
            Else
            ContieneEstampada=false
        End If
        ValidarBoton
    End Sub
End Class