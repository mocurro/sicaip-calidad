﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los diferentes tipos de formatos que se pueden utilizar en el atributo de Columna.
''' </summary>
''' <remarks>Este enumerador es compatible con el enumerador DevExpress.Utils.FormatType</remarks>
Public Enum ColumnasTiposFormatos As Integer

    ''' <summary>
    ''' No se aplica formato.
    ''' </summary>
    Ninguno = 0

    ''' <summary>
    ''' La información se formatea como un número.
    ''' </summary>
    Numerico = 1

    ''' <summary>
    ''' La información se formatea como fecha y hora.
    ''' </summary>
    FechaHora = 2

    ''' <summary>
    ''' La información se formatea según la cadena de formato.
    ''' </summary>
    Personalizado = 3

End Enum