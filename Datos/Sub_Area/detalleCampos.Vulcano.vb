﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla detalle
''' </summary>
Public Enum detalleCampos As Integer

    cve_detalle
    tipo
    descripcion
    Cve_Area

End Enum

