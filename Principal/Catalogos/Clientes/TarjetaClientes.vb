﻿Imports System.ComponentModel

Public Class TarjetaCliente
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoCliente ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.ClienteCampos, DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.ClienteCampos.Nombre, itemNombre)
            .Add(Datos.ClienteCampos.Descripcion, itemDescripcion)
            .Add(Datos.ClienteCampos.Estatus , ItemEstatus )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.ClienteCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Cliente )()
        Select Case sender.Name
            Case TextoNombre.Name
                Paquete.Nombre = TextoNombre.Text.Trim
                'Return True
            Case TextoDescripcion .Name
                Paquete.Descripcion = TextoDescripcion.Text.Trim

           Case ComboEstatus .Name 
                Paquete.Estatus = CByte(ComboEstatus .SelectedIndex)
                'Paquete.TipoAplicacion = NUTEC.Comun.TipoAplicacion.Embarques
                'Return True
        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paquete = EntidadDe(Of Datos.Cliente )()
        If paquete.Estatus = 0 Then
            ComboEstatus.SelectedIndex = 0
        Else
            ComboEstatus.SelectedIndex = 1
        End If

    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Cliente )()
        Paquete.Estatus  = ComboEstatus.SelectedIndex
    End Sub
End Class