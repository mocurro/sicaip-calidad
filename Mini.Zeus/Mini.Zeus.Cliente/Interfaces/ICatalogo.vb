﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Interface para ventanas catálogo de Zeus.
''' </summary>
''' <remarks>Todas las ventanas catálogo basadas en la plataforma Zeus implementan finalmente esta interface.</remarks>
Public Interface ICatalogo

    ''' <summary>
    ''' Le da el foco a la ventana pestaña.
    ''' </summary>
    Sub Foco()

    ''' <summary>
    ''' Muestra el catálogo para un usuario particular dentro de la ventana padre.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que utiliza la ventana.</param>
    ''' <param name="owner">Ventana padre.</param>
    Sub MostrarVentana(ByVal owner As IWin32Window)

    ''' <summary>
    ''' Determina la ventana padre dentro de la cual esta contenida esta ventana pestaña.
    ''' </summary>
    Property VentanaPadre() As IFormaPrincipal

    ''' <summary>
    ''' Regresa el tipo de entidad raíz que se muestra en el catálogo.
    ''' </summary>
    ReadOnly Property TipoEntidadRaiz() As Type

    ''' <summary>
    ''' Regresa el atributo de texto de la entidad raíz.
    ''' </summary>
    ReadOnly Property TipoEntidadRaizTexto() As TextoAttribute

    ''' <summary>
    ''' Determina cuales entidades serán visibles en el catálogo.
    ''' </summary>
    Property EntidadesVisibles() As EntidadesVisibles

    ''' <summary>
    ''' Determina si al obtener la información se debe incluir los datos relacionados.
    ''' </summary>
    Property IncluirDatosRelacionados() As Boolean

    ''' <summary>
    ''' Determina si la tira de filtro es visible.
    ''' </summary>
    Property FiltroVisible() As Boolean

    ''' <summary>
    ''' Evento que ocurre antes de iniciar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento permite realizar cambios antes de iniciar la actualización.</remarks>
    Event AntesActualizarVentana As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary>
    ''' Evento que ocurre despues de completar el proceso de actualización.
    ''' </summary>
    ''' <remarks>Este evento ocurre únicamente si no fue cancelado ni hubo errores.</remarks>
    Event DespuesActualizarVentana As EventHandler

    ''' <summary>
    ''' Actualiza la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <remarks>Esta función utiliza la fábrica de la entidad raíz y el filtro pre-definido para realizar la consulta. La función tiene un evento Antes y un evento Despues.</remarks>
    Sub ActualizarVentana()

End Interface
