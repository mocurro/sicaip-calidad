﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Clase que contiene la información de entidades que utiliza un catálogo.
''' </summary>
''' <remarks>Esta clase contiene la información necesaria para que un catálogo pueda administrar entidades de diferentes tipos, incluyendo las fábricas y tarjetas requeridas.</remarks>
Public NotInheritable Class EntidadParaCatalogo
    Inherits Objeto

    ''' <summary>
    ''' Contiene la fábrica asociada a la entidad.
    ''' </summary>
    Private MiFabrica As IFabrica

    ''' <summary>
    ''' Contiene el tipo de entidad.
    ''' </summary>
    Private MiTipoEntidad As Type

    ''' <summary>
    ''' Contiene el tipo de tarjeta.
    ''' </summary>
    Private MiTipoTarjeta As Type

    ''' <summary>
    ''' Crea una nueva instancia con la fábrica asociada a la entidad.
    ''' </summary>
    ''' <param name="TipoEntidad">El tipo de entidad.</param>
    ''' <param name="Fabrica">La fábrica asociada a la entidad.</param>
    ''' <param name="TipoTarjeta">El tipo de tarjeta asociada a la entidad.</param>
    Public Sub New(ByVal TipoEntidad As Type, ByVal Fabrica As IFabrica, ByVal TipoTarjeta As Type)

        MiTipoEntidad = TipoEntidad
        MiFabrica = Fabrica
        MiTipoTarjeta = TipoTarjeta

    End Sub

    ''' <summary>
    ''' Regresa el tipo de entidad.
    ''' </summary>
    Public ReadOnly Property TipoEntidad() As Type
        Get
            Return MiTipoEntidad
        End Get
    End Property

    ''' <summary>
    ''' Regresa la fábrica asociada a la entidad.
    ''' </summary>
    Public ReadOnly Property Fabrica() As IFabrica
        Get
            Return MiFabrica
        End Get
    End Property

    ''' <summary>
    ''' Regresa el tipo de tarjeta asociada a la entidad.
    ''' </summary>
    Public ReadOnly Property TipoTarjeta() As Type
        Get
            Return MiTipoTarjeta
        End Get
    End Property

End Class
