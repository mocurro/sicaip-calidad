﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class PlantillaProducto
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Code128Generator1 As DevExpress.XtraPrinting.BarCode.Code128Generator = New DevExpress.XtraPrinting.BarCode.Code128Generator()
        Dim Code128Generator2 As DevExpress.XtraPrinting.BarCode.Code128Generator = New DevExpress.XtraPrinting.BarCode.Code128Generator()
        Dim Code128Generator3 As DevExpress.XtraPrinting.BarCode.Code128Generator = New DevExpress.XtraPrinting.BarCode.Code128Generator()
        Me.topMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.bottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Title = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.FieldCaption = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageInfo = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DataField = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.FuenteDatos = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrBarCode1 = New DevExpress.XtraReports.UI.XRBarCode()
        Me.pictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.label26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line3 = New DevExpress.XtraReports.UI.XRLine()
        Me.label33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.barCode1 = New DevExpress.XtraReports.UI.XRBarCode()
        Me.line6 = New DevExpress.XtraReports.UI.XRLine()
        Me.label34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line7 = New DevExpress.XtraReports.UI.XRLine()
        Me.label35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line8 = New DevExpress.XtraReports.UI.XRLine()
        Me.line9 = New DevExpress.XtraReports.UI.XRLine()
        Me.label2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line10 = New DevExpress.XtraReports.UI.XRLine()
        Me.label3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line11 = New DevExpress.XtraReports.UI.XRLine()
        Me.label4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line12 = New DevExpress.XtraReports.UI.XRLine()
        Me.label5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line13 = New DevExpress.XtraReports.UI.XRLine()
        Me.line14 = New DevExpress.XtraReports.UI.XRLine()
        Me.label6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line15 = New DevExpress.XtraReports.UI.XRLine()
        Me.line16 = New DevExpress.XtraReports.UI.XRLine()
        Me.label8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.label14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line4 = New DevExpress.XtraReports.UI.XRLine()
        Me.Codigo = New DevExpress.XtraReports.UI.XRBarCode()
        Me.line2 = New DevExpress.XtraReports.UI.XRLine()
        Me.label32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.line1 = New DevExpress.XtraReports.UI.XRLine()
        Me.label29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.FuenteDatos2 = New DevExpress.DataAccess.ObjectBinding.ObjectDataSource(Me.components)
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteDatos2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'topMarginBand1
        '
        Me.topMarginBand1.HeightF = 0!
        Me.topMarginBand1.Name = "topMarginBand1"
        '
        'detail
        '
        Me.detail.HeightF = 0.3750483!
        Me.detail.Name = "detail"
        Me.detail.StylePriority.UseTextAlignment = False
        '
        'bottomMarginBand1
        '
        Me.bottomMarginBand1.HeightF = 1.041667!
        Me.bottomMarginBand1.Name = "bottomMarginBand1"
        Me.bottomMarginBand1.Visible = False
        '
        'Title
        '
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.BorderColor = System.Drawing.Color.Black
        Me.Title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Title.BorderWidth = 1.0!
        Me.Title.Font = New System.Drawing.Font("Times New Roman", 24.0!)
        Me.Title.ForeColor = System.Drawing.Color.Black
        Me.Title.Name = "Title"
        '
        'FieldCaption
        '
        Me.FieldCaption.BackColor = System.Drawing.Color.Transparent
        Me.FieldCaption.BorderColor = System.Drawing.Color.Black
        Me.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.FieldCaption.BorderWidth = 1.0!
        Me.FieldCaption.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.FieldCaption.ForeColor = System.Drawing.Color.Black
        Me.FieldCaption.Name = "FieldCaption"
        '
        'PageInfo
        '
        Me.PageInfo.BackColor = System.Drawing.Color.Transparent
        Me.PageInfo.BorderColor = System.Drawing.Color.Black
        Me.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.PageInfo.BorderWidth = 1.0!
        Me.PageInfo.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.PageInfo.ForeColor = System.Drawing.Color.Black
        Me.PageInfo.Name = "PageInfo"
        '
        'DataField
        '
        Me.DataField.BackColor = System.Drawing.Color.Transparent
        Me.DataField.BorderColor = System.Drawing.Color.Black
        Me.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DataField.BorderWidth = 1.0!
        Me.DataField.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.DataField.ForeColor = System.Drawing.Color.Black
        Me.DataField.Name = "DataField"
        Me.DataField.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        '
        'FuenteDatos
        '
        Me.FuenteDatos.DataSource = GetType(AppFin.ProduccionBase)
        Me.FuenteDatos.Name = "FuenteDatos"
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1, Me.GroupHeader1})
        Me.DetailReport.DataMember = "DetalleProceso"
        Me.DetailReport.DataSource = Me.FuenteDatos2
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode1, Me.pictureBox1, Me.label26, Me.label24, Me.label23, Me.label21, Me.label20, Me.label19, Me.label18, Me.label17, Me.line3, Me.label33, Me.barCode1, Me.line6, Me.label34, Me.line7, Me.label35, Me.label1, Me.label36, Me.line8, Me.line9, Me.label2, Me.line10, Me.label3, Me.line11, Me.label4, Me.line12, Me.label5, Me.line13, Me.line14, Me.label6, Me.label7, Me.line15, Me.line16, Me.label8, Me.label9, Me.line5, Me.XrLabel1})
        Me.Detail1.HeightF = 627.4584!
        Me.Detail1.Name = "Detail1"
        '
        'XrBarCode1
        '
        Me.XrBarCode1.AutoModule = True
        Me.XrBarCode1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NoPiezas")})
        Me.XrBarCode1.LocationFloat = New DevExpress.Utils.PointFloat(534.3329!, 242.5418!)
        Me.XrBarCode1.Name = "XrBarCode1"
        Me.XrBarCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.XrBarCode1.ShowText = False
        Me.XrBarCode1.SizeF = New System.Drawing.SizeF(171.6251!, 49.99986!)
        Me.XrBarCode1.Symbology = Code128Generator1
        '
        'pictureBox1
        '
        Me.pictureBox1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Image", Nothing, "Imagen")})
        Me.pictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(756.1666!, 242.5418!)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.SizeF = New System.Drawing.SizeF(338.833!, 363.2083!)
        Me.pictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'label26
        '
        Me.label26.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MiUsuario")})
        Me.label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label26.LocationFloat = New DevExpress.Utils.PointFloat(161.0418!, 404.6251!)
        Me.label26.Name = "label26"
        Me.label26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label26.SizeF = New System.Drawing.SizeF(235.4167!, 29.45837!)
        Me.label26.StyleName = "DataField"
        Me.label26.StylePriority.UseFont = False
        Me.label26.StylePriority.UseTextAlignment = False
        Me.label26.Text = "label26"
        Me.label26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label24
        '
        Me.label24.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NoPiezas")})
        Me.label24.Font = New System.Drawing.Font("Times New Roman", 26.25!, System.Drawing.FontStyle.Bold)
        Me.label24.LocationFloat = New DevExpress.Utils.PointFloat(186.0418!, 256.5417!)
        Me.label24.Name = "label24"
        Me.label24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label24.SizeF = New System.Drawing.SizeF(268.4584!, 35.99994!)
        Me.label24.StyleName = "DataField"
        Me.label24.StylePriority.UseFont = False
        '
        'label23
        '
        Me.label23.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NoParte")})
        Me.label23.Font = New System.Drawing.Font("Times New Roman", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label23.LocationFloat = New DevExpress.Utils.PointFloat(161.0418!, 79.95844!)
        Me.label23.Name = "label23"
        Me.label23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label23.SizeF = New System.Drawing.SizeF(893.4584!, 62.18739!)
        Me.label23.StyleName = "DataField"
        Me.label23.StylePriority.UseFont = False
        Me.label23.Text = "label23"
        '
        'label21
        '
        Me.label21.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MiMaquina")})
        Me.label21.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label21.LocationFloat = New DevExpress.Utils.PointFloat(430.2501!, 564.4167!)
        Me.label21.Name = "label21"
        Me.label21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label21.SizeF = New System.Drawing.SizeF(318.5417!, 46.79156!)
        Me.label21.StyleName = "DataField"
        Me.label21.StylePriority.UseFont = False
        Me.label21.Text = "label21"
        '
        'label20
        '
        Me.label20.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LoteCliente")})
        Me.label20.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label20.LocationFloat = New DevExpress.Utils.PointFloat(430.25!, 464.125!)
        Me.label20.Name = "label20"
        Me.label20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label20.SizeF = New System.Drawing.SizeF(299.9583!, 50.93744!)
        Me.label20.StyleName = "DataField"
        Me.label20.StylePriority.UseFont = False
        '
        'label19
        '
        Me.label19.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LoteCliente")})
        Me.label19.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label19.LocationFloat = New DevExpress.Utils.PointFloat(156.9167!, 464.125!)
        Me.label19.Name = "label19"
        Me.label19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label19.SizeF = New System.Drawing.SizeF(145.2916!, 50.93744!)
        Me.label19.StyleName = "DataField"
        Me.label19.StylePriority.UseFont = False
        '
        'label18
        '
        Me.label18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FechaProducion", "{0:dd/MM/yyyy}")})
        Me.label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label18.LocationFloat = New DevExpress.Utils.PointFloat(504.7083!, 352.5416!)
        Me.label18.Multiline = True
        Me.label18.Name = "label18"
        Me.label18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label18.SizeF = New System.Drawing.SizeF(158.8333!, 34.45828!)
        Me.label18.StyleName = "DataField"
        Me.label18.StylePriority.UseFont = False
        Me.label18.StylePriority.UseTextAlignment = False
        Me.label18.Text = "labe"
        Me.label18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label17
        '
        Me.label17.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FechaCaducidad", "{0:dd/MM/yyyy}")})
        Me.label17.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label17.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 352.5416!)
        Me.label17.Name = "label17"
        Me.label17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label17.SizeF = New System.Drawing.SizeF(146.6667!, 41.99994!)
        Me.label17.StyleName = "DataField"
        Me.label17.StylePriority.UseFont = False
        Me.label17.Text = "label17"
        '
        'line3
        '
        Me.line3.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 68.41685!)
        Me.line3.Name = "line3"
        Me.line3.SizeF = New System.Drawing.SizeF(1090.0!, 11.54167!)
        '
        'label33
        '
        Me.label33.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label33.LocationFloat = New DevExpress.Utils.PointFloat(44.58349!, 0.7082303!)
        Me.label33.Name = "label33"
        Me.label33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label33.SizeF = New System.Drawing.SizeF(78.12494!, 38.45835!)
        Me.label33.StyleName = "FieldCaption"
        Me.label33.StylePriority.UseFont = False
        Me.label33.Text = "1248"
        '
        'barCode1
        '
        Me.barCode1.AutoModule = True
        Me.barCode1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NoParteCodigo")})
        Me.barCode1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.barCode1.LocationFloat = New DevExpress.Utils.PointFloat(218.0418!, 0.7082303!)
        Me.barCode1.Name = "barCode1"
        Me.barCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.barCode1.ShowText = False
        Me.barCode1.SizeF = New System.Drawing.SizeF(836.4583!, 67.70852!)
        Me.barCode1.StylePriority.UseFont = False
        Me.barCode1.Symbology = Code128Generator2
        '
        'line6
        '
        Me.line6.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 147.625!)
        Me.line6.Name = "line6"
        Me.line6.SizeF = New System.Drawing.SizeF(1090.0!, 11.54166!)
        '
        'label34
        '
        Me.label34.BackColor = System.Drawing.Color.LightGray
        Me.label34.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label34.ForeColor = System.Drawing.Color.Black
        Me.label34.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 74.62502!)
        Me.label34.Name = "label34"
        Me.label34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label34.SizeF = New System.Drawing.SizeF(151.0!, 84.54163!)
        Me.label34.StyleName = "FieldCaption"
        Me.label34.StylePriority.UseBackColor = False
        Me.label34.StylePriority.UseFont = False
        Me.label34.StylePriority.UseForeColor = False
        Me.label34.StylePriority.UseTextAlignment = False
        Me.label34.Text = "Número de Wet Blast"
        Me.label34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line7
        '
        Me.line7.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line7.LocationFloat = New DevExpress.Utils.PointFloat(148.5835!, 74.62502!)
        Me.line7.Name = "line7"
        Me.line7.SizeF = New System.Drawing.SizeF(8.333328!, 541.0418!)
        '
        'label35
        '
        Me.label35.BackColor = System.Drawing.Color.LightGray
        Me.label35.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label35.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 153.6875!)
        Me.label35.Name = "label35"
        Me.label35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label35.SizeF = New System.Drawing.SizeF(147.7084!, 85.24979!)
        Me.label35.StyleName = "FieldCaption"
        Me.label35.StylePriority.UseBackColor = False
        Me.label35.StylePriority.UseFont = False
        Me.label35.StylePriority.UseTextAlignment = False
        Me.label35.Text = "Nombre de parte"
        Me.label35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label1
        '
        Me.label1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DescripcionProducto")})
        Me.label1.Font = New System.Drawing.Font("Times New Roman", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.LocationFloat = New DevExpress.Utils.PointFloat(185.0!, 170.5!)
        Me.label1.Name = "label1"
        Me.label1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label1.SizeF = New System.Drawing.SizeF(892.4164!, 60.5!)
        Me.label1.StylePriority.UseFont = False
        '
        'label36
        '
        Me.label36.BackColor = System.Drawing.Color.LightGray
        Me.label36.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label36.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 242.5416!)
        Me.label36.Name = "label36"
        Me.label36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label36.SizeF = New System.Drawing.SizeF(146.6667!, 67.54163!)
        Me.label36.StyleName = "FieldCaption"
        Me.label36.StylePriority.UseBackColor = False
        Me.label36.StylePriority.UseFont = False
        Me.label36.StylePriority.UseTextAlignment = False
        Me.label36.Text = "Cantidad"
        Me.label36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line8
        '
        Me.line8.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 231.0!)
        Me.line8.Name = "line8"
        Me.line8.SizeF = New System.Drawing.SizeF(1090.0!, 11.54166!)
        '
        'line9
        '
        Me.line9.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 298.5416!)
        Me.line9.Name = "line9"
        Me.line9.SizeF = New System.Drawing.SizeF(750.125!, 11.54166!)
        '
        'label2
        '
        Me.label2.BackColor = System.Drawing.Color.LightGray
        Me.label2.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 304.5417!)
        Me.label2.Name = "label2"
        Me.label2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label2.SizeF = New System.Drawing.SizeF(146.6667!, 38.0!)
        Me.label2.StyleName = "FieldCaption"
        Me.label2.StylePriority.UseBackColor = False
        Me.label2.StylePriority.UseFont = False
        Me.label2.StylePriority.UseTextAlignment = False
        Me.label2.Text = "Caducidad"
        Me.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line10
        '
        Me.line10.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 336.1251!)
        Me.line10.Name = "line10"
        Me.line10.SizeF = New System.Drawing.SizeF(750.125!, 11.54004!)
        '
        'label3
        '
        Me.label3.BackColor = System.Drawing.Color.LightGray
        Me.label3.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.LocationFloat = New DevExpress.Utils.PointFloat(158.0417!, 304.5417!)
        Me.label3.Name = "label3"
        Me.label3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label3.SizeF = New System.Drawing.SizeF(267.9999!, 37.00003!)
        Me.label3.StyleName = "FieldCaption"
        Me.label3.StylePriority.UseBackColor = False
        Me.label3.StylePriority.UseFont = False
        Me.label3.StylePriority.UseTextAlignment = False
        Me.label3.Text = "Operador"
        Me.label3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line11
        '
        Me.line11.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 446.5835!)
        Me.line11.Name = "line11"
        Me.line11.SizeF = New System.Drawing.SizeF(746.875!, 11.54166!)
        '
        'label4
        '
        Me.label4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ClaveUsuario")})
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.LocationFloat = New DevExpress.Utils.PointFloat(161.0418!, 352.5416!)
        Me.label4.Name = "label4"
        Me.label4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label4.SizeF = New System.Drawing.SizeF(235.4167!, 34.45831!)
        Me.label4.StylePriority.UseFont = False
        Me.label4.StylePriority.UseTextAlignment = False
        Me.label4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line12
        '
        Me.line12.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line12.LocationFloat = New DevExpress.Utils.PointFloat(747.7501!, 231.0!)
        Me.line12.Name = "line12"
        Me.line12.SizeF = New System.Drawing.SizeF(8.333313!, 386.4583!)
        '
        'label5
        '
        Me.label5.BackColor = System.Drawing.Color.LightGray
        Me.label5.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.LocationFloat = New DevExpress.Utils.PointFloat(426.0416!, 304.5417!)
        Me.label5.Name = "label5"
        Me.label5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label5.SizeF = New System.Drawing.SizeF(330.1251!, 37.0!)
        Me.label5.StyleName = "FieldCaption"
        Me.label5.StylePriority.UseBackColor = False
        Me.label5.StylePriority.UseFont = False
        Me.label5.StylePriority.UseTextAlignment = False
        Me.label5.Text = "Fecha y hora"
        Me.label5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line13
        '
        Me.line13.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line13.LocationFloat = New DevExpress.Utils.PointFloat(421.9168!, 343.375!)
        Me.line13.Name = "line13"
        Me.line13.SizeF = New System.Drawing.SizeF(8.333313!, 109.0!)
        '
        'line14
        '
        Me.line14.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 515.0624!)
        Me.line14.Name = "line14"
        Me.line14.SizeF = New System.Drawing.SizeF(746.875!, 11.54169!)
        '
        'label6
        '
        Me.label6.BackColor = System.Drawing.Color.LightGray
        Me.label6.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.LocationFloat = New DevExpress.Utils.PointFloat(6.041667!, 452.375!)
        Me.label6.Name = "label6"
        Me.label6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label6.SizeF = New System.Drawing.SizeF(146.6667!, 68.0!)
        Me.label6.StyleName = "FieldCaption"
        Me.label6.StylePriority.UseBackColor = False
        Me.label6.StylePriority.UseFont = False
        Me.label6.StylePriority.UseTextAlignment = False
        Me.label6.Text = "Lote interno"
        Me.label6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label7
        '
        Me.label7.BackColor = System.Drawing.Color.LightGray
        Me.label7.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.LocationFloat = New DevExpress.Utils.PointFloat(304.9167!, 452.375!)
        Me.label7.Name = "label7"
        Me.label7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label7.SizeF = New System.Drawing.SizeF(120.7916!, 68.0!)
        Me.label7.StyleName = "FieldCaption"
        Me.label7.StylePriority.UseBackColor = False
        Me.label7.StylePriority.UseFont = False
        Me.label7.StylePriority.UseTextAlignment = False
        Me.label7.Text = "Lote cliente"
        Me.label7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line15
        '
        Me.line15.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line15.LocationFloat = New DevExpress.Utils.PointFloat(302.2083!, 451.375!)
        Me.line15.Name = "line15"
        Me.line15.SizeF = New System.Drawing.SizeF(8.333344!, 176.0834!)
        '
        'line16
        '
        Me.line16.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line16.LocationFloat = New DevExpress.Utils.PointFloat(421.9168!, 451.375!)
        Me.line16.Name = "line16"
        Me.line16.SizeF = New System.Drawing.SizeF(8.333344!, 159.8334!)
        '
        'label8
        '
        Me.label8.BackColor = System.Drawing.Color.LightGray
        Me.label8.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.LocationFloat = New DevExpress.Utils.PointFloat(6.041731!, 549.667!)
        Me.label8.Name = "label8"
        Me.label8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label8.SizeF = New System.Drawing.SizeF(146.6667!, 67.54163!)
        Me.label8.StyleName = "FieldCaption"
        Me.label8.StylePriority.UseBackColor = False
        Me.label8.StylePriority.UseFont = False
        Me.label8.StylePriority.UseTextAlignment = False
        Me.label8.Text = "Inicio de Horneado"
        Me.label8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label9
        '
        Me.label9.BackColor = System.Drawing.Color.LightGray
        Me.label9.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.LocationFloat = New DevExpress.Utils.PointFloat(306.9169!, 549.667!)
        Me.label9.Name = "label9"
        Me.label9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label9.SizeF = New System.Drawing.SizeF(118.0!, 66.0!)
        Me.label9.StyleName = "FieldCaption"
        Me.label9.StylePriority.UseBackColor = False
        Me.label9.StylePriority.UseFont = False
        Me.label9.StylePriority.UseTextAlignment = False
        Me.label9.Text = "Maquina Wet Blast"
        Me.label9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'line5
        '
        Me.line5.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical
        Me.line5.LocationFloat = New DevExpress.Utils.PointFloat(160.0!, 0!)
        Me.line5.Name = "line5"
        Me.line5.SizeF = New System.Drawing.SizeF(8.333328!, 78.20835!)
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "FechaProducion", "{0:HH:mm:ss}")})
        Me.XrLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(504.7083!, 386.9999!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(158.8333!, 29.45844!)
        Me.XrLabel1.StyleName = "DataField"
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "labe"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.label14, Me.label27, Me.label31, Me.line4, Me.Codigo, Me.line2, Me.label32, Me.line1, Me.label29, Me.label10, Me.label28, Me.label30})
        Me.GroupHeader1.HeightF = 114.5833!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'label14
        '
        Me.label14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Almacen")})
        Me.label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label14.LocationFloat = New DevExpress.Utils.PointFloat(907.9163!, 59.18748!)
        Me.label14.Name = "label14"
        Me.label14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label14.SizeF = New System.Drawing.SizeF(148.6667!, 41.41671!)
        Me.label14.StyleName = "DataField"
        Me.label14.StylePriority.UseFont = False
        '
        'label27
        '
        Me.label27.BackColor = System.Drawing.Color.Black
        Me.label27.Font = New System.Drawing.Font("Times New Roman", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label27.ForeColor = System.Drawing.Color.White
        Me.label27.LocationFloat = New DevExpress.Utils.PointFloat(284.458!, 8.145841!)
        Me.label27.Name = "label27"
        Me.label27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label27.SizeF = New System.Drawing.SizeF(249.8749!, 57.49998!)
        Me.label27.StylePriority.UseBackColor = False
        Me.label27.StylePriority.UseFont = False
        Me.label27.StylePriority.UseForeColor = False
        Me.label27.StylePriority.UseTextAlignment = False
        Me.label27.Text = "Wet Blast"
        Me.label27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label31
        '
        Me.label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label31.LocationFloat = New DevExpress.Utils.PointFloat(800.6245!, 60.39581!)
        Me.label31.Name = "label31"
        Me.label31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label31.SizeF = New System.Drawing.SizeF(107.2917!, 41.41671!)
        Me.label31.StyleName = "FieldCaption"
        Me.label31.StylePriority.UseFont = False
        Me.label31.Text = "Almacen:"
        '
        'line4
        '
        Me.line4.LocationFloat = New DevExpress.Utils.PointFloat(813.1245!, 47.64582!)
        Me.line4.Name = "line4"
        Me.line4.SizeF = New System.Drawing.SizeF(255.0417!, 11.54166!)
        '
        'Codigo
        '
        Me.Codigo.AutoModule = True
        Me.Codigo.LocationFloat = New DevExpress.Utils.PointFloat(61.87471!, 8.145841!)
        Me.Codigo.Name = "Codigo"
        Me.Codigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.Codigo.ShowText = False
        Me.Codigo.SizeF = New System.Drawing.SizeF(199.7501!, 68.75!)
        Me.Codigo.Symbology = Code128Generator3
        Me.Codigo.Text = "1248"
        '
        'line2
        '
        Me.line2.LocationFloat = New DevExpress.Utils.PointFloat(0!, 101.8126!)
        Me.line2.Name = "line2"
        Me.line2.SizeF = New System.Drawing.SizeF(1090.0!, 11.54165!)
        '
        'label32
        '
        Me.label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label32.LocationFloat = New DevExpress.Utils.PointFloat(907.916!, 9.270795!)
        Me.label32.Name = "label32"
        Me.label32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label32.SizeF = New System.Drawing.SizeF(148.6667!, 38.37503!)
        Me.label32.StyleName = "FieldCaption"
        Me.label32.StylePriority.UseFont = False
        Me.label32.Text = "Wet Blast"
        '
        'line1
        '
        Me.line1.LocationFloat = New DevExpress.Utils.PointFloat(180.166!, 2.270826!)
        Me.line1.Name = "line1"
        Me.line1.SizeF = New System.Drawing.SizeF(888.0!, 2.0!)
        '
        'label29
        '
        Me.label29.BackColor = System.Drawing.Color.Black
        Me.label29.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label29.ForeColor = System.Drawing.Color.White
        Me.label29.LocationFloat = New DevExpress.Utils.PointFloat(285.0414!, 65.64585!)
        Me.label29.Name = "label29"
        Me.label29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label29.SizeF = New System.Drawing.SizeF(516.1663!, 29.16667!)
        Me.label29.StylePriority.UseBackColor = False
        Me.label29.StylePriority.UseFont = False
        Me.label29.StylePriority.UseForeColor = False
        Me.label29.StylePriority.UseTextAlignment = False
        Me.label29.Text = "MX-PR-WB-HT-001"
        Me.label29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label10
        '
        Me.label10.BackColor = System.Drawing.Color.Black
        Me.label10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label10.ForeColor = System.Drawing.Color.White
        Me.label10.LocationFloat = New DevExpress.Utils.PointFloat(535.1664!, 47.64582!)
        Me.label10.Name = "label10"
        Me.label10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label10.SizeF = New System.Drawing.SizeF(266.0414!, 29.25002!)
        Me.label10.StylePriority.UseBackColor = False
        Me.label10.StylePriority.UseFont = False
        Me.label10.StylePriority.UseForeColor = False
        Me.label10.StylePriority.UseTextAlignment = False
        Me.label10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label28
        '
        Me.label28.BackColor = System.Drawing.Color.Black
        Me.label28.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label28.ForeColor = System.Drawing.Color.White
        Me.label28.LocationFloat = New DevExpress.Utils.PointFloat(534.3329!, 8.145841!)
        Me.label28.Name = "label28"
        Me.label28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label28.SizeF = New System.Drawing.SizeF(266.2915!, 40.70832!)
        Me.label28.StylePriority.UseBackColor = False
        Me.label28.StylePriority.UseFont = False
        Me.label28.StylePriority.UseForeColor = False
        Me.label28.StylePriority.UseTextAlignment = False
        Me.label28.Text = "(Hoja de traslado)"
        Me.label28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'label30
        '
        Me.label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label30.LocationFloat = New DevExpress.Utils.PointFloat(800.6245!, 9.270795!)
        Me.label30.Name = "label30"
        Me.label30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label30.SizeF = New System.Drawing.SizeF(107.2917!, 38.37503!)
        Me.label30.StyleName = "FieldCaption"
        Me.label30.StylePriority.UseFont = False
        Me.label30.Text = "Emision:"
        '
        'FuenteDatos2
        '
        Me.FuenteDatos2.DataSource = GetType(AppFin.ProduccionBase)
        Me.FuenteDatos2.Name = "FuenteDatos2"
        '
        'PlantillaProducto
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.topMarginBand1, Me.detail, Me.bottomMarginBand1, Me.DetailReport})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.FuenteDatos2})
        Me.DataSource = Me.FuenteDatos2
        Me.DisplayName = "Plantilla"
        Me.Extensions.Add("DataSerializationExtension", "XtraReport")
        Me.Extensions.Add("DataEditorExtension", "XtraReport")
        Me.Extensions.Add("ParameterEditorExtension", "XtraReport")
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(0, 0, 0, 1)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Title, Me.FieldCaption, Me.PageInfo, Me.DataField})
        Me.Version = "17.1"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteDatos2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents topMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents bottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    'Friend collectionDataSource1 As DevExpress.Persistent.Base.ReportsV2.CollectionDataSource
    Friend WithEvents Title As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FieldCaption As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageInfo As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DataField As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FuenteDatos As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents pictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents label26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents barCode1 As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents line6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line7 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line8 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents line9 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line10 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line11 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line12 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line13 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents line14 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line15 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents line16 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents label14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents Codigo As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents line2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents line1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents label29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrBarCode1 As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents FuenteDatos2 As DevExpress.DataAccess.ObjectBinding.ObjectDataSource
End Class
