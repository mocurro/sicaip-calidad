﻿Imports Mini.Zeus.Cliente

Public Class CatalogoTipo_Inpeccion

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))


        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Tipo_Inspeccion   ), Datos.Fabricas .Tipo_Inpeccion  , GetType(TarjetaTipo_Inpeccion  )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Tipo_Inspeccion  )

    End Function
End Class