﻿Imports DevExpress.XtraEditors

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TarjetaLABDIMSolicitud
    Inherits Mini.Zeus.Cliente.UX.FormaTarjeta

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ComboComponente = New DevExpress.XtraEditors.LookUpEdit()
        Me.FuenteModelos = New System.Windows.Forms.BindingSource(Me.components)
        Me.itemComponente = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtSolicitante = New DevExpress.XtraEditors.TextEdit()
        Me.itemSolicitante = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit()
        Me.ItemDescripción = New DevExpress.XtraLayout.LayoutControlItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.itemDiseño = New DevExpress.XtraLayout.LayoutControlItem()
        Me.combodiseño = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtComentarios = New DevExpress.XtraEditors.TextEdit()
        Me.itemComentarios = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SpinPiezas = New DevExpress.XtraEditors.SpinEdit()
        Me.ItemPiezas = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.txtNoParte = New DevExpress.XtraEditors.TextEdit()
        Me.itemComponenteNew = New DevExpress.XtraLayout.LayoutControlItem()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutTarjeta.SuspendLayout()
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolicitante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemSolicitante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemDescripción, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemDiseño, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.combodiseño.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComentarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemComentarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpinPiezas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemPiezas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoParte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.itemComponenteNew, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutTarjeta
        '
        Me.LayoutTarjeta.Controls.Add(Me.CheckEdit1)
        Me.LayoutTarjeta.Controls.Add(Me.txtNoParte)
        Me.LayoutTarjeta.Controls.Add(Me.SpinPiezas)
        Me.LayoutTarjeta.Controls.Add(Me.txtComentarios)
        Me.LayoutTarjeta.Controls.Add(Me.txtDescripcion)
        Me.LayoutTarjeta.Controls.Add(Me.txtSolicitante)
        Me.LayoutTarjeta.Controls.Add(Me.ComboComponente)
        Me.LayoutTarjeta.Controls.Add(Me.combodiseño)
        Me.LayoutTarjeta.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(725, 62, 250, 350)
        Me.LayoutTarjeta.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups
        Me.LayoutTarjeta.OptionsView.AllowHotTrack = True
        Me.LayoutTarjeta.OptionsView.HighlightFocusedItem = True
        Me.LayoutTarjeta.Size = New System.Drawing.Size(520, 222)
        '
        'LayoutTarjetaRaiz
        '
        Me.LayoutTarjetaRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.itemComponente, Me.itemSolicitante, Me.ItemDescripción, Me.itemDiseño, Me.itemComentarios, Me.ItemPiezas, Me.itemComponenteNew, Me.LayoutControlItem2})
        Me.LayoutTarjetaRaiz.Size = New System.Drawing.Size(520, 222)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 191)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(500, 11)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'ComboComponente
        '
        Me.ComboComponente.Location = New System.Drawing.Point(130, 36)
        Me.ComboComponente.Name = "ComboComponente"
        Me.ComboComponente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboComponente.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("np_gkn", "np_gkn", 58, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.ComboComponente.Properties.DataSource = Me.FuenteModelos
        Me.ComboComponente.Properties.DisplayMember = "np_gkn"
        Me.ComboComponente.Properties.NullText = "Seleccione número de parte"
        Me.ComboComponente.Properties.ValueMember = "cve_modelo"
        Me.ComboComponente.Size = New System.Drawing.Size(378, 20)
        Me.ComboComponente.StyleController = Me.LayoutTarjeta
        Me.ComboComponente.TabIndex = 8
        '
        'itemComponente
        '
        Me.itemComponente.Control = Me.ComboComponente
        Me.itemComponente.Location = New System.Drawing.Point(0, 24)
        Me.itemComponente.Name = "itemComponente"
        Me.itemComponente.Size = New System.Drawing.Size(500, 24)
        Me.itemComponente.Text = "Número de parte"
        Me.itemComponente.TextSize = New System.Drawing.Size(114, 13)
        '
        'txtSolicitante
        '
        Me.txtSolicitante.Enabled = False
        Me.txtSolicitante.Location = New System.Drawing.Point(130, 12)
        Me.txtSolicitante.Name = "txtSolicitante"
        Me.txtSolicitante.Size = New System.Drawing.Size(378, 20)
        Me.txtSolicitante.StyleController = Me.LayoutTarjeta
        Me.txtSolicitante.TabIndex = 9
        '
        'itemSolicitante
        '
        Me.itemSolicitante.Control = Me.txtSolicitante
        Me.itemSolicitante.Location = New System.Drawing.Point(0, 0)
        Me.itemSolicitante.Name = "itemSolicitante"
        Me.itemSolicitante.Size = New System.Drawing.Size(500, 24)
        Me.itemSolicitante.Text = "Solicitante"
        Me.itemSolicitante.TextSize = New System.Drawing.Size(114, 13)
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Enabled = False
        Me.txtDescripcion.Location = New System.Drawing.Point(130, 107)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(378, 20)
        Me.txtDescripcion.StyleController = Me.LayoutTarjeta
        Me.txtDescripcion.TabIndex = 11
        '
        'ItemDescripción
        '
        Me.ItemDescripción.Control = Me.txtDescripcion
        Me.ItemDescripción.Location = New System.Drawing.Point(0, 95)
        Me.ItemDescripción.Name = "ItemDescripción"
        Me.ItemDescripción.Size = New System.Drawing.Size(500, 24)
        Me.ItemDescripción.Text = "Descripción"
        Me.ItemDescripción.TextSize = New System.Drawing.Size(114, 13)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'itemDiseño
        '
        Me.itemDiseño.Control = Me.combodiseño
        Me.itemDiseño.Location = New System.Drawing.Point(0, 143)
        Me.itemDiseño.Name = "itemDiseño"
        Me.itemDiseño.Size = New System.Drawing.Size(500, 24)
        Me.itemDiseño.Text = "Diseño"
        Me.itemDiseño.TextSize = New System.Drawing.Size(114, 13)
        '
        'combodiseño
        '
        Me.BehaviorManager1.SetBehaviors(Me.combodiseño, New DevExpress.Utils.Behaviors.Behavior() {CType(DevExpress.Utils.Behaviors.Common.OpenFileBehavior.Create(GetType(DevExpress.XtraEditors.Behaviors.OpenFileBehaviorSourceForButtonEdit), True, DevExpress.Utils.Behaviors.Common.FileIconSize.Small, Nothing, Nothing, DevExpress.Utils.Behaviors.Common.CompletionMode.FilesAndDirectories, Nothing), DevExpress.Utils.Behaviors.Behavior)})
        Me.combodiseño.EditValue = "Seleccionar diseño"
        Me.combodiseño.Location = New System.Drawing.Point(130, 155)
        Me.combodiseño.Name = "combodiseño"
        Me.combodiseño.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.combodiseño.Properties.NullText = "[EditValue is null]"
        Me.combodiseño.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.combodiseño.Size = New System.Drawing.Size(378, 20)
        Me.combodiseño.StyleController = Me.LayoutTarjeta
        Me.combodiseño.TabIndex = 12
        '
        'txtComentarios
        '
        Me.txtComentarios.Location = New System.Drawing.Point(130, 179)
        Me.txtComentarios.Name = "txtComentarios"
        Me.txtComentarios.Size = New System.Drawing.Size(378, 20)
        Me.txtComentarios.StyleController = Me.LayoutTarjeta
        Me.txtComentarios.TabIndex = 13
        '
        'itemComentarios
        '
        Me.itemComentarios.Control = Me.txtComentarios
        Me.itemComentarios.Location = New System.Drawing.Point(0, 167)
        Me.itemComentarios.Name = "itemComentarios"
        Me.itemComentarios.Size = New System.Drawing.Size(500, 24)
        Me.itemComentarios.Text = "Comentarios"
        Me.itemComentarios.TextSize = New System.Drawing.Size(114, 13)
        '
        'SpinPiezas
        '
        Me.SpinPiezas.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinPiezas.Location = New System.Drawing.Point(130, 131)
        Me.SpinPiezas.Name = "SpinPiezas"
        Me.SpinPiezas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinPiezas.Properties.MaxLength = 999
        Me.SpinPiezas.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.SpinPiezas.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SpinPiezas.Properties.NullText = "1"
        Me.SpinPiezas.Size = New System.Drawing.Size(378, 20)
        Me.SpinPiezas.StyleController = Me.LayoutTarjeta
        Me.SpinPiezas.TabIndex = 14
        '
        'ItemPiezas
        '
        Me.ItemPiezas.Control = Me.SpinPiezas
        Me.ItemPiezas.Location = New System.Drawing.Point(0, 119)
        Me.ItemPiezas.Name = "ItemPiezas"
        Me.ItemPiezas.Size = New System.Drawing.Size(500, 24)
        Me.ItemPiezas.Text = "Número de piezas"
        Me.ItemPiezas.TextSize = New System.Drawing.Size(114, 13)
        '
        'txtNoParte
        '
        Me.txtNoParte.Location = New System.Drawing.Point(130, 83)
        Me.txtNoParte.Name = "txtNoParte"
        Me.txtNoParte.Properties.Mask.EditMask = "d"
        Me.txtNoParte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNoParte.Size = New System.Drawing.Size(378, 20)
        Me.txtNoParte.StyleController = Me.LayoutTarjeta
        Me.txtNoParte.TabIndex = 15
        '
        'itemComponenteNew
        '
        Me.itemComponenteNew.Control = Me.txtNoParte
        Me.itemComponenteNew.Location = New System.Drawing.Point(0, 71)
        Me.itemComponenteNew.Name = "itemComponenteNew"
        Me.itemComponenteNew.Size = New System.Drawing.Size(500, 24)
        Me.itemComponenteNew.Text = "Nuevo número de parte"
        Me.itemComponenteNew.TextSize = New System.Drawing.Size(114, 13)
        Me.itemComponenteNew.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(12, 60)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "Otro número de parte"
        Me.CheckEdit1.Size = New System.Drawing.Size(496, 19)
        Me.CheckEdit1.StyleController = Me.LayoutTarjeta
        Me.CheckEdit1.TabIndex = 16
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.CheckEdit1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(500, 23)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'TarjetaLABDIMSolicitud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 348)
        Me.Name = "TarjetaLABDIMSolicitud"
        Me.Text = "TarjetaLabDim"
        CType(Me.FuenteDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ManejadorValidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutTarjeta.ResumeLayout(False)
        CType(Me.LayoutTarjetaRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboComponente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FuenteModelos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComponente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolicitante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemSolicitante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemDescripción, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemDiseño, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.combodiseño.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComentarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComentarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpinPiezas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemPiezas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoParte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.itemComponenteNew, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboComponente As LookUpEdit
    Friend WithEvents itemComponente As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents FuenteModelos As BindingSource
    Friend WithEvents txtComentarios As TextEdit
    Friend WithEvents txtDescripcion As TextEdit
    Friend WithEvents txtSolicitante As TextEdit
    Friend WithEvents itemSolicitante As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemDescripción As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemDiseño As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents itemComentarios As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents SpinPiezas As SpinEdit
    Friend WithEvents ItemPiezas As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents combodiseño As ButtonEdit
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents CheckEdit1 As CheckEdit
    Friend WithEvents txtNoParte As TextEdit
    Friend WithEvents itemComponenteNew As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
End Class
