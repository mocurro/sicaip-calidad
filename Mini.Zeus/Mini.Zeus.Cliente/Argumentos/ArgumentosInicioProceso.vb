﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Argumentos para eventos que indican el inicio de un proceso.
''' </summary>
''' <remarks>Estos argumentos permiten preparar la interface visual para notificar progreso de procesos.</remarks>
Public Class ArgumentosInicioProceso
    Inherits EventArgs

    ''' <summary>
    ''' Contiene el número total de pasos del proceso.
    ''' </summary>
    Private MiTotalPasos As Integer

    ''' <summary>
    ''' Crea un nuevo argumento.
    ''' </summary>
    ''' <param name="TotalPasos">El número total de pasos del proceso.</param>
    Public Sub New(ByVal TotalPasos As Integer)

        MyBase.New()
        MiTotalPasos = TotalPasos

    End Sub

    ''' <summary>
    ''' Determinal el número total de pasos del proceso.
    ''' </summary>
    Public ReadOnly Property TotalPasos() As Integer
        Get
            Return MiTotalPasos
        End Get
    End Property

End Class
