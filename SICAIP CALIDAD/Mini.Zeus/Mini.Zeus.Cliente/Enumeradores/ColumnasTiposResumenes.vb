﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los diferentes tipos de resumen (summary) que se pueden utilizar en el atributo de Columna.
''' </summary>
''' <remarks>Este enumerador es compatible con el enumerador DevExpress.Data.SummaryItemType</remarks>
Public Enum ColumnasTiposResumenes As Integer

    ''' <summary>
    ''' No se aplica resumen.
    ''' </summary>
    Ninguno = 6

    ''' <summary>
    ''' El resumen es la suma de todos los valores.
    ''' </summary>
    Suma = 0

    ''' <summary>
    ''' El resumen es el valor más pequeño de todos los valores.
    ''' </summary>
    Minimo = 1

    ''' <summary>
    ''' El resumen es el valor más grande de todos los valores.
    ''' </summary>
    Maximo = 2

    ''' <summary>
    ''' El resumen es la cuenta del número de valores.
    ''' </summary>
    Cuenta = 3

    ''' <summary>
    ''' El resumen es el valor promedio de todos los valores.
    ''' </summary>
    Promedio = 4

    ''' <summary>
    ''' El resumen se genera de forma personalizada a través de un evento especial.
    ''' </summary>
    Personalizado = 5

End Enum