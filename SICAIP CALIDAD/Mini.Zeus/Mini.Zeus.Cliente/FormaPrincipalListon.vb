﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports Microsoft.WindowsAPICodePack
Imports Microsoft.WindowsAPICodePack.Dialogs
Imports Microsoft.WindowsAPICodePack.Taskbar

''' <summary>
''' Ventana base con listón para cualquier ventana principal de aplicación basada en Zeus.
''' </summary>
''' <remarks>Esta ventana contiene funciones comunes para cualquier ventana principal que implemente UX de Zeus. Todas las ventanas principales heredan finalmente de esta ventana.</remarks>
Public Class FormaPrincipalListon
    Implements IFormaPrincipal

    ''' <summary>
    ''' Evento para notificar que se ha realizado un cambio de ambiente.
    ''' </summary>
    Public Event CambiarAmbiente As EventHandler

    ''' <summary>
    ''' Contiene una lista referenciada de todas las pestañas que están abiertas.
    ''' </summary>
    Private MisPestañas As List(Of IPestaña)

    '''' <summary>
    '''' Determina el tipo de sesión que manejará la aplicación.
    '''' </summary>
    'Private MiTipoSesion As Sesion.SesionTipos

    ''' <summary>
    ''' Contiene la cuenta del número de pestañas que están ejecutando un trabajo.
    ''' </summary>
    Private MisPestañasTrabajando As Integer

    ''' <summary>
    ''' Contiene una referencia al tema actual.
    ''' </summary>
    Private MiTema As DevExpress.Skins.Skin

    Private MiInstancia As String
    Private MiBaseDeDatos As String
    Private MiUsuario As String
    Private MiContraseña As String

    ''' <summary>
    ''' Regresa el singleton del tema.
    ''' </summary>
    Private ReadOnly Property Tema() As DevExpress.Skins.Skin
        Get
            If MiTema Is Nothing Then
                MiTema = DevExpress.Skins.CommonSkins.GetSkin(TemaDefault.LookAndFeel)
            End If
            Return MiTema
        End Get
    End Property

    Public Property Instancia() As String
        Get
            Return MiInstancia
        End Get
        Set(ByVal value As String)
            MiInstancia = value
        End Set
    End Property

    Public Property BaseDeDatos() As String
        Get
            Return MiBaseDeDatos
        End Get
        Set(ByVal value As String)
            MiBaseDeDatos = value
        End Set
    End Property
    Public Property Usuario() As String
        Get
            Return MiUsuario
        End Get
        Set(ByVal value As String)
            MiUsuario = value
        End Set
    End Property
    Public Property Contraseña() As String
        Get
            Return MiContraseña
        End Get
        Set(ByVal value As String)
            MiContraseña = value
        End Set
    End Property
    ''' <summary>
    ''' Regresa el color para información según el tema.
    ''' </summary>
    Public ReadOnly Property ColorInfo() As Color Implements IFormaPrincipal.ColorInfo
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.Info)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color del texto para información según el tema.
    ''' </summary>
    Public ReadOnly Property ColorInfoTexto() As Color Implements IFormaPrincipal.ColorInfoTexto
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.InfoText)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color de la ventana según el tema.
    ''' </summary>
    Public ReadOnly Property ColorVentana() As Color Implements IFormaPrincipal.ColorVentana
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.Window)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color del texto para una ventana según el tema.
    ''' </summary>
    Public ReadOnly Property ColorVentanaTexto() As Color Implements IFormaPrincipal.ColorVentanaTexto
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.WindowText)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color del control según el tema.
    ''' </summary>
    Public ReadOnly Property ColorControl() As Color Implements IFormaPrincipal.ColorControl
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.Control)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color del control deshabilitado según el tema.
    ''' </summary>
    Public ReadOnly Property ColorControlDeshabilitado() As Color Implements IFormaPrincipal.ColorControlDeshabilitado
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.DisabledControl)
        End Get
    End Property

    ''' <summary>
    ''' Regresa el color del texto para un control según el tema.
    ''' </summary>
    Public ReadOnly Property ColorControlTexto() As Color Implements IFormaPrincipal.ColorControlTexto
        Get
            Return Tema.Colors(DevExpress.Skins.CommonColors.ControlText)
        End Get
    End Property

    ''' <summary>
    ''' Prepara información general de la ventana.
    ''' </summary>
    Private Sub Principal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Aplicar únicamente durante ejecución
        If Not DesignMode Then

            'Prepara aspectos visuales de la ventana
            Text = My.Application.Info.Title
            BarraEtiquetaCopyright.Caption = String.Format("{0} | {1}", My.Application.Info.Trademark.Replace("&", "&&"), My.Application.Info.Copyright.Replace("&", "&&"))
            'Sincronizacion.ConfigurarCadenaConexion("desarrollo\des", "Embarques", "desarrollo", "VerifID2")
            'Sincronizacion.ConfigurarCadenaConexion("DES05\SQLEXPRESS", "Embarques","Admin","VerifID12")
            If Usuario Is Nothing AndAlso Contraseña Is Nothing Then
                Sincronizacion.ConfigurarCadenaConexion(Instancia, BaseDeDatos)
            Else
                Sincronizacion.ConfigurarCadenaConexion(Instancia, BaseDeDatos, Usuario, Contraseña)
            End If

            Sincronizacion.UbicacionFuenteDatos = UbicacionesFuentesDatos.SQLServer
            'Inicializa la aplicación sin sesión activa
            CerrarSesion()

        End If

    End Sub

    ''' <summary>
    ''' Inicia sesión de usuario.
    ''' </summary>
    ''' <remarks>Si no es posible iniciar sesión, la aplicación se cierra.</remarks>
    Private Sub FormaPrincipalListon_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        'Aplicar únicamente durante ejecución
        If Not DesignMode Then

            ''Determina que se tenga un tipo de sesión válida
            'If MiTipoSesion = Sesion.SesionTipos.Desconocida Then

            '    'Mostrar error 
            '    MessageBox.Show("Tipo de sesión no definida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '    Close()

            'Else

            'Abrir sesión de usuario al inicio
            'AbrirSesion()

            'End If

        End If

    End Sub

    ''' <summary>
    ''' Capta la necesidad de cambiar el ambiente.
    ''' </summary>
    Private Sub FormaInicioSesionZeus_CambiarAmbiente(ByVal sender As Object, ByVal e As EventArgs)

        RaiseEvent CambiarAmbiente(sender, e)

    End Sub

    ''' <summary>
    ''' Administra la colección referenciada.
    ''' </summary>
    ''' <remarks>Este evento agrega la pestaña a la colección interna y oculta el panel de fondo.</remarks>
    Private Sub ManejadorPestañas_PageAdded(ByVal sender As Object, ByVal e As DevExpress.XtraTabbedMdi.MdiTabPageEventArgs) Handles ManejadorPestañas.PageAdded

        Dim Pestaña = MisPestañas.Find(Function(Item) Item.GetType.Equals(e.Page.MdiChild.GetType()))
        If Pestaña Is Nothing Then
            Pestaña = DirectCast(e.Page.MdiChild, IPestaña)
            'e.Page.Image = Pestaña.Imagen
            MisPestañas.Add(Pestaña)
        End If
        PanelCliente.Visible = (ManejadorPestañas.Pages.Count = 0)

    End Sub

    ''' <summary>
    ''' Administra la colección referenciada.
    ''' </summary>
    ''' <remarks>Este evento remueve la pestaña de la colección interna, y muestra el panel de fondo de ser necesario.</remarks>
    Private Sub ManejadorPestañas_PageRemoved(ByVal sender As Object, ByVal e As DevExpress.XtraTabbedMdi.MdiTabPageEventArgs) Handles ManejadorPestañas.PageRemoved

        Dim Pestaña = MisPestañas.Find(Function(Item) Item.GetType.Equals(e.Page.MdiChild.GetType()))
        If Pestaña IsNot Nothing Then
            MisPestañas.Remove(Pestaña)
        End If
        PanelCliente.Visible = ManejadorPestañas.Pages.Count = 0

    End Sub

    ''' <summary>
    ''' Cierra la sesión actual y reinicia sesión nuevamente.
    ''' </summary>
    Private Sub MenuAplicacionSesionUsuarioCambiarSesion_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles MenuAplicacionSesionUsuarioCambiarSesion.ItemClick

        'CerrarSesion()
        'AbrirSesion()

    End Sub

    '''' <summary>
    '''' Muestra la opción para cambiar la contraseña del usuario.
    '''' </summary>
    'Private Sub MenuAplicacionSesionUsuarioCambiarContraseña_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles MenuAplicacionSesionUsuarioCambiarContraseña.ItemClick

    '    Using Dialogo = New DialogoUsuarioCambiarContraseña()
    '        Dialogo.Usuario = Sesion.UsuarioActual
    '        Dialogo.MostrarVentana(Me)
    '    End Using

    'End Sub

    ''' <summary>
    ''' Cierra la ventana.
    ''' </summary>
    Private Sub MenuAplicacionSalir_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles MenuAplicacionSalir.ItemClick

        Close()

    End Sub

    '''' <summary>
    '''' Muestra la ventana de opciones de la aplicación.
    '''' </summary>
    'Private Sub MenuAplicacionOpciones_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)

    '    Using Ventana = New FormaAcercaDe()
    '        Ventana.ShowDialog(Me)
    '    End Using

    'End Sub

    ''' <summary>
    ''' Actualiza todas las pestañas que correspondan a un listado de pestañas afectadas.
    ''' </summary>
    ''' <param name="PestañasAfectadas">La lista de tipos de pestañas que deben de ser actualizadas.</param>
    Public Sub ActualizaPestañasAfectadas(ByVal PestañasAfectadas As System.Collections.Generic.List(Of System.Type)) Implements IFormaPrincipal.ActualizaPestañasAfectadas

        For Each TipoPestaña In PestañasAfectadas
            Dim TipoPestañaLambda = TipoPestaña
            Dim Pestaña = MisPestañas.Find(Function(Item) Item.GetType.Equals(TipoPestañaLambda))
            If Pestaña IsNot Nothing Then
                Pestaña.ActualizarVentana()
            End If
        Next

    End Sub

    ''' <summary>
    ''' Muestra una ventana tipo pestaña en la pantalla.
    ''' </summary>
    ''' <param name="TipoPestaña">El tipo del catálogo a mostrar.</param>
    ''' <remarks>Esta función permite mostrar cualquier tipo de ventana que finalmente representa una pestaña. Esta función puede determinar si la pestaña ya está abierta, en cuyo caso le da el foco en lugar de abrir una nueva.</remarks>
    Public Sub MostrarPestaña(ByVal TipoPestaña As System.Type) Implements IFormaPrincipal.MostrarPestaña
        If MisPestañas Is Nothing Then MisPestañas = New List(Of IPestaña)
        Dim Pestaña = MisPestañas.Find(Function(Item) Item.GetType.Equals(TipoPestaña))
        If Pestaña IsNot Nothing Then
            Pestaña.Foco()
        Else
            Pestaña = DirectCast(Activator.CreateInstance(TipoPestaña), IPestaña)
            Pestaña.MostrarVentana(Me)
        End If

    End Sub

    ''' <summary>
    ''' Aumenta el número de pestañas trabajando e indica el estado.
    ''' </summary>
    Public Sub IniciaTrabajoPestaña() Implements IFormaPrincipal.IniciaTrabajoPestaña

        MisPestañasTrabajando += 1
        If MisPestañasTrabajando = 1 Then
            If TaskbarManager.IsPlatformSupported Then
                'TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Indeterminate, Handle)

                'TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Indeterminate, Handle)
                'TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Indeterminate, Handle)
                'TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Indeterminate)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Disminuye el número de pestañas trabajando e indica el estado.
    ''' </summary>
    Public Sub TerminaTrabajoPestaña() Implements IFormaPrincipal.TerminaTrabajoPestaña

        MisPestañasTrabajando -= 1
        If MisPestañasTrabajando = 0 Then
            If TaskbarManager.IsPlatformSupported Then
                TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Cierra la sesión actual.
    ''' </summary>
    Private Sub CerrarSesion()

        'Modificar aspectos visuales
        'Liston.Visible = False
        'BarraEtiquetaSesionUsuario.Visibilidad(False)
        'BarraEtiquetaAmbiente.Visibilidad(False)

        'Cerrar todas las ventanas abiertas
        If MisPestañas IsNot Nothing Then
            While MisPestañas.Count > 0
                DirectCast(MisPestañas(0), Form).Close()
            End While
        End If

        'Prepara variables internas
        MisPestañas = New List(Of IPestaña)
        MisPestañasTrabajando = 0

        ''Cerrar sesión de usuario
        'Sesion.Cerrar()

    End Sub

    '''' <summary>
    '''' Abrea una nueva sesión de usuario.
    '''' </summary>
    'Private Function AbrirSesion() As Boolean

    '    'Determinar tipo de sesión
    '    Dim Resultado = True
    '    Select Case MiTipoSesion
    '        Case Sesion.SesionTipos.Zeus

    '            'Iniciar sesión con usuario y contraseña
    '            Using Dialogo = New FormaInicioSesionZeus()
    '                AddHandler Dialogo.CambiarAmbiente, AddressOf FormaInicioSesionZeus_CambiarAmbiente
    '                Resultado = (Dialogo.MostrarVentana(Me) = System.Windows.Forms.DialogResult.OK)
    '                RemoveHandler Dialogo.CambiarAmbiente, AddressOf FormaInicioSesionZeus_CambiarAmbiente
    '            End Using

    '        Case Sesion.SesionTipos.Sistema

    '            'Iniciar sesión como sistema
    '            RaiseEvent CambiarAmbiente(Me, EventArgs.Empty)
    '            Sesion.Abrir(String.Empty, String.Empty, Sesion.SesionTipos.Sistema)

    '    End Select
    '    If Resultado Then
    '        Resultado = IniciarSesion()
    '    End If

    '    'Configurar visibilidad de elementos de la barra de estado
    '    If Sesion.EstaActiva Then
    '        If TypeOf Sesion.UsuarioActual Is Sesion.Autenticacion.UsuarioSistema Or _
    '           TypeOf Sesion.UsuarioActual Is Sesion.Autenticacion.UsuarioAnonimo Then
    '            BarraEtiquetaSesionUsuario.Caption = Sesion.UsuarioActual.ToString()
    '        Else
    '            BarraEtiquetaSesionUsuario.Caption = Sesion.UsuarioActual.Personas.ToString()
    '        End If
    '    End If
    '    BarraEtiquetaSesionUsuario.Visibilidad(Sesion.EstaActiva)
    '    BarraEtiquetaAmbiente.Caption = Sincronizacion.AmbienteTexto
    '    BarraEtiquetaAmbiente.Visibilidad(Sesion.EstaActiva)

    '    'Configurar visiblidad de elementos del menú principal
    '    MenuAplicacionSesionUsuario.Visibilidad(MiTipoSesion = Sesion.SesionTipos.Zeus)
    '    MenuAplicacionSesionUsuarioCambiarContraseña.Visibilidad(Sesion.EstaActiva)
    '    MenuAplicacionOpciones.Visibilidad(Sesion.EstaActiva)

    '    'Configurar visibilidad de elementos del listón
    '    For Each Pestaña As DevExpress.XtraBars.Ribbon.RibbonPage In Liston.Pages

    '        'Determinar visibilidad de la pestaña
    '        Dim PestañaTieneContenido = False
    '        For Each Grupo As DevExpress.XtraBars.Ribbon.RibbonPageGroup In Pestaña.Groups

    '            'Determinar visibilidad del grupo
    '            Dim GrupoTieneContenido = False
    '            For Each Liga As DevExpress.XtraBars.BarItemLink In Grupo.ItemLinks
    '                If Liga.CanVisible Then
    '                    GrupoTieneContenido = True
    '                    Exit For
    '                End If
    '            Next
    '            Grupo.Visible = GrupoTieneContenido
    '            If GrupoTieneContenido Then
    '                PestañaTieneContenido = True
    '            End If

    '        Next
    '        Pestaña.Visible = PestañaTieneContenido And Resultado

    '    Next
    '    Liston.Visible = True

    '    'Regresar resultado final
    '    Return Resultado

    'End Function

    ''' <summary>
    ''' Realizar los procesos necesarios al iniciar una sesión.
    ''' </summary>
    ''' <returns>Esta función regresa false cuando no es posible completar el proceso de inicio de sesión, de forma que resulte en el equivalente a cancelar el inicio de sesión de usuario.</returns>
    ''' <remarks>Esta función permite manipular la interface después de iniciar con éxito una sesión de usuario.</remarks>
    Protected Overridable Function IniciarSesion() As Boolean

        'No ejecuta nada, es responsabilidad de la ventana heredada implementar el código.
        Return True

    End Function

    Private Sub MenuAplicacionOpciones_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles MenuAplicacionOpciones.ItemClick
        Using Ventana = New FormaAcercaDe()
            Ventana.ShowDialog(Me)
        End Using
    End Sub
   
End Class