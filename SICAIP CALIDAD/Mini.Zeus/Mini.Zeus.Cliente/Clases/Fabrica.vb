﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Objeto base del cual derivan todas las fábricas de entidades de Zeus.
''' </summary>
''' <typeparam name="TTipoEntidad">Define el tipo específico de entidad que maneja la fábrica.</typeparam>
''' <remarks>Esta clase incluye el atributo de contrato de datos (DataContract) para poder consumirlo en un WCF.</remarks>
<Runtime.Serialization.DataContract()> _
Public MustInherit Class Fabrica(Of TTipoEntidad)
    Inherits ObjetoConectado
    Implements IFabrica
    Implements IFabricaGenerico(Of TTipoEntidad)

    ''' <summary>
    ''' Notifica que el proceso de destruir está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoDestruir As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoDestruir

    ''' <summary>
    ''' Notifica progreso del proceso de destruir.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event Destruyendo As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.Destruyendo

    ''' <summary>
    ''' Notifica que el proceso de borrar está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoBorrar As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoBorrar

    ''' <summary>
    ''' Notifica progreso del proceso de borrar.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event Borrando As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.Borrando

    ''' <summary>
    ''' Notifica que el proceso de recuperar está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoRecuperar As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoRecuperar

    ''' <summary>
    ''' Notifica progreso del proceso de recuperar.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event Recuperando As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.Recuperando

    ''' <summary>
    ''' Notifica que el proceso de guardar está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoGuardar As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoGuardar

    ''' <summary>
    ''' Notifica progreso del proceso de guardar.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event Guardando As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.Guardando

    ''' <summary>
    ''' Notifica que el proceso de obtener uno está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoObtenerUno As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoObtenerUno

    ''' <summary>
    ''' Notifica progreso del proceso de obtener uno.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event ObteniendoUno As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.ObteniendoUno

    ''' <summary>
    ''' Notifica que el proceso de obtener existentes está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoObtenerExistentes As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoObtenerExistentes

    ''' <summary>
    ''' Notifica progreso del proceso de obtener existentes.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event ObteniendoExistentes As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.ObteniendoExistentes

    ''' <summary>
    ''' Notifica que el proceso de crear nuevo está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoCrearNuevo As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoCrearNuevo

    ''' <summary>
    ''' Notifica progreso del proceso de crear nuevo.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event CreandoNuevo As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.CreandoNuevo

    ''' <summary>
    ''' Notifica que el proceso de deserializar está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoDeserializar As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoDeserializar

    ''' <summary>
    ''' Notifica progreso del proceso de deserializar.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event Deserializando As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.Deserializando

    ''' <summary>
    ''' Notifica que el proceso de deserializar lista está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoDeserializarLista As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoDeserializarLista

    ''' <summary>
    ''' Notifica progreso del proceso de deserializar lista.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event DeserializandoLista As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.DeserializandoLista

    ''' <summary>
    ''' Notifica que el proceso de serializar lista está iniciando.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event IniciandoSerializarLista As EventHandler(Of ArgumentosInicioProceso) Implements IFabrica.IniciandoSerializarLista

    ''' <summary>
    ''' Notifica progreso del proceso de serializar lista.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Event SerializandoLista As EventHandler(Of ArgumentosProgresoProceso) Implements IFabrica.SerializandoLista

    

    ''' <summary>
    ''' Crea una nueva fábrica dinámica de entidades Zeus.
    ''' </summary>
    ''' <remarks>La información de conexión se obtiene a partir de la información existente en Sincronización. Cada consulta a la información se obtiene en el momento, por lo que la fábrica puede reaccionar a cambios después de haber sido creada sin manipular sus valores de forma directa.</remarks>
    Public Sub New()

        MyBase.New()

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica local de entidades Zeus.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que utiliza la fábrica y que genera entidades.</param>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos SQL Server.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos SQL Server.</remarks>
    Public Sub New(ByVal CadenaConexion As String)

        Me.New(CadenaConexion, Nothing, UbicacionesFuentesDatos.SQLServer)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica remota de entidades Zeus.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que utiliza la fábrica y que genera entidades.</param>
    ''' <param name="UrlServicio">La ruta del servicio web.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales predeterminados para una fuente de datos a través de un servicio web.</remarks>
    Public Sub New(ByVal UrlServicio As Uri)

        Me.New(Nothing, UrlServicio, UbicacionesFuentesDatos.ServicioWeb)

    End Sub

    ''' <summary>
    ''' Crea una nueva fábrica de entidades Zeus.
    ''' </summary>
    ''' <param name="UsuarioActual">El usuario que utiliza la fábrica y que genera entidades.</param>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos SQL Server.</param>
    ''' <param name="UrlServicio">La ruta del servicio web.</param>
    ''' <param name="UbicacionFuenteDatos">Determina la ubicación inicial de la fuente de datos de la fábrica. Este valor puede ser modificado posteriormente.</param>
    ''' <remarks>Este constructor crea una fábrica con valores iniciales determinados por los parámetros.</remarks>
    Public Sub New(ByVal CadenaConexion As String, ByVal UrlServicio As Uri, ByVal UbicacionFuenteDatos As UbicacionesFuentesDatos)

        MyBase.New(CadenaConexion, UrlServicio, UbicacionFuenteDatos)

    End Sub

    ''' <summary>
    ''' Regresa la cadena de conexión a la base de datos.
    ''' </summary>
    Public Overloads Property CadenaConexionSQLServer() As String Implements IFabrica.CadenaConexionSQLServer
        Get
            Return MyBase.CadenaConexionSQLServer
        End Get
        Set(ByVal value As String)
            MyBase.CadenaConexionSQLServer = value
        End Set
    End Property

    ''' <summary>
    ''' Define la ubicación de la fuente de datos que utiliza la fábrica al momento de ejecutar su métodos.
    ''' </summary>
    Public Overloads Property UbicacionFuenteDatos() As UbicacionesFuentesDatos Implements IFabrica.UbicacionFuenteDatos
        Get
            Return MyBase.UbicacionFuenteDatos
        End Get
        Set(ByVal value As UbicacionesFuentesDatos)
            If Not EsConexionDinamica Then
                If UbicacionFuenteDatos <> value Then
                End If
            End If
            MyBase.UbicacionFuenteDatos = value
        End Set
    End Property

    ''' <summary>
    ''' Regresa la ruta al servicio web.
    ''' </summary>
    Public Overloads ReadOnly Property UrlServicioWeb() As System.Uri Implements IFabrica.UrlServicioWeb
        Get
            Return MyBase.UrlServicioWeb
        End Get
    End Property


    ''' <summary>
    ''' Genera una fábrica de un tipo particular dependiendo del tipo de conexión de esta fábrica.
    ''' </summary>
    ''' <param name="TipoFabrica">El tipo de fábrcia a crear.</param>
    Protected Function CrearFabrica(ByVal TipoFabrica As Type) As Object

        Dim Resultado As Object
        If EsConexionDinamica Then
            Resultado = Activator.CreateInstance(TipoFabrica)
        Else
            Resultado = Activator.CreateInstance(TipoFabrica, CadenaConexionSQLServer, UrlServicioWeb, UbicacionFuenteDatos)
        End If
        Return Resultado

    End Function

    ''' <summary>
    ''' Genera una fábrica de un tipo particular dependiendo del tipo de conexión de esta fábrica.
    ''' </summary>
    ''' <typeparam name="TTipoFabrica">El tipo de fábrcia a crear.</typeparam>
    Protected Function CrearFabrica(Of TTipoFabrica)() As TTipoFabrica

        Return CType(CrearFabrica(GetType(TTipoFabrica)), TTipoFabrica)

    End Function

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Public Sub Borrar(ByVal Entidad As TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).Borrar

        BorrarInterno(Entidad)

    End Sub


    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando la entidad no es del tipo correcto para la fábrica.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub Borrar(ByVal Entidad As IEntidad) Implements IFabrica.Borrar

        If Not TypeOf Entidad Is TTipoEntidad Then
            Throw New InvalidCastException()
        End If
        Borrar(DirectCast(Entidad, TTipoEntidad))

    End Sub

    ''' <summary>
    ''' Ejectua el reciclado de la entidad en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    ''' <param name="Usuario">El usuario que borrará la entidad.</param>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando la entidad no es del tipo correcto para la fábrica.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Private Sub BorrarInterno(ByVal Entidad As TTipoEntidad)

        Dim EntidadGenerica = DirectCast(Entidad, IEntidad)
        With EntidadGenerica

            'Ejecuta el borrado interno solo si no es de solo lectura
            If EntidadGenerica.EsSoloLectura = False Then

                ''Determina si se tienen los permisos necesarios
                'If Usuario Is Nothing Then Usuario = .UsuarioActual
                'If Usuario.Puede(TiposPermisos.Borrar, EntidadGenerica) Then

                'Determina si la entidad se puede borrar
                If (.EstatusEntidad(True) = EstatusEntidades.Existente Or _
                    .EstatusEntidad = EstatusEntidades.Borrado) And _
                   Not .EstatusEntidad = EstatusEntidades.PendienteDestruir Then

                    'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
                    RaiseEvent IniciandoBorrar(Me, New ArgumentosInicioProceso(2))
                    Select Case UbicacionFuenteDatos
                        Case UbicacionesFuentesDatos.SQLServer
                            BorrarSQLServer(Entidad)

                        Case UbicacionesFuentesDatos.ServicioWeb
                            BorrarServicioWeb(Entidad)

                    End Select
                    RaiseEvent Borrando(Me, New ArgumentosProgresoProceso(1, 2))

                    'Actualiza el estatus de la entidad
                    .CuentaEliminadoEntidad += 1
                    .EstatusEntidad = EstatusEntidades.Borrado
                    .InicializarOriginal()
                    RaiseEvent Borrando(Me, New ArgumentosProgresoProceso(2, 2))

                ElseIf .EstatusEntidad(True) = EstatusEntidades.Nuevo And _
                       Not .EstatusEntidad = EstatusEntidades.PendienteDestruir Then

                    'Actualiza el estatus de la entidad
                    .EstatusEntidad = EstatusEntidades.Desconocido

                Else

                    'Estatus inválido, arrojar excepción
                    'Throw New ExcepcionOperacionInvalidaPorEstatus(Entidad, Reflection.MethodBase.GetCurrentMethod()) MARCABA ERROR Y SE COMENTO
                    'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
                    RaiseEvent IniciandoBorrar(Me, New ArgumentosInicioProceso(2))
                    Select Case UbicacionFuenteDatos
                        Case UbicacionesFuentesDatos.SQLServer
                            BorrarSQLServer(Entidad)

                        Case UbicacionesFuentesDatos.ServicioWeb
                            BorrarServicioWeb(Entidad)

                    End Select
                    RaiseEvent Borrando(Me, New ArgumentosProgresoProceso(1, 2))

                    'Actualiza el estatus de la entidad
                    .CuentaEliminadoEntidad += 1
                    .EstatusEntidad = EstatusEntidades.Borrado
                    .InicializarOriginal()
                    RaiseEvent Borrando(Me, New ArgumentosProgresoProceso(2, 2))
                End If

                'Else

                '    'Permisos insuficientes, arrojar excepción
                '    Throw New ExcepcionOperacionInvalidaPorPermisos(Entidad, Reflection.MethodBase.GetCurrentMethod())

                'End If

            End If

        End With

    End Sub

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad directamente en la base de datos para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    Protected MustOverride Sub BorrarSQLServer(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad a través de un servicio web para marcarla como borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será borrada.</param>
    Protected MustOverride Sub BorrarServicioWeb(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a destruir no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Public Sub Destruir(ByVal Entidad As TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).Destruir

        DestruirInterno(Entidad)

    End Sub


    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando la entidad no es del tipo correcto para la fábrica.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a destruir no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub Destruir(ByVal Entidad As IEntidad) Implements IFabrica.Destruir

        If Not TypeOf Entidad Is TTipoEntidad Then
            Throw New InvalidCastException()
        End If
        Destruir(DirectCast(Entidad, TTipoEntidad))

    End Sub


    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a destruir no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Private Sub DestruirInterno(ByVal Entidad As TTipoEntidad)

        Dim EntidadGenerica = DirectCast(Entidad, IEntidad)
        With EntidadGenerica

            'Ejecuta la destrucción interna solo si no es de solo lectura
            If EntidadGenerica.EsSoloLectura = False Then

                ''Determina si se tienen los permisos necesarios
                'If Usuario Is Nothing Then Usuario = .UsuarioActual
                'If Usuario.Puede(TiposPermisos.Destruir, EntidadGenerica) Then

                'Determina si la entidad se puede borrar
                If .EstatusEntidad(True) = EstatusEntidades.Existente Or _
                   .EstatusEntidad = EstatusEntidades.Borrado Then

                    'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
                    RaiseEvent IniciandoDestruir(Me, New ArgumentosInicioProceso(1))
                    Select Case UbicacionFuenteDatos
                        Case UbicacionesFuentesDatos.SQLServer
                            DestruirSQLServer(Entidad)

                        Case UbicacionesFuentesDatos.ServicioWeb
                            DestruirServicioWeb(Entidad)

                    End Select
                    RaiseEvent Destruyendo(Me, New ArgumentosProgresoProceso(1, 1))

                    'Actualiza el estatus de la entidad
                    .EstatusEntidad = EstatusEntidades.Destruido

                ElseIf .EstatusEntidad(True) = EstatusEntidades.Nuevo Then

                    'Actualiza el estatus de la entidad
                    .EstatusEntidad = EstatusEntidades.Desconocido

                Else

                    'Estatus inválido, arrojar excepción
                    Throw New ExcepcionOperacionInvalidaPorEstatus(Entidad, Reflection.MethodBase.GetCurrentMethod())

                End If

                'Else

                '    'Permisos insuficientes, arrojar excepción
                '    Throw New ExcepcionOperacionInvalidaPorPermisos(Entidad, Reflection.MethodBase.GetCurrentMethod())

                'End If

            End If

        End With

    End Sub

    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos directamente.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    Protected MustOverride Sub DestruirSQLServer(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el borrado definitivo de la entidad en la base de datos a través de un servicio web.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será destruida.</param>
    Protected MustOverride Sub DestruirServicioWeb(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Public Sub Guardar(ByVal Entidad As TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).Guardar

        GuardarInterno(Entidad)

    End Sub


    ''' <summary>
    ''' Ejecuta el guardado de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando la entidad no es del tipo correcto para la fábrica.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub Guardar(ByVal Entidad As IEntidad) Implements IFabrica.Guardar

        If Not TypeOf Entidad Is TTipoEntidad Then
            Throw New InvalidCastException()
        End If
        Guardar(DirectCast(Entidad, TTipoEntidad))

    End Sub


    ''' <summary>
    ''' Ejecuta el guardado de la entidad en la base de datos.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a borrar no es alguna de las siguientes: Borrado, Existente o Nuevo.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Private Sub GuardarInterno(ByVal Entidad As TTipoEntidad)

        Dim EntidadGenerica = DirectCast(Entidad, IEntidad)
        With EntidadGenerica

            'Ejecuta el guardado interno solo si no es de solo lectura
            If EntidadGenerica.EsSoloLectura = False Then

                Dim TipoPermiso As TiposPermisos = TiposPermisos.Ninguno
                If .EstatusEntidad(True) = EstatusEntidades.Nuevo Then
                    TipoPermiso = TiposPermisos.Agregar
                Else
                    TipoPermiso = TiposPermisos.Editar
                End If
                'If Usuario.Puede(TipoPermiso, EntidadGenerica) Then

                'Determinar si la entidad se puede guardar
                If (.EstatusEntidad = EstatusEntidades.Nuevo Or _
                    .EstatusEntidad = EstatusEntidades.Existente) Then

                    'Realizar operación sólo cuando la entidad está sucia
                    If .EstaSucio() Then

                        'Determinar si la entidad es válida
                        If .EsValido() Then

                            'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos y estatus de la entidad
                            RaiseEvent IniciandoGuardar(Me, New ArgumentosInicioProceso(2))
                            Select Case UbicacionFuenteDatos
                                Case UbicacionesFuentesDatos.SQLServer
                                    Select Case .EstatusEntidad
                                        Case EstatusEntidades.Nuevo
                                            Try
                                                GuardarNuevoSQLServer(Entidad)
                                            Catch ex As SqlClient.SqlException
                                                If ex.ErrorCode = -2146232060 And ex.Class = 14 Then
                                                    .EstatusEntidad = EstatusEntidades.Existente
                                                    GuardarExistenteSQLServer(Entidad)
                                                Else
                                                    Throw
                                                End If
                                            End Try

                                        Case EstatusEntidades.Existente
                                            GuardarExistenteSQLServer(Entidad)

                                    End Select

                                Case UbicacionesFuentesDatos.ServicioWeb
                                    Select Case .EstatusEntidad
                                        Case EstatusEntidades.Nuevo
                                            Try
                                                GuardarNuevoServicioWeb(Entidad)
                                            Catch ex As SqlClient.SqlException
                                                If ex.ErrorCode = -2146232060 And ex.Class = 14 Then
                                                    .EstatusEntidad = EstatusEntidades.Existente
                                                    GuardarExistenteServicioWeb(Entidad)
                                                Else
                                                    Throw
                                                End If
                                            End Try

                                        Case EstatusEntidades.Existente
                                            GuardarExistenteServicioWeb(Entidad)

                                    End Select

                            End Select
                            RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 2))

                            'Actualiza el estatus de la entidad
                            If .EstatusEntidad = EstatusEntidades.Nuevo Then
                                .EstatusEntidad = EstatusEntidades.Existente
                            End If
                            .InicializarOriginal()
                            RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 2))

                        Else

                            'Entidad inválida, arrojar excepción
                            Throw New ExcepcionEntidadInvalida(Entidad, Reflection.MethodBase.GetCurrentMethod())

                        End If

                    End If

                ElseIf .EstatusEntidad = EstatusEntidades.PendienteBorrar Then

                    'Determinar acción a tomar en base al estatus real
                    If .EstatusEntidad(True) = EstatusEntidades.Nuevo Then

                        'Actualizar el estatus de la entidad
                        .EstatusEntidad = EstatusEntidades.Desconocido

                    Else

                        'Ejecutar la acción de borrado
                        RaiseEvent IniciandoGuardar(Me, New ArgumentosInicioProceso(1))
                        Borrar(Entidad)
                        RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 1))

                    End If

                ElseIf .EstatusEntidad = EstatusEntidades.PendienteDestruir Then

                    'Determinar acción a tomar en base al estatus real
                    If .EstatusEntidad(True) = EstatusEntidades.Nuevo Then

                        'Actualizar el estatus de la entidad
                        .EstatusEntidad = EstatusEntidades.Desconocido

                    Else

                        'Ejecutar la acción de destruir
                        RaiseEvent IniciandoGuardar(Me, New ArgumentosInicioProceso(1))
                        Destruir(Entidad)
                        RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 1))

                    End If

                Else

                    'Estatus inválido, arrojar excepción
                    'Throw New ExcepcionOperacionInvalidaPorEstatus(Entidad, Reflection.MethodBase.GetCurrentMethod()) se comento porque no guardaba

                    'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos y estatus de la entidad
                    .EstatusEntidad = EstatusEntidades.Existente
                    RaiseEvent IniciandoGuardar(Me, New ArgumentosInicioProceso(2))
                    Select Case UbicacionFuenteDatos
                        Case UbicacionesFuentesDatos.SQLServer
                            Select Case .EstatusEntidad
                                Case EstatusEntidades.Nuevo
                                    Try
                                        GuardarNuevoSQLServer(Entidad)
                                    Catch ex As SqlClient.SqlException
                                        If ex.ErrorCode = -2146232060 And ex.Class = 14 Then
                                            .EstatusEntidad = EstatusEntidades.Existente
                                            GuardarExistenteSQLServer(Entidad)
                                        Else
                                            Throw
                                        End If
                                    End Try

                                Case EstatusEntidades.Existente
                                    GuardarExistenteSQLServer(Entidad)

                            End Select

                        Case UbicacionesFuentesDatos.ServicioWeb
                            Select Case .EstatusEntidad
                                Case EstatusEntidades.Nuevo
                                    Try
                                        GuardarNuevoServicioWeb(Entidad)
                                    Catch ex As SqlClient.SqlException
                                        If ex.ErrorCode = -2146232060 And ex.Class = 14 Then
                                            .EstatusEntidad = EstatusEntidades.Existente
                                            GuardarExistenteServicioWeb(Entidad)
                                        Else
                                            Throw
                                        End If
                                    End Try

                                Case EstatusEntidades.Existente
                                    GuardarExistenteServicioWeb(Entidad)

                            End Select

                    End Select
                    RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 2))

                    'Actualiza el estatus de la entidad
                    If .EstatusEntidad = EstatusEntidades.Nuevo Then
                        .EstatusEntidad = EstatusEntidades.Existente
                    End If
                    .InicializarOriginal()
                    RaiseEvent Guardando(Me, New ArgumentosProgresoProceso(1, 2))

                End If

                'Else

                '    'Permisos insuficientes, arrojar excepción
                '    Throw New ExcepcionOperacionInvalidaPorPermisos(Entidad, Reflection.MethodBase.GetCurrentMethod())

                'End If

            End If

        End With

    End Sub

    ''' <summary>
    ''' Ejecuta el guardado de la entidad nueva en la base de datos directamente.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Protected MustOverride Sub GuardarNuevoSQLServer(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad nueva en la base de datos a través de un servicio web.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Protected MustOverride Sub GuardarNuevoServicioWeb(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad existente en la base de datos directamente.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Protected MustOverride Sub GuardarExistenteSQLServer(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el guardado de la entidad existente en la base de datos a través de un servicio web.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será guardada.</param>
    ''' <remarks>Al ejectuar esta función se determina si es necesario agregar o actualizar la información de la base de datos, basandose en el estatus de la entidad.</remarks>
    Protected MustOverride Sub GuardarExistenteServicioWeb(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a recuperar no es alguna de las siguientes: Borrado.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Public Sub Recuperar(ByVal Entidad As TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).Recuperar

        RecuperarInterno(Entidad)

    End Sub

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    ''' <exception cref="InvalidCastException">Arroja la excepción cuando la entidad no es del tipo correcto para la fábrica.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a recuperar no es alguna de las siguientes: Borrado.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Sub Recuperar(ByVal Entidad As IEntidad) Implements IFabrica.Recuperar

        If Not TypeOf Entidad Is TTipoEntidad Then
            Throw New InvalidCastException()
        End If
        Recuperar(DirectCast(Entidad, TTipoEntidad))

    End Sub

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    ''' <exception cref="ExcepcionOperacionInvalidaPorEstatus">Arroja la excepción cuando el estatus de la entidad a recuperar no es alguna de las siguientes: Borrado.</exception>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Private Sub RecuperarInterno(ByVal Entidad As TTipoEntidad)

        Dim EntidadGenerica = DirectCast(Entidad, IEntidad)
        With EntidadGenerica

            'Ejecuta el recuperar interno solo si no es de solo lectura
            If EntidadGenerica.EsSoloLectura = False Then

                ''Determina si se tienen los permisos necesarios
                'If Usuario Is Nothing Then Usuario = .UsuarioActual
                'If Usuario.Puede(TiposPermisos.Recuperar, EntidadGenerica) Then

                'Determina si la entidad se puede desborrar
                If .EstatusEntidad = EstatusEntidades.Borrado Then

                    'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
                    RaiseEvent IniciandoRecuperar(Me, New ArgumentosInicioProceso(2))
                    Select Case UbicacionFuenteDatos
                        Case UbicacionesFuentesDatos.SQLServer
                            RecuperarSQLServer(Entidad)

                        Case UbicacionesFuentesDatos.ServicioWeb
                            RecuperarServicioWeb(Entidad)

                    End Select
                    RaiseEvent Recuperando(Me, New ArgumentosProgresoProceso(1, 2))

                    'Actualiza el estatus de la entidad
                    .CuentaEliminadoEntidad -= 1
                    If .CuentaEliminadoEntidad = 0 Then
                        .EstatusEntidad = EstatusEntidades.Existente
                    End If
                    .InicializarOriginal()
                    RaiseEvent Recuperando(Me, New ArgumentosProgresoProceso(1, 2))

                Else

                    'Estatus inválido, arrojar excepción
                    Throw New ExcepcionOperacionInvalidaPorEstatus(Entidad, Reflection.MethodBase.GetCurrentMethod())

                End If

                'Else

                '    'Permisos insuficientes, arrojar excepción
                '    Throw New ExcepcionOperacionInvalidaPorPermisos(Entidad, Reflection.MethodBase.GetCurrentMethod())

                'End If

            End If

        End With

    End Sub

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad directamente en la base de datos para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    Protected MustOverride Sub RecuperarSQLServer(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Ejecuta el reciclado de la entidad a través de un servicio web para marcarla como no borrada.
    ''' </summary>
    ''' <param name="Entidad">La entidad que será recuperada.</param>
    Protected MustOverride Sub RecuperarServicioWeb(ByVal Entidad As TTipoEntidad)

    ''' <summary>
    ''' Configura la información de datos relacionados para una consulta.
    ''' </summary>
    ''' <param name="DatosRelacionados">La colección de datos relacionados.</param>
    Protected MustOverride Sub DefinirDatosRelacionados(ByRef DatosRelacionados As Data.Linq.DataLoadOptions)

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ObtenerUnoGenerico(ByVal Id As System.Guid) As Object Implements IFabrica.ObtenerUno

        Return ObtenerUno(Id)

    End Function

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)>
    Public Function ObtenerUnoParaSerializarGenerico(ByVal Id As Guid) As Object Implements IFabrica.ObtenerUnoParaSerializar

        Return ObtenerUnoParaSerializar(Id)

    End Function


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)>
    Public Function ObtenerUnoParaSerializarGenerico(ByVal Id As String) As Object Implements IFabrica.ObtenerUnoParaSerializar

        Return ObtenerUnoParaSerializar(Id)

    End Function

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Public Function ObtenerUno(ByVal Id As System.Guid) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).ObtenerUno

        Return ObtenerUnoInterno(Id, True)

    End Function

    
    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Public Function ObtenerUno(ByVal Id As Integer ) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).ObtenerUno

        Return ObtenerUnoInterno(Id, True)

    End Function


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Public Function ObtenerUnoParaSerializar(ByVal Id As Guid) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).ObtenerUnoParaSerializar

        Return ObtenerUnoInterno(Id, False)

    End Function


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular sin datos relacionados.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Public Function ObtenerUnoParaSerializar(ByVal Id As String) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).ObtenerUnoParaSerializar

        Return ObtenerUnoInterno(Id, False)

    End Function

    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <param name="IncluirDatosRelacionados">Determina si los datos relacionados se inlcuyen al momento de crear la entidad.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Private Function ObtenerUnoInterno(ByVal Id As Guid, ByVal IncluirDatosRelacionados As Boolean) As TTipoEntidad

        'Genera las instrucciones de carga para datos relacionados
        RaiseEvent IniciandoObtenerUno(Me, New ArgumentosInicioProceso(2))
        Dim DatosRelacionados = New Data.Linq.DataLoadOptions()
        If IncluirDatosRelacionados Then
            DefinirDatosRelacionados(DatosRelacionados)
        End If

        'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
        Dim Resultado As TTipoEntidad = Nothing
        Select Case UbicacionFuenteDatos
            Case UbicacionesFuentesDatos.SQLServer
                Resultado = ObtenerUnoSQLServer(Id, DatosRelacionados)

            Case UbicacionesFuentesDatos.ServicioWeb
                Resultado = ObtenerUnoServicioWeb(Id)

        End Select
        RaiseEvent ObteniendoUno(Me, New ArgumentosProgresoProceso(1, 2))

        'Trabaja con el resultado
        If Resultado IsNot Nothing Then
            Dim EntidadGenerica = DirectCast(Resultado, IEntidad)
            With EntidadGenerica

                ''Determina si se tienen los permisos necesarios
                'If Usuario Is Nothing Then Usuario = UsuarioActual
                'If Usuario.Puede(TiposPermisos.Ver, EntidadGenerica) Then

                'Inicializa el resultado
                .InicializarExistente()
                If IncluirDatosRelacionados Then
                    InicializarDatosRelacionados(EntidadGenerica)
                End If

                'Else

                ''Permisos insuficientes, limpia resultado
                'Resultado = Nothing

                'End If

            End With

        End If
        RaiseEvent ObteniendoUno(Me, New ArgumentosProgresoProceso(2, 2))
        Return Resultado

    End Function


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <param name="IncluirDatosRelacionados">Determina si los datos relacionados se inlcuyen al momento de crear la entidad.</param>
    ''' <remarks>Si el usuario de la fábrica no tiene suficientes permisos para ver entidades del tipo indicado, esta función regresará Nothing.</remarks>
    Private Function ObtenerUnoInterno(ByVal Id As String, ByVal IncluirDatosRelacionados As Boolean) As TTipoEntidad

        'Genera las instrucciones de carga para datos relacionados
        RaiseEvent IniciandoObtenerUno(Me, New ArgumentosInicioProceso(2))
        Dim DatosRelacionados = New Data.Linq.DataLoadOptions()
        If IncluirDatosRelacionados Then
            DefinirDatosRelacionados(DatosRelacionados)
        End If

        'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
        Dim Resultado As TTipoEntidad = Nothing
        Select Case UbicacionFuenteDatos
            Case UbicacionesFuentesDatos.SQLServer
                Resultado = ObtenerUnoSQLServer(Id, DatosRelacionados)

            Case UbicacionesFuentesDatos.ServicioWeb
                Resultado = ObtenerUnoServicioWeb(Id)

        End Select
        RaiseEvent ObteniendoUno(Me, New ArgumentosProgresoProceso(1, 2))

        'Trabaja con el resultado
        If Resultado IsNot Nothing Then
            Dim EntidadGenerica = DirectCast(Resultado, IEntidad)
            With EntidadGenerica

                ''Determina si se tienen los permisos necesarios
                'If Usuario Is Nothing Then Usuario = UsuarioActual
                'If Usuario.Puede(TiposPermisos.Ver, EntidadGenerica) Then

                'Inicializa el resultado
                .InicializarExistente()
                If IncluirDatosRelacionados Then
                    InicializarDatosRelacionados(EntidadGenerica)
                End If

                'Else

                ''Permisos insuficientes, limpia resultado
                'Resultado = Nothing

                'End If

            End With

        End If
        RaiseEvent ObteniendoUno(Me, New ArgumentosProgresoProceso(2, 2))
        Return Resultado

    End Function



    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular directamente a la base de datos.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <param name="DatosRelacionados">La colección de datos relacionados.</param>
    Protected MustOverride Function ObtenerUnoSQLServer(ByVal Id As Guid, ByVal DatosRelacionados As Data.Linq.DataLoadOptions) As TTipoEntidad



    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular directamente a la base de datos.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    ''' <param name="DatosRelacionados">La colección de datos relacionados.</param>
    Protected MustOverride Function ObtenerUnoSQLServer(ByVal Id As String, ByVal DatosRelacionados As Data.Linq.DataLoadOptions) As TTipoEntidad


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular a través de un servicio web.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Protected MustOverride Function ObtenerUnoServicioWeb(ByVal Id As Guid) As TTipoEntidad


    ''' <summary>
    ''' Ejecuta la selección de una entidad en particular a través de un servicio web.
    ''' </summary>
    ''' <param name="Id">El identificador de la entidad a obtener.</param>
    Protected MustOverride Function ObtenerUnoServicioWeb(ByVal Id As String) As TTipoEntidad

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes.
    ''' </summary>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ObtenerExistentesGenerico() As List(Of Object) Implements IFabrica.ObtenerExistentes

        Return ObtenerExistentes().ConvertAll(Of Object)(Function(Item) DirectCast(Item, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes sin datos relacionados.
    ''' </summary>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ObtenerExistentesParaSerializarGenerico() As List(Of Object) Implements IFabrica.ObtenerExistentesParaSerializar

        Return ObtenerExistentesParaSerializar().ConvertAll(Of Object)(Function(Item) DirectCast(Item, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes.
    ''' </summary>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    Public Function ObtenerExistentes() As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).ObtenerExistentes

        Return ObtenerExistentes(New Dictionary(Of String, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes sin traer datos relacionados
    ''' </summary>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    Public Function ObtenerExistentesParaSerializar() As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).ObtenerExistentesParaSerializar

        Return ObtenerExistentesParaSerializar(New Dictionary(Of String, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro sin traer datos relacionados
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Public Function ObtenerExistentesParaSerializar(ByVal Filtro As Dictionary(Of String, Object)) As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).ObtenerExistentesParaSerializar

        Return ObtenerExistentesInterno(Filtro, False)

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ObtenerExistentesGenerico(ByVal Filtro As Dictionary(Of String, Object)) As List(Of Object) Implements IFabrica.ObtenerExistentes

        Return ObtenerExistentes(Filtro).ConvertAll(Of Object)(Function(Item) DirectCast(Item, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro sin datos relacionados.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function ObtenerExistentesParaSerializarGenerico(ByVal Filtro As Dictionary(Of String, Object)) As List(Of Object) Implements IFabrica.ObtenerExistentesParaSerializar

        Return ObtenerExistentesParaSerializar(Filtro).ConvertAll(Of Object)(Function(Item) DirectCast(Item, Object))

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    ''' <remarks>Considerando los permisos del usuario de la fábrica, esta función regresará la lista de aquellas entidades sobre las cuales el usuario sí tiene permisos suficientes.</remarks>
    Public Function ObtenerExistentes(ByVal Filtro As Dictionary(Of String, Object)) As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).ObtenerExistentes

        Return ObtenerExistentesInterno(Filtro, True)

    End Function

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades existentes con base en un filtro, opcionalmente incluyendo datos relacionados.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    ''' <param name="IncluirDatosRelacionados">Determina si se deben traer los datos relacionados.</param>
    Private Function ObtenerExistentesInterno(ByVal Filtro As Dictionary(Of String, Object), ByVal IncluirDatosRelacionados As Boolean) As List(Of TTipoEntidad)

        'Genera las instrucciones de carga para datos relacionados
        RaiseEvent IniciandoObtenerExistentes(Me, New ArgumentosInicioProceso(2))
        Dim DatosRelacionados = New Data.Linq.DataLoadOptions()
        If IncluirDatosRelacionados Then
            DefinirDatosRelacionados(DatosRelacionados)
        End If

        'Ejecuta la acción de acuerdo a la ubicación de la fuente de datos
        Dim ResultadoConsulta = New List(Of TTipoEntidad)
        'If Usuario Is Nothing Then Usuario = UsuarioActual
        Select Case UbicacionFuenteDatos
            Case UbicacionesFuentesDatos.SQLServer
                ResultadoConsulta = ObtenerExistentesSQLServer(Filtro, DatosRelacionados)

            Case UbicacionesFuentesDatos.ServicioWeb
                ResultadoConsulta = ObtenerExistentesServicioWeb(Filtro)

        End Select
        RaiseEvent ObteniendoExistentes(Me, New ArgumentosProgresoProceso(1, 2))

        'Depura la lista en base a los permisos del usuario de cada entidad
        Dim CuentaTotal As Integer
        If ResultadoConsulta IsNot Nothing Then
            CuentaTotal = ResultadoConsulta.Count + 1
        End If

        Dim Cuenta = 1
        RaiseEvent IniciandoObtenerExistentes(Me, New ArgumentosInicioProceso(CuentaTotal))
        RaiseEvent ObteniendoExistentes(Me, New ArgumentosProgresoProceso(Cuenta, CuentaTotal))
        Dim Resultado = New List(Of TTipoEntidad)
        For Each EntidadGenerica As IEntidad In ResultadoConsulta
            'If Usuario.Puede(TiposPermisos.Ver, EntidadGenerica) Then
            'EntidadGenerica.InicializarExistente()
            If IncluirDatosRelacionados Then
                InicializarDatosRelacionados(EntidadGenerica)
            End If
            Resultado.Add(CType(EntidadGenerica, TTipoEntidad))
            'End If
            Cuenta += 1
            RaiseEvent ObteniendoExistentes(Me, New ArgumentosProgresoProceso(Cuenta, CuentaTotal))
        Next
        Return Resultado

    End Function

    ''' <summary>
    ''' Premite ejecutar las acciones necesarias para inicializar los datos relacionados.
    ''' </summary>
    ''' <param name="Entidad">La entidad a la cual se le deben inicializar los datos relacionados.</param>
    ''' <remarks>Esta operación se ejecuta únicamente sobre aquellas entidades necesarias (las que pasan la prueba de seguridad).</remarks>
    Protected Overridable Sub InicializarDatosRelacionados(ByVal Entidad As IEntidad)

        'Es responsabilidad de la fábrica que hereda implementar el código necesario según aplique.

    End Sub

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades direcamente en la base de datos.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    ''' <param name="DatosRelacionados">La colección de datos relacionados.</param>
    Protected MustOverride Function ObtenerExistentesSQLServer(ByVal Filtro As Dictionary(Of String, Object), ByVal DatosRelacionados As Data.Linq.DataLoadOptions) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Ejecuta la selección de todas las entidades a través de un servicio web.
    ''' </summary>
    ''' <param name="Filtro">Colección de valores para aplicar como filtro.</param>
    Protected MustOverride Function ObtenerExistentesServicioWeb(ByVal Filtro As Dictionary(Of String, Object)) As List(Of TTipoEntidad)

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function CrearNuevoGenerico() As Object Implements IFabrica.CrearNuevo

        Return CrearNuevoInterno()

    End Function

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Public Function CrearNuevo() As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).CrearNuevo

        Return CrearNuevoInterno()

    End Function

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    ''' <exception cref="ExcepcionOperacionInvalidaPorPermisos">Arroja la excepción cuando el usuario de la entidad no tiene suficientes permisos para ejectuar la operación.</exception>
    Private Function CrearNuevoInterno() As TTipoEntidad

        'Crea la nueva entidad
        RaiseEvent IniciandoCrearNuevo(Me, New ArgumentosInicioProceso(1))
        Dim Resultado = CrearNuevaEntidad()
        With DirectCast(Resultado, IEntidad)
            .MetadatosEntidad = 0
            .CuentaEliminadoEntidad = 0
            .FechaHoraLocalCreacion = Date.Now
            .FechaHoraLocalModificacion = .FechaHoraLocalCreacion
            .InicializarNuevo()
        End With
        RaiseEvent CreandoNuevo(Me, New ArgumentosProgresoProceso(1, 1))

        'Determina si se tienen los permisos necesarios
        Dim EntidadGenerica = DirectCast(Resultado, IEntidad)
        'If Usuario Is Nothing Then Usuario = EntidadGenerica.UsuarioActual
        'If Not Usuario.Puede(TiposPermisos.Agregar, EntidadGenerica) Then

        '    'Permisos insuficientes, arrojar excepción
        '    Throw New ExcepcionOperacionInvalidaPorPermisos(Resultado, Reflection.MethodBase.GetCurrentMethod())

        'End If

        'Regresa entidad resultante
        Return Resultado

    End Function

    ''' <summary>
    ''' Crea una nueva entidad.
    ''' </summary>
    Protected MustOverride Function CrearNuevaEntidad() As TTipoEntidad

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarGenerico(ByVal EntidadSerializada As String) As Object Implements IFabrica.Deserializar

        Return Deserializar(EntidadSerializada)

    End Function

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se deserializa como entidad nueva.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarGenerico(ByVal EntidadSerializada As String, ByVal InicializarComoNuevo As Boolean) As Object Implements IFabrica.Deserializar

        Return Deserializar(EntidadSerializada, InicializarComoNuevo)

    End Function

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Overridable Function Deserializar(ByVal EntidadSerializada As String) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).Deserializar

        Return Deserializar(EntidadSerializada, False)

    End Function

    ''' <summary>
    ''' Deserializa la información en su entidad correspondiente.
    ''' </summary>
    ''' <param name="EntidadSerializada">La información serializada de la entidad.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se deserializa como entidad nueva.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function Deserializar(ByVal EntidadSerializada As String, ByVal InicializarComoNuevo As Boolean) As TTipoEntidad Implements IFabricaGenerico(Of TTipoEntidad).Deserializar

        'Determinar si hay información a deserializar
        If Not String.IsNullOrEmpty(EntidadSerializada) Then

            'Crear deserializador
            Dim Serializador = New Xml.Serialization.XmlSerializer(GetType(TTipoEntidad))
            Using Lector = New IO.StringReader(EntidadSerializada)

                'Deserializar e inicializar como nuevo
                RaiseEvent IniciandoDeserializar(Me, New ArgumentosInicioProceso(2))
                Dim Resultado = CType(Serializador.Deserialize(Lector), TTipoEntidad)
                RaiseEvent Deserializando(Me, New ArgumentosProgresoProceso(1, 2))
                If InicializarComoNuevo Then
                    DirectCast(Resultado, IEntidad).InicializarNuevo()
                Else
                    DirectCast(Resultado, IEntidad).InicializarExistente()
                End If
                RaiseEvent Deserializando(Me, New ArgumentosProgresoProceso(2, 2))
                Lector.Close()
                Return Resultado

            End Using

        Else

            'Regresar un objeto vacío
            Return Nothing

        End If

        ''Determinar si hay información a deserializar (WCF)
        'If Not String.IsNullOrEmpty(EntidadSerializada) Then

        '    'Crear deserializador
        '    Dim Serializador = New Runtime.Serialization.DataContractSerializer(GetType(TTipoEntidad))
        '    Using Lector = Xml.XmlDictionaryReader.CreateDictionaryReader(Xml.XmlReader.Create(New IO.StringReader(EntidadSerializada)))

        '        'Deserializar
        '        Dim Resultado = CType(Serializador.ReadObject(Lector, True), TTipoEntidad)
        '        Lector.Close()
        '        Return Resultado

        '    End Using

        'Else

        '    'Regresar un objeto vacío
        '    Return Nothing

        'End If

    End Function

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarLista(ByVal ListaSerializada As List(Of String)) As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).DeserializarLista

        Return DeserializarLista(ListaSerializada.ToArray())

    End Function

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se deserializa como entidad nueva.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarLista(ByVal ListaSerializada As System.Collections.Generic.List(Of String), ByVal InicializarComoNuevo As Boolean) As System.Collections.Generic.List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).DeserializarLista

        Return DeserializarLista(ListaSerializada.ToArray(), InicializarComoNuevo)

    End Function

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarLista(ByVal ListaSerializada() As String) As List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).DeserializarLista

        Return DeserializarLista(ListaSerializada, False)

    End Function

    ''' <summary>
    ''' Deserializa una lista de información en sus entidades correspondientes.
    ''' </summary>
    ''' <param name="ListaSerializada">La lista de información serializada de las entidades.</param>
    ''' <param name="InicializarComoNuevo">Determina si la entidad se deserializa como entidad nueva.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function DeserializarLista(ByVal ListaSerializada() As String, ByVal InicializarComoNuevo As Boolean) As System.Collections.Generic.List(Of TTipoEntidad) Implements IFabricaGenerico(Of TTipoEntidad).DeserializarLista

        RaiseEvent IniciandoDeserializarLista(Me, New ArgumentosInicioProceso(ListaSerializada.Length))
        Dim Cuenta = 0
        Dim Resultado As New List(Of TTipoEntidad)
        For Each EntidadSerializada In ListaSerializada
            Resultado.Add(Deserializar(EntidadSerializada, InicializarComoNuevo))
            Cuenta += 1
            RaiseEvent DeserializandoLista(Me, New ArgumentosProgresoProceso(Cuenta, ListaSerializada.Length))
        Next
        Return Resultado

    End Function

    ''' <summary>
    ''' Serializa una lista de entidades en su información correspondiente.
    ''' </summary>
    ''' <param name="Lista">La lista de entidades a serializar.</param>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Function SerializarLista(ByVal Lista As List(Of TTipoEntidad)) As List(Of String) Implements IFabricaGenerico(Of TTipoEntidad).SerializarLista

        RaiseEvent IniciandoSerializarLista(Me, New ArgumentosInicioProceso(Lista.Count))
        Dim Resultado As New List(Of String)
        Dim Cuenta = 0
        For Each Entidad As IEntidad In Lista
            Resultado.Add(Entidad.Serializar())
            Cuenta += 1
            RaiseEvent SerializandoLista(Me, New ArgumentosProgresoProceso(Cuenta, Lista.Count))
        Next
        Return Resultado

    End Function

End Class