﻿Imports System.IO
Imports Datos

Public Class FormaFinalizar

    Private MiSolicitud As Datos.LABDIMSolicitud

    Private MiRutaArchivo As String

    Private MiLabDimArchivo As Datos.LABDIMArchivo

    Private MiCadenaConexión As String

    Public Property RutaArchivo As String
        Get
            Return MiRutaArchivo
        End Get
        Set(value As String)
            MiRutaArchivo = value
        End Set
    End Property

    Public Property Solicitud As Datos.LABDIMSolicitud
        Get
            Return MiSolicitud
        End Get
        Set(value As Datos.LABDIMSolicitud)
            MiSolicitud = value
        End Set
    End Property

    Private Sub FormaFinalizar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MiCadenaConexión = Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer
        txtNoParte.Text = Solicitud.NoParte.np_gkn
        txtDescripcion.Text = Solicitud.descripcion
        txtComentarios.Text = Solicitud.comentarios
    End Sub


    Private Sub LookUpEdit1_MouseClick(sender As Object, e As MouseEventArgs) Handles combodiseño.MouseClick
        Try
            Dim result As DialogResult = OpenFileDialog1.ShowDialog()
            ' Test result.
            If result = Windows.Forms.DialogResult.OK Then

                ' Get the file name.
                MiRutaArchivo = OpenFileDialog1.FileName
                combodiseño.ResetText()

                combodiseño.Text = MiRutaArchivo
                combodiseño.Refresh()

                MiLabDimArchivo = New Datos.LABDIMArchivo
                MiLabDimArchivo.Nombre = OpenFileDialog1.SafeFileName
                If MiLabDimArchivo.Nombre.Count > 80 Then
                    MiLabDimArchivo.Nombre = MiLabDimArchivo.Nombre.Substring(MiLabDimArchivo.Nombre.Count - 80, MiLabDimArchivo.Nombre.Count)
                    'MiLabDimArchivo.Nombre = MiLabDimArchivo.Nombre.Substring(0, 80)
                End If
                MiLabDimArchivo.Tipo = 1
                If File.Exists(MiRutaArchivo) Then
                    Dim fs As New FileStream(MiRutaArchivo, FileMode.Open)
                    Dim data(fs.Length) As Byte
                    fs.Read(data, 0, Convert.ToInt32(fs.Length))
                    MiLabDimArchivo.Archivo = data
                    fs.Close()
                Else
                    MessageBox.Show("El archivo seleccionado no existe, favor de verificarlo")
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(String.Format("No se pudo cargar el archivo seleccionado por la siguiente razón: {0}", ex.Message))
        End Try

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnFinalizar_Click(sender As Object, e As EventArgs) Handles btnFinalizar.Click
        If MiLabDimArchivo Is Nothing Then
            If MessageBox.Show("¿Está seguro de finalizar el registro sin adjuntar un reporte?", "Finalizar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Using Contexto = New ModeloDatosDataContext(MiCadenaConexión)
                    Dim Objeto = Contexto.LABDIMSolicituds.Single(Function(p As LABDIMSolicitud) p.cve_solicitud = MiSolicitud.cve_solicitud)
                    Objeto.Estatus = 2
                    Objeto.Fecha_Salida = Date.Now
                    Contexto.SubmitChanges()
                End Using
                DialogResult = DialogResult.OK
            End If
        Else
            Using Contexto = New ModeloDatosDataContext(MiCadenaConexión)
                Dim Objeto = Contexto.LABDIMSolicituds.Single(Function(p As LABDIMSolicitud) p.cve_solicitud = MiSolicitud.cve_solicitud)
                Objeto.Estatus = 2
                Objeto.Fecha_Salida = Date.Now
                Contexto.SubmitChanges()
            End Using
            Dim NuevoArchivo = Datos.Fabricas.LABDIMArchivos.CrearNuevo
            NuevoArchivo.Archivo = MiLabDimArchivo.Archivo
            NuevoArchivo.cve_solicitud = MiSolicitud.cve_solicitud
            NuevoArchivo.Tipo = 1
            NuevoArchivo.Nombre = MiLabDimArchivo.Nombre
            Datos.Fabricas.LABDIMArchivos.Guardar(NuevoArchivo)
            DialogResult = DialogResult.OK
            Close()
        End If
    End Sub
End Class