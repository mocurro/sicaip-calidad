﻿Public Class LABDIMSolicitud

    Private MiNombreArchivo As String

    Private MiEsNew As Boolean = False

    Private Micve_modelonew As Long


    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public Property EsNew As Boolean
        Get
            Return MiEsNew
        End Get
        Set(value As Boolean)
            MiEsNew = value
        End Set
    End Property

    Public Property cve_modelonew As Long
        Get
            Return Micve_modelonew

        End Get
        Set(value As Long)
            Micve_modelonew = value
        End Set
    End Property

    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public  Property NombreArchivo As String
        Get
         Return MiNombreArchivo 
        End Get
        Set(value As String)
            MiNombreArchivo=value 
        End Set
    End Property

    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property Folio() As String
        Get
            Return cve_solicitud .ToString .PadLeft (5,CChar ("0"))
        End Get
    End Property

    
    Public ReadOnly Property user As SEGURIDAD_USUARIO 
        Get
            Dim MiCliente=Datos.Fabricas .Seguridad_Usuarios .ObtenerUno (cve_usuario )
            Return MiCliente 
        End Get
    End Property

    Public ReadOnly Property Archivo As LABDIMArchivo
        Get
            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("cv_solicitud", cve_solicitud)
            Dim MiArchivo = Datos.Fabricas.LABDIMArchivos.ObtenerExistentes(Filtro)
            If MiArchivo IsNot Nothing AndAlso MiArchivo.Count > 0 Then
                Return MiArchivo.SingleOrDefault
            Else
                Return Nothing
            End If
        End Get
    End Property

    ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case Estatus
                Case 0
                    Return "Solicitud"
                Case 1
                    Return "Registro"
                Case 2
                    Return "Finalizado"
                Case 3
                    Return "Finalizado/Notificado"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property


    Public ReadOnly Property NoParte As Datos.Modelo
        Get
            Dim Filtro = New Dictionary(Of String, Object)
            Filtro.Add("cve_modelo", cve_modelo)
            Dim MiModelo = Datos.Fabricas.Modelo.ObtenerExistentes(Filtro)
            If MiModelo IsNot Nothing AndAlso MiModelo.Count > 0 Then
                Return MiModelo.SingleOrDefault
            Else
                Return Nothing
            End If
        End Get
    End Property
End Class
