﻿Imports Mini.Zeus.Cliente

Public Class CatalogoUnidad_Negocio

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))


        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Unidad_Negocio  ), Datos.Fabricas .Unidad_Negocio   , GetType(TarjetaUnidad_Negocio )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Unidad_Negocio )

    End Function
End Class