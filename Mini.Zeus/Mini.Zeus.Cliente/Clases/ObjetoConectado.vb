﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Objeto base del cual derivan todas las entidades conectadas de Zeus.
''' </summary>
''' <remarks>Esta clase incluye el atributo de contrato de datos (DataContract) para poder consumirlo en un WCF.</remarks>
<Runtime.Serialization.DataContract()> _
<Serializable()> _
Public MustInherit Class ObjetoConectado
    Inherits Objeto
    Implements IObjetoConectado

    ''' <summary>
    ''' Contiene la cadena de conexión a la base de datos cuando el objeto conectado es local.
    ''' </summary>
    <Runtime.Serialization.DataMember()> _
    Private MiCadenaConexion As String

    ''' <summary>
    ''' Contiene la ruta Url al servicio Web que proporciona funcionalidad remota al objeto conectado.
    ''' </summary>
    <Runtime.Serialization.DataMember()> _
    Private MiUrlServicio As Uri

    ''' <summary>
    ''' Contiene la información que le indica al objeto conectado si la manipulación es local o remota.
    ''' </summary>
    <Runtime.Serialization.DataMember()> _
    Private MiUbicacionFuenteDatos As UbicacionesFuentesDatos

    ''' <summary>
    ''' Determinar si la información de conexión se obtiene de forma dinámica.
    ''' </summary>
    ''' <remarks>Cuando este valor es True, toda la información de conexión se obtiene a partir de la información existente en Sincronización. Cada consulta a la información se obtiene en el momento, por lo que el objeto conectado puede reaccionar a cambios después de haber sido creado sin manipular sus valores de forma directa.</remarks>
    <Runtime.Serialization.DataMember()> _
    Private MiEsConexionDinamica As Boolean

    ''' <summary>
    ''' Crea un nuevo objeto conectado dinámico.
    ''' </summary>
    ''' <remarks>La información de conexión se obtiene a partir de la información existente en Sincronización. Cada consulta a la información se obtiene en el momento, por lo que el objeto conectado puede reaccionar a cambios después de haber sido creado sin manipular sus valores de forma directa.</remarks>
    Public Sub New()

        Me.New(Sincronizacion.CadenaConexionSQLServer, Sincronizacion.UrlServicioWeb, Sincronizacion.UbicacionFuenteDatos)
        MiEsConexionDinamica = True

    End Sub

    ''' <summary>
    ''' Crea un nuevo objeto conectado directamente a SQL Server.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos SQL Server.</param>
    ''' <remarks>Este constructor crea un objeto conectado con valores iniciales predeterminados para una fuente de datos SQL Server.</remarks>
    Public Sub New(ByVal CadenaConexion As String)

        Me.New(CadenaConexion, Nothing, UbicacionesFuentesDatos.SQLServer)

    End Sub

    ''' <summary>
    ''' Crea un nuevo objeto conectado a un servicio Web.
    ''' </summary>
    ''' <param name="UrlServicio">La ruta del servicio web.</param>
    ''' <remarks>Este constructor crea un objeto conectado con valores iniciales predeterminados para una fuente de datos a través de un servicio web.</remarks>
    Public Sub New(ByVal UrlServicio As Uri)

        Me.New(Nothing, UrlServicio, UbicacionesFuentesDatos.ServicioWeb)

    End Sub

    ''' <summary>
    ''' Crea un nuevo objeto conectado.
    ''' </summary>
    ''' <param name="CadenaConexion">La cadena de conexión a la base de datos SQL Server.</param>
    ''' <param name="UrlServicio">La ruta del servicio web.</param>
    ''' <param name="UbicacionFuenteDatos">Determina la ubicación inicial de la fuente de datos de la fábrica. Este valor puede ser modificado posteriormente.</param>
    ''' <remarks>Este constructor crea un objeto conectado con valores iniciales determinados por los parámetros.</remarks>
    Public Sub New(ByVal CadenaConexion As String, ByVal UrlServicio As Uri, ByVal UbicacionFuenteDatos As UbicacionesFuentesDatos)

        MyBase.New()
        MiCadenaConexion = CadenaConexion
        MiUrlServicio = UrlServicio
        MiUbicacionFuenteDatos = UbicacionFuenteDatos
        MiEsConexionDinamica = False

    End Sub

    ''' <summary>
    ''' Regresa la cadena de conexión a la base de datos.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Property CadenaConexionSQLServer() As String Implements IObjetoConectado.CadenaConexionSQLServer
        Get
            If MiEsConexionDinamica Then
                Return Sincronizacion.CadenaConexionSQLServer
            Else
                Return MiCadenaConexion
            End If
        End Get
        Set(ByVal value As String)
            If Not MiEsConexionDinamica Then
                MiCadenaConexion = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Regresa el identificador del objeto conectado.
    ''' </summary>
    Public Overrides ReadOnly Property Id() As System.Guid Implements IObjetoConectado.Id
        Get
            Return MyBase.Id
        End Get
    End Property

    ''' <summary>
    ''' Define la ubicación de la fuente de datos que utiliza el objeto conectado al momento de ejecutar su métodos.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public Property UbicacionFuenteDatos() As UbicacionesFuentesDatos Implements IObjetoConectado.UbicacionFuenteDatos
        Get
            If MiEsConexionDinamica Then
                Return Sincronizacion.UbicacionFuenteDatos
            Else
                Return MiUbicacionFuenteDatos
            End If
        End Get
        Set(ByVal value As UbicacionesFuentesDatos)
            If Not MiEsConexionDinamica Then
                MiUbicacionFuenteDatos = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Regresa la ruta al servicio web.
    ''' </summary>
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)> _
    Public ReadOnly Property UrlServicioWeb() As System.Uri Implements IObjetoConectado.UrlServicioWeb
        Get
            If MiEsConexionDinamica Then
                Return Sincronizacion.UrlServicioWeb
            Else
                Return MiUrlServicio
            End If
        End Get
    End Property

    ''' <summary>
    ''' Regresa si la conexión del objeto conectado se maneja de forma dinámica.
    ''' </summary>
    Protected ReadOnly Property EsConexionDinamica() As Boolean
        Get
            Return MiEsConexionDinamica
        End Get
    End Property

End Class
