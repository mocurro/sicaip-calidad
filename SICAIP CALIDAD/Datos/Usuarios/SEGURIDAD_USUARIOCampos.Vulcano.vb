﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los campos de la tabla Usuarios
''' </summary>
Public Enum SEGURIDAD_USUARIOCampos As Integer

    cve_usuario
    id_usuario
    Pass
    nombre
    Email
    cve_tipo_usuario
    estatus
    codigo_empleado

End Enum

