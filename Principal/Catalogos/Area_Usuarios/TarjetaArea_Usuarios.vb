﻿Imports System.ComponentModel

Public Class TarjetaArea_Usuarios

    'Private client As Datos.Defectos 

    'Private component As Datos.modelo 
    ''' <summary>
    ''' Define la lista de ventanas que se deben actualizar al cambiar la información.
    ''' </summary>
    ''' <param name="TiposVentanasAfectadas">Lista de ventanas afectadas.</param>
    Protected Overrides Sub DefinirTiposVentanasAfectadas(ByVal TiposVentanasAfectadas As System.Collections.Generic.List(Of System.Type))

        TiposVentanasAfectadas.Add(GetType(CatalogoArea_usuarios ))

    End Sub

    ''' <summary>
    ''' Define la lista de campos para validación visual.
    ''' </summary>
    Protected Overrides Function DefinirCamposValidacion() As System.Collections.Generic.Dictionary(Of Integer, DevExpress.XtraLayout.LayoutControlItem)

        Dim Resultado = New Dictionary(Of Datos.Area_UsuarioCampos , DevExpress.XtraLayout.LayoutControlItem)
        With Resultado
            .Add(Datos.Area_UsuarioCampos .Cve_Area , ItemUnidadNegocio)
            .Add(Datos.Area_UsuarioCampos.CVE_Usuario, itemUsuario)
            .Add(Datos.Area_UsuarioCampos.Estatus , ItemEstatus )
        End With
        Return ConvertirDiccionarioCamposValidacion(Of Datos.Area_UsuarioCampos)(Resultado)

    End Function

    ''' <summary>
    ''' Maneja casos especiales de cambios.
    ''' </summary>
    Protected Overrides Function CambioInformacionPersonalizada(ByVal sender As System.Windows.Forms.Control, ByVal e As System.EventArgs) As Boolean
        Dim Paquete = EntidadDe(Of Datos.Area_Usuario   )()
        
        Select Case sender.Name
            Case ComboArea.Name
                If ComboArea.EditValue IsNot Nothing Then
                      Dim MiGUID As Integer = CInt(ComboArea.EditValue.ToString)
                    Paquete.Cve_Sub_Area = MiGUID
                End If
              
                Return True

            Case ComboUsuario.Name
                If ComboUsuario.EditValue IsNot Nothing Then
                    Dim MiGUID As Long = CInt(ComboUsuario.EditValue.ToString)
                    Paquete.cve_usuario = MiGUID
                    Paquete.Estatus  = CInt(ComboEstatus.SelectedIndex )
                End If
                
                Return True
              Case ComboEstatus .Name
                Dim MiGUID As Long = CInt(ComboEstatus.SelectedIndex )
                Paquete.Estatus  = MiGUID
                Return True

        End Select
    End Function

    Private Sub TarjetaTerminales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim paquete = EntidadDe(Of Datos.Area_Usuario)()
            'Cargar los combos
            With EntidadDe(Of Datos.Area_Usuario)()
                FuenteUsuarios.DataSource = CatalogoDe(Of CatalogoArea_usuarios).Usuarios ' .FindAll(Function(Item) Item.Estatus  = 0)
            End With

            'Cargar los combos
            With EntidadDe(Of Datos.Area_Usuario)()
                FuenteArea.DataSource = CatalogoDe(Of CatalogoArea_usuarios).SubAreas   '.FindAll(Function(Item) Item )
            End With

            If paquete IsNot Nothing Then
                If paquete.Estatus = 0 Or paquete.Estatus = Nothing Then
                    ComboEstatus.SelectedIndex = 0
                Else
                    ComboEstatus.SelectedIndex = 1
                End If

                If paquete.Are IsNot Nothing Then
                    ComboArea.Text = paquete.Are.descripcion
                End If

                ComboArea.EditValue = paquete.Cve_Sub_Area

                ComboArea.Text = String.Empty
                ComboArea.EditValue = paquete.Cve_Sub_Area

                OmitirCambiosPersonalizados = True
                ComboUsuario.EditValue = paquete.cve_usuario

                OmitirCambiosPersonalizados = False
                ComboArea.EditValue = paquete.Cve_Sub_Area

                ComboArea.Refresh()
            End If

        Catch ex As Exception
            MessageBox.Show(String.Format("{0};{1}", ex.Message, ex.StackTrace))
        End Try


    End Sub

    Private Sub TarjetaTerminales_AntesAceptar(sender As Object, e As CancelEventArgs) Handles Me.AntesAceptar
        Dim Paquete = EntidadDe(Of Datos.Area_Usuario  )()
        Paquete.Estatus = ComboEstatus.SelectedIndex
    End Sub
End Class