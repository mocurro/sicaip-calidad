﻿
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Excepción que ocurre cuando intenta cancelar, pausar, reanudar una tarea que no ha sido registrada o ya ha sido cancelada.
''' </summary>
Public Class ExcepcionTareaNoExiste
    Inherits Excepcion

    ''' <summary>
    ''' Crea la excepción basada en un objeto y método específico.
    ''' </summary>
    ''' <param name="Objeto">El objeto que generó la excepción. Si el objeto implementa IObjeto el mensaje de la excepción contendrá información adicional.</param>
    ''' <param name="Metodo">El método donde ocurrió la excepción.</param>
    Public Sub New(ByVal Objeto As Object, ByVal Metodo As Reflection.MethodBase)

        MyBase.New(Objeto, Metodo)

    End Sub

    ''' <summary>
    ''' Regresa el mensaje de la excepción.
    ''' </summary>
    Protected Overrides Function Mensaje(ByVal NombreObjeto As String, ByVal NombreMetodo As String) As String

        Return String.Format("La tarea no existe.", NombreObjeto, NombreMetodo)

    End Function

End Class

