﻿Imports System.IO
Imports Datos
Imports Mini
Imports Mini.Zeus.Cliente
Imports Mini.Zeus.Cliente.UX

Public Class CatalogoLABDIMRegistro

    Private MiListaModelos As List(Of Datos.Modelo)

    Private MiListaClientes As List(Of Datos.Cliente)

    Private MiListaArchivos As List(Of Datos.LABDIMArchivo)

    Private MiListaTipoInspeccion As List(Of Datos.Tipo_Inspeccion)

    Private ListadoRutas As New List(Of String)

    ''' <summary>
    ''' Contiene el botón especial de finalizar.
    ''' </summary>
    Private MiBotonFinalizar As DevExpress.XtraBars.BarButtonItem

    ''' <summary>
    ''' Contiene el botón especial de notificar.
    ''' </summary>
    Private MiBotonNotificar As DevExpress.XtraBars.BarButtonItem

    Private ListadoCorreosAtiende As New List(Of String)

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))

        TituloFijo = "Registro"
        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.LABDIMSolicitud), Datos.Fabricas.LABDIMSolicitud, GetType(TarjetaLABDIMRegistro)))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.LABDIMSolicitud)

    End Function

    Private Function CreateData() As List(Of Datos.Solicitud)

    End Function


    ''' <summary>
    ''' Deshabilita botones especiales.
    ''' </summary>
    Private Sub CatalogoLABDIMSolicitud_AntesActualizarVentana(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.AntesActualizarVentana

        FuenteDatos.DataSource = Nothing

        If MiBotonFinalizar IsNot Nothing Then
            MiBotonFinalizar.Enabled = False
        End If

    End Sub


    Private Sub CatalogoLABDIMSolicitud_DespuesAgregarNuevo(sender As Object, e As ArgumentosDespuesAccionCatalogo(Of IEntidad)) Handles Me.DespuesAgregarNuevo
        Dim solicitud = CType(e.ElementosInvolucrados, Datos.LABDIMSolicitud)

        Dim data As New List(Of Datos.Solicitud)()

        Dim datos = New Datos.Solicitud
        datos.Folio = CType(solicitud.cve_solicitud.ToString.PadLeft(5, CChar("0")), String)
        datos.FechaIngreso = solicitud.Fecha_Entrada 'Date.Now
        datos.Solicitante = solicitud.cve_usuario
        datos.Descripcion = solicitud.descripcion
        datos.FechaPrevista = CDate(Date.Now).AddDays(1)
        datos.NoPiezas = solicitud.no_piezas
        data.Add(datos)

        Dim direcotorioEtiquetas As String = "C:\\GKN\\"
        Dim rutaEtiqueta = String.Format("{0}\{1}.pdf", direcotorioEtiquetas, Guid.NewGuid)
        If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
            System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
        End If

        Dim report As New Solicitud()
        report.DataSource = data
        report.ExportToPdf(rutaEtiqueta)
        'Dim Tool As ReportPrintTool = New ReportPrintTool(report)
        'Tool.ShowPreview()
        Dim impresionPDF As New ProcessStartInfo() With {
                    .UseShellExecute = True,
                    .Verb = "print",
                    .WindowStyle = ProcessWindowStyle.Hidden,
                    .FileName = rutaEtiqueta
                }

        Process.Start(impresionPDF)
    End Sub

    Private Sub CatalogoLABDIMRegistro_DespuesModificarSeleccionado(sender As Object, e As ArgumentosDespuesAccionCatalogo(Of IEntidad)) Handles Me.DespuesModificarSeleccionado
        Dim solicitud = CType(e.ElementosInvolucrados, Datos.LABDIMSolicitud)

        Dim data As New List(Of Datos.Solicitud)()

        Dim datos = New Datos.Solicitud
        datos.Folio = CType(solicitud.cve_solicitud.ToString.PadLeft(5, CChar("0")), String)
        datos.FechaIngreso = solicitud.Fecha_Entrada
        datos.Solicitante = solicitud.user.Nombre
        datos.Descripcion = solicitud.descripcion
        If solicitud.fecha_prevista IsNot Nothing Then
            datos.FechaPrevista = CDate(solicitud.fecha_prevista)
        Else
            datos.FechaPrevista = CDate(Date.Now).AddDays(1)
        End If

        datos.NoPiezas = solicitud.no_piezas
        data.Add(datos)

        Dim direcotorioEtiquetas As String = "C:\\GKN\\"
        Dim rutaEtiqueta = String.Format("{0}\{1}.pdf", direcotorioEtiquetas, Guid.NewGuid)
        If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
            System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
        End If

        Try
            Dim report As New Solicitud()
            report.DataSource = data
            report.ExportToPdf(rutaEtiqueta)
            'Dim Tool As ReportPrintTool = New ReportPrintTool(report)
            'Tool.ShowPreview()
            Dim impresionPDF As New ProcessStartInfo() With {
                        .UseShellExecute = True,
                        .Verb = "print",
                        .WindowStyle = ProcessWindowStyle.Hidden,
                        .FileName = rutaEtiqueta
                    }

            Process.Start(impresionPDF)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al momento de imprimir", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub CatalogoLABDIMRegistro_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Crear botón de exportación avanzada
        MiBotonFinalizar = Barras.Items.CreateButton("Finalizar inspección")
        'MiBotonFinalizar.Glyph = Zeus.Cliente.Windows.UX.My.Resources.Imagen16_Exportar
        MiBotonFinalizar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        AddHandler MiBotonFinalizar.ItemClick, AddressOf MiBotonFinalizar_Click
        BarraHerramientas.AddItem(MiBotonFinalizar)


        MiBotonNotificar = Barras.Items.CreateButton("Notificar inspección")
        'MiBotonFinalizar.Glyph = Zeus.Cliente.Windows.UX.My.Resources.Imagen16_Exportar
        MiBotonNotificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        AddHandler MiBotonNotificar.ItemClick, AddressOf MiBotonNotificar_Click
        BarraHerramientas.AddItem(MiBotonNotificar)
    End Sub

    ''' <summary>
    ''' Actualizar botones especiales.
    ''' </summary>
    Private Sub CatalogoCortesBoletos_DuranteActualizarBarraHerramientas(ByVal sender As Object, ByVal e As Mini.Zeus.Cliente.UX.ArgumentosDuranteActualizarBarraHerramientas) Handles Me.DuranteActualizarBarraHerramientas

        'Validar botón de video
        If MiBotonFinalizar IsNot Nothing Then
            MiBotonFinalizar.Enabled = e.UnRenglon And e.NoEstaOcupado
        End If

    End Sub

    ''' <summary>
    ''' Realiza la exportación siguiendo el formato de exportación.
    ''' </summary>
    Private Sub MiBotonNotificar_Click(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim solicitud = EntidadesSeleccionadasDe(Of Datos.LABDIMSolicitud)()
        ListadoRutas.Clear()
        If solicitud IsNot Nothing AndAlso solicitud.Count > 0 Then
            If solicitud(0).Estatus = 3 Then
                MessageBox.Show("Se ha notificado anteriormente", "Solicitud finalizada", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                If solicitud(0).Estatus = 2 Then

                    ListadoCorreosAtiende.Clear()
                    Dim usuario = Fabricas.Seguridad_Usuarios.ObtenerUno(solicitud(0).cve_usuario)
                    SesiónLocal.crear_contenido_mensaje_LabDimFinalizarSolicitud(solicitud(0), usuario)
                    ListadoCorreosAtiende.Add(usuario.Email)
                    Dim archivo As Datos.LABDIMArchivo
                    Using Contexto = New ModeloDatosDataContext(Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer)
                        Try
                            archivo = Contexto.LABDIMArchivos.Single(Function(p As LABDIMArchivo) p.cve_solicitud = solicitud(0).cve_solicitud And p.Tipo = 1)
                        Catch ex As Exception

                        End Try

                    End Using
                    Dim RutaArchivo As String
                    If archivo IsNot Nothing Then
                        Dim direcotorioEtiquetas As String = "C:\\GKN\\"
                        RutaArchivo = String.Format("{0}\{1}", direcotorioEtiquetas, archivo.Nombre)
                        If Not System.IO.Directory.Exists(direcotorioEtiquetas) Then
                            System.IO.Directory.CreateDirectory(direcotorioEtiquetas)
                        End If
                        If File.Exists(RutaArchivo) Then
                            File.Delete(RutaArchivo)
                        End If
                        File.WriteAllBytes(RutaArchivo, archivo.Archivo.ToArray)
                    End If

                    If Not String.IsNullOrEmpty(RutaArchivo) Then
                        ListadoRutas.Add(RutaArchivo)
                    End If
                    Try
                        SesiónLocal.envia_notificacion_adjunto(ListadoCorreosAtiende, ListadoRutas)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "Error al enviar correo", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                    Using Contexto = New ModeloDatosDataContext(Datos.Fabricas.Usuario_Unidad_Negocio.CadenaConexionSQLServer)
                        Dim Objeto = Contexto.LABDIMSolicituds.Single(Function(p As LABDIMSolicitud) p.cve_solicitud = solicitud(0).cve_solicitud)
                        Objeto.Estatus = 3
                        Contexto.SubmitChanges()
                    End Using
                    ActualizarVentana()
                Else
                    MessageBox.Show("La solicitud no se ha registrado. Dar entrada para finalizar la solicitud", "Solicitud no registrada", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If
        Else
            MessageBox.Show("No se ha seleccionado ningún registro", "No hay registro seleccionado", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    ''' <summary>
    ''' Realiza la exportación siguiendo el formato de exportación.
    ''' </summary>
    Private Sub MiBotonFinalizar_Click(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim solicitud = EntidadesSeleccionadasDe(Of Datos.LABDIMSolicitud)()
        ListadoRutas.Clear()
        If solicitud IsNot Nothing AndAlso solicitud.Count > 0 Then
            If solicitud(0).Estatus >= 2 Then
                MessageBox.Show("La solicitud se ha finalizado anteriormente", "Solicitud finalizada", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                If solicitud(0).Estatus = 1 Then
                    Using ventana = New FormaFinalizar
                        ventana.Solicitud = solicitud(0)
                        If ventana.ShowDialog() = DialogResult.OK Then

                            ListadoCorreosAtiende.Clear()
                            solicitud(0).Fecha_Salida = Date.Now
                            Dim usuario = Fabricas.Seguridad_Usuarios.ObtenerUno(solicitud(0).cve_usuario)
                            SesiónLocal.crear_contenido_mensaje_LabDimFinalizarSolicitud(solicitud(0), usuario)
                            ListadoCorreosAtiende.Add(usuario.Email)
                            'If Not String.IsNullOrEmpty(ventana.RutaArchivo) Then
                            '    ListadoRutas.Add(ventana.RutaArchivo)
                            'End If
                            Try
                                SesiónLocal.envia_notificacion_adjunto(ListadoCorreosAtiende, ListadoRutas)
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "Error al enviar correo", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Try


                            ActualizarVentana()
                        End If
                    End Using
                Else
                    MessageBox.Show("La solicitud no se ha registrado. Dar entrada para finalizar la solicitud", "Solicitud no registrada", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If
        Else
            MessageBox.Show("No se ha seleccionado ningún registro", "No hay registro seleccionado", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub CatalogoLABDIMRegistro_AntesBorrarEntidad(sender As Object, e As ArgumentosAntesAccionCatalogo(Of IEntidad)) Handles Me.AntesBorrarEntidad
        Dim solicitud = EntidadesSeleccionadasDe(Of Datos.LABDIMSolicitud)()
        If solicitud IsNot Nothing AndAlso solicitud.Count > 0 Then
            If solicitud(0).Estatus > 0 Then
                e.Cancel = True
                'Argumentos.Cancel = True
                MessageBox.Show("No se puede eliminar el registro debido a que ya fue dada de alta la entrada  ", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End If
        End If
    End Sub

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Modelos() As List(Of Datos.Modelo)
        Get
            If MiListaModelos Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaModelos = Fabricas.Modelo.ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaModelos
        End Get
    End Property


    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Clientes() As List(Of Datos.Cliente)
        Get
            If MiListaClientes Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaClientes = Fabricas.Cliente.ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaClientes
        End Get
    End Property

    Public Property Archivos As List(Of Datos.LABDIMArchivo)
        Get
            Return MiListaArchivos
        End Get
        Set(value As List(Of Datos.LABDIMArchivo))
            MiListaArchivos = value
        End Set
    End Property

    Public Property TiposInspección As List(Of Datos.Tipo_Inspeccion)
        Get
            If MiListaTipoInspeccion Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                MiListaTipoInspeccion = Fabricas.Tipo_Inpeccion.ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaTipoInspeccion
        End Get
        Set(value As List(Of Datos.Tipo_Inspeccion))
            MiListaTipoInspeccion = value
        End Set
    End Property


End Class