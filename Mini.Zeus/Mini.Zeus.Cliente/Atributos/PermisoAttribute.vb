﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Atributo para definir los permisos que utiliza una clase.
''' </summary>
''' <remarks>Se puede aplicar este atributo más de una vez en una clase. Cada instancia define un tipo de permiso diferente que utiliza la clase.</remarks>
<AttributeUsage(AttributeTargets.Class, AllowMultiple:=True, Inherited:=False)> _
Public NotInheritable Class PermisoAttribute
    Inherits Attribute

    '''' <summary>
    '''' Contiene el Guid del permiso.
    '''' </summary>
    'Private MiGuidPermiso As Guid

    '''' <summary>
    '''' Contiene el tipo de permiso que representa este atributo.
    '''' </summary>
    'Private MiTipo As TiposPermisos

    '''' <summary>
    '''' Contiene el nivel predefinido del permiso.
    '''' </summary>
    '''' <remarks>Si el nivel es "Indefinido", se determinará durante la ejecución la autorización en base al usuario. Para cualquier otro nivel, se asumira dicho valor para la autorización, independiente del usuario.</remarks>
    'Private MiNivel As NivelesPermisos

    '''' <summary>
    '''' Crea un permiso de un tipo específico.
    '''' </summary>
    '''' <param name="Tipo">Indica el tipo de permiso que representa este atributo.</param>
    '''' <param name="GuidPermiso">El Guid del permiso que representa este atributo en formato de texto.</param>
    '''' <remarks>Este constructor asume que el nivel del permiso lo determinará el usuario al momento de valorarlo, por lo que fija el nivel del atributo a "Indefinido".</remarks>
    'Public Sub New(ByVal Tipo As TiposPermisos, ByVal GuidPermiso As String)

    '    Me.New(Tipo, New Guid(GuidPermiso), NivelesPermisos.Indefinido)

    'End Sub

    '''' <summary>
    '''' Crea un permiso de un tipo y nivel específicos.
    '''' </summary>
    '''' <param name="Tipo">Indica el tipo de permiso que representa este atributo.</param>
    '''' <param name="GuidPermiso">El Guid del permiso que representa este atributo en formato de texto.</param>
    '''' <param name="Nivel">Indica el nivel del permiso que representa este atributo.</param>
    '''' <remarks>Este constructor fija el nivel del permiso, por lo que no será evaluado con referencia al usuario.</remarks>
    'Public Sub New(ByVal Tipo As TiposPermisos, ByVal GuidPermiso As String, ByVal Nivel As NivelesPermisos)

    '    Me.New(Tipo, New Guid(GuidPermiso), Nivel)

    'End Sub

    '''' <summary>
    '''' Crea un permiso de un tipo específico.
    '''' </summary>
    '''' <param name="Tipo">Indica el tipo de permiso que representa este atributo.</param>
    '''' <param name="GuidPermiso">El Guid del permiso que representa este atributo.</param>
    '''' <remarks>Este constructor asume que el nivel del permiso lo determinará el usuario al momento de valorarlo, por lo que fija el nivel del atributo a "Indefinido".</remarks>
    'Public Sub New(ByVal Tipo As TiposPermisos, ByVal GuidPermiso As Guid)

    '    Me.New(Tipo, GuidPermiso, NivelesPermisos.Indefinido)

    'End Sub

    '''' <summary>
    '''' Crea un permiso de un tipo y nivel específicos.
    '''' </summary>
    '''' <param name="Tipo">Indica el tipo de permiso que representa este atributo.</param>
    '''' <param name="GuidPermiso">El Guid del permiso que representa este atributo.</param>
    '''' <param name="Nivel">Indica el nivel del permiso que representa este atributo.</param>
    '''' <remarks>Este constructor fija el nivel del permiso, por lo que no será evaluado con referencia al usuario.</remarks>
    'Public Sub New(ByVal Tipo As TiposPermisos, ByVal GuidPermiso As Guid, ByVal Nivel As NivelesPermisos)

    '    MiTipo = Tipo
    '    MiGuidPermiso = GuidPermiso
    '    MiNivel = Nivel

    'End Sub

    '''' <summary>
    '''' Regresa el tipo de permiso que representa este atributo.
    '''' </summary>
    'Public ReadOnly Property Tipo() As TiposPermisos
    '    Get
    '        Return MiTipo
    '    End Get
    'End Property

    '''' <summary>
    '''' Regresa el nivel del permiso que se le asignó originalmente al atributo.
    '''' </summary>
    'Public ReadOnly Property Nivel() As NivelesPermisos
    '    Get
    '        Return MiNivel
    '    End Get
    'End Property

    ''' <summary>
    ''' Regresa el Guid del permiso que representa este atributo.
    ''' </summary>
    'Public ReadOnly Property GuidPermiso() As Guid
    '    Get
    '        Return MiGuidPermiso
    '    End Get
    'End Property

End Class
