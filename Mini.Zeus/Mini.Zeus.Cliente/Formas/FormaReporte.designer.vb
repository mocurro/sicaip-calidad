﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormaReporte
    Inherits FormaPestaña

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormaReporte))
        Me.Barras = New DevExpress.XtraPrinting.Preview.PrintBarManager
        Me.BarraHerramientas = New DevExpress.XtraPrinting.Preview.PreviewBar
        Me.PrintPreviewBarItem2 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem3 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem4 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem5 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem6 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem7 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem8 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem9 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem10 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem11 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem12 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem13 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem14 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem15 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.ZoomBarEditItem1 = New DevExpress.XtraPrinting.Preview.ZoomBarEditItem
        Me.PrintPreviewRepositoryItemComboBox1 = New DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox
        Me.PrintPreviewBarItem16 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem17 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem18 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem19 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem20 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem21 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem22 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem23 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem24 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem25 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem26 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.BarraEstado = New DevExpress.XtraPrinting.Preview.PreviewBar
        Me.PrintPreviewStaticItem1 = New DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem
        Me.ProgressBarEditItem1 = New DevExpress.XtraPrinting.Preview.ProgressBarEditItem
        Me.RepositoryItemProgressBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
        Me.PrintPreviewBarItem1 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.PrintPreviewStaticItem2 = New DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem
        Me.BarraMenu = New DevExpress.XtraPrinting.Preview.PreviewBar
        Me.PrintPreviewSubItem1 = New DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
        Me.PrintPreviewSubItem2 = New DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
        Me.PrintPreviewSubItem4 = New DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
        Me.PrintPreviewBarItem27 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.PrintPreviewBarItem28 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
        Me.BarToolbarsListItem1 = New DevExpress.XtraBars.BarToolbarsListItem
        Me.PrintPreviewSubItem3 = New DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.PrintPreviewBarCheckItem1 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem2 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem3 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem4 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem5 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem6 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem7 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem8 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem9 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem10 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem11 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem12 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem13 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem14 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem15 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem16 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.PrintPreviewBarCheckItem17 = New DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
        Me.AreaImpresion = New DevExpress.XtraPrinting.Control.PrintControl
        Me.SistemaImpresion = New DevExpress.XtraPrinting.PrintingSystem(Me.components)
        Me.PanelFiltro = New DevExpress.XtraEditors.PanelControl
        Me.LayoutFiltro = New DevExpress.XtraLayout.LayoutControl
        Me.LayoutFiltroRaiz = New DevExpress.XtraLayout.LayoutControlGroup
        Me.SplitContenido = New DevExpress.XtraEditors.SplitContainerControl
        Me.PanelMensaje = New DevExpress.XtraEditors.PanelControl
        Me.LayoutMensaje = New DevExpress.XtraLayout.LayoutControl
        Me.ProgresoDefinidoMensaje = New DevExpress.XtraEditors.ProgressBarControl
        Me.ProgresoMensaje = New DevExpress.XtraEditors.MarqueeProgressBarControl
        Me.EtiquetaMensaje = New DevExpress.XtraEditors.LabelControl
        Me.ImagenMensaje = New System.Windows.Forms.PictureBox
        Me.LayoutMensajeRaiz = New DevExpress.XtraLayout.LayoutControlGroup
        Me.ItemForImagenMensaje = New DevExpress.XtraLayout.LayoutControlItem
        Me.ItemForEtiquetaMensaje = New DevExpress.XtraLayout.LayoutControlItem
        Me.ItemForProgresoMensaje = New DevExpress.XtraLayout.LayoutControlItem
        Me.ItemForProgresoDefinidoMensaje = New DevExpress.XtraLayout.LayoutControlItem
        Me.EtiquetaCopyright = New DevExpress.XtraEditors.LabelControl
        CType(Me.Barras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrintPreviewRepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SistemaImpresion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelFiltro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelFiltro.SuspendLayout()
        CType(Me.LayoutFiltro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutFiltroRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContenido.SuspendLayout()
        CType(Me.PanelMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelMensaje.SuspendLayout()
        CType(Me.LayoutMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutMensaje.SuspendLayout()
        CType(Me.ProgresoDefinidoMensaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgresoMensaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImagenMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMensajeRaiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForImagenMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForEtiquetaMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForProgresoMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemForProgresoDefinidoMensaje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Barras
        '
        Me.Barras.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.BarraHerramientas, Me.BarraEstado, Me.BarraMenu})
        Me.Barras.DockControls.Add(Me.barDockControlTop)
        Me.Barras.DockControls.Add(Me.barDockControlBottom)
        Me.Barras.DockControls.Add(Me.barDockControlLeft)
        Me.Barras.DockControls.Add(Me.barDockControlRight)
        Me.Barras.Form = Me
        Me.Barras.ImageStream = CType(resources.GetObject("Barras.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.Barras.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.PrintPreviewStaticItem1, Me.BarStaticItem1, Me.ProgressBarEditItem1, Me.PrintPreviewBarItem1, Me.BarButtonItem1, Me.PrintPreviewStaticItem2, Me.PrintPreviewBarItem2, Me.PrintPreviewBarItem3, Me.PrintPreviewBarItem4, Me.PrintPreviewBarItem5, Me.PrintPreviewBarItem6, Me.PrintPreviewBarItem7, Me.PrintPreviewBarItem8, Me.PrintPreviewBarItem9, Me.PrintPreviewBarItem10, Me.PrintPreviewBarItem11, Me.PrintPreviewBarItem12, Me.PrintPreviewBarItem13, Me.PrintPreviewBarItem14, Me.PrintPreviewBarItem15, Me.ZoomBarEditItem1, Me.PrintPreviewBarItem16, Me.PrintPreviewBarItem17, Me.PrintPreviewBarItem18, Me.PrintPreviewBarItem19, Me.PrintPreviewBarItem20, Me.PrintPreviewBarItem21, Me.PrintPreviewBarItem22, Me.PrintPreviewBarItem23, Me.PrintPreviewBarItem24, Me.PrintPreviewBarItem25, Me.PrintPreviewBarItem26, Me.PrintPreviewSubItem1, Me.PrintPreviewSubItem2, Me.PrintPreviewSubItem3, Me.PrintPreviewSubItem4, Me.PrintPreviewBarItem27, Me.PrintPreviewBarItem28, Me.BarToolbarsListItem1, Me.PrintPreviewBarCheckItem1, Me.PrintPreviewBarCheckItem2, Me.PrintPreviewBarCheckItem3, Me.PrintPreviewBarCheckItem4, Me.PrintPreviewBarCheckItem5, Me.PrintPreviewBarCheckItem6, Me.PrintPreviewBarCheckItem7, Me.PrintPreviewBarCheckItem8, Me.PrintPreviewBarCheckItem9, Me.PrintPreviewBarCheckItem10, Me.PrintPreviewBarCheckItem11, Me.PrintPreviewBarCheckItem12, Me.PrintPreviewBarCheckItem13, Me.PrintPreviewBarCheckItem14, Me.PrintPreviewBarCheckItem15, Me.PrintPreviewBarCheckItem16, Me.PrintPreviewBarCheckItem17})
        Me.Barras.MainMenu = Me.BarraMenu
        Me.Barras.MaxItemId = 56
        Me.Barras.PreviewBar = Me.BarraHerramientas
        Me.Barras.PrintControl = Me.AreaImpresion
        Me.Barras.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemProgressBar1, Me.PrintPreviewRepositoryItemComboBox1})
        Me.Barras.StatusBar = Me.BarraEstado
        '
        'BarraHerramientas
        '
        Me.BarraHerramientas.BarName = "BarraHerramientas"
        Me.BarraHerramientas.DockCol = 0
        Me.BarraHerramientas.DockRow = 1
        Me.BarraHerramientas.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.BarraHerramientas.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem3), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem4), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem5, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem6, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem7), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem8, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem9), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem11), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem12), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem13, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem14), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem15, True), New DevExpress.XtraBars.LinkPersistInfo(Me.ZoomBarEditItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem16), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem17, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem18), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem19), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem20), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem21, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem22), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem23), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem24, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem25), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem26, True)})
        Me.BarraHerramientas.OptionsBar.AllowQuickCustomization = False
        Me.BarraHerramientas.OptionsBar.DisableClose = True
        Me.BarraHerramientas.OptionsBar.DisableCustomization = True
        Me.BarraHerramientas.OptionsBar.DrawDragBorder = False
        Me.BarraHerramientas.OptionsBar.UseWholeRow = True
        Me.BarraHerramientas.Text = "BarraHerramientas"
        '
        'PrintPreviewBarItem2
        '
        Me.PrintPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem2.Caption = "Document Map"
        Me.PrintPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap
        Me.PrintPreviewBarItem2.Enabled = False
        Me.PrintPreviewBarItem2.Hint = "Document Map"
        Me.PrintPreviewBarItem2.Id = 6
        Me.PrintPreviewBarItem2.ImageIndex = 19
        Me.PrintPreviewBarItem2.Name = "PrintPreviewBarItem2"
        Me.PrintPreviewBarItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'PrintPreviewBarItem3
        '
        Me.PrintPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem3.Caption = "Parameters"
        Me.PrintPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters
        Me.PrintPreviewBarItem3.Enabled = False
        Me.PrintPreviewBarItem3.Hint = "Parameters"
        Me.PrintPreviewBarItem3.Id = 7
        Me.PrintPreviewBarItem3.ImageIndex = 22
        Me.PrintPreviewBarItem3.Name = "PrintPreviewBarItem3"
        Me.PrintPreviewBarItem3.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'PrintPreviewBarItem4
        '
        Me.PrintPreviewBarItem4.Caption = "Search"
        Me.PrintPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find
        Me.PrintPreviewBarItem4.Enabled = False
        Me.PrintPreviewBarItem4.Hint = "Search"
        Me.PrintPreviewBarItem4.Id = 8
        Me.PrintPreviewBarItem4.ImageIndex = 20
        Me.PrintPreviewBarItem4.Name = "PrintPreviewBarItem4"
        '
        'PrintPreviewBarItem5
        '
        Me.PrintPreviewBarItem5.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem5.Caption = "Customize"
        Me.PrintPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize
        Me.PrintPreviewBarItem5.Enabled = False
        Me.PrintPreviewBarItem5.Hint = "Customize"
        Me.PrintPreviewBarItem5.Id = 9
        Me.PrintPreviewBarItem5.ImageIndex = 14
        Me.PrintPreviewBarItem5.Name = "PrintPreviewBarItem5"
        '
        'PrintPreviewBarItem6
        '
        Me.PrintPreviewBarItem6.Caption = "Open"
        Me.PrintPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open
        Me.PrintPreviewBarItem6.Hint = "Open a document"
        Me.PrintPreviewBarItem6.Id = 10
        Me.PrintPreviewBarItem6.ImageIndex = 23
        Me.PrintPreviewBarItem6.Name = "PrintPreviewBarItem6"
        '
        'PrintPreviewBarItem7
        '
        Me.PrintPreviewBarItem7.Caption = "Save"
        Me.PrintPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save
        Me.PrintPreviewBarItem7.Enabled = False
        Me.PrintPreviewBarItem7.Hint = "Save the document"
        Me.PrintPreviewBarItem7.Id = 11
        Me.PrintPreviewBarItem7.ImageIndex = 24
        Me.PrintPreviewBarItem7.Name = "PrintPreviewBarItem7"
        '
        'PrintPreviewBarItem8
        '
        Me.PrintPreviewBarItem8.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem8.Caption = "&Print..."
        Me.PrintPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print
        Me.PrintPreviewBarItem8.Enabled = False
        Me.PrintPreviewBarItem8.Hint = "Print"
        Me.PrintPreviewBarItem8.Id = 12
        Me.PrintPreviewBarItem8.ImageIndex = 0
        Me.PrintPreviewBarItem8.Name = "PrintPreviewBarItem8"
        '
        'PrintPreviewBarItem9
        '
        Me.PrintPreviewBarItem9.Caption = "P&rint"
        Me.PrintPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect
        Me.PrintPreviewBarItem9.Enabled = False
        Me.PrintPreviewBarItem9.Hint = "Quick Print"
        Me.PrintPreviewBarItem9.Id = 13
        Me.PrintPreviewBarItem9.ImageIndex = 1
        Me.PrintPreviewBarItem9.Name = "PrintPreviewBarItem9"
        '
        'PrintPreviewBarItem10
        '
        Me.PrintPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem10.Caption = "Page Set&up..."
        Me.PrintPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup
        Me.PrintPreviewBarItem10.Enabled = False
        Me.PrintPreviewBarItem10.Hint = "Page Setup"
        Me.PrintPreviewBarItem10.Id = 14
        Me.PrintPreviewBarItem10.ImageIndex = 2
        Me.PrintPreviewBarItem10.Name = "PrintPreviewBarItem10"
        '
        'PrintPreviewBarItem11
        '
        Me.PrintPreviewBarItem11.Caption = "Header And Footer"
        Me.PrintPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF
        Me.PrintPreviewBarItem11.Enabled = False
        Me.PrintPreviewBarItem11.Hint = "Header And Footer"
        Me.PrintPreviewBarItem11.Id = 15
        Me.PrintPreviewBarItem11.ImageIndex = 15
        Me.PrintPreviewBarItem11.Name = "PrintPreviewBarItem11"
        '
        'PrintPreviewBarItem12
        '
        Me.PrintPreviewBarItem12.ActAsDropDown = True
        Me.PrintPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.PrintPreviewBarItem12.Caption = "Scale"
        Me.PrintPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale
        Me.PrintPreviewBarItem12.Enabled = False
        Me.PrintPreviewBarItem12.Hint = "Scale"
        Me.PrintPreviewBarItem12.Id = 16
        Me.PrintPreviewBarItem12.ImageIndex = 25
        Me.PrintPreviewBarItem12.Name = "PrintPreviewBarItem12"
        '
        'PrintPreviewBarItem13
        '
        Me.PrintPreviewBarItem13.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem13.Caption = "Hand Tool"
        Me.PrintPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool
        Me.PrintPreviewBarItem13.Enabled = False
        Me.PrintPreviewBarItem13.Hint = "Hand Tool"
        Me.PrintPreviewBarItem13.Id = 17
        Me.PrintPreviewBarItem13.ImageIndex = 16
        Me.PrintPreviewBarItem13.Name = "PrintPreviewBarItem13"
        '
        'PrintPreviewBarItem14
        '
        Me.PrintPreviewBarItem14.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem14.Caption = "Magnifier"
        Me.PrintPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier
        Me.PrintPreviewBarItem14.Enabled = False
        Me.PrintPreviewBarItem14.Hint = "Magnifier"
        Me.PrintPreviewBarItem14.Id = 18
        Me.PrintPreviewBarItem14.ImageIndex = 3
        Me.PrintPreviewBarItem14.Name = "PrintPreviewBarItem14"
        '
        'PrintPreviewBarItem15
        '
        Me.PrintPreviewBarItem15.Caption = "Zoom Out"
        Me.PrintPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut
        Me.PrintPreviewBarItem15.Enabled = False
        Me.PrintPreviewBarItem15.Hint = "Zoom Out"
        Me.PrintPreviewBarItem15.Id = 19
        Me.PrintPreviewBarItem15.ImageIndex = 5
        Me.PrintPreviewBarItem15.Name = "PrintPreviewBarItem15"
        '
        'ZoomBarEditItem1
        '
        Me.ZoomBarEditItem1.Caption = "Zoom"
        Me.ZoomBarEditItem1.Edit = Me.PrintPreviewRepositoryItemComboBox1
        Me.ZoomBarEditItem1.EditValue = "100%"
        Me.ZoomBarEditItem1.Enabled = False
        Me.ZoomBarEditItem1.Hint = "Zoom"
        Me.ZoomBarEditItem1.Id = 20
        Me.ZoomBarEditItem1.Name = "ZoomBarEditItem1"
        Me.ZoomBarEditItem1.Width = 70
        '
        'PrintPreviewRepositoryItemComboBox1
        '
        Me.PrintPreviewRepositoryItemComboBox1.AutoComplete = False
        Me.PrintPreviewRepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PrintPreviewRepositoryItemComboBox1.DropDownRows = 11
        Me.PrintPreviewRepositoryItemComboBox1.Name = "PrintPreviewRepositoryItemComboBox1"
        '
        'PrintPreviewBarItem16
        '
        Me.PrintPreviewBarItem16.Caption = "Zoom In"
        Me.PrintPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn
        Me.PrintPreviewBarItem16.Enabled = False
        Me.PrintPreviewBarItem16.Hint = "Zoom In"
        Me.PrintPreviewBarItem16.Id = 21
        Me.PrintPreviewBarItem16.ImageIndex = 4
        Me.PrintPreviewBarItem16.Name = "PrintPreviewBarItem16"
        '
        'PrintPreviewBarItem17
        '
        Me.PrintPreviewBarItem17.Caption = "First Page"
        Me.PrintPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage
        Me.PrintPreviewBarItem17.Enabled = False
        Me.PrintPreviewBarItem17.Hint = "First Page"
        Me.PrintPreviewBarItem17.Id = 22
        Me.PrintPreviewBarItem17.ImageIndex = 7
        Me.PrintPreviewBarItem17.Name = "PrintPreviewBarItem17"
        '
        'PrintPreviewBarItem18
        '
        Me.PrintPreviewBarItem18.Caption = "Previous Page"
        Me.PrintPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage
        Me.PrintPreviewBarItem18.Enabled = False
        Me.PrintPreviewBarItem18.Hint = "Previous Page"
        Me.PrintPreviewBarItem18.Id = 23
        Me.PrintPreviewBarItem18.ImageIndex = 8
        Me.PrintPreviewBarItem18.Name = "PrintPreviewBarItem18"
        '
        'PrintPreviewBarItem19
        '
        Me.PrintPreviewBarItem19.Caption = "Next Page"
        Me.PrintPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage
        Me.PrintPreviewBarItem19.Enabled = False
        Me.PrintPreviewBarItem19.Hint = "Next Page"
        Me.PrintPreviewBarItem19.Id = 24
        Me.PrintPreviewBarItem19.ImageIndex = 9
        Me.PrintPreviewBarItem19.Name = "PrintPreviewBarItem19"
        '
        'PrintPreviewBarItem20
        '
        Me.PrintPreviewBarItem20.Caption = "Last Page"
        Me.PrintPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage
        Me.PrintPreviewBarItem20.Enabled = False
        Me.PrintPreviewBarItem20.Hint = "Last Page"
        Me.PrintPreviewBarItem20.Id = 25
        Me.PrintPreviewBarItem20.ImageIndex = 10
        Me.PrintPreviewBarItem20.Name = "PrintPreviewBarItem20"
        '
        'PrintPreviewBarItem21
        '
        Me.PrintPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.PrintPreviewBarItem21.Caption = "Multiple Pages"
        Me.PrintPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages
        Me.PrintPreviewBarItem21.Enabled = False
        Me.PrintPreviewBarItem21.Hint = "Multiple Pages"
        Me.PrintPreviewBarItem21.Id = 26
        Me.PrintPreviewBarItem21.ImageIndex = 11
        Me.PrintPreviewBarItem21.Name = "PrintPreviewBarItem21"
        '
        'PrintPreviewBarItem22
        '
        Me.PrintPreviewBarItem22.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.PrintPreviewBarItem22.Caption = "&Color..."
        Me.PrintPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground
        Me.PrintPreviewBarItem22.Enabled = False
        Me.PrintPreviewBarItem22.Hint = "Background"
        Me.PrintPreviewBarItem22.Id = 27
        Me.PrintPreviewBarItem22.ImageIndex = 12
        Me.PrintPreviewBarItem22.Name = "PrintPreviewBarItem22"
        '
        'PrintPreviewBarItem23
        '
        Me.PrintPreviewBarItem23.Caption = "&Watermark..."
        Me.PrintPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark
        Me.PrintPreviewBarItem23.Enabled = False
        Me.PrintPreviewBarItem23.Hint = "Watermark"
        Me.PrintPreviewBarItem23.Id = 28
        Me.PrintPreviewBarItem23.ImageIndex = 21
        Me.PrintPreviewBarItem23.Name = "PrintPreviewBarItem23"
        '
        'PrintPreviewBarItem24
        '
        Me.PrintPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.PrintPreviewBarItem24.Caption = "Export Document..."
        Me.PrintPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile
        Me.PrintPreviewBarItem24.Enabled = False
        Me.PrintPreviewBarItem24.Hint = "Export Document..."
        Me.PrintPreviewBarItem24.Id = 29
        Me.PrintPreviewBarItem24.ImageIndex = 18
        Me.PrintPreviewBarItem24.Name = "PrintPreviewBarItem24"
        '
        'PrintPreviewBarItem25
        '
        Me.PrintPreviewBarItem25.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.PrintPreviewBarItem25.Caption = "Send via E-Mail..."
        Me.PrintPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile
        Me.PrintPreviewBarItem25.Enabled = False
        Me.PrintPreviewBarItem25.Hint = "Send via E-Mail..."
        Me.PrintPreviewBarItem25.Id = 30
        Me.PrintPreviewBarItem25.ImageIndex = 17
        Me.PrintPreviewBarItem25.Name = "PrintPreviewBarItem25"
        '
        'PrintPreviewBarItem26
        '
        Me.PrintPreviewBarItem26.Caption = "E&xit"
        Me.PrintPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview
        Me.PrintPreviewBarItem26.Hint = "Close Preview"
        Me.PrintPreviewBarItem26.Id = 31
        Me.PrintPreviewBarItem26.ImageIndex = 13
        Me.PrintPreviewBarItem26.Name = "PrintPreviewBarItem26"
        '
        'BarraEstado
        '
        Me.BarraEstado.BarName = "BarraEstado"
        Me.BarraEstado.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.BarraEstado.DockCol = 0
        Me.BarraEstado.DockRow = 0
        Me.BarraEstado.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.BarraEstado.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewStaticItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1, True), New DevExpress.XtraBars.LinkPersistInfo(Me.ProgressBarEditItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewStaticItem2, True)})
        Me.BarraEstado.OptionsBar.AllowQuickCustomization = False
        Me.BarraEstado.OptionsBar.DrawDragBorder = False
        Me.BarraEstado.OptionsBar.UseWholeRow = True
        Me.BarraEstado.Text = "BarraEstado"
        '
        'PrintPreviewStaticItem1
        '
        Me.PrintPreviewStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PrintPreviewStaticItem1.Caption = "Nothing"
        Me.PrintPreviewStaticItem1.Id = 0
        Me.PrintPreviewStaticItem1.LeftIndent = 1
        Me.PrintPreviewStaticItem1.Name = "PrintPreviewStaticItem1"
        Me.PrintPreviewStaticItem1.RightIndent = 1
        Me.PrintPreviewStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        Me.PrintPreviewStaticItem1.Type = "PageOfPages"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BarStaticItem1.Id = 1
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'ProgressBarEditItem1
        '
        Me.ProgressBarEditItem1.Edit = Me.RepositoryItemProgressBar1
        Me.ProgressBarEditItem1.EditHeight = 12
        Me.ProgressBarEditItem1.Id = 2
        Me.ProgressBarEditItem1.Name = "ProgressBarEditItem1"
        Me.ProgressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        Me.ProgressBarEditItem1.Width = 150
        '
        'RepositoryItemProgressBar1
        '
        Me.RepositoryItemProgressBar1.Name = "RepositoryItemProgressBar1"
        '
        'PrintPreviewBarItem1
        '
        Me.PrintPreviewBarItem1.Caption = "Stop"
        Me.PrintPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding
        Me.PrintPreviewBarItem1.Enabled = False
        Me.PrintPreviewBarItem1.Hint = "Stop"
        Me.PrintPreviewBarItem1.Id = 3
        Me.PrintPreviewBarItem1.Name = "PrintPreviewBarItem1"
        Me.PrintPreviewBarItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BarButtonItem1.Enabled = False
        Me.BarButtonItem1.Id = 4
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'PrintPreviewStaticItem2
        '
        Me.PrintPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.PrintPreviewStaticItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PrintPreviewStaticItem2.Caption = "100%"
        Me.PrintPreviewStaticItem2.Id = 5
        Me.PrintPreviewStaticItem2.Name = "PrintPreviewStaticItem2"
        Me.PrintPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near
        Me.PrintPreviewStaticItem2.Type = "ZoomFactor"
        Me.PrintPreviewStaticItem2.Width = 40
        '
        'BarraMenu
        '
        Me.BarraMenu.BarName = "BarraMenu"
        Me.BarraMenu.DockCol = 0
        Me.BarraMenu.DockRow = 0
        Me.BarraMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.BarraMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewSubItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewSubItem3)})
        Me.BarraMenu.OptionsBar.MultiLine = True
        Me.BarraMenu.OptionsBar.UseWholeRow = True
        Me.BarraMenu.Text = "BarraMenu"
        Me.BarraMenu.Visible = False
        '
        'PrintPreviewSubItem1
        '
        Me.PrintPreviewSubItem1.Caption = "&File"
        Me.PrintPreviewSubItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.File
        Me.PrintPreviewSubItem1.Id = 32
        Me.PrintPreviewSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem8), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem9), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem24, True), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem25), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem26, True)})
        Me.PrintPreviewSubItem1.Name = "PrintPreviewSubItem1"
        '
        'PrintPreviewSubItem2
        '
        Me.PrintPreviewSubItem2.Caption = "&View"
        Me.PrintPreviewSubItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.View
        Me.PrintPreviewSubItem2.Id = 33
        Me.PrintPreviewSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewSubItem4, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarToolbarsListItem1, True)})
        Me.PrintPreviewSubItem2.Name = "PrintPreviewSubItem2"
        '
        'PrintPreviewSubItem4
        '
        Me.PrintPreviewSubItem4.Caption = "&Page Layout"
        Me.PrintPreviewSubItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayout
        Me.PrintPreviewSubItem4.Id = 35
        Me.PrintPreviewSubItem4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem27), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem28)})
        Me.PrintPreviewSubItem4.Name = "PrintPreviewSubItem4"
        '
        'PrintPreviewBarItem27
        '
        Me.PrintPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem27.Caption = "&Facing"
        Me.PrintPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutFacing
        Me.PrintPreviewBarItem27.Enabled = False
        Me.PrintPreviewBarItem27.GroupIndex = 100
        Me.PrintPreviewBarItem27.Id = 36
        Me.PrintPreviewBarItem27.Name = "PrintPreviewBarItem27"
        '
        'PrintPreviewBarItem28
        '
        Me.PrintPreviewBarItem28.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.PrintPreviewBarItem28.Caption = "&Continuous"
        Me.PrintPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutContinuous
        Me.PrintPreviewBarItem28.Enabled = False
        Me.PrintPreviewBarItem28.GroupIndex = 100
        Me.PrintPreviewBarItem28.Id = 37
        Me.PrintPreviewBarItem28.Name = "PrintPreviewBarItem28"
        '
        'BarToolbarsListItem1
        '
        Me.BarToolbarsListItem1.Caption = "Bars"
        Me.BarToolbarsListItem1.Id = 38
        Me.BarToolbarsListItem1.Name = "BarToolbarsListItem1"
        '
        'PrintPreviewSubItem3
        '
        Me.PrintPreviewSubItem3.Caption = "&Background"
        Me.PrintPreviewSubItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Background
        Me.PrintPreviewSubItem3.Id = 34
        Me.PrintPreviewSubItem3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem22), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewBarItem23)})
        Me.PrintPreviewSubItem3.Name = "PrintPreviewSubItem3"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(624, 53)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 417)
        Me.barDockControlBottom.Size = New System.Drawing.Size(624, 25)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 53)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 364)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(624, 53)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 364)
        '
        'PrintPreviewBarCheckItem1
        '
        Me.PrintPreviewBarCheckItem1.Caption = "PDF File"
        Me.PrintPreviewBarCheckItem1.Checked = True
        Me.PrintPreviewBarCheckItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf
        Me.PrintPreviewBarCheckItem1.Enabled = False
        Me.PrintPreviewBarCheckItem1.GroupIndex = 2
        Me.PrintPreviewBarCheckItem1.Hint = "PDF File"
        Me.PrintPreviewBarCheckItem1.Id = 39
        Me.PrintPreviewBarCheckItem1.Name = "PrintPreviewBarCheckItem1"
        '
        'PrintPreviewBarCheckItem2
        '
        Me.PrintPreviewBarCheckItem2.Caption = "HTML File"
        Me.PrintPreviewBarCheckItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm
        Me.PrintPreviewBarCheckItem2.Enabled = False
        Me.PrintPreviewBarCheckItem2.GroupIndex = 2
        Me.PrintPreviewBarCheckItem2.Hint = "HTML File"
        Me.PrintPreviewBarCheckItem2.Id = 40
        Me.PrintPreviewBarCheckItem2.Name = "PrintPreviewBarCheckItem2"
        '
        'PrintPreviewBarCheckItem3
        '
        Me.PrintPreviewBarCheckItem3.Caption = "MHT File"
        Me.PrintPreviewBarCheckItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht
        Me.PrintPreviewBarCheckItem3.Enabled = False
        Me.PrintPreviewBarCheckItem3.GroupIndex = 2
        Me.PrintPreviewBarCheckItem3.Hint = "MHT File"
        Me.PrintPreviewBarCheckItem3.Id = 41
        Me.PrintPreviewBarCheckItem3.Name = "PrintPreviewBarCheckItem3"
        '
        'PrintPreviewBarCheckItem4
        '
        Me.PrintPreviewBarCheckItem4.Caption = "RTF File"
        Me.PrintPreviewBarCheckItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf
        Me.PrintPreviewBarCheckItem4.Enabled = False
        Me.PrintPreviewBarCheckItem4.GroupIndex = 2
        Me.PrintPreviewBarCheckItem4.Hint = "RTF File"
        Me.PrintPreviewBarCheckItem4.Id = 42
        Me.PrintPreviewBarCheckItem4.Name = "PrintPreviewBarCheckItem4"
        '
        'PrintPreviewBarCheckItem5
        '
        Me.PrintPreviewBarCheckItem5.Caption = "XLS File"
        Me.PrintPreviewBarCheckItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls
        Me.PrintPreviewBarCheckItem5.Enabled = False
        Me.PrintPreviewBarCheckItem5.GroupIndex = 2
        Me.PrintPreviewBarCheckItem5.Hint = "XLS File"
        Me.PrintPreviewBarCheckItem5.Id = 43
        Me.PrintPreviewBarCheckItem5.Name = "PrintPreviewBarCheckItem5"
        '
        'PrintPreviewBarCheckItem6
        '
        Me.PrintPreviewBarCheckItem6.Caption = "XLSX File"
        Me.PrintPreviewBarCheckItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx
        Me.PrintPreviewBarCheckItem6.Enabled = False
        Me.PrintPreviewBarCheckItem6.GroupIndex = 2
        Me.PrintPreviewBarCheckItem6.Hint = "XLSX File"
        Me.PrintPreviewBarCheckItem6.Id = 44
        Me.PrintPreviewBarCheckItem6.Name = "PrintPreviewBarCheckItem6"
        '
        'PrintPreviewBarCheckItem7
        '
        Me.PrintPreviewBarCheckItem7.Caption = "CSV File"
        Me.PrintPreviewBarCheckItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv
        Me.PrintPreviewBarCheckItem7.Enabled = False
        Me.PrintPreviewBarCheckItem7.GroupIndex = 2
        Me.PrintPreviewBarCheckItem7.Hint = "CSV File"
        Me.PrintPreviewBarCheckItem7.Id = 45
        Me.PrintPreviewBarCheckItem7.Name = "PrintPreviewBarCheckItem7"
        '
        'PrintPreviewBarCheckItem8
        '
        Me.PrintPreviewBarCheckItem8.Caption = "Text File"
        Me.PrintPreviewBarCheckItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt
        Me.PrintPreviewBarCheckItem8.Enabled = False
        Me.PrintPreviewBarCheckItem8.GroupIndex = 2
        Me.PrintPreviewBarCheckItem8.Hint = "Text File"
        Me.PrintPreviewBarCheckItem8.Id = 46
        Me.PrintPreviewBarCheckItem8.Name = "PrintPreviewBarCheckItem8"
        '
        'PrintPreviewBarCheckItem9
        '
        Me.PrintPreviewBarCheckItem9.Caption = "Image File"
        Me.PrintPreviewBarCheckItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic
        Me.PrintPreviewBarCheckItem9.Enabled = False
        Me.PrintPreviewBarCheckItem9.GroupIndex = 2
        Me.PrintPreviewBarCheckItem9.Hint = "Image File"
        Me.PrintPreviewBarCheckItem9.Id = 47
        Me.PrintPreviewBarCheckItem9.Name = "PrintPreviewBarCheckItem9"
        '
        'PrintPreviewBarCheckItem10
        '
        Me.PrintPreviewBarCheckItem10.Caption = "PDF File"
        Me.PrintPreviewBarCheckItem10.Checked = True
        Me.PrintPreviewBarCheckItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf
        Me.PrintPreviewBarCheckItem10.Enabled = False
        Me.PrintPreviewBarCheckItem10.GroupIndex = 1
        Me.PrintPreviewBarCheckItem10.Hint = "PDF File"
        Me.PrintPreviewBarCheckItem10.Id = 48
        Me.PrintPreviewBarCheckItem10.Name = "PrintPreviewBarCheckItem10"
        '
        'PrintPreviewBarCheckItem11
        '
        Me.PrintPreviewBarCheckItem11.Caption = "MHT File"
        Me.PrintPreviewBarCheckItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht
        Me.PrintPreviewBarCheckItem11.Enabled = False
        Me.PrintPreviewBarCheckItem11.GroupIndex = 1
        Me.PrintPreviewBarCheckItem11.Hint = "MHT File"
        Me.PrintPreviewBarCheckItem11.Id = 49
        Me.PrintPreviewBarCheckItem11.Name = "PrintPreviewBarCheckItem11"
        '
        'PrintPreviewBarCheckItem12
        '
        Me.PrintPreviewBarCheckItem12.Caption = "RTF File"
        Me.PrintPreviewBarCheckItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf
        Me.PrintPreviewBarCheckItem12.Enabled = False
        Me.PrintPreviewBarCheckItem12.GroupIndex = 1
        Me.PrintPreviewBarCheckItem12.Hint = "RTF File"
        Me.PrintPreviewBarCheckItem12.Id = 50
        Me.PrintPreviewBarCheckItem12.Name = "PrintPreviewBarCheckItem12"
        '
        'PrintPreviewBarCheckItem13
        '
        Me.PrintPreviewBarCheckItem13.Caption = "XLS File"
        Me.PrintPreviewBarCheckItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls
        Me.PrintPreviewBarCheckItem13.Enabled = False
        Me.PrintPreviewBarCheckItem13.GroupIndex = 1
        Me.PrintPreviewBarCheckItem13.Hint = "XLS File"
        Me.PrintPreviewBarCheckItem13.Id = 51
        Me.PrintPreviewBarCheckItem13.Name = "PrintPreviewBarCheckItem13"
        '
        'PrintPreviewBarCheckItem14
        '
        Me.PrintPreviewBarCheckItem14.Caption = "XLSX File"
        Me.PrintPreviewBarCheckItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx
        Me.PrintPreviewBarCheckItem14.Enabled = False
        Me.PrintPreviewBarCheckItem14.GroupIndex = 1
        Me.PrintPreviewBarCheckItem14.Hint = "XLSX File"
        Me.PrintPreviewBarCheckItem14.Id = 52
        Me.PrintPreviewBarCheckItem14.Name = "PrintPreviewBarCheckItem14"
        '
        'PrintPreviewBarCheckItem15
        '
        Me.PrintPreviewBarCheckItem15.Caption = "CSV File"
        Me.PrintPreviewBarCheckItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv
        Me.PrintPreviewBarCheckItem15.Enabled = False
        Me.PrintPreviewBarCheckItem15.GroupIndex = 1
        Me.PrintPreviewBarCheckItem15.Hint = "CSV File"
        Me.PrintPreviewBarCheckItem15.Id = 53
        Me.PrintPreviewBarCheckItem15.Name = "PrintPreviewBarCheckItem15"
        '
        'PrintPreviewBarCheckItem16
        '
        Me.PrintPreviewBarCheckItem16.Caption = "Text File"
        Me.PrintPreviewBarCheckItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt
        Me.PrintPreviewBarCheckItem16.Enabled = False
        Me.PrintPreviewBarCheckItem16.GroupIndex = 1
        Me.PrintPreviewBarCheckItem16.Hint = "Text File"
        Me.PrintPreviewBarCheckItem16.Id = 54
        Me.PrintPreviewBarCheckItem16.Name = "PrintPreviewBarCheckItem16"
        '
        'PrintPreviewBarCheckItem17
        '
        Me.PrintPreviewBarCheckItem17.Caption = "Image File"
        Me.PrintPreviewBarCheckItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic
        Me.PrintPreviewBarCheckItem17.Enabled = False
        Me.PrintPreviewBarCheckItem17.GroupIndex = 1
        Me.PrintPreviewBarCheckItem17.Hint = "Image File"
        Me.PrintPreviewBarCheckItem17.Id = 55
        Me.PrintPreviewBarCheckItem17.Name = "PrintPreviewBarCheckItem17"
        '
        'AreaImpresion
        '
        Me.AreaImpresion.BackColor = System.Drawing.Color.Empty
        Me.AreaImpresion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AreaImpresion.ForeColor = System.Drawing.Color.Empty
        Me.AreaImpresion.IsMetric = True
        Me.AreaImpresion.Location = New System.Drawing.Point(0, 0)
        Me.AreaImpresion.Name = "AreaImpresion"
        Me.AreaImpresion.PrintingSystem = Me.SistemaImpresion
        Me.AreaImpresion.Size = New System.Drawing.Size(219, 279)
        Me.AreaImpresion.TabIndex = 4
        Me.AreaImpresion.TooltipFont = New System.Drawing.Font("Tahoma", 8.25!)
        '
        'PanelFiltro
        '
        Me.PanelFiltro.Controls.Add(Me.LayoutFiltro)
        Me.PanelFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelFiltro.Location = New System.Drawing.Point(0, 0)
        Me.PanelFiltro.Name = "PanelFiltro"
        Me.PanelFiltro.Size = New System.Drawing.Size(400, 279)
        Me.PanelFiltro.TabIndex = 9
        Me.PanelFiltro.Visible = False
        '
        'LayoutFiltro
        '
        Me.LayoutFiltro.AllowCustomizationMenu = False
        Me.LayoutFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutFiltro.Location = New System.Drawing.Point(2, 2)
        Me.LayoutFiltro.Name = "LayoutFiltro"
        Me.LayoutFiltro.OptionsView.AllowHotTrack = True
        Me.LayoutFiltro.Root = Me.LayoutFiltroRaiz
        Me.LayoutFiltro.Size = New System.Drawing.Size(396, 275)
        Me.LayoutFiltro.TabIndex = 0
        Me.LayoutFiltro.Text = "LayoutControl1"
        '
        'LayoutFiltroRaiz
        '
        Me.LayoutFiltroRaiz.CustomizationFormText = "LayoutFiltroRaiz"
        Me.LayoutFiltroRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutFiltroRaiz.GroupBordersVisible = False
        Me.LayoutFiltroRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutFiltroRaiz.Name = "LayoutFiltroRaiz"
        Me.LayoutFiltroRaiz.Size = New System.Drawing.Size(396, 275)
        Me.LayoutFiltroRaiz.Text = "LayoutFiltroRaiz"
        Me.LayoutFiltroRaiz.TextVisible = False
        '
        'SplitContenido
        '
        Me.SplitContenido.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1
        Me.SplitContenido.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContenido.Location = New System.Drawing.Point(0, 125)
        Me.SplitContenido.Name = "SplitContenido"
        Me.SplitContenido.Panel1.Controls.Add(Me.PanelFiltro)
        Me.SplitContenido.Panel1.Text = "Panel1"
        Me.SplitContenido.Panel2.Controls.Add(Me.AreaImpresion)
        Me.SplitContenido.Panel2.Text = "Panel2"
        Me.SplitContenido.Size = New System.Drawing.Size(624, 279)
        Me.SplitContenido.SplitterPosition = 400
        Me.SplitContenido.TabIndex = 10
        Me.SplitContenido.Text = "SplitContainerControl1"
        '
        'PanelMensaje
        '
        Me.PanelMensaje.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.PanelMensaje.Appearance.Options.UseBackColor = True
        Me.PanelMensaje.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelMensaje.Controls.Add(Me.LayoutMensaje)
        Me.PanelMensaje.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelMensaje.Location = New System.Drawing.Point(0, 53)
        Me.PanelMensaje.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelMensaje.LookAndFeel.UseWindowsXPTheme = True
        Me.PanelMensaje.Name = "PanelMensaje"
        Me.PanelMensaje.Size = New System.Drawing.Size(624, 72)
        Me.PanelMensaje.TabIndex = 15
        Me.PanelMensaje.Visible = False
        '
        'LayoutMensaje
        '
        Me.LayoutMensaje.AllowCustomizationMenu = False
        Me.LayoutMensaje.Controls.Add(Me.ProgresoDefinidoMensaje)
        Me.LayoutMensaje.Controls.Add(Me.ProgresoMensaje)
        Me.LayoutMensaje.Controls.Add(Me.EtiquetaMensaje)
        Me.LayoutMensaje.Controls.Add(Me.ImagenMensaje)
        Me.LayoutMensaje.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutMensaje.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMensaje.Name = "LayoutMensaje"
        Me.LayoutMensaje.Root = Me.LayoutMensajeRaiz
        Me.LayoutMensaje.Size = New System.Drawing.Size(624, 72)
        Me.LayoutMensaje.TabIndex = 0
        Me.LayoutMensaje.Text = "LayoutControl1"
        '
        'ProgresoDefinidoMensaje
        '
        Me.ProgresoDefinidoMensaje.Location = New System.Drawing.Point(69, 46)
        Me.ProgresoDefinidoMensaje.MenuManager = Me.Barras
        Me.ProgresoDefinidoMensaje.Name = "ProgresoDefinidoMensaje"
        Me.ProgresoDefinidoMensaje.Size = New System.Drawing.Size(543, 14)
        Me.ProgresoDefinidoMensaje.StyleController = Me.LayoutMensaje
        Me.ProgresoDefinidoMensaje.TabIndex = 7
        '
        'ProgresoMensaje
        '
        Me.ProgresoMensaje.EditValue = 0
        Me.ProgresoMensaje.Location = New System.Drawing.Point(69, 30)
        Me.ProgresoMensaje.MenuManager = Me.Barras
        Me.ProgresoMensaje.Name = "ProgresoMensaje"
        Me.ProgresoMensaje.Size = New System.Drawing.Size(543, 12)
        Me.ProgresoMensaje.StyleController = Me.LayoutMensaje
        Me.ProgresoMensaje.TabIndex = 6
        '
        'EtiquetaMensaje
        '
        Me.EtiquetaMensaje.AllowHtmlString = True
        Me.EtiquetaMensaje.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtiquetaMensaje.Appearance.ForeColor = System.Drawing.SystemColors.InfoText
        Me.EtiquetaMensaje.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.EtiquetaMensaje.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.EtiquetaMensaje.Location = New System.Drawing.Point(69, 12)
        Me.EtiquetaMensaje.Name = "EtiquetaMensaje"
        Me.EtiquetaMensaje.Size = New System.Drawing.Size(543, 14)
        Me.EtiquetaMensaje.StyleController = Me.LayoutMensaje
        Me.EtiquetaMensaje.TabIndex = 5
        Me.EtiquetaMensaje.Text = "[Mensaje]"
        '
        'ImagenMensaje
        '
        Me.ImagenMensaje.Location = New System.Drawing.Point(12, 12)
        Me.ImagenMensaje.Name = "ImagenMensaje"
        Me.ImagenMensaje.Size = New System.Drawing.Size(48, 48)
        Me.ImagenMensaje.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ImagenMensaje.TabIndex = 4
        Me.ImagenMensaje.TabStop = False
        '
        'LayoutMensajeRaiz
        '
        Me.LayoutMensajeRaiz.CustomizationFormText = "LayoutMensajeRaiz"
        Me.LayoutMensajeRaiz.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutMensajeRaiz.GroupBordersVisible = False
        Me.LayoutMensajeRaiz.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.ItemForImagenMensaje, Me.ItemForEtiquetaMensaje, Me.ItemForProgresoMensaje, Me.ItemForProgresoDefinidoMensaje})
        Me.LayoutMensajeRaiz.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMensajeRaiz.Name = "LayoutMensajeRaiz"
        Me.LayoutMensajeRaiz.Size = New System.Drawing.Size(624, 72)
        Me.LayoutMensajeRaiz.Text = "LayoutMensajeRaiz"
        Me.LayoutMensajeRaiz.TextVisible = False
        '
        'ItemForImagenMensaje
        '
        Me.ItemForImagenMensaje.Control = Me.ImagenMensaje
        Me.ItemForImagenMensaje.CustomizationFormText = "ItemForImagenMensaje"
        Me.ItemForImagenMensaje.Location = New System.Drawing.Point(0, 0)
        Me.ItemForImagenMensaje.MaxSize = New System.Drawing.Size(57, 52)
        Me.ItemForImagenMensaje.MinSize = New System.Drawing.Size(57, 52)
        Me.ItemForImagenMensaje.Name = "ItemForImagenMensaje"
        Me.ItemForImagenMensaje.Size = New System.Drawing.Size(57, 52)
        Me.ItemForImagenMensaje.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForImagenMensaje.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 5, 0, 0)
        Me.ItemForImagenMensaje.Text = "ItemForImagenMensaje"
        Me.ItemForImagenMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForImagenMensaje.TextToControlDistance = 0
        Me.ItemForImagenMensaje.TextVisible = False
        '
        'ItemForEtiquetaMensaje
        '
        Me.ItemForEtiquetaMensaje.Control = Me.EtiquetaMensaje
        Me.ItemForEtiquetaMensaje.CustomizationFormText = "ItemForEtiquetaMensaje"
        Me.ItemForEtiquetaMensaje.Location = New System.Drawing.Point(57, 0)
        Me.ItemForEtiquetaMensaje.Name = "ItemForEtiquetaMensaje"
        Me.ItemForEtiquetaMensaje.Size = New System.Drawing.Size(547, 18)
        Me.ItemForEtiquetaMensaje.Text = "ItemForEtiquetaMensaje"
        Me.ItemForEtiquetaMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForEtiquetaMensaje.TextToControlDistance = 0
        Me.ItemForEtiquetaMensaje.TextVisible = False
        '
        'ItemForProgresoMensaje
        '
        Me.ItemForProgresoMensaje.Control = Me.ProgresoMensaje
        Me.ItemForProgresoMensaje.CustomizationFormText = "ItemForProgresoMensaje"
        Me.ItemForProgresoMensaje.Location = New System.Drawing.Point(57, 18)
        Me.ItemForProgresoMensaje.MaxSize = New System.Drawing.Size(0, 16)
        Me.ItemForProgresoMensaje.MinSize = New System.Drawing.Size(54, 16)
        Me.ItemForProgresoMensaje.Name = "ItemForProgresoMensaje"
        Me.ItemForProgresoMensaje.Size = New System.Drawing.Size(547, 16)
        Me.ItemForProgresoMensaje.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.ItemForProgresoMensaje.Text = "ItemForProgresoMensaje"
        Me.ItemForProgresoMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForProgresoMensaje.TextToControlDistance = 0
        Me.ItemForProgresoMensaje.TextVisible = False
        '
        'ItemForProgresoDefinidoMensaje
        '
        Me.ItemForProgresoDefinidoMensaje.Control = Me.ProgresoDefinidoMensaje
        Me.ItemForProgresoDefinidoMensaje.CustomizationFormText = "LayoutControlItem1"
        Me.ItemForProgresoDefinidoMensaje.Location = New System.Drawing.Point(57, 34)
        Me.ItemForProgresoDefinidoMensaje.Name = "ItemForProgresoDefinidoMensaje"
        Me.ItemForProgresoDefinidoMensaje.Size = New System.Drawing.Size(547, 18)
        Me.ItemForProgresoDefinidoMensaje.Text = "ItemForProgresoDefinidoMensaje"
        Me.ItemForProgresoDefinidoMensaje.TextSize = New System.Drawing.Size(0, 0)
        Me.ItemForProgresoDefinidoMensaje.TextToControlDistance = 0
        Me.ItemForProgresoDefinidoMensaje.TextVisible = False
        '
        'EtiquetaCopyright
        '
        Me.EtiquetaCopyright.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.EtiquetaCopyright.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EtiquetaCopyright.Location = New System.Drawing.Point(0, 404)
        Me.EtiquetaCopyright.Name = "EtiquetaCopyright"
        Me.EtiquetaCopyright.Size = New System.Drawing.Size(624, 13)
        Me.EtiquetaCopyright.TabIndex = 20
        Me.EtiquetaCopyright.Text = "[Trademark | Copyright]"
        '
        'FormaReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(624, 442)
        Me.Controls.Add(Me.SplitContenido)
        Me.Controls.Add(Me.PanelMensaje)
        Me.Controls.Add(Me.EtiquetaCopyright)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "FormaReporte"
        CType(Me.Barras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrintPreviewRepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SistemaImpresion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelFiltro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelFiltro.ResumeLayout(False)
        CType(Me.LayoutFiltro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutFiltroRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContenido.ResumeLayout(False)
        CType(Me.PanelMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelMensaje.ResumeLayout(False)
        CType(Me.LayoutMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutMensaje.ResumeLayout(False)
        CType(Me.ProgresoDefinidoMensaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgresoMensaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImagenMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMensajeRaiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForImagenMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForEtiquetaMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForProgresoMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemForProgresoDefinidoMensaje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents Barras As DevExpress.XtraPrinting.Preview.PrintBarManager
    Protected Friend WithEvents BarraHerramientas As DevExpress.XtraPrinting.Preview.PreviewBar
    Protected Friend WithEvents PrintPreviewBarItem2 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem3 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem4 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem5 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem6 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem7 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem8 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem9 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem10 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem11 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem12 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem13 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem14 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem15 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents ZoomBarEditItem1 As DevExpress.XtraPrinting.Preview.ZoomBarEditItem
    Protected Friend WithEvents PrintPreviewRepositoryItemComboBox1 As DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox
    Protected Friend WithEvents PrintPreviewBarItem16 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem17 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem18 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem19 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem20 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem21 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem22 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem23 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem24 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem25 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem26 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents BarraEstado As DevExpress.XtraPrinting.Preview.PreviewBar
    Protected Friend WithEvents PrintPreviewStaticItem1 As DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem
    Protected Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Protected Friend WithEvents ProgressBarEditItem1 As DevExpress.XtraPrinting.Preview.ProgressBarEditItem
    Protected Friend WithEvents RepositoryItemProgressBar1 As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Protected Friend WithEvents PrintPreviewBarItem1 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Protected Friend WithEvents PrintPreviewStaticItem2 As DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem
    Protected Friend WithEvents BarraMenu As DevExpress.XtraPrinting.Preview.PreviewBar
    Protected Friend WithEvents PrintPreviewSubItem1 As DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
    Protected Friend WithEvents PrintPreviewSubItem2 As DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
    Protected Friend WithEvents PrintPreviewSubItem4 As DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
    Protected Friend WithEvents PrintPreviewBarItem27 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents PrintPreviewBarItem28 As DevExpress.XtraPrinting.Preview.PrintPreviewBarItem
    Protected Friend WithEvents BarToolbarsListItem1 As DevExpress.XtraBars.BarToolbarsListItem
    Protected Friend WithEvents PrintPreviewSubItem3 As DevExpress.XtraPrinting.Preview.PrintPreviewSubItem
    Protected Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Protected Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Protected Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Protected Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Protected Friend WithEvents AreaImpresion As DevExpress.XtraPrinting.Control.PrintControl
    Protected Friend WithEvents PrintPreviewBarCheckItem1 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem2 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem3 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem4 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem5 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem6 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem7 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem8 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem9 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem10 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem11 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem12 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem13 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem14 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem15 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem16 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents PrintPreviewBarCheckItem17 As DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem
    Protected Friend WithEvents SistemaImpresion As DevExpress.XtraPrinting.PrintingSystem
    Protected Friend WithEvents PanelFiltro As DevExpress.XtraEditors.PanelControl
    Protected Friend WithEvents LayoutFiltro As DevExpress.XtraLayout.LayoutControl
    Protected Friend WithEvents LayoutFiltroRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Protected Friend WithEvents SplitContenido As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents PanelMensaje As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutMensaje As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents ProgresoDefinidoMensaje As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents ProgresoMensaje As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents EtiquetaMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ImagenMensaje As System.Windows.Forms.PictureBox
    Friend WithEvents LayoutMensajeRaiz As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ItemForImagenMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemForEtiquetaMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemForProgresoMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ItemForProgresoDefinidoMensaje As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EtiquetaCopyright As DevExpress.XtraEditors.LabelControl

End Class
