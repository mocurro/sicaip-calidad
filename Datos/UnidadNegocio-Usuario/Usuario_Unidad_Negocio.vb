﻿Public Class Usuario_Unidad_Negocio
     ''' <summary>
    ''' Regresa la representación textual del estatus.
    ''' </summary>
    Public ReadOnly Property EstatusTexto() As String
        Get
            Select Case estatus
                Case ClienteEstatus.Activo 
                    Return "Activo"
                Case ClienteEstatus.Inactivo  
                    Return "Inactivo"
                Case Else
                    Return "Sin estatus"
            End Select
        End Get
    End Property

    Public ReadOnly Property usuario As SEGURIDAD_USUARIO   
    Get
            Dim MiComponente=Datos.Fabricas.Seguridad_Usuarios .ObtenerUno (cve_usuario )
            Return MiComponente 
    End Get
    End Property

     Public ReadOnly Property Unidad_Nego As Unidad_Negocio 
    Get

            Dim ComponenteUnidad_Neg=Datos.Fabricas .ComponenteUnidadNegocio .ObtenerUno (cve_componente_unidad_negocio )
            Dim UnidadNegocio=Datos.Fabricas .Unidad_Negocio .ObtenerUno (ComponenteUnidad_Neg.cve_unidad_negocio  )
            Return UnidadNegocio 
    End Get
    End Property

End Class
