﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CatalogoLABDIMRegistro
    Inherits Mini.Zeus.Cliente.UX.FormaCatalogo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        CType(Me.FuenteDatos,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PanelFiltro,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelFiltro.SuspendLayout
        CType(Me.LayoutFiltro,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutFiltroRaiz,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutFiltro
        '
        Me.LayoutFiltro.OptionsView.AllowHotTrack = True
        '
        'CatalogoLABDIMRegistro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 442)
        Me.EntidadesVisibles = Mini.Zeus.Cliente.UX.EntidadesVisibles.Existentes
        Me.FiltroVisible = True
        Me.HabilitarActualizar = True
        Me.HabilitarAgregar = True
        Me.HabilitarBorrar = True
        Me.HabilitarEditar = True
        Me.HabilitarExportar = True
        Me.IncluirDatosRelacionados = True
        Me.Name = "CatalogoLABDIMRegistro"
        Me.Text = "CatalogoClientes_Modelo"
        CType(Me.FuenteDatos,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PanelFiltro,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelFiltro.ResumeLayout(false)
        CType(Me.LayoutFiltro,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutFiltroRaiz,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
End Class
