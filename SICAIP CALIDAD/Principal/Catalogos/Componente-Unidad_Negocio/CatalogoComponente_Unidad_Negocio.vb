﻿Imports Datos
Imports Mini.Zeus.Cliente

Public Class CatalogoComponente_Unidad_Negocio

    Private MiListaComponentes As List(Of Datos.componente )

    Private MiListaUnidadNegocio As List(Of Datos.Unidad_Negocio  )

    ''' <summary>
    ''' Define la lista de entidades que se muestran en el catálogo.
    ''' </summary>
    ''' <param name="Entidades">La lista de entidades del catálgo</param>
    Protected Overrides Sub DefinirEntidadesParaCatalogo(ByVal Entidades As System.Collections.Generic.List(Of UX.EntidadParaCatalogo))


        Entidades.Add(New UX.EntidadParaCatalogo(GetType(Datos.Componente_Unidad_Negocio ), Datos.Fabricas.ComponenteUnidadNegocio, GetType(TarjetaComponente_Unidad_Negocio )))

    End Sub

    ''' <summary>
    ''' Define el tipo de entidad raíz del catálogo.
    ''' </summary>
    Protected Overrides Function DefinirTipoEntidadRaiz() As System.Type

        Return GetType(Datos.Componente_Unidad_Negocio )

    End Function

    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property componentes() As List(Of Datos.componente )
        Get
            If MiListaComponentes  Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 1)
                MiListaComponentes = Fabricas.Componente .ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaComponentes
        End Get
    End Property

    
    ''' <summary>
    ''' Regresa la lista de terminales.
    ''' </summary>
    Public ReadOnly Property Unidades_Negocio() As List(Of Datos.Unidad_Negocio   )
        Get
            If MiListaUnidadNegocio  Is Nothing Then
                Dim Filtro = New Dictionary(Of String, Object)
                Filtro.Add("Estatus", 0)
                MiListaUnidadNegocio = Fabricas.Unidad_Negocio .ObtenerExistentesParaSerializar(Filtro)
            End If
            Return MiListaUnidadNegocio
        End Get
    End Property
End Class