﻿'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

''' <summary>
''' Lista los estatus de un Defecto
''' </summary>
Public Enum Unidad_NegocioEstatus As Integer

    ''' <summary>
    ''' Estatus activo
    ''' </summary>
    Activo = 0

    ''' <summary>
    ''' Producto Inactivo
    ''' </summary>
    Inactivo = 1

End Enum
