﻿Option Compare Binary
'VerifID data collection & id experts ®
'==========================================================================================
'D.R. © 2009 Mobile Computing & Data Collection, SA de CV
'Documento Confidencial. Para uso exclusivo de Mobile Computing & Data Collection, SA de CV
'==========================================================================================

Imports System.Drawing
Imports Mini.Zeus.Cliente.UX

<Texto("es-MX", "Defecto", "Defectos", Generos.Masculino)>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "nombre", 1, "Nombre", "Nombre", Mini.Zeus.Cliente.UX.ColumnasTiposResumenes.Cuenta, "Defectos: {0:n0}")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "descripcion", 2, "Descripción")>
<Mini.Zeus.Cliente.UX.Columna("es-MX", "EstatusTexto", 3, "Estatus")>
Public Class Defecto
    Inherits Entidad(Of Defecto, DefectosCampos)


    Protected Overrides Sub ClonarEntidad(ByVal EntidadNueva As Defecto)
        With EntidadNueva
            .cve_defecto  = cve_defecto
            .Nombre = Nombre
            .Descripcion = Descripcion
            .Estatus = Estatus
        End With
    End Sub

    Protected Overrides Sub CopiarAOriginalEntidad()
        With MiOriginal
            .cve_defecto = cve_defecto
            .Nombre = Nombre
            .Descripcion = Descripcion
            .Estatus = Estatus
        End With
    End Sub

    Protected Overrides Sub CopiarDesdeOriginalEntidad()
        With MiOriginal
            cve_defecto = .cve_defecto
            Nombre = .Nombre
            Descripcion = .Descripcion
            Estatus = .Estatus
        End With
    End Sub

    Protected Overrides Function CrearNuevoOriginalEntidad() As Defecto
        Return New Defecto
    End Function

    Protected Overrides Property CuentaEliminadoEntidad() As Integer
        Get
            'Return CuentaEliminado
        End Get
        Set(ByVal value As Integer)
            'CuentaEliminado = value
        End Set
    End Property

    Public Overrides Function Equivalente(ByVal Entidad As Defecto) As Boolean
        Return Equals(Entidad.descripcion, descripcion) AndAlso
             Equals(Entidad.Nombre, Nombre)AndAlso
             Equals(Entidad.Estatus , Estatus )
    End Function

    Protected Overrides Property FechaHoraCreacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    Protected Overrides Property FechaHoraModificacionEntidad() As Date
        Get
            'Return FechaHoraModificacion
        End Get
        Set(ByVal value As Date)
            'FechaHoraModificacion = value
        End Set
    End Property

    'Public Overrides ReadOnly Property IconoEntidad() As System.Drawing.Icon
    '    Get
    '        Return My.Resources.Line
    '    End Get
    'End Property

    Public Overrides ReadOnly Property Id() As System.Guid
        Get
            'Return ID_LineasProductos
        End Get
    End Property

    Public Overrides ReadOnly Property ImagenEntidadEspecifica(ByVal Tamaño As Mini.Zeus.Cliente.UX.TamañosImagenes) As System.Drawing.Image
        Get
            Select Case Tamaño
                Case TamañosImagenes.x16
                    Return My.Resources.User32x32
                Case TamañosImagenes.x32
                    Return My.Resources.User32x32
                Case TamañosImagenes.x48
                    Return My.Resources.User48x48
                    'Return My.Resources.Imagen48_Line
                Case TamañosImagenes.x256
                    'Return My.Resources.Imagen48_Line
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property

    Protected Overrides Property MetadatosEntidad() As Integer
        Get
            ' Return Metadatos
        End Get
        Set(ByVal value As Integer)
            'Metadatos = CType(value, LineasDispositivosMetadatos)
        End Set
    End Property

    Public Overrides ReadOnly Property IdTexto As String
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Overrides ReadOnly Property IconoEntidad As Icon
        Get
              Return My.Resources .businessman 
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return Nombre
    End Function

    Protected Overrides Function ValidarEntidad(ByVal Campo As DefectosCampos) As System.Collections.Generic.List(Of String)
        Dim Resultado = New List(Of String)
        Select Case Campo
            Case DefectosCampos.Nombre '.Nombre  
                If STRING.IsNullOrEmpty (nombre ) Then
                    Resultado.Add(My.Resources.Str_NombreClienteRequerido)
                End If

                'Case LineasProductosCampos.Id_Codigo
                '    If Equals(Id_Codigo, Guid.Empty) Then
                '        Resultado.Add(My.Resources.Str_LineaProductosValidarProductoRequerido)
                '    End If
                'Case LineasProductosCampos.Lote
                '    If String.IsNullOrEmpty(Lote) Then
                '        Resultado.Add(My.Resources.Str_LineaProductosValidarLoteRequerido)
                '    End If
        End Select
        Return Resultado
    End Function
End Class
